#Oroboros Core
- - -
> **Version:** 0.2.5
> **Author:** Brian Dayhoff
> **Copyright:** Copyright 2011-2017, all rights reserved.
> **License:** [MIT][license]
- - -

**This package is still in alpha. It should not be considered stable for production use as of yet. This is a large undertaking that has gone through five prior builds. Much of the requisite logic exists, and needs to be ported into this package from older builds, so development should progress quickly.**

 * [Documentation][api]
 * [Developer Wiki][wiki]
 * [Git Repo][repo]
 * [Contact Us][slack]

[ ![Codeship Status for oroborosframework/oroboros-core](https://app.codeship.com/projects/da4a0760-fbbf-0134-13c6-567134c392a1/status?branch=master)](https://app.codeship.com/projects/211614)

[![codecov](https://codecov.io/bb/oroborosframework/oroboros-core/branch/master/graph/badge.svg)](https://codecov.io/bb/oroborosframework/oroboros-core)

Oroboros core is an unopinionated, interoperable, trait-based MVC toolbox. It's primary objective is to tame a number of reasons why *[frameworks suck](https://www.youtube.com/watch?v=DuB6UjEsY_Y)*, without excluding the ongoing business need or reliance on them or forcing a full rewrite of existing code that fills a real-world business need.

It also provides a number of compatibility constructs between PHP and other common languages, making PHP easier to use in familiar terms to Java, Javascript, and Python developers who are unfamiliar with PHP internals, but have a solid grasp of good coding practice in their own right, and also provides backward compatibility for a number of modern PHP base classes and functions to legacy PHP versions as far back as 5.4, in order to make future PHP upgrades less cumbersome.

Great pains have been taken to provide baseline interoperability solutions to all currently released [Psr Standards][psr], so that you can leverage them without providing your own solution. The internal architecture of this package will accept substitution for any class that honors these specifications, but also provides it's own way to substitute painlessly without direct class inheritance.

It **does not** provide execution opinion, or assume control without explicit direction to do so.

It **does** provide you an interoperable way to piece together numerous MVC constructs regardless of their original implementation into a coherent custom architecture, and provides you with default functionality to fill in the blanks and smooth inconsistencies between unrelated approaches to the same objective.

This is not a framework, this is a library for taming the frustrations of frameworks and allowing them to be used interchangeably, that incidentally can be used as a fully fledged framework if needed. It has no direct opinion over your use case unless directed to, but MAY provide opinion you do not care to write yourself if it is explicitly told to do so.

[api]: https://oroborosframework.bitbucket.io/core/
[wiki]: https://bitbucket.org/oroborosframework/oroboros-core/wiki/Home
[repo]: https://bitbucket.org/oroborosframework/oroboros-core
[slack]: https://oroboros-framework.slack.com/