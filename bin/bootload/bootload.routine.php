<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.

namespace oroboros\core\bin\bootload;

/**
 * <Bootload Routine>
 * This is the bootload routine. This is the first file that is run after
 * calling init.php. The purpose of this routine is to aggregate all of
 * the required parameters about the environment, server, and current runtime,
 * declare the initial fixed global constants that are required for
 * interfaces to compile, and include all of the baseline files that
 * are needed prior to the autoloader taking over.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
/**
 * These MUST be run in the specified order.
 */
$subroutines = array(
    'autoload.routine.php', //Checks if composer is available and decides wheteher to rely on the Composer autoloader or the system one.
    'overrides.routine.php', //Collects the user defined config overrides, if they exist
    'config.routine.php', //Collects oroboros settings into a read-only array
    'environment.routine.php', //Checks the environment, and defines a few baseline settings to optimize bootstrapping
    'definitions.routine.php', //Creates the final system definitions, honoring overrides where appropriate to do so
    'compatibility.routine.php', //Preflights compatibility with minimum package PHP version supported.
    'patch.routine.php', //Patches deprecated PHP versions with missing functionality
    'application.routine.php', //Checks if there is an app.config, and collects it's values if it exists
    'initialization.routine.php', //Performs the final initialization effort.
    'build.routine.php', //Compile the core build settings into the base accessor.
);

$bootload = realpath( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR;
$subroutine = $bootload . 'subroutines' . DIRECTORY_SEPARATOR;

foreach ( $subroutines as
    $bootload_step )
{
    try
    {
        require_once $subroutine . $bootload_step;
    } catch ( \Exception $e )
    {
        //Write to stderr if cli, or phpoutput if http
        $resource = (defined( 'STDERR' )
            ? STDERR
            : fopen( 'php://output', 'w' ));
        //can't bootload, exit with error.
        fwrite( $resource,
            sprintf( '<pre>Could not bootload Oroboros core due to an exception in the bootload process:' . PHP_EOL . '%s' . PHP_EOL . PHP_EOL . 'This error originated at line [%s] of [%s].' . PHP_EOL . \oroboros\format\StringFormatter::indent( 'Backtrace:' . PHP_EOL . '%s',
                    4 ) . '</pre>',
                \oroboros\format\StringFormatter::indent( $e->getMessage(),
                    4 ), $e->getLine(), $e->getFile(),
                \oroboros\format\StringFormatter::indent( $e->getTraceAsString(),
                    8 ) ) );
        if ( !defined( 'STDERR' ) )
        {
            fclose( $resource );
        }
        die( 1 );
    }
}

//clean up the global scope.
unset( $bootload );
unset( $subroutine );
unset( $subroutines );

//good to go.
