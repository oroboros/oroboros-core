<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\bin\bootload\subroutines;

/**
 * <Autoload Routine>
 *
 * This routine is executed to determine autoloading should occur using the
 * native Psr-4 autoloading schema, or if it should be handled by composer.
 * If composer is present and it's autoload file exists in the vendor directory,
 * it will simply load the composer autoload file and resolve. If composer is
 * not present, or if it's autoload file has not been built as of yet, the system
 * will proceed to load it's own Psr-4 autoloader to perform the task of loading,
 * and will require all of the autoloaders dependencies prior to doing this, so
 * that it can compile and execute normally.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.5
 * @since 0.2.4-alpha
 */
/**
 * Let Composer handle autoloading if it is present
 */
if ( is_readable( OROBOROS_ROOT_DIRECTORY . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php' ) )
{
    require_once OROBOROS_ROOT_DIRECTORY . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
}
/**
 * Load the bootload classmap as a fallback utility,
 * which covers the baseline oroboros core class requirements.
 */
$bootload_autoloader_file = OROBOROS_ROOT_DIRECTORY . 'src/core/utilities/core/BootloadAutoloader.php';
$bootload_autoloader_class = '\\oroboros\\core\\utilities\\core\\BootloadAutoloader';
if ( !in_array( $bootload_autoloader_file, get_included_files() ) && !class_exists( $bootload_autoloader_class,
        false ) )
{
    require_once $bootload_autoloader_file;
}
$autoloader = new $bootload_autoloader_class();
/**
 * Load the single class outside the package namespace,
 * if they were not loaded elsewhere by another
 * installation.
 */
if ( !class_exists( 'oroboros\Oroboros' ) )
{
    require_once OROBOROS_ROOT_DIRECTORY . 'src/Oroboros.php';
}
