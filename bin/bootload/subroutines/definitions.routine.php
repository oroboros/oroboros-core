<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.

namespace oroboros\core\bin\bootload\subroutines;

/**
 * <Definitions Routine>
 * This routine makes the global constant declarations based on known
 * knowledge about the runtime environment at bootload, and any overrides
 * or custom configurations that exist.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
/**
 * Oroboros Global Constants
 * These check the current environment, and are used to determine the correct
 * configuration at runtime.
 * This file must be loaded before any Oroboros class useage.
 */
/**
 * <Oroboros Identity>
 * Fixed Declarations.
 */
/**
 * <Oroboros Runtime Filepaths>
 * Fixed Declarations.
 * Basic runtime definitions to ease bootstrapping.
 * These compile into interfaces and should not be overridden ever.
 * The program will exit with an error if you attempt to redefine these.
 */
define( "OROBOROS_BIN", OROBOROS_ROOT_DIRECTORY . "bin" . DIRECTORY_SEPARATOR );
define( "OROBOROS_TESTS",
    OROBOROS_ROOT_DIRECTORY . "tests" . DIRECTORY_SEPARATOR );
define( "OROBOROS_TMP", OROBOROS_ROOT_DIRECTORY . "tmp" . DIRECTORY_SEPARATOR );
define( "OROBOROS_VENDOR",
    OROBOROS_ROOT_DIRECTORY . "vendor" . DIRECTORY_SEPARATOR );
define( "OROBOROS_CACHE", OROBOROS_TMP . "cache" . DIRECTORY_SEPARATOR );
define( "OROBOROS_CONFIG",
    OROBOROS_ROOT_DIRECTORY . "conf" . DIRECTORY_SEPARATOR );
define( "OROBOROS_DOCS", OROBOROS_ROOT_DIRECTORY . "docs" . DIRECTORY_SEPARATOR );
define( "OROBOROS_SRC", OROBOROS_ROOT_DIRECTORY . 'src' . DIRECTORY_SEPARATOR );
define( "OROBOROS_LIB", OROBOROS_SRC . "core" . DIRECTORY_SEPARATOR );
define( "OROBOROS_DEPENDENCIES",
    OROBOROS_SRC . "dependencies" . DIRECTORY_SEPARATOR );
define( "OROBOROS_MODULES", OROBOROS_SRC . "modules" . DIRECTORY_SEPARATOR );
define( "OROBOROS_ROUTINES", OROBOROS_SRC . "routines" . DIRECTORY_SEPARATOR );
define( "OROBOROS_SERVICES", OROBOROS_SRC . "services" . DIRECTORY_SEPARATOR );
define( "OROBOROS_TEMPLATES", OROBOROS_SRC . "templates" . DIRECTORY_SEPARATOR );
define( "OROBOROS_THEMES", OROBOROS_SRC . "themes" . DIRECTORY_SEPARATOR );

//define the overrideable definitions if not already done.
$oroboros_definitions = ( ( isset( $oroboros_definitions ) )
    ? $oroboros_definitions
    : array() );
$oroboros_fixed_definitions = \oroboros\core\utilities\core\CoreConfig::get( 'core' );

/**
 * Define any constants that were not overridden prior to initialization.
 */
$oroboros_dynamic_define = function( $definitions, $allow_override = true )
{

    foreach ( $definitions as
        $key =>
        $value )
    {
        if ( $allow_override && getenv( $key ) )
        {
            //For overrideable constants,
            //environment variables take precedence.
            define( $key, getenv( $key ) );
        }
        if ( !$allow_override || !defined( $key ) )
        {
            //This will error on purpose for constants
            //that should not be modified. Constants that
            //can be modified may be declared by either
            //environment variable or predefining the
            //onstant, in that order of preference.
            define( $key, $value );
        }
    }
};

//define immutable config definitions
$oroboros_dynamic_define( $oroboros_fixed_definitions, 1 );

//define overrideable definitions
$oroboros_dynamic_define( $oroboros_definitions );

//define overrideable environment definitions
$oroboros_dynamic_define( $environment );

//clean up the global scope
unset( $oroboros_definitions );
unset( $oroboros_fixed_definitions );
unset( $oroboros_dynamic_define );
unset( $environment );
