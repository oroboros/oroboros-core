<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.

namespace oroboros\core\bin\bootload\subroutines;

/**
 * <Environment Routine>
 * This routine establishes the current runtime environment,
 * and generates the values required to proceed with actual
 * application compile.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 * 
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
/**
 * <Oroboros Environment>
 * Dynamic declarations.
 * These can be overridden for unit-testing, load balancing, etc.
 */
//override these by declaring the constants before running here,
//not by leaving global scope variables lingering.
$environment = array();

//define cli or http environment
$environment['OROBOROS_IS_CLI'] = defined( 'STDIN' ) || strpos( php_sapi_name(),
        'cli' ) !== false || strpos( php_sapi_name(), 'cgi' ) !== false || ( empty( $_SERVER['REMOTE_ADDR'] ) &&
    !isset( $_SERVER['HTTP_USER_AGENT'] ) && count( $_SERVER['argv'] ) > 0) || !array_key_exists( 'REQUEST_METHOD',
        $_SERVER )
    ? true
    : false;

//define the temp directory
if ( is_writable( sys_get_temp_dir() ) )
{
    //use the temp directory
    $environment['OROBOROS_TEMP_DIRECTORY'] = realpath( sys_get_temp_dir() ) . DIRECTORY_SEPARATOR;
} else
{
    //see if our own directory is writable
    if ( is_writable( OROBOROS_CORE_ROOT . 'tmp' ) )
    {
        $environment['OROBOROS_TEMP_DIRECTORY'] = OROBOROS_ROOT_DIRECTORY . 'tmp' . DIRECTORY_SEPARATOR;
    } else
    {
        //this will probably error later, but it is supposed to
        $environment['OROBOROS_TEMP_DIRECTORY'] = false;
    }
}


//get server software, if available
if ( array_key_exists( 'SERVER_SOFTWARE', $_SERVER ) )
{
    $environment['OROBOROS_SERVER_SOFTWARE'] = strtolower( $_SERVER['SERVER_SOFTWARE'] );
} else
{
    $environment['OROBOROS_SERVER_SOFTWARE'] = false;
}

//get the log file location
if ( ini_get( 'error_log' ) )
{
    $environment['OROBOROS_ERROR_LOG'] = ini_get( 'error_log' );
} else
{
    $environment['OROBOROS_ERROR_LOG'] = false;
}

//determine if shell exec is enabled, and what the server username is
if ( is_callable( 'shell_exec' ) && false === stripos( ini_get( 'disable_functions' ),
        'shell_exec' ) )
{
    //shell exec is enabled
    $environment['OROBOROS_SHELL_ENABLED'] = true;
    $environment['OROBOROS_SHELL_USER'] = trim( `whoami` );
} else
{
    //shell exec is disabled
    $environment['OROBOROS_SHELL_ENABLED'] = false;
    $environment['OROBOROS_SHELL_USER'] = false;
}

//set runtime environment constants, respecting testing overrides if they exist.
if ( ( defined( 'OROBOROS_IS_CLI' ) && OROBOROS_IS_CLI ) || (!defined( 'OROBOROS_IS_CLI' ) &&
    $environment['OROBOROS_IS_CLI'] ) )
{
    //define the cli environment, null the http one
    $environment['OROBOROS_IS_HTTP'] = false;
    $environment['OROBOROS_IS_SSL'] = false;
    $environment['OROBOROS_IS_AJAX'] = false;
    $environment['OROBOROS_URL'] = false;
    $environment['OROBOROS_PAGE'] = false;
    $environment['OROBOROS_REQUEST_METHOD'] = 'cli';
} else
{
    //define the http environment, null the cli one
    $environment['OROBOROS_IS_HTTP'] = true;
    //check ssl
    $environment['OROBOROS_IS_SSL'] = (isset( $_SERVER['HTTPS'] ) && strtolower( $_SERVER['HTTPS']
            == 'on' )) || (!empty( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO']
        == 'https' )
        ? true
        : false;
    //check ajax
    $environment['OROBOROS_IS_AJAX'] = !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) &&
        strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest'
        ? true
        : false;
    //define the current host, so we don't have to use the superglobal again
    $environment["OROBOROS_URL"] = 'http'
        //checks for SSL (small hack to validate on IIS servers)
        . ((defined( 'OROBOROS_IS_SSL' ) && OROBOROS_IS_SSL) || $environment['OROBOROS_IS_SSL']
        ? 's'
        : NULL)
        . '://' . rtrim( $_SERVER['HTTP_HOST'], '/' );
    //define the current page uri, so we don't have to use the superglobal again
    $environment["OROBOROS_PAGE"] = $environment["OROBOROS_URL"]
        . ( (isset( $_SERVER['REQUEST_URI'] ) && $_SERVER['REQUEST_URI'] !== '/' )
        ? $_SERVER['REQUEST_URI']
        : '/' );
    //set the request method
    $environment['OROBOROS_REQUEST_METHOD'] = strtolower( $_SERVER['REQUEST_METHOD'] );
}