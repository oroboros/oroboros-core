<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.
namespace oroboros\core\bin\bootload\subroutines;

/**
 * <Patch Routine>
 * This routine applies any compatibility patches that are required
 * to emulate at least a php 5.5.0 environment if PHP is a 5.4.* release.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 * 
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */

//check if we need to patch
if ( !( version_compare( OROBOROS_MINIMUM_NO_PATCH_VERSION, PHP_VERSION, '>=' ) ) )
{
    //The patch directory
    $patch_directory = OROBOROS_BIN
        . 'bootload' . DIRECTORY_SEPARATOR
        . 'patch' . DIRECTORY_SEPARATOR;

    //patch the deprecated version with the expected functionality.
    $patches = array(
        'tokens.patch.php',
        'arraycolumn.patch.php',
        'bcrypt.patch.php',
        'getallheaders.patch.php',
        'jsonlasterror.patch.php'
    );

    //apply patches
    foreach ( $patches as $patch )
    {
        require_once $patch_directory . $patch;
    }


    //clean up the global scope
    unset($patch_directory);
    unset($patches);
}