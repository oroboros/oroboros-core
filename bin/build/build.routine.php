<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.
namespace oroboros\core\bin\build;

/**
 * <Build Routine>
 * This is the build routine.
 * This file creates the initial build, generates the correct
 * baseline configurations for your own environment, insures
 * the correct directory permissions are applied.
 *
 * @important THIS ROUTINE MUST BE RUN FROM CGI UNLESS YOU HAVE OPEN FILE PERMISSIONS
 *
 * You SHOULD NOT under any circumstances leave directory permissions
 * open to satisfy this requirement through a web load,
 * though it SHALL honor this if it is applied.
 *
 * From the root directory, run the following command
 * in your console to run the build.
 *
 * Use the following on any UNIX platform (linux, Mac OS, etc)
 *
 * @example ./oroboros.sh --build
 *
 * if you are on Windows, use the following instead:
 *
 * @example ./oroboros.bat --build
 *
 * Builds will ignore this command if there is an oroboros.lock file
 * in your root directory. Otherwise it will erase all build files
 * and regenerate them even if they exist.
 *
 * You must delete this file to run an override build.
 *
 * --------
 * Routines
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 * --------
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage build
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */

/**
 * @todo Finish the build script
 */
