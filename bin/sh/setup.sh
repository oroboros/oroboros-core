#!/bin/bash

# Decare base level directory navigation variables
declare -r bash_root=$(pwd);
declare -r bash_bin="$(pwd)/bin/sh";
declare -r bash_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd && cd $bash_root)";
declare -r bash_file="${BASH_SOURCE[0]}";

# Source common functions, used by most extension scripts
source $bash_dir/src/functions.sh;
_clean_locks;
_set_lock;
_check_lock;
# Initialize source directories for extensions
for i in $( ls $bash_dir/src); do
  tmp_dir=$bash_dir/src/$i
  if [[ -d $tmp_dir ]]
    then
      declare bi_$i=$tmp_dir;
      varname=bi_$i;
      source ${!varname}/setup.sh;
  fi;
done;

# Execute the preflight script
source $bash_dir/src/preflight.sh;

# Execute the cleanup script
source $bash_dir/src/cleanup.sh;