#!/bin/bash

function _app_info() {
  echo "App info..."
}

function _app_version() {
  echo "App version..."
}

function _app_build() {
  echo "App build..."
}

function _app_update() {
  echo "App update..."
}

function _app_delete() {
  echo "App delete..."
}

function _app_clone() {
  echo "App clone..."
}