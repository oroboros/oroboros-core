#!/bin/bash

# Main app control method
function _app() {
  source $bash_dir/src/app/functions.sh;
  _check_lock;
  _set_lock;
  if [[ $_command == 1 ]];
    then
      echo -e "\033[0;31mNo app command supplied.\033[0m";
      echo -e "\033[1;33mUse oroboros.sh -h app for help.\033[0m";
  else
    case $_command in
      -i|--info)
          _app_info;
        ;;
      -v|--version)
          _app_version;
        ;;
      -b|--build)
          _app_build;
        ;;
      -u|--update)
          _app_update;
        ;;
      -d|--delete)
          _app_delete;
        ;;
      -c|--clone)
          _app_clone;
        ;;
      *)
        # unknown option
        echo -e "\033[0;31mUnknown app action: $_command\033[0m";
        echo -e "\033[1;33mUse oroboros.sh -h app for help.\033[0m";
      ;;
    esac;
  fi
  _remove_lock;
}

function _a() {
  _app;
}