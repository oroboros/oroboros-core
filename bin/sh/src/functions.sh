#!/bin/bash
# Commonly used functions

# Sets a lock to prevent parallel operations
function _set_lock() {
  _lockDir=$bash_bin/var/lock;
  _lockfile=$_lockDir"/lock."$$;
  if [[ ! -f "$_lockfile" ]]; 
    then
      touch $_lockfile;
  fi
}

# Removes the lock on completion of the subroutine
function _remove_lock() {
  _lockDir=$bash_bin/var/lock;
  _lockfile=$_lockDir"/lock."$$;
  if [[ -f "$_lockfile" ]]; 
    then
      rm $_lockfile;
  fi
}

# Check if a lock exists, exit gracefully if it does
function _check_lock() {
  _lockDir=$bash_bin/var/lock;
    for i in $( ls "$_lockDir" ); do
      _pid=$(echo $_lockDir/$i | cut -d'.' -f 2);
      if [[ $_pid -ne $$ ]];
        then
          echo "Process: $_pid is already running. Exiting.";
          _exit_clean;
      fi
    done;
}

# Removes any lock files that are bound to processes no longer running.
function _clean_locks() {
  _lockDir=$bash_bin/var/lock;
  for i in $( ls "$_lockDir/" ); do
      if [[ -f "$_lockDir/$i" ]];
        then
          _pid=$(echo $_lockDir/$i | cut -d'.' -f 2);
          _is_current=0;
          for _bash_pid in `pgrep bash`; do
            if [[ $_pid==$_bash_pid ]];
              then
                _is_current=1;
            fi
          done
          if [[ $_is_current -ne 0 ]];
            then
              _bad_lock=$(echo $_lockDir/lock.$_pid);
              rm "$_bad_lock";
          fi
      fi
    done;
}

# Parse arguments passed into the function for php injection
function _parse_args() {
  declare _tmp_args="";
  for i in $_args; do
    if [[ $_tmp_args == "" ]];
      then
        _tmp_args=$i;
    else
      _tmp_args=$_tmp_args"&+&"$i;
    fi
  done;
  _args=$_tmp_args;
}

# Perform a graceful exit, with cleanup
function _exit_clean() {
  _remove_lock
  exit;
}