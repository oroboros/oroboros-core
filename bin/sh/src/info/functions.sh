#!/bin/bash

function _info_app() {
  echo "Getting App subset info...";
}

function _info_build() {
  echo "Getting Build subset info...";
}

function _info_cron() {
  echo "Getting Cron subset info...";
}

function _info_deploy() {
  echo "Getting Deploy subset info...";
}

function _info_execute() {
  echo "Getting Execute subset info...";
}

function _info_git() {
  echo "Getting Git subset info...";
}

function _info_help() {
  echo "Getting Help subset info...";
}

function _info_pull() {
  echo "Getting Pull subset info...";
}

function _info_version() {
  echo "Getting Version subset info...";
}