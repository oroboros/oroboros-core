#!/bin/bash
function _info() {
  source $bash_dir/src/info/functions.sh;
  _set_lock;
  if [[ $_command == 1 ]];
    then
      echo -e '  \033[0;31mOroboros Command Line Utility, UNIX edition.';
      echo -e '    \033[0mCopyright Brian Dayhoff, 2017. All rights reserved.';
      echo -e '    \033[0mDistributed under the MIT license.';
      echo -e '    \033[0mVersion: 0.1.1a';
      echo -e '  \033[1;33mUse ./oroboros.sh -h for help.\033[0m';
  else
    case $_command in
      -a|--app)
          _info_app;
        ;;
      -b|--build)
          _info_build;
        ;;
      -c|--cron)
          _info_cron;
        ;;
      -d|--deploy)
          _info_deploy;
        ;;
      -e|--execute)
          _info_execute;
        ;;
      -g|--git)
          _info_git;
        ;;
      -h|--help)
          _info_help;
        ;;
      -p|--pull)
          _info_pull;
        ;;
      -v|--version)
          _info_version;
        ;;
      *)
        # unknown option
        echo -e "\033[0;31mUnknown info action: $_command\033[0m";
        echo -e "\033[1;33mUse oroboros.sh -h app for help.\033[0m";
      ;;
    esac;
  fi
  _remove_lock;
}
function _i() {
  _info;
}