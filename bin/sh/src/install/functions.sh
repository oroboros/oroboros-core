#!/bin/bash

function _install_base() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="full" arg="$_args"
}

function _install_app() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="app" arg="$_args"
}

function _install_update() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="update" arg="$_args"
}

function _install_info() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="info" arg="$_args"
}

function _install_db() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="database" arg="$_args"
}

function _install_profile() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="profile" arg="$_args"
}

function _install_clone() {
  _parse_args;
  php $bash_root/public/index.php mode="install" command="clone" arg="$_args"
}