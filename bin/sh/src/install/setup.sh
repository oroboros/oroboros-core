#!/bin/bash
function _install() {
  source $bash_dir/src/install/functions.sh;
  _check_lock;
  _set_lock;
  if [[ $_command == 1 ]];
    then
      _install_base;
  else
    case $_command in
      -a|--app)
          _install_app;
        ;;
      -b|--base)
          _install_base;
        ;;
      -c|--clone)
          _install_clone;
        ;;
      -d|--db)
          _install_db;
        ;;
      -i|--info)
          _install_info;
        ;;
      -p|--profile)
          _install_profile;
        ;;
      -u|--update)
          _install_update;
        ;;
      *)
        # unknown option
        echo -e "\033[0;31mUnknown installation action: $_command\033[0m";
        echo -e "\033[1;33mUse oroboros.sh -h installer for help.\033[0m";
      ;;
    esac;
  fi
  _remove_lock;
}