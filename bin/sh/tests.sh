#!/bin/bash

# Decare base level directory navigation variables
declare -r bash_root=$(pwd);
declare -r bash_shunit="$(pwd)/tests/shunit/shunit2";
declare -r bash_bin="$(pwd)/bin/sh";
declare -r bash_tests="$(pwd)/bin/sh/tests";
declare -r bash_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd && cd $bash_root)";
declare -r bash_file="${BASH_SOURCE[0]}";

## Source common functions, used by most extension scripts
source $bash_dir/src/functions.sh;
_clean_locks;
_set_lock;
_check_lock;
# Initialize source directories for extensions
for i in $( ls $bash_dir/src); do
  tmp_dir=$bash_dir/src/$i
  if [[ -d $tmp_dir ]]
    then
      declare bi_$i=$tmp_dir;
      varname=bi_$i;
      source ${!varname}/setup.sh;
  fi;
done;

echo -e '  \033[0;31mOroboros Command Line Test Suite, UNIX edition.';
echo -e '    \033[0mCopyright Brian Dayhoff, 2017. All rights reserved.';
echo -e '    \033[0mDistributed under the MIT license.';
echo -e '    \033[0mVersion: 0.2.5';

##source the test functions
for i in $( ls $bash_tests ); do
  tmp_file=$bash_tests/$i
    if [[ -f $tmp_file ]]
      then
        source $tmp_file
    fi;
done;
echo '';
echo '';
echo -e '  \033[0;36m---------------------------\033[0m';
echo -e '  \033[0;36m| Executing test cases... |\033[0m';
echo -e '  \033[0;36m---------------------------\033[0m';
echo '';
echo '';
## Execute the unit tests
source $bash_shunit

## Execute the cleanup script
#source $bash_dir/src/cleanup.sh;

