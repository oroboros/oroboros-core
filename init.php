<?php
/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Core Initialization>
 * Include this file to bootstrap the Oroboros core.
 *
 * @note Environment and application settings are fixed after initialization, and cannot be changed during runtime.
 *
 * @note If you want to override this behavior, simply define() your overrides prior to including this file.
 *
 * @note You MAY NOT override internals, such as version, location of logic directories, etc for security reasons.
 *
 * @note You MAY override the server type, request details, application details, temp directory, etc. prior to including this file.
 *
 * @note Your override paths resolving are your own responsibility. Oroboros takes no responsibility for your own lack of care about defining your custom environment.
 *
 * --------
 * @important READ THE OVERRIDE DOCS BEFORE ATTEMPTING TO DO SO.
 *
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/overrides.md
 * --------
 */

/**
 * Proceeding with initialization.
 */

/**
 * Declare the root directory (you MAY NOT override this).
 * --------
 * since we have no idea what version of PHP we are using yet,
 * we will declare this using the legacy-safe method.
 * --------
 * All other constant declarations will occur during the bootload routine.
 */

define('OROBOROS_ROOT_DIRECTORY', realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR);

require_once __DIR__ . DIRECTORY_SEPARATOR
    . 'bin' . DIRECTORY_SEPARATOR
    . 'bootload' . DIRECTORY_SEPARATOR
    . 'bootload.routine.php';

//All good.