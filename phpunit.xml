<?xml version="1.0" encoding="UTF-8"?>
<!--
The MIT License

@author Brian Dayhoff <mopsyd@me.com>
@copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
@license http://opensource.org/licenses/MIT The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<phpunit
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="http://schema.phpunit.de/4.1/phpunit.xsd"
    backupGlobals="false"
    colors="true"
    backupStaticAttributes="false"
    cacheTokens="false"

    convertErrorsToExceptions="true"
    convertNoticesToExceptions="true"
    convertWarningsToExceptions="true"
    forceCoversAnnotation="false"
    mapTestClassNameToCoveredClassName="false"

    processIsolation="false"
    stopOnError="false"
    stopOnFailure="false"
    stopOnIncomplete="false"
    stopOnSkipped="false"
    
    bootstrap="tests/phpunit/setup.php"
    verbose="false"
>
    <testsuites>
        <testsuite name="Oroboros Core Build Test Suite">
            <directory suffix="Test.php">./tests/phpunit/build</directory>
        </testsuite>
        <testsuite name="Oroboros Core Internals Test Suite">
            <directory suffix="Test.php">./tests/phpunit/core</directory>
        </testsuite>
        <testsuite name="Oroboros Core Psr Standards Test Suite">
            <directory suffix="Test.php">./tests/phpunit/psr</directory>
        </testsuite>
        <testsuite name="Oroboros Core Patterns Test Suite">
            <directory suffix="Test.php">./tests/phpunit/patterns</directory>
        </testsuite>
        <testsuite name="Oroboros Core Utilities Test Suite">
            <directory suffix="Test.php">./tests/phpunit/utilities</directory>
        </testsuite>
        <testsuite name="Oroboros Core Bootstrap Test Suite">
            <directory suffix="Test.php">./tests/phpunit/bootstrap</directory>
        </testsuite>
        <testsuite name="Oroboros Core Model Test Suite">
            <directory suffix="Test.php">./tests/phpunit/models</directory>
        </testsuite>
        <testsuite name="Oroboros Core View Test Suite">
            <directory suffix="Test.php">./tests/phpunit/views</directory>
        </testsuite>
        <testsuite name="Oroboros Core Controller Test Suite">
            <directory suffix="Test.php">./tests/phpunit/controllers</directory>
        </testsuite>
        <testsuite name="Oroboros Core Adapter Test Suite">
            <directory suffix="Test.php">./tests/phpunit/adapters</directory>
        </testsuite>
        <testsuite name="Oroboros Core Library Test Suite">
            <directory suffix="Test.php">./tests/phpunit/libraries</directory>
        </testsuite>
        <testsuite name="Oroboros Core Codex Test Suite">
            <directory suffix="Test.php">./tests/phpunit/codex</directory>
        </testsuite>
        <testsuite name="Oroboros Core Api Test Suite">
            <directory suffix="Test.php">./tests/phpunit/api</directory>
        </testsuite>
        <testsuite name="Oroboros Core Module Test Suite">
            <directory suffix="Test.php">./tests/phpunit/modules</directory>
        </testsuite>
        <testsuite name="Oroboros Core Extension Test Suite">
            <directory suffix="Test.php">./tests/phpunit/extensions</directory>
        </testsuite>
    </testsuites>

    <filter>
        <whitelist processUncoveredFilesFromWhitelist="true">
            <directory suffix=".php">./src</directory>
            <file></file>
            <exclude>
                <directory suffix=".php">./src/routines</directory>
                <directory suffix=".php">./src/dependencies</directory>
                <directory suffix=".php">./vendor</directory>
            </exclude>
        </whitelist>
    </filter>
    
    <php>
        <const name="PHPUNIT_TESTSUITE" value="true"/>
        <ini name="error_reporting" value="-1" />
        <ini name="intl.default_locale" value="en" />
        <ini name="intl.error_level" value="0" />
        <ini name="memory_limit" value="-1" />
        <env name="DUMP_LIGHT_ARRAY" value="" />
        <env name="DUMP_STRING_LENGTH" value="" />
        <env name="LDAP_HOST" value="127.0.0.1" />
        <env name="LDAP_PORT" value="3389" />
        <env name="REDIS_HOST" value="localhost" />
        <env name="MEMCACHED_HOST" value="localhost" />
    </php>
    <logging>
        <log type="coverage-clover" target="tests/coverage.xml"/>
        <log type="testdox-html" target="docs/coverage.html" />
        <log type="coverage-html" target="docs/coverage" lowUpperBound="35"
             highLowerBound="70"/>
        <log type="coverage-php" target="tests/coverage.serialized"/>
        <log type="coverage-text" target="php://stdout" showUncoveredFiles="false"/>
        <log type="junit" target="tests/junit_log.xml" logIncompleteSkipped="false"/>
        <log type="testdox-html" target="docs/testdox.html"/>
        <log type="testdox-text" target="docs/testdox.txt"/>
    </logging>
</phpunit>
