<?php

namespace oroboros;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Core Global Accessor>
 * This class exposes the Oroboros API to other classes,
 * executing calls to the core system. All other functionality
 * is deferred to other classes in keeping with the
 * Single Responsibility Principle.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/oroboros.md
 * @category concrete-classes
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.0.1a
 * @satisfies \oroboros\core\interfaces\contract\OroborosContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
 */
final class Oroboros
    implements \oroboros\core\interfaces\contract\OroborosContract,
    \oroboros\core\interfaces\api\OroborosApi,
    \oroboros\environment\interfaces\enumerated\CoreEnvironment,
    \oroboros\environment\interfaces\enumerated\Environment
{

    //Include the core logic
    use \oroboros\core\traits\OroborosTrait;

    /**
     * Set the class type to core.
     */
    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_ACCESSOR;

    /**
     * Set the class scope to global core accessor.
     */
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_GLOBAL;

    /**
     * Set the class context to core internal
     */
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_CORE;

    /**
     * Declare the api as the baseline core api.
     */
    const OROBOROS_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    /**
     * Declare the core class context designations.
     */
    const OROBOROS_CORE = true;
    const OROBOROS_PATTERN = false;
    const OROBOROS_UTILITY = false;
    const OROBOROS_LIBRARY = false;
    const OROBOROS_ROUTINE = false;
    const OROBOROS_EXTENSION = false;
    const OROBOROS_MODULE = false;
    const OROBOROS_COMPONENT = false;
    const OROBOROS_ADAPTER = false;
    const OROBOROS_CONTROLLER = false;
    const OROBOROS_MODEL = false;
    const OROBOROS_VIEW = false;

}
