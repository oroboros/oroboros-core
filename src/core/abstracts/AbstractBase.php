<?php

/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\abstracts;

/**
 * <Oroboros Abstract Base>
 * This class only designates that a class has valid core contextual information.
 * It implements the base contract so that some contract is declared, and sets
 * the baseline contextual class constants. It does not declare any methods
 * whatsoever.
 *
 * Any class extending from this class should be parsable by the codex
 * as a valid Oroboros class without issue.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractBase
    implements \oroboros\core\interfaces\contract\BaseContract
{

    const OROBOROS_CLASS_TYPE = false;
    const OROBOROS_CLASS_SCOPE = false;
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_THIRDPARTY;
    const OROBOROS_API = false;
    const OROBOROS_CORE = false;
    const OROBOROS_PATTERN = false;
    const OROBOROS_UTILITY = false;
    const OROBOROS_LIBRARY = false;
    const OROBOROS_ROUTINE = false;
    const OROBOROS_EXTENSION = false;
    const OROBOROS_MODULE = false;
    const OROBOROS_COMPONENT = false;
    const OROBOROS_ADAPTER = false;
    const OROBOROS_CONTROLLER = false;
    const OROBOROS_MODEL = false;
    const OROBOROS_VIEW = false;

}
