<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\abstracts\utilities\error;

/**
 * <Abstract Error Handler>
 * Provides baseline methods for representing an error handler object.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @category abstracts
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 */
abstract class AbstractErrorHandler
    extends \oroboros\core\abstracts\utilities\AbstractUtility
    implements \oroboros\core\interfaces\contract\utilities\error\ErrorHandlerContract
{

    use \oroboros\core\traits\utilities\error\ErrorHandlerTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\UtilityClassTypes::CLASS_TYPE_UTILITY_ERROR_HANDLER;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\UtilityClassScopes::CLASS_SCOPE_UTILITY_ERROR_HANDLER_ABSTRACT;
    const OROBOROS_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

}
