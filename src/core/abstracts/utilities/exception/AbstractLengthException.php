<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\abstracts\utilities\exception;

/**
 * Abstract Length Exception
 * Provides an oroboros flavored length exception abstract.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/abstract_classes.md
 * @category abstract-classes
 * @package oroboros/core
 * @subpackage exception
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
abstract class AbstractLengthException
    extends \LengthException
    implements \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
{

    use \oroboros\core\traits\utilities\exception\ExceptionTrait {
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_validate insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_validationModes insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_throwException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_getException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_recastException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_getExceptionCode insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_getExceptionMessage insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\utilities\exception\ExceptionTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\core\StaticBaselineTrait;
    }
    use \oroboros\core\traits\core\StaticBaselineTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_UTILITY_EXCEPTION;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_UTILITY_ERROR_LENGTH;
    const OROBOROS_API = '\\oroboros\\core\\interfaces\\api\\ExceptionApi';

}
