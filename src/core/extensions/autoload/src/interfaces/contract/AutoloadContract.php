<?php

/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\autoload\interfaces\contract;

/**
 * <Oroboros Core Autoload Library Contract>
 * This interface enforces the methods required for a valid autoloader library.
 * These methods may be manually honored, or may be satisfied by using the
 * corresponding library trait. Traits that extend upon this functionality
 * should be expected to implement this for you.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage autoload
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\autoload\traits\Psr4Trait
 * @see \oroboros\autoload\interfaces\api\AutoloadApi
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface AutoloadContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract,
 \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
{

    /**
     * <Autoloader Constructor>
     * The autoloader constructor MUST declare it's worker scope
     * and worker category, so they are available when the autoload
     * manager calls getWorkerScope and getWorkerCategory.
     */
    public function __construct();

    /**
     * Register loader with SPL autoloader stack.
     *
     * @return void
     */
    public function register();

    /**
     * Adds a base directory for a namespace prefix.
     *
     * This functionality is modeled after Psr0 and Psr4 methodology,
     * however classmap based loaders should still honor this method,
     * and may substitute [$prefix] for a class name convention
     * and/or namespace pattern, and $base_dir for a map file or
     * resource that can reverse parse the prefix correctly as needed.
     *
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     */
    public function addNamespace( $prefix, $base_dir, $prepend = false );

    /**
     * Loads the class file for a given class name.
     *
     * @param string $class The fully-qualified class name.
     * @return mixed The mapped file name on success, or boolean false on
     * failure.
     */
    public function loadClass( $class );


    /**
     * Takes a given namespace or schema, and returns the directory or directories associated with it.
     * If multiple directories are associated with the namespace,
     * it will return an array of each of them.
     * If no directories are associated with the namespace or schema, it will return false.
     * @param string $namespace The namespace to evaluate
     * @return bool|string|array False if namespace/schema not found, directory absolute path
     * if it is found, and an array of absolute paths if multiple are found.
     */
    public function getPath( $namespace );

    /**
     * Takes a given disk path, and returns the namespace or schema the
     * given path is registered under. If the path is not found,
     * it will return false.
     * @param string $path
     * @return bool|string
     */
    public function getNamespace( $path );
}
