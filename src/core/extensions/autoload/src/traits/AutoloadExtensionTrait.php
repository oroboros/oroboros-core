<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\autoload\traits;

/**
 * <Oroboros Static Autoloader Api Trait>
 * This trait provides the base accessor class with autoloader passthrough
 * methods compatible with the StaticControlApi declaration.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait AutoloadExtensionTrait
{

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Autoloader Registration Method>
     * Registers a new autoloader into the autoloader pool. The autoloader will
     * automatically fire on Spl Autoload calls, even if it has not individually
     * registered itself into the autoload stack.
     *
     * @param \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract $autoloader
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given autoloader does not honor its contract
     */
    protected static function _autoloadAdd( $autoloader )
    {
        if ( !( $autoloader instanceof \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException( sprintf(
                'Error encountered at [%s]. Provided [%s] must be an instance of [%s].',
                __METHOD__, '$autoloader',
                '\\oroboros\\autoload\\interfaces\\contract\\AutoloadContract' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $name = $autoloader->getWorkerScope();
        self::_getAutoloader()->addAutoloader( $autoloader, $name );
    }

    /**
     * <Autoloader Mapping Method>
     * Adds a base directory for a namespace prefix.
     * This method is slower than direct autoload method declaration,
     * and may generate false positives if it is schema based on a 3rd party
     * schema and the provided schema is unclear.
     *
     * @note For stability and performance, it is advised to use
     * \oroboros\Oroboros::autoload('set') in place of this method
     * whenever possible.
     *
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend (optional) If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If no available autoloader can handle the provided parameters.
     */
    protected static function _autoloadMap( $prefix, $path, $prepend = false )
    {
        self::_getAutoloader()->addNamespace( $prefix, $path, $prepend );
    }

    /**
     * <Autoloader Mapping By Autoload Mode Method>
     * Adds a mapping for a specified autoloader.
     *
     * @param string $type The autoloader to add the mapping for (eg: psr4)
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend (optional) If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given autoloader is not registered.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given parameters do not work with the specified autoloader.
     */
    protected static function _autoloadSet( $type, $prefix, $path,
        $prepend = false )
    {
        self::_getAutoloader()->addNamespaceFor( $type, $prefix, $path, $prepend );
    }

    /**
     * <Autoloader Path Getter Method>
     * Takes a given namespace or schema, and returns the disk path(s) associated with it.
     * If multiple directories are associated with the namespace/schema,
     * it will return an array of each of them.
     * If no disk paths are associated with the namespace/schema, it will return false.
     * @param string $namespace The namespace/schema to evaluate
     * @return bool|string|array False if namespace/schema not found, disk absolute path
     * if it is found, and an array of absolute paths if multiple are found.
     */
    protected static function _autoloadGetPath( $namespace )
    {
        return self::_getAutoloader()->getPath( $namespace );
    }

    /**
     * <Autoloader Namespace Getter Method>
     * Takes a given disk path, and returns the namespace(s) or schema(s) the
     * disk path is registered under. If the path is not found,
     * it will return false.
     * @param string $path
     * @return bool|string
     */
    protected static function _autoloadGetNamespace( $path )
    {
        return self::_getAutoloader()->getNamespace( $path );
    }

    /**
     * <Autoloader Type Getter Method>
     * Returns the identifiers of the autoloaders currently loaded.
     * @return array
     */
    protected static function _autoloadLoaders()
    {
        return self::_getAutoloader()->checkAutoloaders();
    }

    /**
     * <Autoloader Mode Getter Method>
     * Returns the modes of autoloading currently registered.
     * @return array
     */
    protected static function _autoloadTypes()
    {
        return self::_getAutoloader()->getAutoloadTypes();
    }

    /**
     * <Autoloader Type Check Method>
     * Returns whether or not a given autoload type
     * exists in the autoload manager.
     * @param string $autoloader
     * @return bool
     */
    protected static function _autoloadHas( $type )
    {
        return self::_getAutoloader()->HasAutoloader( $type );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    private static function &_getAutoloader()
    {
        $autoloader = self::_staticBaselineGetDependency('autoload');
        if ( is_null( $autoloader ) )
        {
            throw new \oroboros\core\utilities\exception\RuntimeException(
            sprintf( 'Error encountered at [%s]. Autoloader instance of [%s] '
                . 'must be set as a static baseline dependency to use '
                . 'this trait.', __METHOD__,
                '\\oroboros\\autoload\\interfaces\\contract\\AutoloadManagerContract' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_EXTENSION_FAILURE );
        }
        return $autoloader;
    }
}
