<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\autoload\traits;

/**
 * <Autoload Manager Trait>
 * This trait provides methods required to manage various autoloader constructs.
 * This represents the logic that will typically be directly interacted with by
 * other logic that requires interaction with an autoloader, acting as a facade
 * that abstracts out any details about the specific autoload path required to
 * resolve class autoloading.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category autoload
 * @package oroboros/core
 * @subpackage autoload
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\BaselineContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract
 * @satisfies \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract
 */
trait AutoloadManagerTrait
{

    use \oroboros\core\traits\patterns\behavioral\ManagerTrait;

    /**
     * An associative array designating which autoload
     * formats are covered by the autoloader.
     * @var array
     */
    protected $_autoload_manager_autoload_types = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\CodexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Autoloader Registration Method>
     * Registers a new autoloader into the autoloader pool. The autoloader will
     * automatically fire on Spl Autoload calls, even if it has not individually
     * registered itself into the autoload stack.
     *
     * @param \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract $autoloader
     * @param string $name (optional) A slug name to register the autoloader into the stack by. If not provided it will be automatically determined.
     * @return void
     */
    public function addAutoloader( \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract $autoloader,
        $name = null )
    {
        $this->_autoloadManagerRegisterNewAutoloader( $autoloader, $name );
    }

    /**
     * <Autoloader Type Getter Method>
     * Returns the identifiers of the autoloaders currently loaded.
     * @return array
     */
    public function checkAutoloaders()
    {
        return array_keys( $this->_baseline_dependencies );
    }

    /**
     * <Autoloader Type Check Method>
     * Returns whether or not a given autoload type
     * exists in the autoload manager.
     * @param string $autoloader
     * @return bool
     */
    public function hasAutoloader( $type )
    {
        return in_array( $type, $this->getAutoloadTypes() );
    }

    /**
     * <Autoloader Mode Getter Method>
     * Returns the modes of autoloading currently registered.
     * @return array
     */
    public function getAutoloadTypes()
    {
        $keys = $this->checkAutoloaders();
        foreach ( $keys as
            $key =>
            $value )
        {
            $tmp = trim( $value, ':' );
            $tmp = substr( $tmp, strpos( $tmp, '-' ) + 1 );
            $tmp = substr( $tmp, 0, strrpos( $tmp, '-' ) );
            $keys[$key] = $tmp;
        }
        return $keys;
    }

    /**
     * <Autoloader Mapping By Autoload Mode Method>
     * Adds a mapping for a specified autoloader.
     *
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given autoloader is not registered.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given parameters do not work with the specified autoloader.
     */
    public function addNamespaceFor( $type, $prefix, $base_dir, $prepend = false )
    {
        if ( !$this->hasAutoloader( $type ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_KEY_NOT_FOUND_MESSAGE,
                $type, __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        try
        {
            $this->_baseline_dependencies[$this->_autoloadManagerReverseParseAutoloadType( $type )]->addNamespace( $prefix,
                $base_dir, $prepend );
            return;
        } catch ( \InvalidArgumentException $e )
        {
            if ( \oroboros\Oroboros::isInitialized() )
            {
                \oroboros\Oroboros::log( 'warning',
                    'Could not determine an accurate mapping '
                    . 'of {prefix} with the provided {path} '
                    . 'for autoloader {autoloader}.',
                    array(
                    'prefix' => $prefix,
                    'path' => $base_dir,
                    'autoloader' => $type
                ) );
            }
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. Prefix [%s] cannot be '
                . 'accurately mapped to [%s] by the autoloader because '
                . 'it does not follow any existing standard for any of '
                . 'the current autoloaders. The current autoload pool '
                . 'instances are: [%s].', __METHOD__, $prefix, $base_dir,
                implode( '|', array_keys( $this->_baseline_dependencies ) ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * <Autoloader SPL Registration Method>
     * Register loader with SPL autoloader stack.
     *
     * @return void
     */
    public function register()
    {
        spl_autoload_register( array(
            $this,
            'loadClass' ) );
    }

    /**
     * <Autoloader Mapping Method>
     * Adds a base directory for a namespace prefix.
     *
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if no available autoloader can handle the provided parameters.
     */
    public function addNamespace( $prefix, $base_dir, $prepend = false )
    {
        foreach ( $this->_baseline_dependencies as
            $key =>
            $autoloader )
        {
            try
            {
                $this->_baseline_dependencies[$key]->addNamespace( $prefix,
                    $base_dir, $prepend );
                return;
            } catch ( \InvalidArgumentException $e )
            {
                //no-op
            }
        }
        if ( \oroboros\Oroboros::isInitialized() )
        {
            \oroboros\Oroboros::log( 'warning',
                'Could not determine an accurate mapping '
                . 'of {prefix} with the provided {path} '
                . 'for any currently registered autoloader.',
                array(
                'prefix' => $prefix,
                'path' => $base_dir
            ) );
        }
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( 'Error encountered at [%s]. Prefix [%s] cannot be '
            . 'accurately mapped to [%s] by the autoloader because '
            . 'it does not follow any existing standard for any of '
            . 'the current autoloaders. The current autoload pool '
            . 'instances are: [%s].', __METHOD__, $prefix, $base_dir,
            implode( '|', array_keys( $this->_baseline_dependencies ) ) ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
        );
    }

    /**
     * <Autoloader Class Load Method>
     * Loads the class file for a given class name.
     * This method will be deferred to all given autoloaders
     * currently registered.
     *
     * @param string $class The fully-qualified class name.
     * @return mixed The mapped file name on success, or boolean false on
     * failure.
     */
    public function loadClass( $class )
    {
        foreach ( $this->_baseline_dependencies as
            $key =>
            $autoloader )
        {
            $file = $autoloader->loadClass( $class );
            if ( $file )
            {
                return $file;
            }
        }
        // never found a mapped file
        return false;
    }

    /**
     * <Autoloader Path Getter Method>
     * Takes a given namespace or schema, and returns the disk path(s) associated with it.
     * If multiple directories are associated with the namespace/schema,
     * it will return an array of each of them.
     * If no disk paths are associated with the namespace/schema, it will return false.
     * @param string $namespace The namespace/schema to evaluate
     * @return bool|string|array False if namespace/schema not found, disk absolute path
     * if it is found, and an array of absolute paths if multiple are found.
     */
    public function getPath( $namespace )
    {
        $autoloader_paths = array();
        $paths = array();
        foreach ( $this->_baseline_dependencies as
            $key =>
            $autoloader )
        {
            $autoloader_paths[$key] = $autoloader->getPath( $namespace );
        }
        foreach ( $autoloader_paths as
            $pathset )
        {
            if ( !$pathset )
            {
                continue;
            }
            if ( is_array( $pathset ) )
            {
                foreach ( $pathset as
                    $path )
                {
                    $paths[] = $path;
                }
                continue;
            }
            $paths[] = $pathset;
        }
        if ( empty( $paths ) )
        {
            return false;
        }
        if ( count( $paths ) === 1 )
        {
            return array_shift( $paths );
        }
        return $paths;
    }

    /**
     * <Autoloader Namespace Getter Method>
     * Takes a given disk path, and returns the namespace(s) or schema(s) the
     * disk path is registered under. If the path is not found,
     * it will return false.
     * @param string $path
     * @return bool|string
     */
    public function getNamespace( $path )
    {
        $autoloader_namespaces = array();
        $namespaces = array();
        foreach ( $this->_baseline_dependencies as
            $key =>
            $autoloader )
        {
            $autoloader_namespaces[$key] = $autoloader->getNamespace( $path );
        }
        foreach ( $autoloader_namespaces as
            $namespace_set )
        {
            if ( !$namespace_set )
            {
                continue;
            }
            if ( is_array( $namespace_set ) )
            {
                foreach ( $namespace_set as
                    $namespace )
                {
                    $namespaces[] = $namespace;
                }
                continue;
            }
            $namespaces[] = $namespace_set;
        }
        if ( empty( $namespaces ) )
        {
            return false;
        }
        if ( count( $namespaces ) === 1 )
        {
            return array_shift( $namespaces );
        }
        return $namespaces;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the directory category to the autoload scope.
     * @return void
     */
    protected function _managerSetDirectorCategory()
    {
        $this->_setDirectorCategory( \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_AUTOLOAD );
    }

    /**
     * <Autoloader Mode Getter Method>
     * Reverse parses a standardized autoload mode slug
     * into an internal scope identifier.
     * @return string|false Returns the key of the existing autoloader if it matches, otherwise returns false.
     */
    protected function _autoloadManagerReverseParseAutoloadType( $type )
    {
        $keys = $this->checkAutoloaders();
        foreach ( $keys as
            $key =>
            $value )
        {
            $tmp = trim( $value, ':' );
            $tmp = substr( $tmp, strpos( $tmp, '-' ) + 1 );
            $tmp = substr( $tmp, 0, strrpos( $tmp, '-' ) );

            if ( strtolower( $tmp ) === strtolower( $type ) )
            {
                return $value;
            }
        }
        return false;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Performs the registration tasks required to add the
     * new autoloader into the autoload stack.
     *
     * @param \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract $autoloader
     * @param type $name
     * @return void
     */
    private function _autoloadManagerRegisterNewAutoloader( \oroboros\core\interfaces\contract\libraries\autoload\AutoloadContract $autoloader,
        $name = null )
    {
        if ( is_null( $name ) )
        {
            $name = $autoloader->getWorkerScope();
        }
        $this->_baseline_dependencies[$name] = $autoloader;
    }

}
