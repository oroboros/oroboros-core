<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\autoload\traits;

/**
 * <Psr4 Autoloader Trait>
 * This trait provides methods required to produce a valid Psr4 autoloader.
 * As the Psr-4 standard does not provide an interface but instead relies on a
 * specific approach to namespacing and file structure layout, this trait
 * instead honors a contract interface provided with Oroboros Core that
 * designates a standardized approach to autoloading in general regardless
 * of the specific standard applied.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage Psr4
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\libraries\autoload\Psr4Contract
 */
trait Psr4Trait
{

    use \oroboros\core\traits\patterns\behavioral\WorkerTrait;

    /**
     * An associative array where the key is a namespace prefix and the value
     * is an array of base directories for classes in that namespace.
     *
     * @var array
     */
    protected static $prefixes = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\CodexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Autoloader Constructor>
     * The autoloader constructor MUST declare it's worker scope
     * and worker category, so they are available when the autoload
     * manager calls getWorkerScope and getWorkerCategory.
     */
    public function __construct()
    {
        $class = get_class( $this );
        $this->_setWorkerCategory( defined( $class . '::OROBOROS_CLASS_TYPE' )
                ? $class::OROBOROS_CLASS_TYPE
                : false  );
        $this->_setWorkerScope( defined( $class . '::OROBOROS_CLASS_SCOPE' )
                ? $class::OROBOROS_CLASS_SCOPE
                : false  );
    }

    /**
     * Register loader with SPL autoloader stack.
     *
     * @return void
     */
    public function register()
    {
        spl_autoload_register( array(
            $this,
            'loadClass' ) );
    }

    /**
     * Adds a base directory for a namespace prefix.
     *
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided directory does not exist or is not readable
     */
    public function addNamespace( $prefix, $base_dir, $prepend = false )
    {
        // normalize namespace prefix
        $prefix = trim( $prefix, '\\' ) . '\\';

        // normalize the base directory with a trailing separator
        $base_dir = rtrim( $base_dir, DIRECTORY_SEPARATOR ) . '/';
        if ( !is_readable( $base_dir ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. Cannot map Psr4 namespace [%s] because the provided directory [%s] %s.',
                __METHOD__, $prefix, $base_dir,
                ( is_dir( $base_dir )
                    ? 'is not readable'
                    : 'does not exist' ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        // initialize the namespace prefix array
        if ( isset( self::$prefixes[$prefix] ) === false )
        {
            self::$prefixes[$prefix] = array();
        }

        // retain the base directory for the namespace prefix
        if ( $prepend )
        {
            array_unshift( self::$prefixes[$prefix], $base_dir );
        } else
        {
            array_push( self::$prefixes[$prefix], $base_dir );
        }
    }

    /**
     * Loads the class file for a given class name.
     *
     * @param string $class The fully-qualified class name.
     * @return mixed The mapped file name on success, or boolean false on
     * failure.
     */
    public function loadClass( $class )
    {
        // the current namespace prefix
        $prefix = $class;

        // work backwards through the namespace names of the fully-qualified
        // class name to find a mapped file name
        while ( false !== $pos = strrpos( $prefix, '\\' ) )
        {

            // retain the trailing namespace separator in the prefix
            $prefix = substr( $class, 0, $pos + 1 );

            // the rest is the relative class name
            $relative_class = substr( $class, $pos + 1 );

            // try to load a mapped file for the prefix and relative class
            $mapped_file = $this->loadMappedFile( $prefix, $relative_class );
            if ( $mapped_file )
            {
                return $mapped_file;
            }

            // remove the trailing namespace separator for the next iteration
            // of strrpos()
            $prefix = rtrim( $prefix, '\\' );
        }

        // never found a mapped file
        return false;
    }

    /**
     * Takes a given namespace, and returns the directory or directories associated with it.
     * If multiple directories are associated with the namespace,
     * it will return an array of each of them.
     * If no directories are associated with the namespace, it will return false.
     * @param string $namespace The namespace to evaluate
     * @return bool|string|array False if namespace not found, directory absolute path
     * if it is found, and an array of absolute paths if multiple are found.
     */
    public function getPath( $namespace )
    {
        $namespace = trim( $namespace, '\\' ) . '\\';
        if ( !array_key_exists( $namespace, self::$prefixes ) )
        {
            return false;
        }
        $directories = array();
        foreach ( self::$prefixes[$namespace] as
            $key =>
            $directory )
        {
            $directories[] = $directory;
        }
        if ( count( $directories ) === 1 )
        {
            return array_shift( $directories );
        }
        return $directories;
    }

    /**
     * Takes a given directory, and returns the namespace the
     * directory is registered under. If the path is not found,
     * it will return false.
     * @param string $path
     * @return bool|string
     */
    public function getNamespace( $path )
    {
        if ( !is_string( $path ) || !realpath( $path ) )
        {
            return false;
        }
        $path = realpath( $path ) . DIRECTORY_SEPARATOR;
        foreach ( self::$prefixes as
            $namespace =>
            $set )
        {
            foreach ( $set as
                $set_key =>
                $directory )
            {
                if ( $directory === $path )
                {
                    return rtrim( $namespace, '\\' );
                }
            }
        }
        return false;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Load the mapped file for a namespace prefix and relative class.
     *
     * @param string $prefix The namespace prefix.
     * @param string $relative_class The relative class name.
     * @return mixed Boolean false if no mapped file can be loaded, or the
     * name of the mapped file that was loaded.
     */
    protected function loadMappedFile( $prefix, $relative_class )
    {
        // are there any base directories for this namespace prefix?
        if ( isset( self::$prefixes[$prefix] ) === false )
        {
            return false;
        }

        // look through base directories for this namespace prefix
        foreach ( self::$prefixes[$prefix] as
            $base_dir )
        {

            // replace the namespace prefix with the base directory,
            // replace namespace separators with directory separators
            // in the relative class name, append with .php
            $file = $base_dir
                . str_replace( '\\', '/', $relative_class )
                . '.php';

            // if the mapped file exists, require it
            if ( $this->requireFile( $file ) )
            {
                // yes, we're done
                return $file;
            }
        }

        // never found it
        return false;
    }

    /**
     * If a file exists, require it from the file system.
     *
     * @param string $file The file to require.
     * @return bool True if the file exists, false if not.
     */
    protected function requireFile( $file )
    {
        if ( file_exists( $file ) )
        {
            require $file;
            return true;
        }
        return false;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    //private methods
}
