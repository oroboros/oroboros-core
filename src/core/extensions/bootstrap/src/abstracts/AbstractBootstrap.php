<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\bootstrap\abstracts;

/**
 * <Abstract Bootstrap Class>
 * This class provides a generic implementation of the bootstrap process,
 * with no modification or opinion whatsoever.
 *
 * This class must be told to initialize, and then to execute.
 * It DOES NOT assume control automatically.
 *
 * @execution Default Execution Plan (minimal)
 * //this represents generic usage in a bootload routine
 * $bootstrap = new \path\to\your\class\with\this\trait\YourBootstrapClass();
 * $bootstrap->initialize();
 * try {
 *     $bootstrap->execute();
 * }
 * catch (\oroboros\core\interfaces\contract\libraries\bootstrap\BootstrapExceptionContract $e) {
 *     $bootstrap->error($e);
 * }
 * $bootstrap->shutdown();
 * exit();
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link https://oroborosframework.bitbucket.io/core/ Oroboros Api Documentation
 * @category abstracts
 * @package oroboros/core
 * @subpackage bootstrap
 * @provides \oroboros\core\interfaces\contracts\bootstrap\BootstrapContract
 * @provides \oroboros\core\interfaces\contracts\utilities\UtilityContract
 * @provides \oroboros\core\interfaces\contracts\BaseContract
 * @provides \oroboros\environment\interfaces\enumerated\Environment
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
abstract class AbstractBootstrap
    extends \oroboros\core\abstracts\libraries\AbstractLibrary
    implements \oroboros\bootstrap\interfaces\contract\BootstrapContract,
    \oroboros\environment\interfaces\enumerated\Environment
{

    use \oroboros\bootstrap\traits\BootstrapTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\LibraryClassTypes::CLASS_TYPE_LIBRARY_BOOTSTRAP;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_BOOTSTRAP_ABSTRACT;
    const OROBOROS_API = '\\oroboros\\bootstrap\\interfaces\\api\\BootstrapApi';

}
