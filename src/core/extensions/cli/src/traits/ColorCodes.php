<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\cli\interfaces\enumerated;

/**
 * <CLI Color Code Enumerated Api Interface>
 *
 * Provides the most universally compatible CLI color code values.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage cli
 * @version 0.2.5
 * @since 0.2.5
 */
interface ColorCodes
extends CliBase
{
    const CLI_TEXT_ESCAPE_PREFIX = "\033[";
    const CLI_TEXT_ESCAPE_SUFFIX = "m";

    //These reset attributes to their defaults
    const CLI_TEXT_RESET = "0";
    const CLI_TEXT_RESET_BOLD = "21";
    const CLI_TEXT_RESET_DIM = "22";
    const CLI_TEXT_RESET_UNDERLINE = "24";
    const CLI_TEXT_RESET_BLINK = "25";
    const CLI_TEXT_RESET_REVERSE = "27";
    const CLI_TEXT_RESET_HIDDEN = "28";

    const CLI_COLOR_FOREGROUND_BLACK = "0;30";
    const CLI_COLOR_FOREGROUND_DARK_GREY = "1;30";
    const CLI_COLOR_FOREGROUND_BLUE = "0;34";
    const CLI_COLOR_FOREGROUND_LIGHT_BLUE = "1;34";
    const CLI_COLOR_FOREGROUND_GREEN = "0;32";
    const CLI_COLOR_FOREGROUND_LIGHT_GREEN = "1;32";
    const CLI_COLOR_FOREGROUND_CYAN = "0;36";
    const CLI_COLOR_FOREGROUND_LIGHT_CYAN = "1;36";
    const CLI_COLOR_FOREGROUND_RED = "0;31";
    const CLI_COLOR_FOREGROUND_LIGHT_RED = "1;31";
    const CLI_COLOR_FOREGROUND_PURPLE = "0;35";
    const CLI_COLOR_FOREGROUND_LIGHT_PURPLE = "1;35";
    const CLI_COLOR_FOREGROUND_BROWN = "0;33";
    const CLI_COLOR_FOREGROUND_YELLOW = "1;33";
    const CLI_COLOR_FOREGROUND_lLIGHT_GREY = "0;37";
    const CLI_COLOR_FOREGROUND_WHITE = "1;37";
    const CLI_COLOR_BACKGROUND_BLACK = "40";
    const CLI_COLOR_BACKGROUND_RED = "41";
    const CLI_COLOR_BACKGROUND_GREEN = "42";
    const CLI_COLOR_BACKGROUND_YELLOW = "43";
    const CLI_COLOR_BACKGROUND_BLUE = "44";
    const CLI_COLOR_BACKGROUND_MAGENTA = "45";
    const CLI_COLOR_BACKGROUND_CYAN = "46";
    const CLI_COLOR_BACKGROUND_LIGHT_GREY = "47";

}
