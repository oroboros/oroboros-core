<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\contract;

/**
 * <Codex Contract Interface>
 * Enforces a set of methods for querying the codex,
 * and registering or indexing additional codex entries.
 *
 * The Codex is the primary index of meta information for Oroboros Core. It
 * represents a means of determining package relation, api adherence,
 * declared and undeclared relation to other classes, and ordinal priority of
 * any given asset for completing a specific task. The Codex provides
 * references to ApiInterfaces and the corresponding jobs of any given
 * construct declared within them, and can check various classes for
 * their compatibility with an api interface even if they do not officially
 * declare support for it. This information can be used to find duplicate constructs,
 * usable assets that were not known, or other cross-relations that are
 * otherwise opaque to the developer.
 *
 * Internally, the Codex is used to provide both global and local substitution
 * of related classes to perform similar tasks. This allows for generalized
 * solutions to be universally swapped or reprioritized against more robust
 * focused solutions that are strictly concerned with a specific area of
 * focus, or are more specialized answers to a specific problem.
 *
 * The Codex is also extremely helpful in the course of debugging or getting
 * documentation on constructs that is either unclear or not otherwise provided.
 * This can be accessed directly at runtime on a per-request basis, without
 * the need to run a large dedicated tool like ApiGen or PhpDocumentor that
 * scrapes the entire project to create documentation. This does not exclude
 * the usefulness of tools like that for producing robust, project-wide documentation,
 * but provides an alternative to used in a quick, on-the-fly focused scope.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface CodexContract
extends \oroboros\core\interfaces\contract\BaseContract
{

}
