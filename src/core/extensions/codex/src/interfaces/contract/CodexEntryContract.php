<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\contract;

/**
 * <Codex Entry Contract Interface>
 * Enforces a set of methods for providing a set of information about
 * related classes from a specific package or api.
 *
 * A codex entry is a collection of lexical references related by an api
 * or package. Where a lexicon index contains a set of related informative
 * objects relating to a specific api, the codex entry contains a set of
 * all lexical indexes that involve the same area of focus across one or
 * more packages. This categorization can be used to determine all available
 * assets that can fulfill a specified purpose, and determine which of those
 * assets is the best fit for any given situation.
 *
 * In the course of using a package manager like Composer, it is common
 * to have a broad cross section of dependencies introduced indirectly
 * by various packages, many of which may fill the same purpose. In the
 * interest of keeping a slim project, it is typically worth knowing what
 * assets are already available locally before choosing to include more.
 * If an existing package declared as a sub-dependency of a required package
 * already fills a specified need, there is no real reason to introduce yet
 * another package to perform a task that can already be accomplished with
 * resources that are already available. Codex entries can help make these
 * determinations, and provide a decent means for auditing existing
 * dependencies for redundancy and bloat.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface CodexEntryContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{

}
