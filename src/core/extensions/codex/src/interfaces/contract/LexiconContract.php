<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\contract;

/**
 * <Lexicon Contract Interface>
 * Enforces a set of methods for querying or indexing information about
 * the structure of classes, interfaces, functions, or traits.
 *
 * The lexicon retains a collection of LexiconIndexes, which each
 * represent a specific set of related classes. A new index will be created
 * whenever a construct is registered with the Lexicon that does not already
 * have one. Lexicon entries may exist in multiple indexes if they meet more
 * than one lexical pattern.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface LexiconContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{

    /**
     * Adds a new directory to check for saved archives.
     * If the directory is not valid or not readable,
     * an exception will be raised.
     * @param string $name
     * @param string|\Directory $path
     * @return void
     */
    public function addPath( $name, $path );

    /**
     * Saves a path into the Lexicon archive index,
     * so it can be recalled on another runtime without
     * being redeclared.
     * @param string $name
     * @return void
     */
    public function savePath( $name );

    /**
     * Loads the Lexicon archive index into memory.
     * @return void
     */
    public function loadPaths();

    /**
     * Removes a path from memory, but retains the
     * saved index for another runtime.
     * @param string $name
     * @return void
     */
    public function prunePath( $name );

    /**
     * Removes a path from memory,
     * and also deletes it from the
     * Lexicon archive index.
     * @param string $name
     * @return void
     */
    public function forgetPath( $name );

    /**
     * Returns the absolute filepath disk location of a path by name.
     * @param string $name
     * @return string
     */
    public function getPath( $name );

    /**
     * Checks if the path exists and is known.
     * Returns true if it is known in current memory.
     * @param string $name
     * @return bool
     */
    public function checkPath( $name );

    /**
     * Returns a boolean determination as to whether a path is known and is readable.
     * Returns false if the path is not known or not readable.
     * @param string $name
     * @return bool
     */
    public function checkPathReadable( $name );

    /**
     * Returns a boolean determination as to whether a path is known and is writable.
     * Returns false if the path is not known or not writable
     * @param string $name
     * @return bool
     */
    public function checkPathWritable( $name );

    /**
     * Returns an associative array of the paths in memory where the key is
     * the name and the value is the absolute filepath to the disk
     * location.
     * @return array
     */
    public function getPaths();

    /**
     * Compiles the existing Lexicon indexes into serialized archives
     * that can be restored, and returns a collection of serialized indexes.
     * @return \oroboros\collection\Collection
     */
    public function compile();

    /**
     * Deletes all current LexiconIndexes from memory.
     * This does not delete entries that exist on disk
     * in Lexicon archive paths.
     * @return void
     */
    public function truncate();

    /**
     * Erases all archived Lexicon entries from a given path,
     * and also removes their archived references on disk.
     * @param string $path A valid path index slug
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given parameter is not a string
     */
    public function purge( $name );

    /**
     * Checks if a specific index key exists in memory.
     * @param type $index
     * @return type
     */
    public function check( $index );

    /**
     * Returns a list of all current indexes that exist in memory.
     * @return \oroboros\collection\Collection
     */
    public function index();

    /**
     * <Lexicon Index Getter Method>
     * Returns an existing LexiconIndex if it exists in memory.
     * If the second parameter is passed as true, it will also
     * check the archives on disk for a valid match.
     * @param string $lexicon_index The identifying slug of the LexiconIndex
     * @param bool $autoload (optional) If true, will autoload LexiconIndexes saved to disk but not in memory, otherwise only checks the instances in memory. Default false.
     * @return bool|\oroboros\codex\interfaces\contract\LexiconIndexContract
     */
    public function get( $lexicon_index, $autoload = false );

    /**
     * Adds a new LexiconIndex into memory. This method does not
     * save the LexiconIndex to disk. You will need to call the save()
     * method to do that.
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $lexicon_index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * @throws \oroboros\core\utilities\exception\codex\LexiconException
     */
    public function add( $lexicon_index );

    /**
     * Validates a lexicon index infoset, which can be an
     * ApiInterface, json file, array, or collection.
     *
     * This method will make sure that all required keys
     * are present and resolve truthfully, and return a
     * boolean determination as to whether it can be used
     * to create a LexiconIndex.
     *
     * @param array|string(filepath to or direct input of: serialized, or json)|\stdClass|\oroboros\collection\interfaces\contract\CollectionContract|\oroboros\codex\interfaces\contract\LexiconArchiveContract|\oroboros\codex\interfaces\contract\LexiconIndexContract $details
     * @return bool Returns true if all required keys are present with correct values, false otherwise
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided parameter cannot resolve to a correct set of data
     */
    public function validate( $details );

    /**
     * Creates a package json file from a given index.
     * If the optional path parameter is supplied,
     * it will be saved to the given path instead of returned.
     * @param string $index The index to compile into json.
     * @param string $path (optional) If supplied, represents a save path for the package json as a file. If omitted, it will be returned as a string.
     * @return string|null Returns a string if the second parameter is omitted
     * @throws \oroboros\core\utilities\exception\RuntimeException if the save operation fails.
     */
    public function package( $index, $path = null );

    /**
     * Creates a lexicon index from a index name
     * @param string $lexicon_index a json string or a file path
     * @return \oroboros\codex\LexiconIndex
     */
    public function unPackage( $lexicon_index );

    /**
     * Creates a new LexiconIndex from the given details,
     * named by the specified index, and retains it in memory.
     * @param string $index The slug for the new LexiconIndex to create
     * @param array|string|\oroboros\core\interfaces\libraries\container\CollectionContract $details
     * @param bool $save (optional) if true, the Lexicon will save an archive of the newly created index. Default false.
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given details cannot resolve into a lexicon index.
     */
    public function create( $index, $details, $save = false );

    /**
     * Updates the LexiconIndex of the given index key with
     * the supplied details in memory. This does not update
     * the archive on disk.
     * @param string $index the identifying key of the LexiconArchive to affect
     * @param mixed $details The data to update the LexiconArchive with
     * @param bool $save (optional) If true, will save the archive to disk and keep it in memory. Default false.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided details are invalid
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if provided index is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconException if the specified index does not exist in memory (this method does not update stored archives)
     */
    public function update( $index, $details, $save = false );

    /**
     * Deletes a LexiconEntry from memory by the given index.
     * @param type $index
     * @return void
     */
    public function delete( $index );

    /**
     * Performs a search query against the Lexicon, which will search
     * all LexiconEntries in memory for the given parameters by default,
     * but may be constrained to a particular index. If the second parameter
     * is passed as true, the search will also check all archived LexiconIndexes
     * that are saved on disk.
     * @param type $query The search query to perform
     * @param bool $use_archives (optional) if true, will search saved archives. If false, will only search archives in memory. Default false.
     * @return bool|\oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\codex\LexiconQueryException if a malformed query is passed
     */
    public function search( $query, $use_archives = false );

    /**
     * Returns a multidimensional array representing the archived
     * LexiconIndexes available in a specific path, where the
     * key is the path slug and the value is an array of LexiconIndexes
     * stored in that path.
     * @param string|array $index (optional) if provided, constrains the search to a specific path index or set of path indexes
     * @param string|array $tag (optional) if provided, constrains the search to a specific tag or set of tags
     * @return \oroboros\collection\Collection
     */
    public function tree( $index = null, $tag = null );

    /**
     * Fetches a list of all available LexiconIndexes in all known paths.
     * @return void
     */
    public function fetch();

    /**
     * Tags a LexiconIndex with a simple descriptor (32 chars or less).
     * Multiple LexiconIndexes can share tags, and tags can be queried for,
     * allowing the program to build custom associations between indexes.
     * @param string $index The LexiconIndex slug
     * @param string $tag The tag name to attach to it (32 chars or less, excess will be truncated)
     * @param string $value The tag value to attach to it (32 chars or less, excess will be truncated)
     * @return void
     */
    public function tag( $index, $tag, $value );

    /**
     * Saves a LexiconIndex to disk.
     * @param string $index The LexiconIndex slug
     * @param string $path (optional) if provided, represents the slug of a registered path to save the LexiconIndex under. If not provided, it will be saved under the default path.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid path key or unknown LexiconIndex is supplied
     */
    public function save( $index, $path = null );

    /**
     * Saves all indexes in memory to disk. A specific path
     * can be designated to create an export, but the path
     * must be registered already.
     * @param string $path (optional) if provided, represents the slug of a registered path to save the LexiconIndexes under. If not provided, they will be saved under the default path.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid path key is supplied
     */
    public function saveAll( $path = null );

    /**
     * Loads a LexiconIndex from disk. If it already exists in memory,
     * the in-memory LexiconIndex will be replaced with the archived one.
     * @param string $index the identifying slug of the index to load
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given index is not a string
     */
    public function restore( $index );

    /**
     * Loads all known LexiconIndexes into memory.
     * It is advised to use the fetch command before doing
     * this to insure that all paths are tracked with the
     * most recent updates.
     * @return void
     */
    public function restoreAll();
}
