<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\enumerated;

/**
 * <Token Enumerated Api Interface>
 * This enumerated interface provides more human readable descriptions
 * for common PHP tokens, to ease the readability of error reporting
 * and logging of error messages.
 *
 * This is the master PHP token enumerated interface,
 * and extends all other localized sets of token enumerated interfaces.
 * This can be used with the InterfaceEnumerator library to generate human readable
 * definitions that impart the scope and context of PHP internal tokens,
 * making the debugging effort easier, and allowing for more understandable
 * lexical references.
 *
 * For example, resolves the extremely weird T_PAAMAYIM_NEKUDOTAYIM
 * to "TOKEN_CONSTRUCT_CLASS_OPERATOR_STATIC", so log messages don't have
 * you saying T_PAAMAYa-whaaaat?
 *
 * If you do not speak hebrew, you probably have seen this and scratched
 * your head. It means you used the static "::" reference to call a class
 * method that only works on an instantiated object and needs "->", or you
 * attempted to statically call a method that is not static.
 * There are a number of gems like this that are otherwise difficult
 * to understand without context. This enumerated interface is used internally
 * by the Lexicon and the ErrorHandler to provide more readable context to
 * vanilla PHP errors and token declarations, by abstracting out the internal
 * references into human-readable contexts.
 *
 * This is used by the Lexicon Tokenizer also for validating PHP tokens
 * when auto-parsing packages from the command line api.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
interface Tokens
extends tokens\AssignmentTokens,
    tokens\BitwiseTokens,
    tokens\ClassConstructTokens,
    tokens\ConditionalTokens,
    tokens\LanguageConstructTokens,
    tokens\LogicalTokens,
    tokens\MagicConstantTokens,
    tokens\TypecastingTokens,
    tokens\VariableTokens
{
    //no-op
}
