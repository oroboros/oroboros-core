<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\enumerated\tokens;

/**
 * <Typecasting Token Enumerated Api Interface>
 * This enumerated interface provides more human readable descriptions
 * for PHP bitwise tokens, to ease the readability of error reporting
 * and logging of error messages.
 *
 * Typecasting tokens are language constructs that convert a variable
 * from one type to another.
 *
 * This is used by the Lexicon Tokenizer also for validating PHP tokens
 * when auto-parsing packages from the command line api.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
interface TypecastingTokens
extends \oroboros\codex\interfaces\enumerated\CodexBase
{

    //Typecasting Constructs
    const TOKEN_CONSTRUCT_TYPECAST_ARRAY = T_ARRAY_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_BOOL = T_BOOL_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_CALLABLE = T_CALLABLE;
    const TOKEN_CONSTRUCT_TYPECAST_FLOAT = T_DOUBLE_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_INTEGER = T_INT_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_OBJECT = T_OBJECT_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_STRING = T_STRING_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_UNSET = T_UNSET_CAST;
    const TOKEN_CONSTRUCT_TYPECAST_ARRAY_NAME = 'T_ARRAY_CAST';
    const TOKEN_CONSTRUCT_TYPECAST_BOOL_NAME = 'T_BOOL_CAST';
    const TOKEN_CONSTRUCT_TYPECAST_CALLABLE_NAME = 'T_CALLABLE';
    const TOKEN_CONSTRUCT_TYPECAST_FLOAT_NAME = 'T_DOUBLE_CAST';
    const TOKEN_CONSTRUCT_TYPECAST_INTEGER_NAME = 'T_INT_CAST';
    const TOKEN_CONSTRUCT_TYPECAST_OBJECT_NAME = 'T_OBJECT_CAST';
    const TOKEN_CONSTRUCT_TYPECAST_STRING_NAME = 'T_STRING_CAST';
    const TOKEN_CONSTRUCT_TYPECAST_UNSET_NAME = 'T_UNSET_CAST';

}
