<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Codex Index Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\CodexIndexContract
 */
trait CodexIndexTrait
{

    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\core\BaselineTrait::initialize as private baseline_initialize;
    }

    /**
     * Represents the identifier of the index
     * @var string
     */
    private $_codex_index_context = false;

    /**
     * Represents the entries contained in the index
     * @var \oroboros\collection\Collection
     */
    private $_codex_index_entries;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\CodexIndexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Codex Index Initialization Method>
     * Fires the codex index initialization.
     *
     * Codex Indexes cannot be reinitialized after initial initialization.
     *
     * @param mixed $params Any parameters that should be passed into initialization.
     * @param array $dependencies Any dependencies to inject into initialization.
     * @param array $flags
     * @return $this
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineSetParameterPersistence( true );
        $this->_baselineEnableParameterValidation( true );
        $this->_baselineRegisterInitializationTask( '_codexIndexPostInitialization' );
        $this->_baselineDisableReinitialization();
        $this->baseline_initialize( $params, $dependencies, $flags );
        return $this;
    }

    /**
     * <Codex Index Context Getter Method>
     * Returns the context set for the codex index.
     * @return string
     */
    public function getContext()
    {
        return $this->_codex_index_context;
    }

    /**
     * <Codex Index Entry Getter Method>
     *
     * @param type $id
     * @return bool|\oroboros\codex\interfaces\contract\CodexEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is non scalar and not a codex entry
     */
    public function getEntry( $id )
    {
        if ( $id instanceof \oroboros\codex\interfaces\contract\CodexEntryContract )
        {
            $id = $id->getId();
        }
        self::_validate( 'scalar', $id, null, true );
        if ( !$this->_codex_index_entries->has( $id ) )
        {
            return false;
        }
        return $this->_codex_index_entries[$id];
    }

    /**
     * <Codex Index Entry Check Method>
     * Checks if a given entry already exists.
     * @param scalar $id
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is non scalar and not a codex entry
     */
    public function hasEntry( $id )
    {
        if ( $id instanceof \oroboros\codex\interfaces\contract\CodexEntryContract )
        {
            $id = $id->getId();
        }
        self::_validate( 'scalar', $id, null, true );
        return $this->_codex_index_entries->has( $id );
    }

    /**
     * <Codex Index List Method>
     * Returns a list of all of the entries contained in the index.
     * @return array
     */
    public function listEntries()
    {
        return array_keys( $this->_codex_index_entries->toArray() );
    }

    /**
     * <Codex Index Value Getter Method>
     * Fetches the values from an entry or all entries and returns the results.
     * Returns null if no results are found.
     * @param type $id (optional) If provided, will scope the value query
     *     to the given entry id. If not provided, returns an array of the
     *     values for all entries. Returns null if there are no entries.
     * @return null|array|scalar Returns null if there are no results for
     *     the given parameters. Returns an array if no id is passed and the
     *     index contains any entries. Returns a scalar value if an id is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is not null, non-scalar, and not a codex entry
     */
    public function getValue( $id = null )
    {
        if ( $id instanceof \oroboros\codex\interfaces\contract\CodexEntryContract )
        {
            $id = $id->getId();
        }
        if ( is_null( $id ) )
        {
            $values = array();
            foreach ( $this->_codex_index_entries as
                $id =>
                $entry )
            {
                $values[$id] = $entry->getValue();
            }
            return empty( $values )
                ? null
                : $values;
        }
        self::_validate( 'scalar', $id, null, true );
        if ( !$this->hasEntry( $id ) )
        {
            return null;
        }
        return $this->_codex_index_entries[$id]->getValue();
    }

    /**
     * <Codex Index Metadata Getter Method>
     * Returns the metadata for one or all entries, optionally scoped to a
     * given key, or null if no results are found.
     *
     * If [id] is null, the metadata for all entries will be returned,
     * or null if no entries exist in the index. If [key] is passed and [id]
     * is null, then the return result will be filtered by only the entries
     * that contain the given metadata key.
     *
     * If [id] is not null, the metadata query will return a result only for
     * the given id. If the given id does not exist or does not have the
     * given key, then null will be returned. If the given key does exist,
     * a scalar value will be returned.
     *
     * @param type $id
     * @param type $key
     * @return null|array|scalar
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is not null, non-scalar, and not a codex entry
     */
    public function getMeta( $id = null, $key = null )
    {
        if ( $id instanceof \oroboros\codex\interfaces\contract\CodexEntryContract )
        {
            $id = $id->getId();
        }
        if ( is_null( $id ) )
        {
            $meta = array();
            foreach ( $this->_codex_index_entries as
                $id =>
                $entry )
            {
                $entry_meta = $entry->getMeta( $key );
                if ( $entry_meta )
                {
                    $meta[$id] = $entry_meta;
                }
            }
            return empty( $meta )
                ? null
                : $meta;
        }
        self::_validate( 'scalar', $id, null, true );
        if ( !$this->hasEntry( $id ) )
        {
            return null;
        }
        return $this->_codex_index_entries[$id]->getMeta( $key );
    }

    /**
     * <Codex Index Relation Getter Method>
     * Returns the relations listed for the given entry or all entries.
     *
     * If [id] is null, returns the relations for all entries.
     * If no entries have relations, null will be returned, otherwise an
     * array of the relations will be returned, keyed by the id of the entry.
     *
     * If [id] is not null, the relations for the given id will be returned,
     * or null if it does not exist or doesn't have any.
     *
     * @param type $id
     * @return null|array
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is not null, non-scalar, and not a codex entry
     */
    public function getRelations( $id = null )
    {
        if ( $id instanceof \oroboros\codex\interfaces\contract\CodexEntryContract )
        {
            $id = $id->getId();
        }
        if ( is_null( $id ) )
        {
            $relations = array();
            foreach ( $this->_codex_index_entries as
                $id =>
                $entry )
            {
                $relations[$id] = $entry->getRelations();
            }
            return empty( $relations )
                ? null
                : $relations;
        }
        self::_validate( 'scalar', $id, null, true );
        if ( !$this->hasEntry( $id ) )
        {
            return null;
        }
        return $this->_codex_index_entries[$id]->getRelations();
    }

    /**
     * <Codex Index Relation Check Method>
     * Checks explicitly for a given relation in a specific entry.
     *
     * Returns false if the given entry does not exist or does not have
     * the given relation, otherwise returns true.
     *
     * @param scalar $id The codex entry id to check against
     * @param string $index The codex index to check for the relation in
     * @param scalar $relation The relation within the codex index to check for
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is non-scalar and not a codex entry
     */
    public function hasRelation( $id, $index, $relation )
    {
        if ( $id instanceof \oroboros\codex\interfaces\contract\CodexEntryContract )
        {
            $id = $id->getId();
        }
        self::_validate( 'scalar', $id, null, true );
        if ( !$this->hasEntry( $id ) )
        {
            return false;
        }
        return $this->_codex_index_entries[$id]->hasRelation( $index, $relation );
    }

    /**
     * <Codex Index Entry Setter Method>
     * Sets a new codex entry into the index if it matches
     * the context and has not already been set.
     * @param \oroboros\codex\interfaces\contract\CodexEntryContract $entry
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the codex entry does not match its contract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the codex entry is already defined
     */
    public function setEntry( $entry )
    {
        $this->_codexIndexSetEntry( $entry );
    }

    /**
     * <Codex Index Entry Replace Method>
     * Replaces an existing codex entry with a new one in the index, if it matches
     * the context. If it has not already been added it will be added as is.
     * @param \oroboros\codex\interfaces\contract\CodexEntryContract $entry
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the codex entry does not match its contract
     */
    public function replaceEntry( $entry )
    {
        $this->_codexIndexSetEntry( $entry, true );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Baseline Required Parameter Setter Method>
     * May be overridden to provide a set of parameters that
     * are required to be provided for initialization to resolve.
     *
     * Any parameters that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetParametersRequired()
    {
        return array(
            'context'
        );
    }

    /**
     * <Baseline Valid Parameter Setter Method>
     * May be overridden to provide a validation array for provided parameters.
     * If provided, any provided parameters matching the specified keys MUST
     * resolve to the given type or instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided parameters, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array(
            'context' => 'string',
            'entries' => 'array'
        );
    }

    /**
     * <Baseline Default Parameter Setter Method>
     * May be overridden to provide a set of default parameters.
     * If provided, the given set will act as a baseline set of
     * templated parameters, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * parameters.
     *
     * This allows for parameters to be provided in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return array(
            'entries' => array()
        );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the context value for the codex index.
     * @param string $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If context is not a string
     * @throws \oroboros\core\utilities\exception\RuntimeException If the context is already set
     * @internal
     */
    private function _codexIndexSetContext( $context )
    {
        self::_validate( 'type-of', $context, 'string', true );
        if ( $this->_codex_index_context )
        {
            $this->_throwException( 'runtime-exception',
                sprintf( 'Error encountered at [%s]. Cannot set context as [%s] '
                    . 'because it is already set as  [%s].', __METHOD__,
                    $context, $this->_codex_index_context ),
                self::_getExceptionCode( 'logic-improper-value-mutation' ) );
        }
        $this->_codex_index_context = $context;
    }

    /**
     * Sets a new codex entry into the index, if it matches
     * the context and has not already been set.
     * @param \oroboros\codex\interfaces\contract\CodexEntryContract $entry
     * @param bool $replace (optional) If true, will replace the existing
     *     codex entry with the new one and bypass the ambiguous
     *     reference check.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the codex entry does not match its contract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the codex entry is already defined and replace is false
     * @internal
     */
    private function _codexIndexSetEntry( $entry, $replace = false )
    {
        if ( !$entry->getContext() === $this->getContext() )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s] in instance of [%s].'
                    . PHP_EOL . \oroboros\format\StringFormatter::indent( 'Provided codex entry context [%s] does not match '
                        . 'the expected context [%s].', 4 ), __METHOD__,
                    get_class( $this ), $entry->getContext(),
                    $this->getContext() ),
                self::_getExceptionCode( 'logic-bad-parameters' ) );
        }
        if ( $this->_codex_index_entries->has( $entry->getId() ) )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s] in instance of [%s].'
                    . PHP_EOL . \oroboros\format\StringFormatter::indent( 'Provided codex entry context [%s] has already been defined.',
                        4 ), __METHOD__, get_class( $this ), $entry->getId() ),
                self::_getExceptionCode( 'logic-ambiguous-reference' ) );
        }
        $this->_codex_index_entries[$entry->getId()] = $entry;
    }

    /**
     * Runs the post initialization logic for the codex index.
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException If post-initialization fails
     * @internal
     */
    private function _codexIndexPostInitialization()
    {
        if ( is_null( $this->_codex_index_entries ) )
        {
            $this->_codex_index_entries = new \oroboros\collection\Collection();
        }
        $this->_codexIndexSetContext( $this->_baselineGetParameter( 'context' ) );
        foreach ( $this->_baselineGetParameter( 'entries' ) as
            $entry )
        {
            $this->setEntry( $entry );
        }
    }

}
