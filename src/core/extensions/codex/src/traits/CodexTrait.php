<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Codex Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\CodexContract
 */
trait CodexTrait
{

    /**
     * Most of the underlying logic for the base accessor
     * class is provided by the static control api trait.
     */
    use \oroboros\core\traits\extensions\ExtendableTrait
    {
        \oroboros\core\traits\extensions\ExtendableTrait::staticInitialize as private extendable_staticInitialize;
    }

    /**
     * Represents the current metadata save path
     * @var \Directory
     */
    private static $_codex_path;

    /**
     * Represents the collection of known save paths
     * @var \oroboros\collection\Collection
     */
    private static $_codex_paths;

    /**
     * Represents the default metadata save path,
     * which is used if no other path is supplied
     * @var \Directory
     */
    private static $_codex_path_default;

    /**
     * Represents the internal store of Codex Indexes
     * @var \oroboros\collection\Collection
     */
    private static $_codex_indexes;

    /**
     * Represents the current archive instances for
     * saving and restoring codex indexes
     * @var \oroboros\collection\Collection
     */
    private static $_codex_archives;

    /**
     * Represents the current dictionaries for
     * referencing read-only data
     * @var \oroboros\collection\Collection
     */
    private static $_codex_dictionaries;

    /**
     * Represents the default utilities used for generation of codex contexts
     * @var array
     */
    private static $_codex_default_generators = array(
        'api' => '\\oroboros\\codex\\generators\\entry\\api\\ApiGenerator',
        'class-type' => '\\oroboros\\codex\\generators\\entry\\classes\\TypeGenerator',
        'class-scope' => '\\oroboros\\codex\\generators\\entry\\classes\\ScopeGenerator',
        'class-context' => '\\oroboros\\codex\\generators\\entry\\classes\\ContextGenerator',
        'error-code' => '\\oroboros\\codex\\generators\\entry\\code\\ErrorCodeGenerator',
        'component' => '\\oroboros\\codex\\generators\\entry\\component\\ComponentGenerator',
        'config' => '\\oroboros\\codex\\generators\\entry\\config\\ConfigGenerator',
        'contract' => '\\oroboros\\codex\\generators\\entry\\contract\\ContractGenerator',
        'core' => '\\oroboros\\codex\\generators\\entry\\core\\CoreGenerator',
        'enum' => '\\oroboros\\codex\\generators\\entry\\enum\\EnumGenerator',
        'environment' => '\\oroboros\\codex\\generators\\entry\\environment\\EnvironmentGenerator',
        'extension' => '\\oroboros\\codex\\generators\\entry\\extension\ExtensionGenerator',
        'module' => '\\oroboros\\codex\\generators\\entry\\module\\ModuleGenerator',
        'package' => '\\oroboros\\codex\\generators\\entry\\package\\PackageGenerator',
        'routine' => '\\oroboros\\codex\\generators\\entry\\routine\\RoutineGenerator',
    );

    private static $_codex_default_dictionaries = array(
        'file-types' => '\\oroboros\\core\\utilities\\'
    );

    /**
     * Represents the current set of generators
     * @var \oroboros\collection\Collection
     */
    private static $_codex_generators;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\codex\interfaces\contract\CodexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Initialization>
     * This is the initialization method for the Codex.
     * It will load the default dependencies of the system and insure
     * that they are available for further requests. This is typically
     * called during the bootload routine, and does not need to be
     * called again. However, for unit testing or customization,
     * you may reinitialize with specified parameters at any time.
     *
     * An internal registry of initializations will be kept, so in the
     * event of an error or mis-configuration or unexpected results,
     * some record of when/where the reinitialization can be easily
     * determined and corrected.
     *
     * This method will take three optional parameters.
     *
     * $dependencies is an optional associative array of dependencies for
     * dependency injection. If any of these are passed, it will
     * check that they honor the corresponding contract, and if so,
     * use them in place of it's defaults. This is useful for unit
     * testing or custom configuration. Any parameters that do not
     * implement the expected contract interface will cause an exception
     * to be raised and break initialization. The system will not make
     * any effort to catch this exception, as it will lead to potentially
     * unstable behavior if it is allowed to proceed.
     *
     * $options is an optional associative array of settings or configuration
     * details to pass into initialization. These correspond to the keys listed
     * in the conf/settings_default.json file. You may override these in bulk by
     * creating a file named settings.json and overriding any keys that you wish
     * to provide other defaults for. You only need to override the keys you want
     * to change defaults for, as the result will be array_merge'd with the default,
     * so any defaults that are not provided are retained. You may also provide a
     * settings file outside of the project directory structure by designating
     * its fully qualified path by defining the constant OROBOROS_BOOTLOAD_SETTINGS
     * as its fully qualified file path. The file must be in json format, but may
     * exist anywhere in your filesystem. You may also set an environment
     * variable with this same name in place of defining the constant, and if
     * such an environment variable exists, its value will be used to define
     * the constant in place of the default.
     *
     * $flags is an optional array of bootload flags that may be passed into
     * initialization. No flags are used by default, but any available in the
     * BootloadFlag enumerated interface may be used. These will modify behavior
     * of the system as described in their block comment in the enumerated api
     * interface. If reinitialization occurs, these flags will all be cleared
     * if they are not explicitly passed again. You may override these in bulk
     * by defining a constant named OROBOROS_BOOTLOAD_FLAGS as the fully
     * qualified path to a json file containing an array of the key names of
     * the flags you wish to pass. Only the flags in the BootloadFlag interface
     * will be honored, and all others will be silently discarded.
     * You may also accomplish this by declaring an environment variable
     * of the same name, which will be used in place of the constant if
     * such an environment variable exists.
     *
     * @param array $dependencies
     * @param array $options
     * @param array $flags
     * @return void
     */
    public static function initialize( array $dependencies = array(),
        array $options = array(), array $flags = array() )
    {
        return self::_codexHandleInitialization( $dependencies, $options, $flags );
    }

    /**
     * <Static Initialization Proxy Method>
     * Proxies the static baseline initialization to the codex initialization,
     * so that both contracts can be honored without missing initialization steps.
     * @param array $parameters (optional) Optional flags to pass
     * @param array $dependencies (optional) Optional flags to pass
     * @param array $flags (optional) Optional flags to pass
     */
    public static function staticInitialize( $parameters = null,
        $dependencies = null, array $flags = null )
    {
        if ( is_null( $parameters ) )
        {
            $parameters = array();
        }
        if ( is_null( $dependencies ) )
        {
            $dependencies = array();
        }
        if ( is_null( $flags ) )
        {
            $flags = array();
        }
        return self::initialize( $dependencies, $parameters, $flags );
    }

    /**
     * <Initialization Check Method>
     * This method returns a boolean determination as to whether
     * it is safe to proceed calling other methods. If this
     * returns false, initialization needs to occur prior
     * to other usage.
     * @return bool
     * @since 0.0.0.1a
     */
    public static function isInitialized()
    {
        return self::isStaticInitialized();
    }

    //Codex operations

    /**
     * Gets the save path for Codex metadata and archives
     * @return string
     */
    public static function getPath()
    {

    }

    /**
     * Sets the save path for Codex metadata and archives
     * @param string|\Directory $path
     */
    public static function setPath( $path )
    {

    }

    /**
     * Builds all known indexes using the current generators
     * @return void
     */
    public static function build()
    {

    }

    /**
     * Resets the Codex to its default empty state
     * @return void
     */
    public static function reset()
    {

    }

    /**
     * Saves all indexes to archive
     * @return void
     */
    public static function save()
    {

    }

    /**
     * Restores all archived indexes
     * @return void
     */
    public static function restore()
    {

    }

    /**
     * Lists all indexes
     * @return array
     */
    public static function indexes()
    {

    }

    //Codex generator operations

    public static function generate( $generator, $source )
    {

    }

    public static function setGenerator( $generator )
    {

    }

    public static function getGenerator( $generator )
    {

    }

    public static function listGenerators()
    {

    }

    public static function saveGenerator( $generator )
    {

    }

    public static function restoreGenerator( $generator )
    {

    }

    public static function getGeneratorContext( $generator )
    {

    }

    public static function getGeneratorValid( $generator )
    {

    }

    public static function checkGeneratorValid( $generator, $source )
    {

    }

    public static function validateGenerator( $generator )
    {

    }

    //Codex index operations

    /**
     *
     * @param type $index
     */
    public static function setIndex( $index )
    {

    }

    /**
     * <Codex Index Getter Method>
     * Returns the codex index listed under the given index name,
     * or false if it does not exist.
     * @param type $index
     * @return bool|\oroboros\codex\interfaces\contract\CodexIndexContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-stringable parameter is passed
     */
    public static function getIndex( $index )
    {
        self::_validate( 'stringable', $index, null, true );
        if ( !self::$_codex_indexes->has( (string) $index ) )
        {
            return false;
        }
        return self::$_codex_indexes[(string) $index];
    }

    /**
     *
     * @param type $index
     * @param type $entries
     */
    public static function registerIndex( $index, $entries = array() )
    {
        if ( !self::_codexCheckIndex( $index ) )
        {
            self::_codexCreateIndex( $index, $entries );
        } else
        {
            foreach ( $entries as
                $key =>
                $entry )
            {
                if ( !self::_codexCheckEntry( $index, $entry ) )
                {
                    self::_codexSetEntry( $index, $entry );
                }
            }
        }
    }

    public static function createIndex( $index, $entries = array() )
    {

    }

    public static function updateIndex( $index, $entries = array() )
    {

    }

    public static function deleteIndex( $index )
    {

    }

    public static function mergeIndex( $index, $merge_index )
    {

    }

    public static function cloneIndex( $index, $new_id )
    {

    }

    public static function splitIndex( $index, $new_id, $entry_ids )
    {

    }

    public static function grepIndex( $index, $pattern )
    {

    }

    public static function lockIndex( $index )
    {

    }

    public static function unlockIndex( $index, $key )
    {

    }

    public static function validateIndex( $index )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function pushIndex( $index, $destination )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function pullIndex( $index, $source )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function archiveIndex( $index )
    {

    }

    public static function restoreIndex( $index )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function listIndex( $index )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function purgeIndex( $index )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function dropIndex( $index )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function truncateIndex( $index )
    {

    }

    /**
     *
     * @param type $index
     */
    public static function reportIndex( $index )
    {

    }

    /**
     * Scans all entries for the given key as its id,
     * and returns a collection of the results.
     * @param type $key
     * @param type $subkey
     */
    public static function lookup( $key, $subkey = null, $index = null )
    {

    }

    /**
     * Scans all entries for a given keyword,
     * and returns a collection of the results.
     * @param type $keyword
     * @param type $context
     * @param type $index
     */
    public static function search( $keyword, $context = null, $index = null )
    {

    }

    /**
     * Maps a set of codex entries to the correct indexes.
     * If the indexes do not already exist, they will be created.
     * @param type $indexes
     */
    public static function mapEntries( $entries )
    {

    }

    /**
     *
     * @param type $index
     * @param type $id
     * @param type $entry
     */
    public static function registerEntry( $index, $id, $entry )
    {
        self::_codexCreateEntry( $context, $id, $details );
    }

    /**
     *
     * @param type $entry
     */
    public static function setEntry( $entry )
    {

    }

    /**
     *
     * @param type $index
     * @param type $entry
     */
    public static function getEntry( $index, $entry )
    {

    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the internal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Extension Contract Declaration Method>
     * This method should be overridden with a contract interface
     * expected to be honored by valid extensions.
     * @return string
     */
    protected static function _declareExtensionContract()
    {
        return '\\oroboros\\codex\\interfaces\\contract\\CodexExtensionContract';
    }

    /**
     * <Extension Context Declaration Method>
     * This method should be overridden with the slug identifier
     * that the extension is expected to match.
     * @return string
     */
    protected static function _declareExtensionContext()
    {
        return \oroboros\codex\interfaces\api\CodexApi::API_SCOPE;
    }

    /**
     * Sets the static baseline default parameters
     * @return array
     */
    protected static function _staticBaselineSetParametersDefault()
    {
        return \oroboros\core\utilities\core\CoreConfig::get( 'settings', 'core' )['codex'];
    }

    /**
     * Sets the static baseline default parameters
     * @return array
     */
    protected static function _staticBaselineSetParametersValid()
    {
        return array_keys( self::_staticBaselineSetParametersDefault() );
    }

    /**
     * Sets the static baseline default parameters
     * @return array
     */
    protected static function _staticBaselineSetDependenciesDefault()
    {
        $defaults = array();
        return $defaults;
    }

    /**
     * Sets the static baseline valid flags
     * @return array
     */
    protected static function _staticBaselineSetFlagsValid()
    {
        $validator = new \oroboros\enum\InterfaceEnumerator(
            '\\oroboros\\core\\interfaces\\enumerated\\flags\\BootloadFlags'
        );
        $valid = $validator->toArray();
        return $valid;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Handles the actual initialization. The public method is just a
     * passthrough to this method.
     * @param array $dependencies Any dependencies that should override the defaults.
     * @param array $options Any options that should override the defaults.
     * @param array $flags Any flags that should be retained to modify functionality.
     * @return string current class name for static method chaining
     * @since 0.2.5
     */
    private static function _codexHandleInitialization( array $dependencies,
        array $options, array $flags )
    {
        //The system is now usable
        self::_staticBaselineEnableFlagValidation();
        self::_staticBaselinePreventFlagUnSetting();
        self::_staticBaselineSetParameterPersistence( true );
        self::_staticBaselineSetDependencyPersistence( true );
        self::_staticBaselineSetFlagPersistence( true );
        self::_staticBaselineRegisterInitializationTask( '_codexPostInitialization' );
        self::extendable_staticInitialize( $options, $dependencies, $flags );
        $class = get_called_class();
        return $class;
    }

    /**
     * Handles the post-initialization operations after
     * the baseline initialization.
     * @return void
     */
    private static function _codexPostInitialization()
    {
        self::_codexInitializeContainers();
        self::_codexInitializeGenerators();
        self::_codexInitializeLexicon();
        $settings = self::$_static_baseline_parameters;
        $mode = \oroboros\core\utilities\core\CoreConfig::get( 'mode' );
        foreach ( self::$_static_baseline_dependencies as
            $key =>
            $dependency )
        {
            $dependency_settings = array();
            if ( array_key_exists( $key, $settings ) )
            {
                $dependency_settings = $settings[$key];
                if ( array_key_exists( $mode, $dependency_settings ) )
                {
                    $dependency_settings = $dependency_settings[$mode];
                }
            }
            $dependency::staticInitialize( $dependency_settings, null,
                self::$_static_baseline_flags );
            if ( !is_object( $dependency ) )
            {
                $dependency = new $dependency( $dependency_settings, null,
                    self::$_static_baseline_flags );
            }
            self::$_static_baseline_dependencies[$key] = $dependency;
        }
        $class = get_called_class();
//        dd( new $class(), self::$_codex_indexes->toArray() );
    }

    /**
     * Initializes the default container objects for internal
     * properties if they are not already initialized.
     * @return void
     * @internal
     */
    private static function _codexInitializeContainers()
    {
        $props = array(
            '_codex_paths',
            '_codex_indexes',
            '_codex_archives',
            '_codex_generators',
            '_codex_dictionaries',
        );
        foreach ( $props as
            $prop )
        {
            if ( is_null( self::${$prop} ) )
            {
                self::${$prop} = new \oroboros\collection\Collection();
            }
        }
        $class = get_called_class();
    }

    /**
     * Initializes the default generators
     * @return void
     * @internal
     */
    private static function _codexInitializeGenerators()
    {
        foreach ( self::$_codex_default_generators as
            $id =>
            $generator )
        {
            if ( !self::$_codex_generators->has( $id ) )
            {
                $instance = new $generator( null, null, self::getCompiledFlags() );
                self::setGenerator( $instance );
            }
        }
    }

    /**
     * Initializes the codex Lexicon for handling class referential data.
     * @return void
     * @internal
     */
    private static function _codexInitializeLexicon()
    {
        return;
        $settings = self::$_static_baseline_parameters['lexicon'];
        $lexicon = new \oroboros\codex\Lexicon( $settings, null,
            \oroboros\Oroboros::getCompiledFlags() );
        self::_staticBaselineSetDependency( 'lexicon', $lexicon, true );
    }

    /**
     * Initializes the codex dictionaries for looking up common read-only data.
     * @return void
     * @internal
     */
    private static function _codexInitializeDictionaries()
    {

    }

    /**
     * Creates the baseline set of codex indexes that are always
     * available on system initialization.
     * @return void
     * @internal
     */
    private static function _codexRegisterDefaultGenerators()
    {
        foreach ( self::$_codex_default_generators as
            $slug =>
            $generator )
        {
            self::_codexRegisterGenerator( new $generator( null, null,
                self::getCompiledFlags() ) );
        }
    }

    private static function _codexRegisterGenerator( $generator )
    {
//        d( 'registering generator ', $generator );
        self::_validate( 'instance-of', $generator,
            '\\oroboros\\core\\interfaces\\contract\\utilities\\core\\CodexGeneratorContract',
            true );
        self::registerIndex( $generator->getContext(), $generator->getDefaults() );
        self::$_codex_generators[$generator->getContext()] = $generator;
//        d( $generator, $generator->getContext(), $generator->getDefaults(),
//            self::$_codex_indexes->toArray() );
    }

    /**
     * Creates a new codex index.
     * @param type $context The identifying context of the index
     * @param type $entries (optional) Any initial codex entries to
     * populate the index with, or arrays of valid details for creating them
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If context is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If entry are not valid codex entries, or are not a valid array
     *     of details for creating them
     * @internal
     */
    private static function _codexCreateIndex( $context, $entries = array() )
    {
        if ( self::_codexCheckIndex( $context ) )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s].'
                    . ' Context [%s] is already defined.', __METHOD__, $context ),
                self::_getExceptionCode( 'logic-improper-value-mutation' ) );
        }
        $index = new \oroboros\codex\CodexIndex( array(
            'context' => $context,
            'entries' => $entries ) );
        self::$_codex_indexes[$context] = $index;
    }

    /**
     * Checks if a given context already exists.
     * @param string $context
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If context is not a string
     * @internal
     */
    private static function _codexCheckIndex( $context )
    {
        self::_validate( 'type-of', $context, 'string', true );
        return self::$_codex_indexes->has( $context );
    }

    private static function _codexCreateEntry( $context, $id, $details )
    {
        if ( !self::_codexCheckIndex( $context ) )
        {
//            d( self::$_codex_indexes, $context,
//                self::$_codex_indexes->has( $context ) );
            self::_codexCreateIndex( $context );
        }
        $details['id'] = $id;
        $details['context'] = $context;
        $entry = new \oroboros\codex\CodexEntry( $details, null,
            self::_staticBaselineGetFlags() );
        self::$_codex_indexes[$context]->setEntry( $entry );
    }

    private static function _codexRegisterEntry( $entry )
    {
        self::_validate( 'instance-of', $entry,
            '\\oroboros\\core\\interfaces\\contract\\libraries\\codex\\CodexEntryContract',
            true );
        $index = $entry->getContext();
        if ( !self::_codexCheckIndex( $index ) )
        {
            self::_codexCreateIndex( $context );
        }
        self::$_codex_indexes[$index]->setEntry( $entry );
    }

    private static function _codexCheckEntry( $index, $id )
    {
        if ( !self::_codexCheckIndex( $index ) )
        {
            return false;
        }
        return self::$_codex_indexes[$index]->hasEntry( $id );
    }

    private static function _codexGetIndex( $index )
    {
        self::_validate( 'stringable', $index, null, true );
        $index = (string) $index;
        if ( !array_key_exists( $index, self::$_codex_indexes ) )
        {
            return false;
        }
        return self::$_codex_indexes[$index];
    }

}
