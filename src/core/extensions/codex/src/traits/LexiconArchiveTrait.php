<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Lexicon Archive Trait>
 * Provides a set of methods for storing and retrieving serialized sets
 * of LexiconIndexes to and from disk.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\LexiconArchiveContract
 */
trait LexiconArchiveTrait
{

    use \oroboros\core\traits\patterns\behavioral\WorkerTrait;

    /**
     *
     * @var type
     */
    private $_lexicon_archive_key;

    /**
     *
     * @var type
     */
    private $_lexicon_archive_path;

    /**
     * Represents the disk location of the serialized LexiconIndex archive
     * @var string
     */
    private $_lexicon_archive_file;

    /**
     * Represents the LexiconIndex that is represented by the Archive
     * @var \oroboros\codex\interfaces\contract\LexiconIndexContract
     */
    private $_lexicon_archive_index;

    /**
     * Represents whether the LexiconArchive has successfully initialized
     * @var bool
     */
    private $_lexicon_archive_initialized = false;
    private static $_lexicon_archive_file_suffix = '.lexicon.index.lex';

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\LexiconContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Lexicon Archive Constructor>
     * Creates a new LexiconArchive instance to reflect a LexiconIndex
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @param string $key
     * @param string $path
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException if the path cannot resolve or the LexiconIndex cannot be serialized
     */
    public function __construct( \oroboros\codex\interfaces\contract\LexiconIndexContract &$index,
        $key, $path )
    {
        $class = get_class( $this );
        $this->_setWorkerCategory( defined( $class . '::OROBOROS_CLASS_TYPE' )
                ? constant( $class . '::OROBOROS_CLASS_TYPE' )
                : false  );
        $this->_setWorkerScope( defined( $class . '::OROBOROS_CLASS_SCOPE' )
                ? constant( $class . '::OROBOROS_CLASS_SCOPE' )
                : false  );
        $this->_initializeLexiconArchive( $index, $key, $path );
    }

    /**
     * Returns a serialized representation of the archive for stashing
     * in a Lexicon header file or creating a data export.
     * @return string
     */
    public function serialize()
    {
        if ( is_null( $this->_lexicon_archive_file ) )
        {
            $this->_lexiconArchiveSave();
        }
        $properties = array(
            'key' => $this->_lexicon_archive_key,
            'path' => $this->_lexicon_archive_path,
            'file' => $this->_lexicon_archive_file,
        );
        return serialize( $properties );
    }

    /**
     * Restores a LexiconArchive from a serialized state
     * @param type $serialized
     * @return void
     */
    public function unserialize( $serialized )
    {
        $this->_baselineGetObjectFingerprint();
        $values = unserialize( $serialized );
        $this->_lexicon_archive_key = $values['key'];
        $this->_lexicon_archive_path = $values['path'];
        $this->_lexicon_archive_file = $values['file'];
        if ( !is_null( $this->_lexicon_archive_file ) )
        {
            $this->_lexicon_archive_index = unserialize( file_get_contents( $this->_lexicon_archive_file ) );
        }
        $this->_lexicon_archive_initialized = true;
    }

    /**
     * Saves the LexiconIndex to disk and returns the file name.
     * @return string
     */
    public function save()
    {
        return $this->_lexiconArchiveSave();
    }

    /**
     * Restores a LexiconIndex from a given key and path.
     * If there are multiple available, the most recent will be returned.
     * @param string $key The identifying slug of the LexiconIndex
     * @param string $path The save path of the LexiconArchive store
     * @param bool $versioned If true, will return a collection of versioned changes to the LexiconIndex. If false, will return only the most recent version. Default false.
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException if the key or path is not valid, and cannot resolve to a valid archive
     */
    public static function load( $key, $path, $versioned = false )
    {
        return self::_lexiconArchiveRestoreFromKey( $key, $path, $versioned );
    }

    /**
     * Restores a LexiconIndex directly from a specific file.
     * Use this if there are multiple versions of the LexiconIndex
     * and you want to specify a specific version.
     * @param string $file
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException if the specified archive does not exist
     */
    public static function loadFromFile( $file )
    {
        return self::_lexiconArchiveRestoreFromFile( $file );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    protected function _initializeLexiconArchive( \oroboros\codex\interfaces\contract\LexiconIndexContract &$index,
        $key, $path )
    {
        $this->_lexiconArchiveCheckIndex( $index );
        $this->_lexiconArchiveCheckPath( $path );
        $this->_lexicon_archive_index = $index;
        $this->_lexicon_archive_key = $key;
        $this->_lexicon_archive_path = $path;
        $this->_lexicon_archive_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Checks that the archive directory is valid, readable, and writable.
     * @param string $path a valid directory path
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException if the provided path is not a directory, is not readable, or is not writable
     */
    private function _lexiconArchiveCheckPath( $path )
    {
        if ( !is_string( $path ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $path ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !is_dir( $path ) || !is_readable( $path ) || !is_writable( $path ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                sprintf( 'Directory [%s] %s', $path,
                    (!is_dir( $path )
                        ? 'does not exist.'
                        : (!is_writable( $dir )
                            ? 'is not writable.'
                            : 'is not readable.') ) ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Checks to insure that the LexiconIndex can be properly serialized.
     * @param type $index \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException if serialization of the LexiconIndex fails
     */
    private function _lexiconArchiveCheckIndex( $index )
    {
        try
        {
            serialize( clone $index );
        } catch ( \Exception $e )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
            sprintf( 'Error encountered at [%s]. Could not resolve [%s] because serialization of [%s] failed.',
                __METHOD__, get_class( $this ), get_class( $index ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_SERIALIZATION_FAILURE
            );
        }
    }

    /**
     * Saves the LexiconIndex to disk and
     * @return string
     */
    private function _lexiconArchiveSave()
    {
        $filename = $this->_lexiconArchiveCreateSaveName();
        $data = serialize( clone $this->_lexicon_archive_index );
        $sh = fopen( $filename, 'wb' );
        fwrite( $sh, $data );
        fclose( $sh );
        $this->_lexicon_archive_file = $filename;
        return $filename;
    }

    /**
     * Creates a filename based on the exact version of the given LexiconIndex.
     * @return string
     */
    private function _lexiconArchiveCreateSaveName()
    {
        $sha1 = sha1( serialize( clone $this->_lexicon_archive_index ) );
        $filename = $this->_lexicon_archive_key . '.' . $sha1 . self::$_lexicon_archive_file_suffix;
        return realpath( $this->_lexicon_archive_path ) . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * Restores a LexiconIndex from an archive saved on disk.
     * @param type $file
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException if the given file is not readable or does not exist
     */
    private static function &_lexiconArchiveRestoreFromFile( $file )
    {
        if ( !is_string( $file ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $file ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !is_readable( $file ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
            sprintf( 'Error encountered at [%s]. Could not resolve [%s] because the given file %s.',
                __METHOD__, $file,
                (!file_exists( $file )
                    ? 'does not exist'
                    : 'is not readable' ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $index = unserialize( file_get_contents( $file ) );
        // @codeCoverageIgnoreStart
        if ( !is_object( $index ) && ( $index instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract ) )
        {
            self::_lexiconArchiveSecurityAdvisory( $index, __METHOD__ );
        }
        // @codeCoverageIgnoreEnd
        return $index;
    }

    /**
     * Fetches and returns LexiconIndexes that have been saved to disk.
     * @param string $key The identifying slug of the LexiconIndex to restore
     * @param type $path The save path to the directory of the LexiconArchive save file
     * @param type $versioned (optional) If true, returns a collection of all versioned saves of the LexiconIndex, otherwise returns only the most recent version. Default false.
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract|\oroboros\collection\Collection If not versioned, returns the LexiconIndex directly. If versioned, returns a collection of all saved versions of the given LexiconIndex, sorted from most recent to oldest.
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException If the specified key is not found in the given path
     */
    private static function _lexiconArchiveRestoreFromKey( $key, $path,
        $versioned = false )
    {
        $key_set = self::_lexiconArchiveListByKey( $key, $path );
        if ( !$key_set )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
            sprintf( 'Error encountered at [%s]. No key of [%s] found for path [%s].',
                __METHOD__, $key, $path ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        $versions = array();
        foreach ( $key_set as
            $key =>
            $value )
        {
            $lex_index = unserialize( $value );
            //We want to use the literal last update time if it exists,
            //in case someone uses touch on the archive or some programatic
            //feature skews the file update time. If an update time is not
            //set in the actual object, we will use the file time instead.
            $lastmod = $lex_index->getLastUpdate();
            $versions[is_null( $lastmod )
                ? $key
                : $lastmod] = unserialize( $value );
        }
        krsort( $versions );
        if ( !$versioned )
        {
            return array_shift( $versions );
        }
        return new \oroboros\collection\Collection( $versions );
    }

    /**
     * Lists all saved versions of the LexiconArchive
     * @param string $key The identifying slug of the LexiconIndex to restore
     * @param type $path The save path to the directory of the LexiconArchive save file
     * @return bool|array Returns false if not found, otherwise returns an array of the saved archive versions.
     */
    private static function _lexiconArchiveListByKey( $key, $path )
    {
        $files = self::_lexiconArchiveListByDate( $key, $path );
        if ( !$files )
        {
            return false;
        }
        $set = array();
        $path = rtrim( $path, DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR;
        foreach ( $files as
            $k =>
            $file )
        {
            $set[$k] = file_get_contents( $file );
        }
        krsort( $set );
        return $set;
    }

    /**
     * Lists archives for the given key and path
     * by the date they were last accessed.
     * @param type $key
     * @param type $path
     * @return bool|array Returns false if not found or not readable, otherwise returns an array of the saved archive file names by timestamp.
     */
    private static function _lexiconArchiveListByDate( $key, $path )
    {
        if ( !is_dir( $path ) || !is_readable( $path ) )
        {
            return false;
        }
        $set = array();
        $path = rtrim( $path, DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR;
        foreach ( glob( $path . $key . '*' . self::$_lexicon_archive_file_suffix ) as
            $k =>
            $file )
        {
            $date = date( 'Y-m-d H:i:s', filemtime( $file ) );
            $set[$date] = $file;
        }
        if ( empty( $set ) )
        {
            return false;
        }
        return $set;
    }

    /**
     * This only happens if someone was tampering with the serialized data,
     * and represents a security threat. This needs to be logged, and the
     * file will be automatically purged. An exception will be raised with
     * a security advisory code.
     * @param type $file
     * @param type $method
     * @throws \oroboros\core\utilities\exception\codex\LexiconArchiveException
     * @codeCoverageIgnore
     */
    private static function _lexiconArchiveSecurityAdvisory( $file, $method )
    {
        try
        {
            unlink( $file );
        } catch ( \Exception $e )
        {
            //no-op
        }
        \oroboros\Oroboros::log( 'critical',
            'SECURITY RISK! Saved file {file} did not '
            . 'match the expected {type}. It is possible that someone has tampered with '
            . 'the cache directory maliciously. Please investigate your setup for '
            . 'possible security breaches. The file was automatically purged '
            . 'to prevent possible code injection attacks.',
            array(
            'file' => $file,
            'method' => $method,
            'type' => '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
            'trace' => debug_backtrace( 2 ),
            'time' => time()
        ) );
        throw new \oroboros\core\utilities\exception\codex\LexiconArchiveException(
        sprintf( 'Security risk encountered at [%s]. Refused to resolve [%s] because '
            . 'the given file is possibly compromised. It was automatically removed '
            . 'to prevent possible code injection attacks.', __METHOD__, $file ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY
        );
    }

}
