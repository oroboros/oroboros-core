<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Lexicon Entry Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\LexiconEntryContract
 */
trait LexiconEntryTrait
{

    use \oroboros\core\traits\utilities\core\ConstructUtilityTrait;
    use \oroboros\core\traits\core\BaselineTrait;
    /**
     * Represents the property the lexicon is meant to represent.
     * @var string
     */
    private $_lexicon_entry_property = null;

    /**
     * Represents a checksum of the file the
     * property exists in for versioning purposes.
     * @var string
     */
    private $_lexicon_entry_checksum = null;

    /**
     * Represents the parent class, trait, or interface. This will be expressed
     * as either a LexiconEntry or a Collection. If multiple inheritance
     * applies, as is the case with interfaces and traits, a collection
     * will be stored. If it is a class, a LexiconEntry will be stored.
     * @var \oroboros\codex\LexiconEntry|\oroboros\collection\Collection
     */
    private $_lexicon_entry_parent = null;

    /**
     * Represents the type of property the lexicon object is tracking.
     * @var string
     */
    private $_lexicon_entry_type = null;

    /**
     * Represents the reflector object for the property,
     * or null if no information has been queried as of
     * yet that requires one.
     *
     * This property will not be populated until a
     * request for information is made against the
     * lexicon entry.
     *
     * @var \oroboros\core\utilities\reflection\Reflection
     */
    private $_lexicon_entry_reflector = null;

    /**
     * Represents the namespace the property belongs to, if any;
     * @var string|null
     */
    private $_lexicon_entry_namespace = null;

    /**
     * Represents the package the property belongs to, if any.
     * @var string
     */
    private $_lexicon_entry_package;

    /**
     * Represents the primary Api that the object belongs to.
     * @var array
     */
    private $_lexicon_entry_primary_api = null;

    /**
     * Represents the file that the lexicon entry is declared in.
     * @var \SplFileInfo
     */
    private $_lexicon_entry_file = null;

    /**
     * Represents the constants declared either
     * directly or indirectly in the object.
     * @var \oroboros\collection\Collection
     */
    private $_lexicon_entry_constants = null;

    /**
     * Represents the properties declared either
     * directly or indirectly in the object.
     * @var \oroboros\collection\Collection
     */
    private $_lexicon_entry_properties = null;

    /**
     * Represents the methods declared either
     * directly or indirectly in the object.
     * @var \oroboros\collection\Collection
     */
    private $_lexicon_entry_methods = null;

    /**
     * Represents the interfaces inherited either
     * directly or indirectly in the object.
     * @var \oroboros\collection\Collection
     */
    private $_lexicon_entry_interfaces = null;

    /**
     * Represents the interfaces inherited either
     * directly or indirectly in the object.
     * @var \oroboros\collection\Collection
     */
    private $_lexicon_entry_traits = null;

    /**
     * Represents the Api Interface(s) that the property
     * is known to belong to, if any have been determined.
     * @var array
     */
    private $_lexicon_entry_api = array();

    /**
     * This will be true once the initialization method
     * has been called successfully.
     * @var bool
     */
    private $_lexicon_entry_initialized = false;

    /**
     * Represents the internal Lexicon instance, for self registration
     * @var \oroboros\codex\Lexicon
     */
    private static $_lexicon_entry_lexicon;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\LexiconContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Lexicon Entry Constructor>
     * Instantiates the lexicon entry.
     *
     * Will accept a string or an object.
     *
     * If the parameter is a string, it MUST be the name
     * of a valid class, interface, trait, or function that
     * has already compiled. It MUST NOT attempt to find
     * uncompiled constructs. The user is responsible for insuring that the
     * construct requested exists before instantiating a new lexicon entry.
     *
     * This method SHOULD be implemented in such a way where a reflector is
     * NOT generated until a query for information requires it. It SHOULD only
     * validate that the property provided is valid, and store the property name
     * and type for later use.
     *
     * @param string|object $property a valid class, interface, trait, or function that has been compiled prior to initialization
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided property is not valid
     */
    public function __construct( $property )
    {
        $this->_initializeLexiconEntry( $property );
    }

    /**
     * <Lexicon Entry Serialize Method>
     * This will instruct PHP to only serialize
     * parameters that are unique to the entry itself.
     * @return array
     */
    public function serialize()
    {
        return serialize( array(
            'property' => $this->_lexicon_entry_property,
            'checksum' => $this->_lexiconEntryGetChecksum(),
            'type' => $this->_lexicon_entry_type,
            'namespace' => $this->_lexicon_entry_namespace,
            'package' => $this->_lexicon_entry_package,
            'api' => $this->_lexicon_entry_api
            ) );
    }

    /**
     * <Lexicon Entry Unserialize Method>
     * Restores a serialized Lexicon Entry
     * to it's proper state.
     */
    public function unserialize( $data )
    {
        $this->_baselineGetObjectFingerprint();
        $data = unserialize( $data );
        $this->_lexicon_entry_property = $data['property'];
        $this->_lexicon_entry_type = $data['type'];
        $this->_lexicon_entry_checksum = $data['checksum'];
        $this->_lexicon_entry_namespace = $data['namespace'];
        $this->_lexicon_entry_package = $data['package'];
        $this->_lexicon_entry_api = $data['api'];
        $this->_lexicon_entry_initialized = true;
    }

    /**
     * <Vendor Getter>
     * This will return the vendor level namespace.
     * If none exists, it will return false.
     * @return string|bool
     */
    public function getVendor()
    {
        return $this->_lexiconEntryGetVendorNamespace();
    }

    /**
     * <Reflection Getter>
     * This will return a reflection object of the subject.
     * @return \oroboros\core\utilities\reflection\Reflection
     */
    public function getReflector()
    {
        return $this->_lexiconEntryGetReflector();
    }

    /**
     * <DocBlock Getter Method>
     * This will return the docblock comment of the construct,
     * by default as a collection. If you would rather have the string value,
     * pass true in as the parameter.
     *
     * @param bool $as_text
     * @return string|\oroboros\collection\interfaces\contract\CollectionContract
     */
    public function getDocBlock( $as_text = false )
    {
        return $this->_lexiconEntryGetParsedDocBlock( $as_text );
    }

    /**
     * <Api Getter>
     * This will return the declared api interface as
     * an enumerated set if one is declared.
     *
     * If one is not declared, it will check if any
     * interface exists on the object that extends
     * \oroboros\core\interfaces\api\DefaultApi
     * and return an enumerated set of that if one does exist.
     * @return \oroboros\enum\InterfaceEnumerator
     */
    public function getApi()
    {
        return $this->_lexiconEntryGetApi();
    }

    /**
     * <Lexicon Entry Method Getter>
     * Lazy loads the entry methods if they are not
     * already loaded and returns a collection of them.
     *
     * The methods returned by this will be packed within the collection
     * as an array containing an evaluation of the input and output
     * parameters, if they can be determined.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getMethods()
    {
        return $this->_lexiconEntryGetMethodUsage();
    }

    /**
     * <Lexicon Entry Constant Getter>
     * Lazy loads the entry methods if they are not
     * already loaded and returns a collection of them.
     *
     * The constants returned will be a simple key value store
     * contained within a collection.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getConstants()
    {
        return $this->_lexiconEntryGetConstants();
    }

    /**
     * <Lexicon Entry Property Getter>
     * Lazy loads the entry properties if they are not
     * already loaded and returns a collection of them.
     *
     * The properties returned will be an array wrapped in a collection,
     * designating the name as the key, and an array of details,
     * including the doc comment and type, if either are present
     * and can be determined. Keys will still exist but will be false
     * if they cannot be determined.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getProperties()
    {
        return $this->_lexiconEntryGetProperties();
    }

    /**
     * <Lexicon Entry Interfaces Getter>
     * Lazy loads the entry interfaces if they are not
     * already loaded and returns a collection of them.
     *
     * The interfaces returned will be lexicon
     * entries wrapped in a collection.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getInterfaces()
    {
        return $this->_lexiconEntryGetInterfaces();
    }

    /**
     * <Lexicon Entry Interfaces Getter>
     * Lazy loads the entry traits if they are not
     * already loaded and returns a collection of them.
     *
     * The traits returned will be lexicon
     * entries wrapped in a collection.
     *
     * @param bool $include_indirect If true, traits used by indirect inheritance will also be collected. Default false.
     * @return \oroboros\collection\Collection|null
     */
    public function getTraits( $include_indirect = false )
    {
        return $this->_lexiconEntryGetTraits( $include_indirect );
    }

    /**
     * <Lexicon Entry Interfaces Getter>
     * Lazy loads the entry parent if they are not
     * already loaded and returns a collection of them.
     *
     * The return entry will be a lexicon entry. If there is no parent,
     * the lexicon entry will always be for stdClass (the PHP base class)
     *
     * @return \oroboros\codex\LexiconEntry|null
     */
    public function getParent()
    {
        return $this->_lexiconEntryGetParent();
    }

    /**
     * <Lexicon Entry Package Getter>
     * Lazy loads the entry package if they are not
     * already loaded and returns a collection of them.
     *
     * Packages will be checked first by ApiInterface, then by oroboros.json
     * for the root namespace using psr4 standards, then for a composer.json
     * at the root namespace using psr4 standards, then via composer itself
     * it is available. If none of these options turn up a parseable package,
     * then the function will return false. This approach follows the specific
     * to the general, only returning false if all available options to
     * determine a package have been exhausted.
     *
     * The return entry will be a lexicon index. If there is no package,
     * the return value will be false
     *
     * @todo allow overloading of package detection scripts
     * @return bool|\oroboros\codex\LexiconIndex
     */
    public function getPackage()
    {
        return $this->_lexiconEntryGetPackage();
    }

    /**
     * <Lexicon File Location Getter>
     * Returns an SplFileInfo object wrapping the file name.
     * To get the fully qualified filepath, just cast this
     * to a string.
     * @return \SplFileInfo
     */
    public function getFile()
    {
        return $this->_lexiconEntryGetFile();
    }

    /**
     * <Lexicon Directory Location Getter>
     * Returns the string name of the directory containing the lexicon entry,
     * or a DirectoryIterator object representing the directory.
     * @param $iterator if true, will return a DirectoryIterator instead of the name, otherwise returns the string name of the directory. Default false.
     * @return \DirectoryIterator|string
     */
    public function getDirectory( $iterator = false )
    {
        return $this->_lexiconEntryGetDirectory( $iterator );
    }

    /**
     * <Lexicon Namespace Check Method>
     * Returns a boolean determination as to whether
     * the entry exists in a target namespace.
     * @return bool
     */
    public function inNamespace( $namespace )
    {
        return $this->_lexiconInNamespace( $namespace );
    }

    /**
     * <Lexicon Package Check Method>
     * Returns a boolean determination as to whether
     * the entry exists in a given package.
     * @return bool
     */
    public function inPackage( $package )
    {
        throw new \Exception( sprintf( 'Method [%s] is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE );
    }

    /**
     * <Lexicon Instance Check Method>
     * Checks if the given lexicon entry is an instance of the given instance,
     * or if it directly uses it if the given instance is a trait.
     * All invalid parameters will return false, as will traits that
     * are indirectly inherited.
     * @param string|object $instance
     * @return bool
     */
    public function isInstanceOf( $instance )
    {
        return $this->_lexiconEntryIsInstanceOf( $instance );
    }

    /**
     * <Lexicon Compile Check Method>
     * Checks if the lexicon entry is currently compiled or not.
     * This method will not fire autoloading or attempt to compile
     * it if it is not already compiled.
     * @return bool
     */
    public function isCompiled()
    {
        return $this->_lexiconEntryIsCompiled();
    }

    /**
     * <Lexicon Psr0 Standard Check Method>
     * Returns a boolean determination as to whether
     * the entry is a Psr0 autoload compliant construct.
     * @return bool
     */
    public function isPsr0()
    {
        throw new \Exception( sprintf( 'Method [%s] is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE );
    }

    /**
     * <Lexicon Psr4 Standard Check Method>
     * Returns a boolean determination as to whether
     * the entry is a Psr4 autoload compliant construct.
     * @return bool
     */
    public function isPsr4()
    {
        throw new \Exception( sprintf( 'Method [%s] is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE );
    }

    /**
     * <Lexicon Closure Check Method>
     * Returns a boolean determination as to whether
     * the entry is a Closure.
     * @return bool
     */
    public function isClosure()
    {
        return $this->_lexiconEntryIsClosure();
    }

    /**
     * <Lexicon Entry Callable Check Method>
     * This will check if the lexicon entry is directly callable like a function.
     * This will return true if the entry is a function, a closure,
     * or an invokable class, and will return false if it is a
     * class method, non-invokable class, interface,
     * or trait. This method can be used to designate if the
     * given property can be directly called like a function
     * without any further consideration.
     * @return bool
     */
    public function isCallable()
    {
        return $this->_lexiconEntryIsCallable();
    }

    /**
     * <Lexicon Interface Check Method>
     * Returns a boolean determination as to whether
     * the entry is an interface.
     * @return bool
     */
    public function isInterface()
    {
        return $this->_lexiconEntryIsInterface();
    }

    /**
     * <Lexicon Trait Check Method>
     * Returns a boolean determination as to whether
     * the entry is a trait.
     * @return bool
     */
    public function isTrait()
    {
        return $this->_lexiconEntryIsTrait();
    }

    /**
     * <Lexicon Interface Usage Check Method>
     * Returns a boolean determination as to whether the entry has
     * implemented a given interface either directly or indirectly.
     * Passing true as the second parameter will cause the method to
     * only return true if the interface is directly implemented by the
     * exact instance, and will return false if indirectly implemented
     * via inheritance. Otherwise the method will return true if the
     * interface exists anywhere in the chain of inheritance.
     *
     * This will always return false if an invalid parameter is given.
     *
     * @param string $interface
     * @return bool
     */
    public function hasInterface( $interface, $direct = false )
    {
        if ( !self::_classUtilityValidateNamespace( $interface ) )
        {
            return false;
        }
        return $this->_lexiconEntryHasInterface( $interface, $direct );
    }

    /**
     * <Lexicon Trait Usage Check Method>
     * Returns a boolean determination as to whether the entry has
     * used a given trait, either directly or indirectly. Passing true as
     * the second parameter will cause the method to only return true if
     * the trait is directly used by this exact instance, and will return
     * false on inherited usage. Otherwise it will return true if the trait
     * exists anywhere along the chain of inheritance.
     *
     * This will always return false if an invalid parameter is given.
     *
     * @param string $trait A valid trait name
     * @param bool $direct (optional) If provided as true, will return true only if the trait is used directly by this specific instance, and will return false on inherited usage.
     * @return bool
     */
    public function hasTrait( $trait, $direct = false )
    {
        if ( !self::_classUtilityValidateNamespace( $trait )
            && !self::_classUtilityValidateFunctionName( $trait ) )
        {
            return false;
        }
        return $this->_lexiconEntryHasTrait( $trait, $direct );
    }

    /**
     * <Lexicon Parent Usage Check Method>
     * Returns a boolean determination as to whether the entry has
     * extended a given trait, interface, or class, either directly
     * or indirectly. This method will only return true if the type of
     * the entry is the same as the type of the given instance
     * (eg: trait uses another trait, interface extends another interface,
     * class extends another class). It will return [false] if the entry
     * represents a class and an interface or trait are given, if it is a
     * trait and a non-trait is given, or if it is an interface and a
     * non-interface is given.
     *
     * If the second parameter is passed as true, the method will only return
     * true if the entry is a direct descendent AND is the same type.
     * Otherwise it will check if the entry extends the construct
     * indirectly, but only along inheritance of the same type.
     *
     * This will always return false if an invalid parameter is given.
     *
     * @param object|string $parent
     * @param bool $direct
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter is passed
     */
    public function hasParent( $parent, $direct = false )
    {
        if ( !is_object( $parent ) && !is_string( $parent ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string|object', gettype( $parent ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_lexiconEntryHasParent( $parent, $direct );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Lexicon Entry Initialization Method>
     * Initializes the lexicon entry.
     * @param type $property a valid class, interface, trait, or function that has been compiled prior to initialization
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided property is not valid
     */
    protected function _initializeLexiconEntry( $property )
    {
        if ( !is_string( $property ) && !is_object( $property ) )
        {
            //immediately bounce invalid params to avoid
            //the weight of parsing the construct set first.
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string|object', gettype( $property ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        //cast the property to a string if it is an object
        if ( is_object( $property ) )
        {
            $property = get_class( $property );
        }
        //remove prefixing namespace if it exists
        $property = ltrim( $property, '\\' );
        $type = $this->_lexiconEntryGetPropertyType( $property );
        if ( !$type )
        {
            //If it's still not present, it's not a valid lexicon construct.
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. Provided [%s] is not a valid class, '
                . 'interface, trait, or function that has loaded at this point in runtime.',
                __METHOD__, $property ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_lexicon_entry_property = $property;
        $this->_lexicon_entry_type = $type;
        $this->_lexicon_entry_namespace = $this->_lexiconEntryGetNamespace( $property );
        if ( is_null(self::$_lexicon_entry_lexicon ) )
        {
            self::$_lexicon_entry_lexicon = new \oroboros\codex\Lexicon();
        }
        $this->_baselineInitialize();
        $this->_lexicon_entry_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Determines the property type (class, interface, trait, function).
     * Returns false if none of these.
     * @param string $property
     * @return string|bool
     */
    private function _lexiconEntryGetPropertyType( $property )
    {
        return self::_classUtilityGetType( $property );
    }

    /**
     * Loads the reflector object for the given property.
     * This is not called until it is needed, and is not
     * loaded again after the first time.
     * @return void
     */
    private function _lexiconEntryLoadReflector()
    {
        if ( is_null( $this->_lexicon_entry_reflector ) )
        {
            $this->_lexicon_entry_reflector = new \oroboros\core\utilities\reflection\Reflection(
                $this->_lexicon_entry_property, null, null,
                $this->_lexicon_entry_type === 'closure'
                ? 'class'
                : $this->_lexicon_entry_type
            );
        }
    }

    /**
     * Returns the namespace of the property.
     * @param string $property
     * @return string|null
     */
    private function _lexiconEntryGetNamespace( $property )
    {
        if ( !self::_classUtilityValidateNamespace( $property ) && !self::_classUtilityValidateFunctionName( $property ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Invalid namespace provided at [%s]', __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        if ( strpos( $property, '\\' ) === false )
        {
            //global namespace
            return '\\';
        }
        $namespace = trim( substr( $property, 0,
                strpos( $property, strrchr( $property, '\\' ) ) ), '\\' );
        return ($namespace === '')
            ? null
            : $namespace;
    }

    /**
     * Gets the vendor namespace, if it exists
     * @return string|null
     */
    private function _lexiconEntryGetVendorNamespace()
    {
        if ( is_null( $this->_lexicon_entry_namespace ) )
        {
            return null;
        }
        return substr( $this->_lexicon_entry_namespace, 0,
            strpos( $this->_lexicon_entry_namespace, '\\' ) );
    }

    /**
     * Lazy loads the reflector if it is not already loaded and returns it,
     * @return \oroboros\core\utilities\reflection\Reflection
     */
    private function _lexiconEntryGetReflector()
    {
        $this->_lexiconEntryLoadReflector();
        return $this->_lexicon_entry_reflector;
    }

    /**
     * Parses the docblock comment, and returns a string or collection
     * of the result, depending on the designation of the text flag
     * passed (default is collection)
     * @param string $text
     * @return string|\oroboros\collection\Collection
     */
    private function _lexiconEntryGetParsedDocBlock( $text = false )
    {
        $this->_lexiconEntryLoadReflector();
        $docblock = $this->_lexicon_entry_reflector->getDocComment();
        if ( $text )
        {
            return $docblock;
        }
        $parser = new \oroboros\parse\DocBlock( $docblock );
        return $parser->getParsed();
    }

    /**
     * Builds the api enumerator if it has not already
     * been built and returns the resulting enumerator.
     * @return \oroboros\enum\InterfaceEnumerator
     */
    private function _lexiconEntryGetApi()
    {
        if ( $this->_lexicon_entry_type === 'function' )
        {
            return null;
        }
        if ( is_null( $this->_lexicon_entry_primary_api ) )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $api = $reflector->getConstant( 'API' );
            if ( !$api )
            {
                $api = '\\oroboros\\core\\interfaces\\api\\DefaultApi';
            }
            $this->_lexicon_entry_primary_api = new \oroboros\enum\InterfaceEnumerator( $api );
            $this->_lexicon_entry_api[$api] = &$this->_lexicon_entry_primary_api;
        }
        return $this->_lexicon_entry_primary_api;
    }

    /**
     * Lazy loads the entry methods if they are not
     * already loaded and returns a collection of them.
     * @return \oroboros\collection\Collection
     */
    private function _lexiconEntryGetMethods()
    {
        if ( is_null( $this->_lexicon_entry_methods ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $methods = $reflector->getMethods();
//            d( $methods );
        }
        return $this->_lexicon_entry_methods;
    }

    /**
     * Lazy loads the entry properties if they are not
     * already loaded and returns a collection of them.
     * @return \oroboros\collection\Collection
     */
    private function _lexiconEntryGetProperties()
    {
        if ( is_null( $this->_lexicon_entry_properties ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $properties = $reflector->getProperties();
            if ( !empty( $properties ) )
            {
                $props = array();
                foreach ( $properties as
                    $property )
                {
                    $props[$property->name] = $property;
                }
                $this->_lexicon_entry_properties = new \oroboros\collection\Collection( $props );
            }
        }
        return $this->_lexicon_entry_properties;
    }

    /**
     * Lazy loads the entry constants if they are not
     * already loaded and returns a collection of them.
     * @return \oroboros\collection\Collection
     */
    private function _lexiconEntryGetConstants()
    {
        if ( is_null( $this->_lexicon_entry_constants ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $constants = $reflector->getConstants();
            if ( $constants && !empty( $constants ) )
            {
                $this->_lexicon_entry_constants = new \oroboros\collection\Collection( $constants );
            }
        }
        return $this->_lexicon_entry_constants;
    }

    /**
     * Lazy loads the entry interfaces if they are not
     * already loaded and returns a collection of them.
     * @return \oroboros\collection\Collection
     */
    private function _lexiconEntryGetInterfaces()
    {
        if ( is_null( $this->_lexicon_entry_interfaces ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $interfaces = $reflector->getInterfaces();
            if ( $interfaces )
            {
                $interface_set = array();
                foreach ( $interfaces as
                    $interface )
                {
                    $interface_set[$interface->name] = new \oroboros\codex\LexiconEntry( $interface->name );
                }
                $this->_lexicon_entry_interfaces = new \oroboros\collection\Collection( $interface_set );
            }
        }
        return $this->_lexicon_entry_interfaces;
    }

    /**
     * Lazy loads the entry traits if they are not
     * already loaded and returns a collection of them.
     * @param bool $include_indirect If true, will collect all traits inherited by parent classes also. Default false.
     * @return \oroboros\collection\Collection
     */
    private function _lexiconEntryGetTraits( $include_indirect = false )
    {
        /**
         * @note PHP is really derpy in how it handles reflection traits.
         * If they have not compiled, it will not indirectly detect them,
         * but if they have compiled, it will indirectly detect them. This would
         * otherwise cause inconsistent results in this method, so
         * measures had to be taken to insure a consistent result.
         */
        $traits = array();
        $reflector = $this->_lexiconEntryGetReflector();
        $direct = class_uses( $reflector->getReflector()->name );
        if ( !$include_indirect && empty( $direct ) )
        {
            return null;
        }
        if ( !$include_indirect )
        {
            foreach ( $direct as
                $trait )
            {
                $traits[] = new \ReflectionClass( $trait );
            }
            return new \oroboros\collection\Collection( $traits );
        }
        if ( is_null( $this->_lexicon_entry_traits ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $traits = $reflector->getTraits();
            while ( $reflector = $reflector->getParentClass() )
            {
                $traits = array_merge( $traits, $reflector->getTraits() );
            }
            $trait_set = array();
            if ( !empty( $traits ) )
            {
                foreach ( $traits as
                    $trait )
                {
                    $trait_set[$trait->name] = new \oroboros\codex\LexiconEntry( $trait->name );
                }
                $this->_lexicon_entry_traits = new \oroboros\collection\Collection( $trait_set );
            }
        }
        return $this->_lexicon_entry_traits;
    }

    /**
     * Lazy loads the entry parent if they are not
     * already loaded and returns a collection of them.
     * @return \oroboros\codex\LexiconEntry
     */
    private function _lexiconEntryGetParent()
    {
        if ( is_null( $this->_lexicon_entry_parent ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $parent_ref = $reflector->getParentClass();
            if ( !$parent_ref )
            {
                return null;
            }
            $parent = $parent_ref->name;
            $this->_lexicon_entry_parent = new \oroboros\codex\LexiconEntry( $parent );
        }
        return $this->_lexicon_entry_parent;
    }

    /**
     * Lazy loads the entry package if they are not
     * already loaded and returns a collection of them.
     * @return \oroboros\codex\LexiconIndex
     */
    private function _lexiconEntryGetPackage()
    {
        if ( is_null( $this->_lexicon_entry_package ) )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $package = $reflector->getProperties();
//            d( $package );
        }
        return $this->_lexicon_entry_package;
    }

    /**
     * Matches the provided namespace against the existing,
     * and determines if the existing namespace is a child of
     * the supplied namespace.
     *
     * @param type $namespace
     * @return bool
     */
    private function _lexiconInNamespace( $namespace )
    {
        if ( !self::_classUtilityValidateNamespace( $namespace ) )
        {
            return false;
        }
        if ( is_null( $this->_lexicon_entry_namespace ) )
        {
            //This entry is in the global namespace,
            //so all namespaces are in it's namespace.
            return true;
        }
        return strpos( ltrim( $this->_lexicon_entry_namespace, '//' ),
                ltrim( $namespace, '//' ) ) === 0;
    }

    /**
     * Returns a SplFileInfo object wrapping the file.
     * If it has not already been created, it will be
     * lazy loaded when this method is called.
     * @return \SplFileInfo
     */
    private function _lexiconEntryGetFile()
    {
        if ( is_null( $this->_lexicon_entry_file ) )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $this->_lexicon_entry_file = new \SplFileInfo( $reflector->getFileName() );
        }
        return $this->_lexicon_entry_file;
    }

    /**
     * Returns a \Directory object wrapping
     * the parent folder of the lexicon entry.
     * @param $iterator if true, will return a DirectoryIterator, otherwise returns the string name of the directory. Default false.
     * @return \DirectoryIterator|string
     */
    private function _lexiconEntryGetDirectory( $iterator = false )
    {
        $dir = dir( dirname( (string) $this->_lexiconEntryGetFile() ) )->path;
        if ( $iterator )
        {
            return new \DirectoryIterator( $dir );
        }
        return $dir . DIRECTORY_SEPARATOR;
    }

    /**
     * Checks if the given lexicon entry is an instance of the given instance,
     * or if it directly uses it if the given instance is a trait.
     * All invalid parameters will return false, as will traits that
     * are indirectly inherited.
     * @param string|object $instance
     * @return bool
     */
    private function _lexiconEntryIsInstanceOf( $instance )
    {
        if ( $this->_lexicon_entry_type === 'function' )
        {
            return false;
        }
        try
        {
            $instance_reflector = new \ReflectionClass( $instance );
            $reflector = $this->_lexiconEntryGetReflector();
            if ( $instance_reflector->isTrait() )
            {
                return in_array( $instance_reflector->name,
                    $reflector->getTraits() );
            }
            return is_subclass_of( $reflector->getReflector()->name,
                $instance_reflector->name );
        } catch ( \ReflectionException $e )
        {
            return false;
        }
    }

    /**
     * Performs a boolean check as to whether the lexicon entry is a trait.
     * @return bool
     */
    private function _lexiconEntryIsTrait()
    {
        return self::_classUtilityGetType( $this->_lexicon_entry_property ) === 'trait';
    }

    /**
     * Checks if the lexicon entry is currently compiled or not.
     * This method will not fire autoloading or attempt to compile
     * it if it is not already compiled.
     * @return bool
     */
    private function _lexiconEntryIsCompiled()
    {
        return self::_classUtilityIsCompiled( $this->_lexicon_entry_property );
    }

    /**
     * Returns a boolean determination as to whether
     * the lexicon entry is a closure.
     * @return bool
     */
    private function _lexiconEntryIsClosure()
    {
        return self::_classUtilityGetType( $this->_lexicon_entry_property ) === 'closure';
    }

    /**
     * Performs a boolean check as to whether
     * the lexicon entry is an interface.
     * @return bool
     */
    private function _lexiconEntryIsInterface()
    {
        return self::_classUtilityGetType( $this->_lexicon_entry_property ) === 'interface';
    }

    /**
     * Checks if the lexicon entry is a function (not a class method).
     * @return bool
     */
    private function _lexiconEntryIsCallable()
    {
        $reflector = $this->_lexiconEntryGetReflector();
        return function_exists( $this->_lexicon_entry_property )
            || ( ( $reflector->getReflector()->name instanceof \ReflectionFunction ) &&
            !( $reflector->getReflector()->name instanceof \ReflectionMethod ) )
            || ( $this->_lexicon_entry_property instanceof \Closure )
            || ( $reflector->hasMethod( '__invoke' )
            );
    }

    /**
     * Performs a check for inheritance based on the parent passed,
     * to see if it is directly or indirectly implemented. If the
     * second parameter is true, it will restrict to only the
     * current property. Otherwise it will check the full
     * chain of inheritance.
     * @param type $parent
     * @param type $direct
     * @return bool
     */
    private function _lexiconEntryHasParent( $parent, $direct = false )
    {
        if ( $this->_lexicon_entry_type === 'function' )
        {
            return false;
        }
        try
        {
            $instance_reflector = new \ReflectionClass( $parent );
        } catch ( \ReflectionException $e )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                'object, or valid name of a trait, interface or class', $parent ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( $instance_reflector->isTrait() )
        {
            return $this->_lexiconEntryHasTrait( $parent, $direct );
        }
        if ( !$direct )
        {
            //Covers direct inheritance of traits,
            //and indirect inheritance of everything else.
            return $this->_lexiconEntryIsInstanceOf( $parent );
        }
        $reflector = $this->_lexiconEntryGetReflector();
        if ( !is_subclass_of( $reflector->getReflector()->name,
                $instance_reflector->name ) )
        {
            //not a subclass at all
            return false;
        }
        if ( !$reflector->getParentClass() )
        {
            //no parent, must be true
            return true;
        }
        //If the parent is also a subclass, then this is false
        return !is_subclass_of( $reflector->getParentClass()->name,
                $instance_reflector->name );
    }

    /**
     * Returns a boolean check as to whether the lexicon
     * entry has the given interface.
     * @param string $interface
     * @param bool $direct
     * @return bool
     */
    private function _lexiconEntryHasInterface( $interface, $direct = false )
    {
        if ( $this->_lexicon_entry_type === 'function' )
        {
            return false;
        }
        try
        {
            $instance_reflector = new \ReflectionClass( $interface );
            if ( !$instance_reflector->isInterface() )
            {
                return false;
            }
        } catch ( \ReflectionException $e )
        {
            return false;
        }
        $reflector = $this->_lexiconEntryGetReflector();
        if ( !$direct )
        {
            return is_subclass_of( $reflector->getReflector()->name,
                $instance_reflector->name );
        }
        return ( is_subclass_of( $reflector->getReflector()->name,
                $instance_reflector->name ) && !is_subclass_of( $reflector->getParentClass()->name,
                $instance_reflector->name ) );
    }

    /**
     * Returns a boolean check as to whether the lexicon
     * entry has the given interface.
     * @param string $interface
     * @param bool $direct
     * @return bool
     */
    private function _lexiconEntryHasTrait( $parent, $direct = false )
    {
        if ( $this->_lexicon_entry_type === 'function' )
        {
            return false;
        }
        try
        {
            $instance_reflector = new \ReflectionClass( $parent );
            if ( !$instance_reflector->isTrait() )
            {
                return false;
            }
        } catch ( \ReflectionException $e )
        {
            return false;
        }
        $reflector = $this->_lexiconEntryGetReflector();
        if ( $direct )
        {
            return in_array( $instance_reflector->name, $reflector->getTraits() );
        }
        //Covers indirect trait inheritance.
        //This is the slowest, so this is done
        //only after all other use cases.
        $reflector = $reflector->getReflector();
        while ( $reflector )
        {
            if ( !in_array( $instance_reflector->name, $reflector->getTraits() ) )
            {
                return true;
            }
            $x = $reflector->getParentClass();
        }
        return false;
    }

    private function _lexiconEntryGetMethodUsage()
    {
        if ( is_null( $this->_lexicon_entry_methods ) && $this->_lexicon_entry_type
            !== 'function' )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $methods = $reflector->getMethods();
            if ( !empty( $methods ) )
            {
                $method_array = array();
                foreach ( $methods as
                    $method )
                {
                    $comment = $method->getDocComment();
                    if ( $comment )
                    {
                        $parser = new \oroboros\parse\DocBlock( $method->getDocComment() );
                        $method_array[$method->name] = $parser->getParsed();
                    } else
                    {
                        $method_array[$method->name] = new \oroboros\collection\Collection();
                    }
                }
                $this->_lexicon_entry_methods = new \oroboros\collection\Collection( $method_array );
            }
        }
        return $this->_lexicon_entry_methods;
    }

    private function _lexiconEntryGetChecksum()
    {
        if ( is_null( $this->_lexicon_entry_checksum ) )
        {
            $reflector = $this->_lexiconEntryGetReflector();
            $this->_lexicon_entry_checksum = sha1_file( $reflector->getFileName() );
        }
        return $this->_lexicon_entry_checksum;
    }

}
