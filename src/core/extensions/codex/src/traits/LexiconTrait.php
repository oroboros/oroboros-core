<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Lexicon Trait>
 * Provides the logic for dynamically generating, searching, and retaining
 * lexical references to PHP constructs, and determining their cohesion or
 * similarity with other PHP constructs that may or may not implement a
 * common interface or descend from a common ancestor.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\LexiconContract
 */
trait LexiconTrait
{

    use \oroboros\core\traits\patterns\behavioral\ManagerTrait;

    /**
     * Represents the indexes currently in memory.
     * @var \oroboros\collection\Collection
     */
    private static $_lexicon_indexes = null;

    /**
     * Represents the known paths to lexicon archive directories
     * @var \oroboros\collection\Collection
     */
    private static $_lexicon_paths = null;

    /**
     * Represents the default save directory for new lexicon archives.
     * @var string
     */
    private static $_lexicon_path_default = null;

    /**
     * Represents the valid cache modes for the lexicon.
     * @var array
     */
    private static $_lexicon_cache_modes_valid = array(
        'file',
        'memcache',
        'memcached',
        'database',
        'service'
    );

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\LexiconContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * Adds a new directory to check for saved archives.
     * If the directory is not valid or not readable,
     * an exception will be raised.
     * @param string $name
     * @param string|\Directory $path
     * @return void
     */
    public function addPath( $name, $path )
    {
        $this->_lexiconAddPath( $name, $path );
    }

    /**
     * Saves a path into the Lexicon archive index,
     * so it can be recalled on another runtime without
     * being redeclared.
     * @param string $name
     * @return void
     */
    public function savePath( $name )
    {
        $path = $this->_lexiconGetPath( $name );
        $this->_lexiconSavePath( $name, $path );
    }

    /**
     * Loads the Lexicon archive index into memory.
     * @return void
     */
    public function loadPaths()
    {
        $this->_lexiconFetchArchiveFilePaths();
    }

    /**
     * Removes a path from memory, but retains the
     * saved index for another runtime.
     * @param string $name
     * @return void
     */
    public function prunePath( $name )
    {
        if ( $this->_lexiconCheckPath( $name ) )
        {
            unset( self::$_lexicon_paths[$name] );
        }
    }

    /**
     * Removes a path from memory,
     * and also deletes it from the
     * Lexicon archive index.
     * @param string $name
     * @return void
     */
    public function forgetPath( $name )
    {
        if ( !is_string( $name ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $name ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( $name === 'default' )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. Cannot override path [%s] at runtime.'
                . 'Update the settings on first instantiation of the Lexicon to change the default path.',
                __METHOD__, 'default' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( self::$_lexicon_paths->has( $name ) )
        {
            unset( self::$_lexicon_paths[$name] );
        }
        $this->_lexiconForgetPath( $name );
    }

    /**
     * Checks if the path exists and is known.
     * Returns true if it is known in current memory.
     * @param string $name
     * @return bool
     */
    public function checkPath( $name )
    {
        if ( !$this->_lexiconCheckPath( $name ) )
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a boolean determination as to whether a path is known and is readable.
     * Returns false if the path is not known or not readable.
     * @param string $name
     * @return bool
     */
    public function checkPathReadable( $name )
    {
        if ( !$this->_lexiconCheckPath( $name ) )
        {
            return false;
        }
        return is_readable( $this->_lexiconGetPath( $name ) );
    }

    /**
     * Returns a boolean determination as to whether a path is known and is writable.
     * Returns false if the path is not known or not writable
     * @param string $name
     * @return bool
     */
    public function checkPathWritable( $name )
    {
        if ( !$this->_lexiconCheckPath( $name ) )
        {
            return false;
        }
        return is_writable( $this->_lexiconGetPath( $name ) );
    }

    /**
     * Returns the absolute filepath to the disk
     * location of the specified path, or false if it does not exist.
     * @return array
     */
    public function getPath( $name )
    {
        if ( !$this->_lexiconCheckPath( $name ) )
        {
            return false;
        }
        return $this->_lexiconGetPath( $name );
    }

    /**
     * Returns an associative array of the paths in memory where the key is
     * the name and the value is the absolute filepath to the disk
     * location.
     * @return array
     */
    public function getPaths()
    {
        return self::$_lexicon_paths->toArray();
    }

    /**
     * Compiles the existing Lexicon indexes into serialized archives
     * that can be restored, and returns a collection of serialized indexes.
     * @return \oroboros\collection\Collection
     */
    public function compile()
    {
        $archives = array();
        $tree = $this->_lexiconGetArchiveTree();
        foreach ( $tree as
            $path_key =>
            $fileset )
        {
            foreach ( $fileset as
                $archive_file )
            {
                $lex = \oroboros\codex\LexiconArchive::loadFromFile( $archive_file );
                $archives[$path_key][$lex->getKey()][$lex->getLastUpdate()] = $lex->serialize();
            }
        }
        return new \oroboros\collection\Collection( $archives );
    }

    /**
     * Deletes all current LexiconIndexes from memory.
     * This does not delete entries that exist on disk
     * in Lexicon archive paths.
     * @return void
     */
    public function truncate()
    {
        self::$_lexicon_indexes = new \oroboros\collection\Collection();
    }

    /**
     * Erases all archived Lexicon entries from a given path,
     * and also removes their archived references on disk.
     * @param string $path A valid path index slug
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given parameter is not a string
     */
    public function purge( $name )
    {
        if ( !is_string( $name ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $name ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_lexiconPurgePath( $name );
    }

    /**
     * Checks if a specific index key exists in memory.
     * @param type $index
     * @return type
     */
    public function check( $index )
    {
        return array_key_exists( $index, self::$_lexicon_indexes );
    }

    /**
     * Returns a list of all current indexes that exist in memory.
     * @return \oroboros\collection\Collection
     */
    public function index()
    {
        return array_keys( self::$_lexicon_indexes );
    }

    /**
     * <Lexicon Index Getter Method>
     * Returns an existing LexiconIndex if it exists in memory.
     * If the second parameter is passed as true, it will also
     * check the archives on disk for a valid match.
     * @param string $lexicon_index The identifying slug of the LexiconIndex
     * @param bool $autoload (optional) If true, will autoload LexiconIndexes saved to disk but not in memory, otherwise only checks the instances in memory. Default false.
     * @return bool|\oroboros\codex\interfaces\contract\LexiconIndexContract
     */
    public function get( $lexicon_index, $autoload = false )
    {
        if ( !$this->_lexiconCheckIndex( $lexicon_index, $autoload ) )
        {
            return false;
        }
        return $this->_lexiconGetIndex( $lexicon_index, $autoload );
    }

    /**
     * Adds a new LexiconIndex into memory. This method does not
     * save the LexiconIndex to disk. You will need to call the save()
     * method to do that.
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $lexicon_index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * @throws \oroboros\core\utilities\exception\codex\LexiconException
     */
    public function add( $lexicon_index )
    {
        if ( !is_object( $lexicon_index ) && ($lexicon_index instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
                is_object( $lexicon_index )
                    ? get_class( $lexicon_index )
                    : gettype( $lexicon_index )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( array_key_exists( $lexicon_index->getKey(), self::$_lexicon_indexes ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. Provided LexiconIndex [%s] already exists.',
                __METHOD__, $lexicon_index->getKey() ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        self::$_lexicon_indexes[$lexicon_index->getKey()] = $lexicon_index;
    }

    /**
     * Validates a lexicon index infoset, which can be an
     * ApiInterface, json file, array, or collection.
     *
     * This method will make sure that all required keys
     * are present and resolve truthfully, and return a
     * boolean determination as to whether it can be used
     * to create a LexiconIndex.
     *
     * @param array|string(filepath to or direct input of: serialized, or json)|\stdClass|\oroboros\collection\interfaces\contract\CollectionContract|\oroboros\codex\interfaces\contract\LexiconArchiveContract|\oroboros\codex\interfaces\contract\LexiconIndexContract $details
     * @return bool Returns true if all required keys are present with correct values, false otherwise
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided parameter cannot resolve to a correct set of data
     */
    public function validate( $details )
    {
        $data = $this->_lexiconUnpackData( $details );
        return \oroboros\codex\LexiconIndex::validate( $data );
    }

    /**
     * Creates a package json set from a given index.
     * If the optional path parameter is supplied,
     * it will be saved to the given path instead of returned.
     * @param string $index The index to compile into json.
     * @param string $path (optional) If supplied, represents a save path for the package json as a file. If omitted, it will be returned as a string.
     * @param string $format (optional) api|package|subpackage|composer Ignored if not cast to a file. If cast to a file, determines the output format for the data, which is (api: ApiInterface, package: package json, subpackage: subpackage json, composer: composer.json). Defaults to package.
     * @return string|null Returns a string if the second parameter is omitted
     * @throws \oroboros\core\utilities\exception\RuntimeException if the save operation fails.
     */
    public function package( $index, $path = null, $format = 'package' )
    {
        if ( !is_string( $index ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $index ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !is_null( $path ) && !is_string( $path ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $path ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $valid_formats = array(
            'package',
            'subpackage',
            'api',
            'composer' );
        if ( !in_array( $format, $valid_formats ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, implode( '|', $valid_formats ),
                is_string( $format )
                    ? $format
                    : gettype( $format )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_lexiconCreatePackageData( $index, $path, $format );
    }

    /**
     * Creates a lexicon index from a index name
     * @param string $lexicon_index a json string or a file path
     * @return \oroboros\codex\LexiconIndex
     */
    public function unPackage( $lexicon_index )
    {
        if ( !is_string( $lexicon_index )
            && !is_array( $lexicon_index )
            && !(is_object( $lexicon_index )
            && ( ( $lexicon_index instanceof \stdClass )
            || ( $lexicon_index instanceof \oroboros\core\interfaces\api\BaseApi )
            || ( $lexicon_index instanceof \oroboros\collection\interfaces\contract\CollectionContract )
            ) ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                'string'
                . '|array'
                . '|\stdClass'
                . '|\\oroboros\\core\\interfaces\\api\\BaseApi'
                . '|\\oroboros\core\\interfaces\\contract\\libraries\\container\\CollectionContract',
                is_object( $lexicon_index )
                    ? get_class( $lexicon_index )
                    : gettype( $lexicon_index )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_lexiconExtractPackageData( $lexicon_index );
    }

    /**
     * Creates a new LexiconIndex from the given details,
     * named by the specified index, and retains it in memory.
     * @param string $index The slug for the new LexiconIndex to create
     * @param array|string|\oroboros\core\interfaces\libraries\container\CollectionContract $details
     * @param bool $save (optional) if true, the Lexicon will save an archive of the newly created index. Default false.
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given details cannot resolve into a lexicon index.
     */
    public function create( $index, $details, $save = false )
    {
        $data = $this->_lexiconUnpackData( $details );
        if ( !$this->validate( $details ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. Could not create new LexiconIndex [%s] because the supplied details are not valid.',
                __METHOD__, $index ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_lexiconCreateNewIndex( $index, $details, $save );
    }

    /**
     * Updates the LexiconIndex of the given index key with
     * the supplied details in memory. This does not update
     * the archive on disk.
     * @param string $index the identifying key of the LexiconArchive to affect
     * @param mixed $details The data to update the LexiconArchive with
     * @param bool $save (optional) If true, will save the archive to disk and keep it in memory. Default false.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided details are invalid
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if provided index is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconException if the specified index does not exist in memory (this method does not update stored archives)
     */
    public function update( $index, $details, $save = false )
    {
        return $this->_lexiconUpdateIndex( $index, $details, $save );
    }

    /**
     * Deletes a LexiconEntry from memory by the given index.
     * @param type $index
     * @return void
     */
    public function delete( $index )
    {
        if ( $this->_lexiconCheckIndex( $index ) )
        {
            unset( self::$_lexicon_indexes[$index] );
        }
    }

    /**
     * Performs a search query against the Lexicon, which will search
     * all LexiconEntries in memory for the given parameters by default,
     * but may be constrained to a particular index. If the second parameter
     * is passed as true, the search will also check all archived LexiconIndexes
     * that are saved on disk.
     * @param type $query The search query to perform
     * @param bool $use_archives (optional) if true, will search saved archives. If false, will only search archives in memory. Default false.
     * @return bool|\oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\codex\LexiconQueryException if a malformed query is passed
     */
    public function search( $query, $use_archives = false )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Returns a multidimensional array representing the archived
     * LexiconIndexes available in a specific path, where the
     * key is the path slug and the value is an array of LexiconIndexes
     * stored in that path.
     * @param string|array $index (optional) if provided, constrains the search to a specific path index or set of path indexes
     * @param string|array $tag (optional) if provided, constrains the search to a specific tag or set of tags
     * @return \oroboros\collection\Collection
     */
    public function tree( $index = null, $tag = null )
    {
        $tree = $this->_lexiconGetArchiveTree( $index );
        $result = array();
        foreach ( $tree as
            $pathname =>
            $archive )
        {
            foreach ( $archive as
                $archive_instance )
            {
                $lex = \oroboros\codex\LexiconArchive::loadFromFile( $archive_instance );
                if ( !is_null( $tag ) && !$lex->getTags()->has( $tag ) )
                {
                    continue;
                }
                $result[$pathname][$lex->getKey()][$lex->getLastUpdate()] = $lex;
            }
        }
        foreach ( $result as
            $key =>
            $value )
        {
            krsort( $result[$key] );
        }
        return $result;
    }

    /**
     * Fetches a list of all available LexiconIndexes in all known paths.
     * @return void
     */
    public function fetch()
    {
        $this->_lexiconFetchExistingArchiveFiles();
        return array_keys( self::$_lexicon_indexes->toArray() );
    }

    /**
     * Tags a LexiconIndex with a simple descriptor (32 chars or less).
     * Multiple LexiconIndexes can share tags, and tags can be queried for,
     * allowing the program to build custom associations between indexes.
     * @param string $index The LexiconIndex slug
     * @param string $tag The tag name to attach to it (32 chars or less, excess will be truncated)
     * @param string $value The tag value to attach to it (32 chars or less, excess will be truncated)
     * @return void
     */
    public function tag( $index, $tag, $value )
    {
        $lex = _lexiconGetIndex( $index );
        try
        {
            $lex->tag( $tag, $value );
        } catch ( \oroboros\core\utilities\exception\codex\LexiconIndexException $e )
        {
            if ( !$e->getCode() === \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_AMBIGUOUS_REFERENCE )
            {
//legit error
                throw $e;
            }
//attempt update instead
            $lex->updateTag( $tag, $value );
        }
    }

    /**
     * Saves a LexiconIndex to disk.
     * @param string $index The LexiconIndex slug
     * @param string $path (optional) if provided, represents the slug of a registered path to save the LexiconIndex under. If not provided, it will be saved under the default path.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid path key or unknown LexiconIndex is supplied
     */
    public function save( $index, $path = null )
    {
        $savedir = $this->_lexiconGetPath( $path );
        $lex = $this->_lexiconGetIndex( $index );
        $lex->save( $savedir );
    }

    /**
     * Saves all indexes in memory to disk. A specific path
     * can be designated to create an export, but the path
     * must be registered already.
     * @param string $path (optional) if provided, represents the slug of a registered path to save the LexiconIndexes under. If not provided, they will be saved under the default path.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid path key is supplied
     */
    public function saveAll( $path = null )
    {
        $savedir = $this->_lexiconGetPath( $path );
        foreach ( self::$_lexicon_indexes as
            $lex )
        {
            $lex->save( $path );
        }
    }

    /**
     * Loads a LexiconIndex from disk. If it already exists in memory,
     * the in-memory LexiconIndex will be replaced with the archived one.
     * @param string $index the identifying slug of the index to load
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given index is not a string
     */
    public function restore( $index )
    {
        if ( !is_string( $index ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $index ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_lexiconFetchExistingArchiveFiles( $index );
    }

    /**
     * Loads all known LexiconIndexes into memory.
     * It is advised to use the fetch command before doing
     * this to insure that all paths are tracked with the
     * most recent updates.
     * @return void
     */
    public function restoreAll()
    {
        $this->_lexiconFetchExistingArchiveFiles();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the directory category to the lexicon scope.
     * @return void
     */
    protected function _managerSetDirectorCategory()
    {
        $this->_setDirectorCategory( \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_CODEX_LEXICON );
    }

    /**
     * Handles the post-initialization adjustments to dependency injection
     * @param type $dependencies
     */
    protected function _managerRegisterDependencies( &$dependencies )
    {
//handle post-initialization of dependency injection here
        if ( $this->_baseline_parameters['cache_enabled'] )
        {
            $this->_lexiconSetupCaching();
        }
//initialize the baseline objects,
//if they have not already been initialized.
        $this->_lexiconInitializeBaseline();
    }

    /**
     * Fetches a given path by its slug identifier.
     * @param string $path The slug identifier for the requested path
     * @return string
     * @throws \oroboros\core\utilities\exception\codex\LexiconException If the given path is not found
     */
    protected function _lexiconGetPath( $path = null )
    {
        if ( is_null( $path ) )
        {
            return self::$_lexicon_path_default;
        }
        if ( !$this->_lexiconCheckPath( $path ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. No valid path found for [%s].',
                __METHOD__, $path ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE
            );
        }
        return self::$_lexicon_paths->get( $path );
    }

    /**
     * Sets a new path designation in the Lexicon,
     * and saves it in the path index as well.
     * @param string $index
     * @param string|\Directory $path
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed, or the given path is not a directory, readable or writable
     * @throws \oroboros\core\utilities\exception\codex\LexiconException If attempting to override the default path
     */
    protected function _lexiconSavePath( $index, $path )
    {
        $this->_lexiconValidatePath( $index, $path );
        if ( is_object( $path ) && ( $path instanceof \Directory ) )
        {
            $path = $path->path;
        }
        $this->_lexiconSaveNewPath( $index, $path );
    }

    /**
     * Sets a new path designation in the Lexicon
     * without persisting it in the path index.
     * @param string $index
     * @param string|\Directory $path
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed, or the given path is not a directory, readable or writable
     * @throws \oroboros\core\utilities\exception\codex\LexiconException If attempting to override the default path
     */
    protected function _lexiconAddPath( $index, $path )
    {
        $this->_lexiconValidatePath( $index, $path );
        if ( is_object( $path ) && ( $path instanceof \Directory ) )
        {
            $path = $path->path;
        }
        self::$_lexicon_paths[$index] = rtrim( $path, DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR;
    }

    /**
     * Checks if a given path exists
     * @param string $index the slug identifier for the path
     * @return bool
     */
    protected function _lexiconCheckPath( $index )
    {
        return self::$_lexicon_paths->has( $index );
    }

    /**
     * Returns a boolean determination as to whether a given LexiconIndex exists.
     * @param string $index the identifying slug of the LexiconIndex to check
     * @param bool $autoload (optional) if true, will check the archives and load the instance if it is not already loaded. Default false.
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given key is not a string
     */
    protected function _lexiconCheckIndex( $index, $autoload = false )
    {
        if ( !is_string( $index ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', $path ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !self::$_lexicon_indexes->has( $index ) && $autoload )
        {
            $this->_lexiconFetchExistingArchiveFiles( $index );
        }
        return self::$_lexicon_indexes->has( $index );
    }

    /**
     * Retrieves a LexiconIndex if it exists.
     * @param string $index the identifying slug of the LexiconIndex to retrieve
     * @param bool $autoload (optional) if true, will check the archives and load the instance if it is not already loaded. Default false.
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconException If the given key is not found
     */
    protected function _lexiconGetIndex( $index, $autoload = false )
    {
        if ( !$this->_lexiconCheckIndex( $index ) && $autoload )
        {
            $this->_lexiconFetchExistingArchiveFiles( $index );
        }
        if ( !$this->_lexiconCheckIndex( $index ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. No LexiconIndex by identifier [%s].',
                __METHOD__, $index ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return self::$_lexicon_indexes[$index];
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Baseline Required Dependency Setter Method>
     * May be overridden to provide a set of dependencies that
     * are required to be provided for initialization to resolve.
     *
     * Any dependencies that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetDependenciesRequired()
    {
        return array();
    }

    /**
     * <Baseline Valid Dependency Setter Method>
     * May be overridden to provide a validation array for provided dependencies.
     * If provided, any provided dependencies matching the specified keys MUST
     * resolve to the given instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided dependencies, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetDependenciesValid()
    {
        return array();
    }

    /**
     * <Baseline Default Dependency Setter Method>
     * May be overridden to provide a set of default dependencies.
     * If provided, the given set will act as a baseline set of
     * templated dependencies, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * dependencies.
     *
     * This allows for dependencies to be injected in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetDependenciesDefault()
    {
        return array();
    }

    /**
     * <Baseline Required Parameter Setter Method>
     * May be overridden to provide a set of parameters that
     * are required to be provided for initialization to resolve.
     *
     * Any parameters that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetParametersRequired()
    {
        return array(
            "cache_enabled",
            "cache_mode",
            "cache_dir",
            "retain_versions"
        );
    }

    /**
     * <Baseline Valid Parameter Setter Method>
     * May be overridden to provide a validation array for provided parameters.
     * If provided, any provided parameters matching the specified keys MUST
     * resolve to the given type or instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided parameters, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array(
            "cache_enabled" => 'boolean',
            "cache_mode" => self::$_lexicon_cache_modes_valid,
            "cache_dir" => "directory",
            "retain_versions" => 'boolean'
        );
    }

    /**
     * <Baseline Default Parameter Setter Method>
     * May be overridden to provide a set of default parameters.
     * If provided, the given set will act as a baseline set of
     * templated parameters, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * parameters.
     *
     * This allows for parameters to be provided in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return array(
            "cache_enabled" => true,
            "cache_mode" => "file",
            "cache_dir" => "codex/lexicon/",
            "retain_versions" => true
        );
    }

    /**
     * <Baseline Required Flags Setter Method>
     * May be overridden to provide a set of flags that
     * are required to be provided for initialization to resolve.
     *
     * Any flags that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetFlagsRequired()
    {
        return array();
    }

    /**
     * <Baseline Valid Flag Setter Method>
     * May be overridden to provide a validation array for provided flags.
     * If provided, any provided flags MUST exist in the given array of
     * valid values.
     *
     * The array should be a standard numerically keyed list of valid flags.
     *
     * If a non-empty validation array is given and any flags are passed
     * that do not exist in the validation array, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetFlagsValid()
    {
        return array();
    }

    /**
     * <Baseline Default Flag Setter Method>
     * May be overridden to provide a set of default flags.
     * If provided, the given set will act as a baseline set of
     * templated flags, and public provided parameters will
     * be recursively appended given array, and ignored if they
     * duplicate provided flags.
     *
     * This allows for flags to be provided in a way
     * that appends the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetFlagsDefault()
    {
        return array();
    }

    /**
     * Sets up the Lexicon cache on first initialization of the Lexicon,
     * if caching is enabled.
     * @throws \oroboros\core\utilities\exception\codex\LexiconException
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * @todo Do this dynamically, so cache extensions can be enabled. Slated for ~v.0.2.6
     */
    private function _lexiconSetupCaching()
    {
        switch ( $this->_baseline_parameters['cache_mode'] )
        {
            case 'file':
//setup file caching
                $cachable = $this->_lexiconUpdateDefaultCacheDir();
                if ( $cachable )
                {
                    $this->_lexiconFetchArchiveFilePaths();
                    $this->_lexiconFetchExistingArchiveFiles();
                }
                break;
            case 'memcached':
//setup memcached caching
//not implemented
            case 'database':
//setup database caching
//not implemented
            case 'service':
//setup remote service caching
//not implemented
                throw new \oroboros\core\utilities\exception\codex\LexiconException(
                sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
                    __METHOD__ ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
                );
                break;
            default:
                {
                    throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                    sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                        __METHOD__, 'file|memcached|database|service',
                        $this->_baseline_parameters['cache_mode'] ),
                    \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
                    );
                }
                break;
        }
    }

    /**
     * Corrects relative Lexicon Cache Paths and
     * returns whether the default path can be used.
     *
     * Lexicon caching will be disabled if it cannot be used.
     *
     * @return bool
     */
    private function _lexiconUpdateDefaultCacheDir()
    {
        if ( !$this->_baseline_parameters['cache_enabled'] )
        {
            return false;
        }
        $this->_baseline_parameters['cache_dir'] = $this->_lexiconCreateCacheDir( $this->_baseline_parameters['cache_dir'] );
        $this->_baseline_parameters['cache_enabled'] = ($this->_baseline_parameters['cache_dir']
            ? true
            : false);
        return $this->_baseline_parameters['cache_enabled'];
    }

    /**
     * Sets up the baseline Lexicon paths if they are not already configured.
     * @return void
     */
    private function _lexiconFetchArchiveFilePaths()
    {
        if ( is_null( self::$_lexicon_paths ) )
        {
//set the default path
            self::$_lexicon_path_default = rtrim( $this->_baseline_parameters['cache_dir'],
                    DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR;
//setup the baseline paths
            $file = $this->_baseline_parameters['cache_dir'] . 'paths.index.lex';
            if ( !file_exists( $file ) )
            {
                $data = serialize( array(
                    'default' => $this->_baseline_parameters['cache_dir'] ) );
                $fh = fopen( $file, 'wb' );
                fwrite( $fh, $data );
                unset( $data );
                fclose( $fh );
                if ( \oroboros\Oroboros::isInitialized() )
                {
//                    \oroboros\Oroboros::log( 'notice',
//                        'Created new Lexicon directory index file at {dirindex}',
//                        array(
//                        'dirindex' => $file ) );
                }
            }
            self::$_lexicon_paths = new \oroboros\collection\Collection( unserialize( file_get_contents( $file ) ) );
        }
//retrieve the default path if it is not already defined
        if ( is_null( self::$_lexicon_path_default ) && self::$_lexicon_paths->has( 'default' ) )
        {
            self::$_lexicon_path_default = self::$_lexicon_paths->get( 'default' );
        }
    }

    /**
     * Adds the existing LexiconIndex archives that are saved to disk
     * on first initialization of the Lexicon.
     * @param string $archive_index (optional) if provided, will restore only the given index from any path location(s) where it exists.
     * @return void
     */
    private function _lexiconFetchExistingArchiveFiles( $archive_index = null )
    {
        if ( is_null( self::$_lexicon_indexes ) )
        {
            $archives = array();
            $indexes = array();
            foreach ( self::$_lexicon_paths as
                $key =>
                $path )
            {
                if ( !is_null( $archive_index ) )
                {
                    $files = glob( $path . $archive_index . '.*.lexicon.index.lex' );
                } else
                {
                    $files = glob( $path . '*.lexicon.index.lex' );
                }
                foreach ( $files as
                    $file )
                {
                    $name = trim( substr( $file, strlen( $path ),
                            strpos( $file, '.' ) ), DIRECTORY_SEPARATOR );
                    try
                    {
                        if ( !array_key_exists( $name, $indexes ) )
                        {
                            $index = \oroboros\codex\LexiconArchive::load( $name,
                                    $path );
                            $indexes[$index->getKey()] = $index;
                            $archives[$index->getKey()] = new \oroboros\codex\LexiconArchive( $index,
                                $index->getKey(), $path );
                        }
                    } catch ( \oroboros\core\utilities\exception\codex\LexiconArchiveException $e )
                    {
//no-op
                    }
                }
            }
            self::$_lexicon_indexes = new \oroboros\collection\Collection( $indexes );
        }
    }

    /**
     * Returns a tree of archive files that exist in each known path.
     * If the path parameter is passed, this can be constrained to a specific path.
     * @param type $path (optional) if provided, the tree is constrained to only the given path.
     * @return array
     */
    private function _lexiconGetArchiveTree( $path = null )
    {
        $tree = array();
        if ( !is_null( $path ) )
        {
            if ( !self::$_lexicon_paths->has( $path ) )
            {
                return $tree;
            }
            $paths = array(
                $path => self::$_lexicon_paths->get( $path ) );
        } else
        {
            $paths = self::$_lexicon_paths->toArray();
        }
        foreach ( $paths as
            $pathname =>
            $directory )
        {
            $files = glob( $directory . '*.lexicon.index.lex' );
            $tree[$pathname] = $files;
        }
        return $tree;
    }

    /**
     * Sets up the baseline properties of the Lexicon on first
     * instantiation if they are not already set.
     * @return void
     */
    private function _lexiconInitializeBaseline()
    {
        if ( is_null( self::$_lexicon_indexes ) )
        {
            self::$_lexicon_indexes = new \oroboros\collection\Collection();
        }
        if ( is_null( self::$_lexicon_paths ) )
        {
            self::$_lexicon_paths = new \oroboros\collection\Collection();
        }
    }

    /**
     * Saves a new path and stashes it in the path index
     * @param string $index
     * @param string $path
     * @return void
     */
    private function _lexiconSaveNewPath( $index, $path )
    {
        self::$_lexicon_paths[$index] = $path;
        $path_index = self::$_lexicon_path_default . 'paths.index.lex';
        $existing = unserialize( file_get_contents( $path_index ) );
        $existing[$index] = rtrim( $path, DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR;
        $serial = serialize( $existing );
        $fh = fopen( $path_index, 'wb' );
        fwrite( $fh, $serial );
        fclose( $fh );
        if ( \oroboros\Oroboros::isInitialized() )
        {
//            \oroboros\Oroboros::log( 'notice',
//                'Saved new Lexicon path {path} at {directory}',
//                array(
//                'path' => $index,
//                'directory' => $path ) );
            self::$_lexicon_paths = new \oroboros\collection\Collection( $existing );
        }
    }

    /**
     * Removes an existing path from the path index
     * @param string $index
     * @return void
     */
    private function _lexiconForgetPath( $index )
    {
        $path_index = self::$_lexicon_path_default . 'paths.index.lex';
        $existing = unserialize( file_get_contents( $path_index ) );
        if ( array_key_exists( $index, $existing ) )
        {
            unset( $existing[$index] );
        }
        $fh = fopen( $path_index, 'wb' );
        $serial = serialize( $existing );
        fwrite( $fh, $serial );
        fclose( $fh );
        if ( \oroboros\Oroboros::isInitialized() && self::$_lexicon_paths->has( $index ) )
        {
//            \oroboros\Oroboros::log( 'notice',
//                'Removed Lexicon path {path} at location {directory}.',
//                array(
//                'path' => $index,
//                'directory' => self::$_lexicon_paths[$index],
//            ) );
        }
        if ( self::$_lexicon_paths->has( $index ) )
        {
            unset( self::$_lexicon_paths[$index] );
        }
    }

    /**
     * Validates a path to insure that it is readable, writable,
     * and the given index and path are valid.
     * @param type $index
     * @param type $path
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If index is not a string, or if the given path is invalid, not readable, or not writable.
     * @throws \oroboros\core\utilities\exception\codex\LexiconException If the given index is default (cannot overwrite default at runtime)
     */
    private function _lexiconValidatePath( $index, $path )
    {
        if ( !is_string( $index ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $index ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !is_string( $path ) && !( is_object( $path ) && ( $path instanceof \Directory ) ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string|\Directory',
                !is_object( $path )
                    ? gettype( $path )
                    : get_class( $path )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( is_object( $path ) && ( $path instanceof \Directory ) )
        {
            $path = $path->path;
        }
        if ( !is_dir( $path ) || !is_writable( $path ) || !is_readable( $path ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                sprintf( '[%s] is not ' . (!is_dir( $path )
                        ? 'a directory'
                        : (!is_writable( $path )
                            ? 'writable'
                            : 'readable' ) ), $path ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( $index === 'default' )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. Cannot override path [%s] at runtime.'
                . 'Update the settings on first instantiation of the Lexicon to change the default path.',
                __METHOD__, 'default' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Creates a directory, and also recursively creates all
     * required directories from the bottom-most valid directory
     * to the final designated directory.
     * @param string $dir
     */
    private function _lexiconMkdir( $dir )
    {
        if ( is_dir( $dir ) )
        {
            return;
        }
        $base = dirname( $dir );
        if ( !is_dir( $base ) )
        {
            while ( !is_dir( $base ) && dirname( $base ) )
            {
                $base = dirname( $base );
            }
        }
        $diff = trim( str_replace( $base, null, $dir ), DIRECTORY_SEPARATOR );
        foreach ( explode( DIRECTORY_SEPARATOR, $diff ) as
            $segment )
        {
            if ( !is_writable( $base ) )
            {
                throw new \Exception( sprintf( 'Cannot create directory [%s]',
                    $dir ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
            }
            $base = realpath( $base ) . DIRECTORY_SEPARATOR . $segment . DIRECTORY_SEPARATOR;
            mkdir( $base, 0755 );
        }
    }

    /**
     * Attempts to create a cache directory for LexiconArchives.
     * If given a relative path, it will resolve to the compiled
     * Oroboros temp directory declaration (system temp by default,
     * or local temp dir if that is not writable and readable).
     * @param string $dir A relative or absolute directory, which does not need to already exist.
     * @return bool Returns the directory name if the directory exists after the operation, and is readable and writable, otherwise returns false.
     */
    private function _lexiconCreateCacheDir( $dir )
    {
        if ( strpos( $dir, DIRECTORY_SEPARATOR ) !== 0 && !strpos( $dir, '~' ) !==
            0 )
        {
//this is a relative path
            $dir = \oroboros\Oroboros::TEMP_DIRECTORY . $dir;
        }

        if ( !is_dir( $dir ) )
        {
            try
            {
                $this->_lexiconMkdir( $dir );
                if ( \oroboros\Oroboros::isInitialized() )
                {
//                    \oroboros\Oroboros::log( 'notice',
//                        'Created new Lexicon cache directory at {cachedir}',
//                        array(
//                        'cachedir' => $dir ) );
                }
            } catch ( \Exception $e )
            {
//failed to create cache directory
                if ( !\oroboros\Oroboros::isInitialized() )
                {
                    throw new \oroboros\core\utilities\exception\RuntimeException(
                    sprintf( 'Error encountered at [%s]. Failed to create Lexicon '
                        . 'cache directory at [%s] because the parent directory [%s] '
                        . 'is not writable. Additionally could not log error normally '
                        . 'because initialization has not yet completed.',
                        __METHOD__, $dir, dirname( $dir ), $e ),
                    \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
                    );
                }
//                \oroboros\Oroboros::log( 'warning',
//                    'Unable to create new Lexicon cache directory at {cachedir}, '
//                    . 'because the parent directory {parentdir} is not writable. '
//                    . 'Lexicon Caching was disabled.',
//                    array(
//                    'cachedir' => $dir,
//                    'parentdir' => dirname( $dir ),
//                ) );
            }
            if ( !is_writable( $dir ) )
            {
                if ( \oroboros\Oroboros::isInitialized() )
                {
//                    \oroboros\Oroboros::log( 'warning',
//                        'Lexicon cache directory at {cachedir} is not writable. '
//                        . 'Lexicon Caching was disabled.',
//                        array(
//                        'cachedir' => $dir,
//                    ) );
                }
                return false;
            }
            if ( !is_readable( $dir ) )
            {
                if ( \oroboros\Oroboros::isInitialized() )
                {
//                    \oroboros\Oroboros::log( 'warning',
//                        'Lexicon cache directory at {cachedir} is not readable. '
//                        . 'Lexicon Caching was disabled.',
//                        array(
//                        'cachedir' => $dir,
//                    ) );
                }
                return false;
            }
        }
        return $dir;
    }

    /**
     * Clears all preexisting cached LexiconArchive instances
     * in a given LexiconIndex cache directory.
     * @param string $path A valid cache dir key, from memory or the cache directory index.
     * @return void
     */
    private function _lexiconPurgePath( $path )
    {
        $path_index = self::$_lexicon_path_default . 'paths.index.lex';
        $existing = unserialize( file_get_contents( $path_index ) );
        if ( array_key_exists( $path, $existing ) )
        {
            $files = glob( $existing[$path] . '*.lexicon.index.lex' );
            foreach ( $files as
                $key =>
                $file )
            {
                unlink( $file );
            }
            if ( \oroboros\Oroboros::isInitialized() )
            {
//                \oroboros\Oroboros::log( 'notice',
//                    'Deleted {count} stored LexiconArchives at at Lexicon path: {dirindex}',
//                    array(
//                    'count' => count( $files ),
//                    'dirindex' => $path ) );
            }
        }
    }

    /**
     * Unpacks provided LexiconIndex data and returns an array if it can be resolved.
     * @param array|string|\oroboros\collection\interfaces\contract\CollectionContract $data
     * @return array
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid data set is passed
     */
    private function _lexiconUnpackData( $data )
    {
        $eval_data = $data;
        if ( is_string( $eval_data ) && file_exists( $eval_data ) )
        {
            if ( !is_readable( $eval_data ) )
            {
                throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                    __METHOD__, sprintf( '%s is not readable', $eval_data ) ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
                );
            }
            $eval_data = file_get_contents( $eval_data );
        }
        $result = false;
        if ( is_array( $eval_data ) )
        {
            return $data;
        }
        if ( is_string( $eval_data ) )
        {
            if ( !interface_exists( $eval_data ) && is_subclass_of( $eval_data,
                    '\\oroboros\\core\\interfaces\\api\\BaseApi' ) )
            {
                $enum = new \oroboros\enum\InterfaceEnumerator( $eval_data );
                $result = array();
                foreach ( $enum as
                    $key =>
                    $value )
                {
                    $result[strtolower( str_replace( '_', '-',
                                str_replace( 'API_', null, $key ) ) )] = $value;
                }
                return $result;
            }
            $result = $this->_lexiconUnpackSerialData( $eval_data );
            if ( !$result )
            {
                $result = $this->_lexiconUnpackJsonData( $eval_data );
            }
            if ( !is_array( $result ) )
            {
                $eval_data = $result;
            }
        }
        if ( is_object( $eval_data ) )
        {
            if ( $eval_data instanceof \stdClass )
            {
                $result = json_decode( json_encode( $eval_data ), true );
            }
            if ( $eval_data instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract )
            {
                $result = unserialize( $eval_data->serialize() );
            }
            if ( $eval_data instanceof \oroboros\codex\interfaces\contract\LexiconArchiveContract )
            {
                $result = unserialize( \oroboros\codex\LexiconArchive::loadFromFile( unserialize( $eval_data->serialize() )['file'] )->serialize() );
            }
            if ( $eval_data instanceof \oroboros\collection\interfaces\contract\CollectionContract )
            {
                $result = $eval_data->toArray();
            }
        }
        if ( !$result )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                'Valid file path containing json or serialized data, or one of the following: json|serialized|\stdClass'
                . '|\\oroboros\\core\\interfaces\\contract\\libraries\\codex\\LexiconIndexContract'
                . '|\\oroboros\\core\\interfaces\\contract\\libraries\\codex\\LexiconArchiveContract'
                . '|\\oroboros\\core\\interfaces\\contract\\libraries\\container\\CollectionContract',
                is_object( $data )
                    ? get_class( $data )
                    : gettype( $data )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $result;
    }

    /**
     * Unpacks serialized data to populate a LexiconIndex and returns an array.
     * Will return false if the data is not valid.
     * @param string $data A string of potentially serialized data to evaluate
     * @return mixed|bool
     * @note Credit originally to Wordpress for this, as per this stackoverflow post: https://stackoverflow.com/a/4994628/1288121
     */
    private function _lexiconUnpackSerialData( $data )
    {
// if it isn't a string, it isn't serialized
        if ( !is_string( $data ) )
            return false;
        $data = trim( $data );
        if ( 'N;' == $data )
            return true;
        if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
            return false;
        switch ( $badions[1] )
        {
            case 'a' :
            case 'O' :
            case 's' :
                if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
                    return true;
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
                    return true;
                break;
        }
        return false;
    }

    /**
     * Unpacks json data to populate a LexiconIndex and returns an array.
     * Will return false if the data is not valid.
     * @param string $data A string of potentially json encoded data to evaluate
     * @return array|bool
     */
    private function _lexiconUnpackJsonData( $data )
    {
        try
        {
//The parser will catch any errors and correctly throw an exception
//on all invalid json formatting, which is not normally the case
//with PHP's native functionality.
            $parser = new \oroboros\parse\Json( $data );
            return $parser->getParsed();
        } catch ( \Exception $e )
        {
//Any error means it's not valid JSON
            return false;
        }
    }

    /**
     * Creates a new LexiconIndex and returns it, if the provided credentials are valid
     * @param string $index
     * @param mixed $details
     * @param bool $save
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided key is not a string, orthe provided details cannot resolve to a valid array of information
     * @throws \oroboros\core\utilities\exception\codex\LexiconException if the provided details do not validate
     */
    private function _lexiconCreateNewIndex( $index, $details, $save = false )
    {
        $data = array_replace_recursive(
            \oroboros\codex\LexiconIndex::getDefaultApiTags(),
            \oroboros\codex\LexiconIndex::getDefaultMetaTags(),
            $this->_lexiconUnpackData( $details )
        );
        if ( !is_string( $index ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $index ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $valid = $this->validate( $details, false, true );
        if ( $this->validate( $details ) !== true )
        {
            $msg = $this->_lexiconGetCreateFailMessage( $valid );
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( $msg, __METHOD__, $index ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        foreach ( $details['properties'] as
            $key =>
            $property )
        {
            $details['properties'][$key] = new \oroboros\codex\LexiconEntry( $property );
        }
        if ( !array_key_exists( 'source', $details ) )
        {
            $details['source'] = '\\oroboros\\core\\interfaces\\api\\DefaultApi';
        }
        $details = new \oroboros\collection\Collection( $details );
        $lex = new \oroboros\codex\LexiconIndex( $index,
            $details['properties'] );
        unset( $details['properties'] );
        if ( $details->has( 'dependencies' ) )
        {
            foreach ( $details->get( 'dependencies' ) as
                $key =>
                $dependency )
            {
                if ( $this->_lexiconCheckIndex( $dependency, true ) )
                {
                    $lex->setDependency( $this->_lexiconGetIndex( $dependency,
                            true ) );
                }
            }
            unset( $details['dependencies'] );
        }
        if ( $details->has( 'subpackages' ) )
        {
            foreach ( $details->get( 'subpackages' ) as
                $key =>
                $subpackage )
            {
                if ( $this->_lexiconCheckIndex( $subpackage, true ) )
                {
                    $lex->setSubpackage( $this->_lexiconGetIndex( $subpackage,
                            true ) );
                }
            }
            unset( $details['subpackages'] );
        }
        $lex->setApi( $details['source'] );
        unset( $details['source'] );
        $lex->setPackage( $details['package'] );
        unset( $details['package'] );
//Set any api tags supplied
        foreach ( \oroboros\codex\LexiconIndex::getValidApiTags() as
            $tagname )
        {
            if ( $details->has( $tagname ) )
            {
                $lex->apiTag( $tagname, $details[$tagname] );
                unset( $details[$tagname] );
            }
        }
//Set any meta tags supplied
        foreach ( \oroboros\codex\LexiconIndex::getValidMetaTags() as
            $tagname )
        {
            if ( $details->has( $tagname ) )
            {
                $lex->metaTag( $tagname, $details[$tagname] );
                unset( $details[$tagname] );
            }
        }
//Set remaining data as user tags
        if ( !empty( $details ) )
        {
            foreach ( $details as
                $tagname =>
                $value )
            {
                $lex->tag( $tagname, $value );
            }
        }
        if ( $save )
        {
            $this->add( $lex );
            $this->save( $lex->getKey() );
        }
        return $lex;
    }

    /**
     * Fetches the readable message for invalid creation parameters.
     * @param type $invalid
     * @return string
     */
    private function _lexiconGetCreateFailMessage( $invalid )
    {
        $msg = 'Error encountered at [%s]. Could not create new LexiconIndex [%s] because the supplied details are not valid.';
        if ( $invalid['missing'] )
        {
            $msg .= sprintf( ' The following required keys were not supplied: [%s].',
                implode( ', ', $invalid['missing'] ) );
        }
        if ( $invalid['invalid'] )
        {
            $props = false;
            if ( array_key_exists( 'properties', $invalid['invalid'] ) )
            {
                $props = $invalid['invalid']['properties'];
                unset( $invalid['invalid']['properties'] );
            }
            $msg .= sprintf( ' The following supplied keys were not valid: [%s].',
                implode( ', ', $invalid['invalid'] ) );
            if ( $props )
            {
                foreach ( $props as
                    $key =>
                    $value )
                {
                    if ( is_object( $value ) )
                    {
                        $props[$key] = get_class( $value );
                    }
                }
                $msg .= sprintf( ' The following supplied properties were not valid: [%s].',
                    implode( ', ', $props ) );
            }
        }
        return $msg;
    }

    /**
     * Updates a LexiconIndex with additional parameters.
     * @param string $index
     * @param mixed $details The details to update the LexiconIndex with
     * @param bool $save (optional) If true, will save the updated LexiconIndex to disk. Default false.
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If any invalid parameters are passed
     * @throws \oroboros\core\utilities\exception\codex\LexiconException If the given LexiconIndex does not currently exist in memory
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Allowed to bubble up if not an expected instance
     */
    private function _lexiconUpdateIndex( $index, $details, $save = false )
    {
        $data = $this->_lexiconUnpackData( $details );
        if ( !is_string( $index ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $index ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !self::$_lexicon_indexes->has( $index ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconException(
            sprintf( 'Error encountered at [%s]. No LexiconIndex by key [%s] exists in memory. Try loading from disk first.',
                __METHOD__, $index ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !\oroboros\codex\LexiconIndex::validate( $details,
                true ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. Could not update LexiconIndex [%s] because the supplied details are not valid.',
                __METHOD__, $index ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $details = new \oroboros\collection\Collection( $details );
        $lex = $this->_lexiconGetIndex( $index );
        if ( $details->has( 'source' ) )
        {
            $lex->setApi( $details['source'] );
        }
        if ( $details->has( 'package' ) )
        {
            $lex->setPackage( $details['package'] );
        }
        if ( $details->has( 'properties' ) )
        {
            foreach ( $details->get( 'properties' ) as
                $key =>
                $property )
            {
                if ( !is_object( $property ) && ( $property instanceof \oroboros\codex\interfaces\contract\LexiconEntryContract ) )
                {
                    $property = new \oroboros\codex\LexiconEntry( $property );
                }
                $lex->pushEntry( $key, $property );
            }
            $lex->setPackage( $details['package'] );
        }
        if ( $details->has( 'dependencies' ) )
        {
            foreach ( $details->get( 'dependencies' ) as
                $key =>
                $dependency )
            {
                if ( $this->_lexiconCheckIndex( $dependency, true ) )
                {
                    $lex->setDependency( $this->_lexiconGetIndex( $dependency,
                            true ) );
                }
            }
        }
        if ( $details->has( 'subpackages' ) )
        {
            foreach ( $details->get( 'subpackages' ) as
                $key =>
                $subpackage )
            {
                if ( $this->_lexiconCheckIndex( $subpackage, true ) )
                {
                    $lex->setSubpackage( $this->_lexiconGetIndex( $subpackage,
                            true ) );
                }
            }
        }
//update tags if present
        foreach ( array(
        'source',
        'scope',
        'category',
        'subcategory',
        'namespace',
        'parent-namespace',
        'map',
        'parent-package' ) as
            $tagname )
        {
            if ( $details->has( $tagname ) )
            {
                try
                {
                    $lex->tag( $tagname, $details->get( $tagname ) );
                } catch ( \oroboros\core\utilities\exception\codex\LexiconIndexException $e )
                {
                    if ( $e->getCode() === \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_AMBIGUOUS_REFERENCE )
                    {
                        throw $e;
                    }
                    $lex->updateTag( $tagname, $details->get( $tagname ) );
                }
            }
        }
        self::$_lexicon_indexes[$index] = $lex;
        if ( $save )
        {
            $this->save( $lex->getKey() );
        }
        return $lex;
    }

    /**
     * Creates a parsable interface or json set that can compile back into a LexiconIndex
     * @param mixed $index the input data to cast to a package.
     * @param type $path (optional) the disk location to save a file to if provided. If not provided, returns output as a string instead.
     * @param string $format (optional) api|package|subpackage|composer Ignored if not cast to a file. If cast to a file, determines the output format for the data, which is (api: ApiInterface, package: package json, subpackage: subpackage json, composer: composer.json)
     * @return string|void
     */
    private function _lexiconCreatePackageData( $index, $path = null,
        $format = 'package' )
    {
        $lex = $this->_lexiconGetIndex( $index, true );
        $data = $lex->export( array(
            'format' => $format ) );
        if ( !is_null( $path ) )
        {
            return $this->_lexiconPackageCastToFile( $data, $path );
        }
        return $data;
    }

    /**
     * Creates a file based on finalized package data for a LexiconIndex
     * @param type $lexicon_index
     * @throws \oroboros\core\utilities\exception\codex\LexiconException
     */
    private function _lexiconExtractPackageData( $lexicon_index )
    {
        $dataset = $this->_lexiconUnpackData( $index );
//        dd( $dataset );
        throw new \oroboros\core\utilities\exception\codex\LexiconException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Casts the given package data to a file.
     * @param string $data The finalized data to cast
     * @param string $path The destination path for the file to be placed
     * @throws \oroboros\core\utilities\exception\codex\LexiconException
     */
    private function _lexiconPackageCastToFile( $data, $path )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

}
