<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\collection\interfaces\contract;

/**
 * <Collection Contract>
 * This interface enforces the methods required collections.
 * This enables most native PHP array functionality on implementing objects,
 * and provides the same methods that are available in the
 * PHP 7.0 SPL Collection class, making this functionality
 * available as far back as PHP 5.4.
 *
 * Collections also honor the Psr-11 container specification.
 * This interface indicates that any class implementing
 * it is both an extension of the Psr-11 container
 * specification, and also a valid Oroboros library.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface CollectionContract
extends ContainerContract,
 \ArrayAccess,
 \Iterator,
 \Countable,
 \JsonSerializable
{

    /**
     * <Collection Key Filter Method>
     * This method allows you to filter keys in the collection
     * by a specified keyword. This operation will not be done
     * recursively on multidimensional arrays.
     * @param scalar $keyword Indicates the keyword to filter the keys by
     * @param bool $is_suffix (optional) if false, applies the keyword as a prefix. If true, applies it as a suffix. Default false (prefix mode)
     * @param bool $is_blacklist (optional) if false, treats the keyword as a whitelist. If true, treats it as a blacklist. Default false (whitelist mode)
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a non-scalar value is passed
     */
    public function filterKey( $keyword, $is_suffix = false,
        $is_blacklist = false );

    public function __isset( $name );

    public function __get( $name );

    public function __set( $name, $value );

    public function __unset( $name );

    public function clear();

    public function copy();

    public function count( $mode = COUNT_NORMAL );

    public function current();

    public function isEmpty();

    public function key();

    public function jsonSerialize();

    public function next();

    public function offsetExists( $offset );

    public function offsetGet( $offset );

    public function offsetSet( $offset, $value );

    public function offsetUnset( $offset );

    public function rewind();

    public function toArray();

    public function valid();
}
