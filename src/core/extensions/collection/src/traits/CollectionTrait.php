<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\collection\traits;

/**
 * <Collection Trait>
 * Represents a collection of other assets.
 * @see http://www.php-fig.org/psr/psr-11/
 * @satisfies \oroboros\collection\interfaces\contract\CollectionContract
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait CollectionTrait
{

    use ContainerTrait;

    /**
     * Whether or not the container is initialized
     * @var bool
     */
    private $_is_initialized = false;

    /**
     * The current iterator pointer index
     * @var int
     */
    private $_collection_pointer = 0;

    /**
     * The possible iterator pointer indices
     * @var array
     */
    private $_collection_pointer_keys = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the collection contract
     *
     * @satisfies \oroboros\collection\interfaces\contract\CollectionContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Collection Constructor>
     * Provides a means of setting multiple values on object instantiation.
     * @param array $params
     */
    public function __construct( array $params = null )
    {
        $this->_init( $params );
    }

    /**
     * <Isset Magic Method>
     * Provides compatibility with the PHP native isset() function.
     * @param scalar $name
     * @return bool
     */
    public function __isset( $name )
    {
        return $this->_check( $name );
    }

    /**
     * <Unset Magic Method>
     * Provides compatibility with the PHP native unset() function.
     * @param scalar $name
     * @return void
     */
    public function __unset( $name )
    {
        $this->_unsetRegistryIndex( $name );
    }

    /**
     * <Set Magic Method>
     * Provides compatibility with the PHP native setter language construct.
     * @param scalar $name
     * @param mixed $value
     * @return void
     */
    public function __set( $name, $value )
    {
        $set = $this->_set( $name, $value );
        $values = $this->_fetchRegistry();
        if ( isset( $this->_collection_pointer_keys[$this->_collection_pointer] ) )
        {
            //This approach is necessary because the prior
            //array_search method was spamming notices when the value was an object.
            $key = null;
            foreach ( $values as
                $k =>
                $v )
            {
                if ( $k === $this->_collection_pointer_keys[$this->_collection_pointer] )
                {
                    $key = $k;
                    break;
                }
            }
            $this->_collection_pointer_keys = array_keys( $values );
            $this->_collection_pointer = array_search( $key,
                $this->_collection_pointer_keys );
        } else
        {
            $this->_collection_pointer_keys = array_keys( $values );
        }
    }

    /**
     * <Get Magic Method>
     * Provides compatibility with the PHP native getter language construct.
     * @param scalar $name
     * @return mixed
     */
    public function __get( $name )
    {
        return $this->_get( $name );
    }

    /**
     * <Collection Key Filter Method>
     * This method allows you to filter keys in the collection
     * by a specified keyword. This operation will not be done
     * recursively on multidimensional arrays.
     * @param scalar $keyword Indicates the keyword to filter the keys by
     * @param bool $is_suffix (optional) if false, applies the keyword as a prefix. If true, applies it as a suffix. Default false (prefix mode)
     * @param bool $is_blacklist (optional) if false, treats the keyword as a whitelist. If true, treats it as a blacklist. Default false (whitelist mode)
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a non-scalar value is passed
     */
    public function filterKey( $keyword, $is_suffix = false,
        $is_blacklist = false )
    {
        if ( !is_scalar( $keyword ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar', gettype( $keyword ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS )
            );
        }
        if ( $is_suffix )
        {
            return $this->_collectionFilterSuffix( $keyword, $is_blacklist );
        }
        return $this->_collectionFilterPrefix( $keyword, $is_blacklist );
    }

    /**
     * <Collection Clear>
     * Resets the collection to an empty state.
     * @return void
     */
    public function clear()
    {
        $this->_resetRegistry();
    }

    /**
     * <Collection Copy>
     * Returns a new instance of the same object this trait is instantiated on.
     * @return \oroboros\collection\traits\CollectionTrait
     */
    public function copy()
    {
        $class = get_class( $this );
        $object = new $class();
        foreach ( $this->toArray() as
            $key =>
            $value )
        {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * <Collection Count>
     * Returns a count of the values in the collection.
     * The $mode parameter does nothing, due to a native PHP bug that
     * prevents recursive counting from working on instances of \Countable,
     * but it must be provided to honor the interface.
     * @param int $mode Currently does nothing due to a native PHP bug
     * @return int
     */
    public function count( $mode = COUNT_NORMAL )
    {
        if ( $mode == COUNT_NORMAL )
        {
            return $this->_countRegistryKeys();
        }
        return count( $this->_fetchRegistry(), COUNT_RECURSIVE );
    }

    /**
     * <Iterator Current>
     * Gets the current iterable index, provided to honor
     * the PHP native \Iterable interface.
     * @return mixed
     */
    public function current()
    {
        return $this->_get( $this->_collection_pointer_keys[$this->_collection_pointer] );
    }

    /**
     * <Empty Check>
     * Returns a boolean determination as to whether the collection is empty.
     * @return bool
     */
    public function isEmpty()
    {
        return $this->_countRegistryKeys() === 0;
    }

    /**
     * <Iterator Key>
     * Provided for compatibility with the PHP native \Iterator interface.
     * Returns the current key assigned to the pointer.
     * @return scalar
     */
    public function key()
    {
        return $this->_collection_pointer_keys[$this->_collection_pointer];
    }

    /**
     * <Json Serializable>
     * Provided for compatibilibty with the PHP native \JsonSerializable interface.
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * <Iterator Next>
     * Provided for compatibility with the PHP native \Iterator interface.
     * Increments the pointer, returns false if it has reached
     * the end of iterable values.
     * @return void|bool
     */
    public function next()
    {
        if ( $this->_collection_pointer >= count( $this->_collection_pointer_keys ) )
        {
            return false;
        }
        $this->_collection_pointer++;
    }

    /**
     * <Array Access Offset Check>
     * Provided for compatibility with the PHP native \ArrayAccess interface.
     * Returns a boolean determination as to whether the specified offset exists.
     * @param scalar $offset
     * @return bool
     */
    public function offsetExists( $offset )
    {
        return $this->_check( $offset );
    }

    /**
     * <Array Access Offset Getter>
     * Provided for compatibility with the PHP native \ArrayAccess interface.
     * Returns the value for the specified offset.
     * @param scalar $offset
     * @return mixed
     */
    public function offsetGet( $offset )
    {
        return $this->_get( $offset );
    }

    /**
     * <Array Access Offset Setter>
     * Provided for compatibility with the PHP native \ArrayAccess interface.
     * Sets the specified value to the specified offset.
     * @param scalar $offset
     * @param mixed $value
     */
    public function offsetSet( $offset, $value )
    {
        $this->_set( $offset, $value );
    }

    /**
     * <Array Access Unsetter>
     * Provided for compatibility with the PHP native \ArrayAccess interface.
     * Unsets the specified offset.
     * @param scalar $offset
     * @return void
     */
    public function offsetUnset( $offset )
    {
        $this->_unsetRegistryIndex( $offset );
    }

    /**
     * <Iterator Rewind>
     * Provided for compatibility with the PHP native \Iterator interface.
     * Rewinds the iterator. Called natively when loops begin.
     * @return void
     */
    public function rewind()
    {
        $this->_collection_pointer = 0;
    }

    /**
     * <Collection Array Method>
     * Returns an array of the values in the collection.
     * @return array
     */
    public function toArray()
    {
        return $this->_fetchRegistry();
    }

    /**
     * <Iterator Valid>
     * Provided for compatibility with the PHP native \Iterator interface.
     * Returns a boolean determination as to whether a specified
     * pointer key exists.
     * @return bool
     */
    public function valid()
    {
        if ( array_key_exists( $this->_collection_pointer,
                $this->_collection_pointer_keys ) )
        {
            return $this->_check( $this->_collection_pointer_keys[$this->_collection_pointer] );
        }
        return false;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Collection Initialization Method>
     * Provides an alternative initialization method if another trait
     * needs to provide a different constructor method.
     * @param array $params
     */
    protected function _initializeCollection( array $params = null )
    {
        $this->_init( $params );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Private Initialization Method>
     * This is the real initialization method.
     * It insures that core initialization logic is not overridden,
     * and backreference always points to this trait on execution.
     * @param type $params
     * @param type $flags
     */
    private function _init( array $params = null )
    {
        $this->_registryInitialize();
        if ( is_array( $params ) )
        {
            foreach ( $params as
                $key =>
                $value )
            {
                $this->__set( $key, $value );
            }
        }
        $this->_is_initialized = true;
    }

    /**
     * <Private Auto Initialization Method>
     * This method will initialize the trait only
     * if it was not already initialized.
     * @return void
     */
    private function _autoInit()
    {
        if ( !$this->_is_initialized )
        {
            $this->_init();
        }
    }

    /**
     * Creates a new collection based on the supplied prefix,
     * and whether it is a blacklist or a whitelist for the
     * existing keys.
     * @param string $prefix
     * @param bool $is_blacklist
     * @return \oroboros\collection\Collection
     */
    private function _collectionFilterPrefix( $prefix, $is_blacklist = false )
    {
        $values = array();
        foreach ( $this->toArray() as
            $key =>
            $value )
        {
            if ( $is_blacklist && ( strpos( $key, $prefix ) === false ) )
            {
                $values[$key] = $value;
            } elseif ( !$is_blacklist && ( strpos( $key, $prefix ) === 0 ) )
            {
                $values[$key] = $value;
            }
        }
        return new \oroboros\collection\Collection( $values );
    }

    /**
     * Creates a new collection based on the supplied suffix,
     * and whether it is a blacklist or a whitelist for the
     * existing keys.
     * @param string $prefix
     * @param bool $is_blacklist
     * @return \oroboros\collection\Collection
     */
    private function _collectionFilterSuffix( $suffix, $is_blacklist = false )
    {
        $values = array();
        foreach ( $this->toArray() as
            $key =>
            $value )
        {
            if ( $is_blacklist && !( substr( $key, -strlen( $suffix ) ) === $suffix ) )
            {
                $values[$key] = $value;
            } elseif ( !$is_blacklist && ( substr( $key, -strlen( $suffix ) ) ===
                $suffix ) )
            {
                $values[$key] = $value;
            }
        }
        return new \oroboros\collection\Collection( $values );
    }

}
