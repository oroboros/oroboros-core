<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\enum\interfaces\contract;

/**
 * <Enum Contract Interface>
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage config
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface EnumContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{
    /**
     * <Enumerator Constructor>
     * This will only resolve if it receives a valid
     * value as defined in the enumerated set.
     *
     * @param type $value
     * @throws \UnexpectedValueException
     */
    public function __construct( $value = null );

    /**
     * <To String Magic Method>
     * This method must cast the valid enumerated value to a string.
     * @return string
     */
    public function __toString();

    /**
     * Returns all valid enumeration keys;
     * @return array
     */
    public static function valid();

    /**
     * Returns whether a specified key is valid for this enumerable class.
     * @param string $value
     * @return bool
     */
    public static function isValid( $value );

    /**
     * Returns a list of the constant names for this class,
     * which represent all valid values.
     * The default value will not be provided.
     */
    public static function values();

    /**
     * Override this function if you need certain class constants not to
     * return as valid enumerable values, and return an array of the ones
     * to exclude.
     * This function is called internally while deciding what values are valid.
     * @return array
     */
    public static function blacklist();
}
