<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\enum\traits;

/**
 * <Container Trait>
 * Provides methods to create a class that can only be instantiated as the value of
 * an existing constant in the class. This allows for quick validation of a provided
 * value against an enumerated set of known values, without the weight of expensive
 * regular expressions or complex validation logic.
 *
 * Certain constants bay be explicitly blacklisted by constant name,
 * by overriding the function blacklist(), and providing an array of
 * the constants to exclude.
 *
 * A default value may be provided, but only within the class that directly
 * implements the trait, by overriding the property $_default. This should
 * remain private, and should not have it's visibility loosened.
 *
 * It is inappropriate to extend a class implementing this trait.
 * It provides a simple, one-off job of validation based on interfaces,
 * and should not be extended further.
 *
 * @satisfies \oroboros\enum\interfaces\contract\EnumContract
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait EnumTrait
{

    use \oroboros\core\traits\libraries\LibraryTrait;
    /**
     * Represents the value of an instantiated instance.
     * As only values that are actually defined in an interface or local constant
     * can be accepted by the constructor, this will always be either scalar or null
     * unless the methods declared here are overridden without regard to this.
     * @var scalar|null
     */
    private $_enum_value;

    /**
     * Represents specific constants that ARE NOT included in the enumerated set,
     * even if they ARE included in the class. Basic exclusion removes the default meta
     * constants used to index classes, and the __default constant, which has a special
     * use case for this logic.
     * @var array
     */
    private static $_enum_blacklist = array(
        //exclude the system interoperability constants, they will never be enumerated.
        'global' => array(
            '__default',
            'CLASS_SCOPE',
            'CLASS_TYPE',
            'API'
        )
    );

    /**
     * Represents the value used as a default if constructed with no parameters.
     * If this is not defined or is null it will be ignored.
     *
     * This should be overridden directly in the class that implements this trait.
     * Classes can override private properties of traits directly.
     *
     * There is no setter for this, because it is not intended to be overridden
     * aside from whoever directly uses this trait.
     *
     * @var scalar
     */
//    private static $_enum_default = null;

    /**
     * Represents the constant keys and values,
     * as defined in the implementing class.
     * @var array
     */
    private static $_enum_values = array();

    /**
     * If defined and not null, constants prefixed with the specified value
     * will be automatically excluded from the enumerated set.
     * @var string
     */
//    protected static $_enum_blacklist_prefix = null;

    /**
     * If defined and not null, constants suffixed with the specified value
     * will be automatically excluded from the enumerated set.
     * @var string
     */
//    protected static $_enum_blacklist_suffix = null;

    /**
     * If defined and not null, only constants prefixed with the specified value
     * will be included in the enumerated set.
     * @var string
     */
//    protected static $_enum_whitelist_prefix = null;

    /**
     * If defined and not null, only constants suffixed with the specified value
     * will be included in the enumerated set.
     * @var string
     */
//    protected static $_enum_whitelist_suffix = null;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the enum contract
     *
     * @satisfies \oroboros\enum\interfaces\contract\EnumContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Enumerator Constructor>
     * This will only resolve if it receives a valid
     * value as defined in the enumerated set.
     *
     * @param type $value
     * @throws \UnexpectedValueException
     */
    public function __construct( $value = null )
    {
        $class = get_class( $this );
        //set the default value if nothing was passed.
        if ( is_null( $value ) && property_exists( $this, '_enum_default' ) && !is_null( $class::$_enum_default ) )
        {
            $value = $class::$_enum_default;
        }
        if ( !self::isValid( $value ) )
        {
            throw new \UnexpectedValueException( sprintf( 'Value [%s] not a const in %s',
                $value, get_class( $this ) ) );
        }
        $this->_enum_value = self::_getConstName( $value );
    }

    /**
     * <To String Magic Method>
     * This method casts the valid enumerated value to a string.
     * @return string
     */
    public function __toString()
    {
        return $this->_enum_value;
    }

    /**
     * Returns all valid enumeration keys;
     * @return array
     */
    public static function valid()
    {
        self::_getConstValues();
        $vals = array();
        foreach ( self::$_enum_values as
            $key =>
            $value )
        {
            $vals[] = $value;
        }
        return $vals;
    }

    /**
     * Returns whether a specified key is valid for this enumerable class.
     * @param string $value
     * @return bool
     */
    public static function isValid( $value )
    {
        $keys = self::valid();
        return in_array( $value, $keys );
    }

    /**
     * Returns a list of the constant names for this class,
     * which represent all valid values.
     * The default value will not be provided.
     */
    public static function values()
    {
        foreach ( self::$_enum_values as
            $key =>
            $value )
        {
            $vals[] = $key;
        }
        return $vals;
    }

    /**
     * Override this function if you need certain class constants not to
     * return as valid enumerable values, and return an array of the ones
     * to exclude.
     * This function is called internally while deciding what values are valid.
     * @return array
     */
    public static function blacklist()
    {
        return array();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * This function is called internally to fetch the blacklist,
     * as defined in any given extension by overriding blacklist()
     * @return type
     */
    private static function _getBlacklist()
    {
        $class = get_called_class();
        if ( !array_key_exists( $class, self::$_enum_blacklist ) )
        {
            self::$_enum_blacklist[$class] = $class::blacklist();
        }
        $blacklist = array_merge( self::$_enum_blacklist['global'],
            self::$_enum_blacklist[$class] );
        return $blacklist;
    }

    /**
     *
     * @param type $key
     * @return type
     */
    private static function _getConstName( $key )
    {
        self::_getConstValues();
        foreach ( self::$_enum_values as
            $constant =>
            $value )
        {
            if ( $key == $value )
            {
                return $constant;
            }
        }
    }

    /**
     * This is the method that actually fetches the correct constant values.
     * This operation is only performed one time, and fires the first time any
     * reference to the enumerated set is made, but not until then.
     * @return void
     */
    private static function _getConstValues()
    {
        if ( empty( self::$_enum_values ) )
        {
            $reflector = new \ReflectionClass( get_called_class() );
            $constants = $reflector->getConstants();
            foreach ( $constants as
                $constant =>
                $value )
            {
                if (
                //denies explicitly blacklisted constants
                    in_array( $constant, self::_getBlacklist() )
                    //if a blacklist prefix is defined, denies constants that contain the blacklist prefix
                    || ($reflector->hasProperty( '_enum_blacklist_prefix' ) && !is_null( self::$_enum_blacklist_prefix ) &&
                    strpos( $constant, self::$_enum_blacklist_prefix ) === 0 )
                    //if a whitelist prefix is defined, denies constants that don't contain the whitelist prefix
                    || ($reflector->hasProperty( '_enum_whitelist_prefix' ) && !is_null( self::$_enum_whitelist_prefix ) &&
                    strpos( $constant, self::$_enum_whitelist_prefix ) !== 0 )
                    //if a blacklist suffix is defined, denies constants that contain the blacklist suffix
                    || ($reflector->hasProperty( '_enum_blacklist_suffix' ) && !is_null( self::$_enum_blacklist_suffix ) &&
                    ( substr( $constant,
                        -strlen( self::$_enum_blacklist_suffix ) ) === self::$_enum_blacklist_suffix ) )
                    //if a whitelist suffix is defined, denies constants that don't contain the whitelist suffix
                    || ($reflector->hasProperty( '_enum_whitelist_suffix' ) && !is_null( self::$_enum_whitelist_suffix ) &&
                    !( substr( $constant,
                        -strlen( self::$_enum_whitelist_suffix ) ) === self::$_enum_whitelist_suffix ) )
                )
                {
                    continue;
                }
                //all other cases are fine to add
                self::$_enum_values[$constant] = $value;
            }
        }
    }

}
