<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\enum\traits;

/**
 * <Interface Enumerator Trait>
 * Provides methods to enumerate the constants of any valid class or interface.
 * In contrast to the EnumTrait, this only produces an immutable, read-only set,
 * and does not act as a valid parameter of a set like the EnumTrait does.
 * @satisfies \oroboros\enum\interfaces\contract\InterfaceInterfaceEnumeratorContract
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait InterfaceEnumeratorTrait
{

    use \oroboros\collection\traits\CollectionTrait;

    /**
     * Represents the name of the class, trait,
     * or interface the enumerator represents.
     * @var string
     */
    private $_enum_reference = null;

    /**
     * Represents specific constants that ARE NOT included in the enumerated set,
     * even if they ARE included in the class. Basic exclusion removes the default meta
     * constants used to index classes, and the __default constant, which has a special
     * use case for this logic.
     * @var array
     */
    protected static $_enum_blacklist = array(
        //exclude the system interoperability constants, they will never be enumerated.
        'global' => array(
            '__default',
            'CLASS_SCOPE',
            'CLASS_TITLE',
            'API'
        )
    );

    /**
     * If defined, constants prefixed with the specified value
     * will be automatically excluded from the enumerated set.
     * @var string
     */
    protected static $_enum_blacklist_prefix = null;

    /**
     * If defined, constants suffixed with the specified value
     * will be automatically excluded from the enumerated set.
     * @var string
     */
    protected static $_enum_blacklist_suffix = null;

    /**
     * If defined, only constants prefixed with the specified value
     * will be included in the enumerated set.
     * @var string
     */
    protected static $_enum_whitelist_prefix = null;

    /**
     * If defined, only constants suffixed with the specified value
     * will be included in the enumerated set.
     * @var string
     */
    protected static $_enum_whitelist_suffix = null;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the enum contract
     *
     * @satisfies \oroboros\enum\interfaces\contract\EnumContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <InterfaceEnumerator Constructor>
     * This MUST recieve either an object, or a string name
     * that resolves to an class, interface, or trait.
     * All other parameters will raise an invalid argument exception.
     *
     * The class constants will be extracted from the result,
     * and populate the collection. These collections are immutable
     * after instantiation, and all attempts to modify, add to, or
     * delete from the set will raise an exception.
     *
     * This allows the set to work as a trustworthy authority for a set
     * of read-only expected values. As with all collections, they can be
     * treated as arrays in most regards, and also honor Psr-11.
     *
     * @param string|object $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    public function __construct( $interface )
    {
        $this->_getConstValues( $interface );
    }

    /**
     * <InterfaceEnumerator Set Magic Method>
     * This method MUST always raise an exception.
     * Enumerators are immutable after instantiation.
     * @param string $name
     * @param mixed $value
     * @throws \InvalidArgumentException All attempts to set raise an exception. InterfaceEnumerator instances are immutable.
     */
    public function __set( $name, $value )
    {
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( 'Error at [%s]. Enumerators are immutable once instantiated. '
            . 'Setting properties is not allowed.', __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT );
    }

    /**
     * <InterfaceEnumerator Set Magic Method>
     * This method MUST always raise an exception.
     * Enumerators are immutable after instantiation.
     * @param string $name
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException All attempts to unset raise an exception. Enumerator instances are immutable.
     */
    public function __unset( $name )
    {
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( 'Error at [%s]. Enumerators are immutable once instantiated. '
            . 'Unsetting properties is not allowed.', __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT );
    }

    /**
     * <InterfaceEnumerator Clear Method>
     * This method MUST always raise an exception.
     * Enumerators are immutable after instantiation.
     * @param string $name
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException All attempts to unset raise an exception. InterfaceEnumerator instances are immutable.
     */
    public function clear()
    {
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( 'Error at [%s]. Enumerators are immutable once instantiated. '
            . 'Clearing the enumerator is not allowed.', __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT );
    }

    /**
     * <Offset Unset Method>
     * This method MUST always raise an exception.
     * Enumerators are immutable after instantiation.
     * @param string $offset
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException All attempts to unset raise an exception. Enumerator instances are immutable.
     */
    public function offsetUnset( $offset )
    {
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( 'Error at [%s]. Enumerators are immutable once instantiated. '
            . 'Clearing the enumerator is not allowed.', __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT );
    }

    /**
     * <Offset Set Method>
     * This method MUST always raise an exception.
     * Enumerators are immutable after instantiation.
     * @param string $offset
     * @param mixed $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException All attempts to unset raise an exception. Enumerator instances are immutable.
     */
    public function offsetSet( $offset, $value )
    {
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( 'Error at [%s]. Enumerators are immutable once instantiated. '
            . 'Clearing the enumerator is not allowed.', __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT );
    }

    /**
     * <Collection Copy>
     * Returns a new instance of the same object this trait is instantiated on.
     * @return \oroboros\collection\traits\CollectionTrait
     */
    public function copy()
    {
        $class = get_class( $this );
        $object = new $class( $this->_enum_reference );
        return $object;
    }

    /**
     * Override this function if you need certain class constants not to
     * return as valid enumerable values, and return an array of the ones
     * to exclude.
     * This function is called internally while deciding what values are valid.
     * @return array
     */
    public static function blacklist()
    {
        return array();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    //none

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * This function is called internally to fetch the blacklist,
     * as defined in any given extension by overriding blacklist()
     * @return type
     */
    private static function _getBlacklist()
    {
        $class = get_called_class();
        if ( !array_key_exists( $class, self::$_enum_blacklist ) )
        {
            self::$_enum_blacklist[$class] = $class::blacklist();
        }
        $blacklist = array_merge( self::$_enum_blacklist['global'],
            self::$_enum_blacklist[$class] );
        return $blacklist;
    }

    /**
     * This is the method that actually fetches the correct constant values.
     * This operation is only performed one time, and fires the first time any
     * reference to the enumerated set is made, but not until then.
     * @return void
     */
    private function _getConstValues( $interface )
    {
        $this->_initializeCollection();
        try
        {
            $reflector = new \ReflectionClass( $interface );
        } catch ( \ReflectionException $e )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. Provided parameter must be an object, '
                . 'or a string that resolves to an object, trait, or interface. Recieved [%s]',
                __METHOD__,
                is_string( $interface )
                    ? 'string: ' . $interface
                    : gettype( $interface )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS,
            $e );
        }
        $this->_enum_reference = $interface;
        $constants = $reflector->getConstants();
        foreach ( $constants as
            $constant =>
            $value )
        {
            if (
            //denies explicitly blacklisted constants
                in_array( $constant, $this->_getBlacklist() )
                //if a blacklist prefix is defined, denies constants that contain the blacklist prefix
                || (!is_null( self::$_enum_blacklist_prefix ) && strpos( $constant,
                    self::$_enum_blacklist_prefix ) === 0 )
                //if a whitelist prefix is defined, denies constants that don't contain the whitelist prefix
                || (!is_null( self::$_enum_whitelist_prefix ) && strpos( $constant,
                    self::$_enum_whitelist_prefix ) !== 0 )
                //if a blacklist suffix is defined, denies constants that contain the blacklist suffix
                || (!is_null( self::$_enum_blacklist_suffix ) && ( substr( $constant,
                    -strlen( self::$_enum_blacklist_suffix ) ) === self::$_enum_blacklist_suffix ) )
                //if a whitelist suffix is defined, denies constants that don't contain the whitelist suffix
                || (!is_null( self::$_enum_whitelist_suffix ) && !( substr( $constant,
                    -strlen( self::$_enum_whitelist_suffix ) ) === self::$_enum_whitelist_suffix ) )
            )
            {
                continue;
            }
            //all other cases are fine to add
            $this->_set( $constant, $value );
        }
    }

}
