<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\environment\interfaces\enumerated;

/**
 * <Server Environment Enumerated Api Interface>
 *
 * This enumerated api interface defines the server runtime environment,
 * and provides information about what sort of webserver (or cgi server)
 * is running the script, so that adapters for the correct environment
 * can be invoked througout runtime.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage environment
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface ServerEnvironment
extends EnvironmentBase
{
    /**
     * Defines the current server software.
     * If shell_exec is disabled, this value will be false.
     * This may be overridden by using the
     * following prior to bootstrapping Oroboros core:
     *
     * //Valid values: [apache, iis, nginx]
     * define('OROBOROS_SERVER_SOFTWARE', 'software');
     *
     * If these values are not provided, the bootloader will
     * make a best guess attempt to configure this correctly,
     * using the defined environment analysis routine.
     */
    const SERVER = OROBOROS_SERVER_SOFTWARE;
}
