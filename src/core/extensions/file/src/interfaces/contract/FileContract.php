<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\file\interfaces\contract;

/**
 * <File Contract Interface>
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage file
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface FileContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{

    /**
     * <File Object Constructor>
     *
     * This constructor should accept three optional parameters,
     * $file, $path, and $type. All three are optional.
     *
     * The object MUST NOT automatically initialize unless $file is provided.
     *
     * The object MUST NOT be considered functionally useable unless initialized.
     *
     * The object MAY provide an internal mechanism for initialization that is protected.
     *
     * If $file is passed, it MUST represent the string name of the fully
     * qualified filepath, or a string name of a resource pointer that can
     * be directly used to create a resource to the file.
     *
     * If $type is supplied, it MUST represent either a
     * file extension or mime type that corresponds to the file name.
     * This value MUST only be used to identify the file type, and MUST NOT
     * be used to build a path to the actual file, which prevents temp files
     * that do not have extensions from accidentally not being identified on disk.
     *
     * If $type is not supplied, it SHOULD be attempted to determine based on
     * $file as an extension if it exists, and MUST default to binary if it
     * cannot be determined to maximize portability.
     *
     * If $temp_alias is supplied, the file MUST be renamed to this name if it
     * is retained or moved from the temp directory to another location and no
     * other name for it is provided. This only applies if it is actually a
     * temp file, and otherwise this parameter has no effect, even if provided.
     *
     * The object should interpret the file as a temp file if it's directory corresponds to the runtime value of:
     * \oroboros\environment\interfaces\enumerated\CoreEnvironmentInterface::TEMP_DIRECTORY,
     * which will determine the fully qualified location of the system temp directory at runtime
     * if not overridden manually. This value will be false if it is not set or is not readable and writeable.
     *
     * If any arguments cannot be determined, this method MUST throw
     * a RuntimeException that implements
     * \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * either directly or indirectly.
     *
     * @param string $file (optional)
     * @param string $type (optional)
     * @param string $temp_alias (optional)
     */
    public function __construct( $file_name = null, $type = null, $temp_alias = null );
}
