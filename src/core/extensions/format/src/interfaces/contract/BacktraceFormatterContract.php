<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\format\interfaces\contract;

/**
 * <Backtrace Formatter Contract Interface>
 * Enforces methods required for common backtrace manipulation operations.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @category formatting
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 */
interface BacktraceFormatterContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract
{

    /**
     * <Backtrace Parse Method>
     * Cleans up a backtrace array to reflect the real origin,
     * and remove huge parameter sets that bloat var dumps.
     * @param array $trace the result of a debug_backtrace or \Exception->getTrace()
     * @param int $start_index (optional) How deep in the stack the real origin point
     *     reflects. If the backtrace is generated from a method that expects
     *     to be called from external methods, it can exclude its own stack
     *     level so it does not show up in the trace. By default, no levels
     *     are removed.
     * @param bool $strip_parameters (optional) If true, the params will be
     *     removed from the trace values to prevent huge redundant data sets
     *     for simple stack trace operations. Default true. If you pass false,
     *     this will be ignored.
     * @param bool $strip_object (optional) If true, the object will be
     *     removed from the trace values to prevent huge redundant data sets
     *     for simple stack trace operations. Default true. If you pass false,
     *     this will be ignored.
     * @return array
     */
    public function parse( $trace, $start_index = 0, $strip_parameters = true,
        $strip_object = true );

    /**
     * <Backtrace String Generator Method>
     * Creates a configurable backtrace as a string.
     * This can be used to reflect a loggable entry, an insertable
     * database record, a csv output, or any other specific schema
     * of recording the stacktrace needed.
     * @param array $trace The stacktrace to evaluate
     * @param bool $lines (optional) If true, line number and file name will be
     *     included in the stacktrace. Default true.
     * @param bool $methods (optional) If true, class name and function name
     *     will be included in the stack trace. Default true.
     * @param type $prefix (optional) Designates a prefix for each stack level.
     *     The current stack depth of each trace level will be injected into
     *     this with sprintf. If you would like to use this to generate a
     *     record, this should reflect your record prefix, and the stacklevel
     *     value if required.
     * @param type $delimiter (optional) Designates a suffix for each
     *     stack level. The default is a line break. If you would like to use
     *     this to generate a record, this should reflect your record suffix.
     * @param type $divisor (optional) Designates a separator between the
     *     file/line segment and the class/method segment. If you would like
     *     to use this to generate a record and would like these two separated,
     *     this should reflect the segment between the two that identifies them
     *     as separate values.
     * @return string|false The fully compiled backtrace string.
     *     Returns false if a trace could not be generated.
     */
    public static function toString( $trace, $lines = true, $methods = true,
        $prefix = '[stacklevel-%s] ', $delimiter = PHP_EOL, $divisor = ' > ' );
}
