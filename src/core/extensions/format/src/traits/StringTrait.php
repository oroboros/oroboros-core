<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\format\traits;

/**
 * <String Formatter Trait>
 * Provides functionality to accomplish simplified means of performing common
 * string manipulation operations. This trait just exposes publicly the methods
 * from the core _stringUtilityTrait so they can be used as an external object
 * instead of implementing the trait directly in all cases where they are needed.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\format\StringFormatterContract
 */
trait StringTrait
{

    use \oroboros\core\traits\utilities\core\StringUtilityTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\utilities\format\StringFormatterContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Canonicalize String Method>
     * This will convert spaces, underscores, camelCased,
     * and BruteCased strings to hyphen separated words.
     *
     * Prefixing and suffixing hyphens will be trimmed.
     *
     * @param string $string
     */
    public static function canonicalize( $string )
    {
        return self::_stringUtilityCanonicalize( $string );
    }

    /**
     * <Camel Case Method>
     * Casts a canonicalized or standard language string to camelCase.
     * @param type $string The string to modify
     * @return string
     */
    public static function camelcase( $string )
    {
        return self::_stringUtilityCamelCase( $string );
    }

    /**
     * <Brute Case Method>
     * Casts a canonicalized or standard language string to BruteCase.
     * @param type $string The string to modify
     * @return string
     */
    public static function brutecase( $string )
    {
        return self::_stringUtilityBruteCase( $string );
    }

    /**
     * <String Left Pad Method>
     * Prefixes a string with the given delimiter by the specified amount.
     *
     * This method does not indent an entire block of text, only the beginning
     * of the string. Use the indent() method to indent an entire block of text
     * based on line breaks.
     *
     * @param string $string The subject to modify.
     * @param int $count (optional) The number of characters to pad.
     *     Default 1.
     * @param string $delimiter (optional) The delimiter to prefix with.
     *     Default is a space character.
     * @return string
     */
    public static function leftPad( $string, $count = 1, $delimiter = ' ' )
    {
        return self::_stringUtilityLeftPad( $string, $count, $delimiter );
    }

    /**
     * <String Right Pad Method>
     * Suffixes a string with the given delimiter by the specified amount.
     * @param string $string The subject to modify.
     * @param int $count (optional) The number of characters to suffix.
     *     Default 1.
     * @param string $delimiter (optional) The delimiter to suffix with.
     *     Default is a space character.
     * @return string
     */
    public static function rightPad( $string, $count = 1, $delimiter = ' ' )
    {
        return self::_stringUtilityRightPad( $string, $count, $delimiter );
    }

    /**
     * <String Wrap Method>
     * Wraps a string in the specified left and right delimiter,
     * and returns the wrapped string.
     * @param type $string The subject to modify.
     * @param string $left (optional) The left-side wrapper. Default "[".
     * @param string $right (optional) The right-side wrapper. Default "]".
     * @return string
     */
    public static function wrap( $string, $left = '[', $right = ']' )
    {
        return self::_stringUtilityWrap( $string, $left, $right );
    }

    /**
     * <Indent Lines Method>
     * Indents a block of text by splitting the lines and padding
     * the beginning of each line with the specified character.
     * @param string $string The string to indent.
     * @param int $count (optional) The number of characters to indent by.
     *     Default 1.
     * @param string $character (optional) The character to indent with.
     *     Default is a space character.
     * @return string
     */
    public static function indent( $string, $count = 1, $character = ' ' )
    {
        return self::_stringUtilityIndentLines( $string, $count, $character );
    }

    /**
     * <String Extraction Method>
     * Returns a segment of the given string between the first occurrence
     * of left and the last occurrence of right.
     *
     * This presents a much more simplified approach than
     * manual use of substring commands.
     *
     * @param type $string The subject to extract from
     * @param type $left (optional) The left side substring. If provided,
     *     content prior to the end of the first occurrence given string will
     *     be truncated from the left side.
     * @param type $right (optional) The right side substring end point. If
     *     provided, content from the beginning of the right-most occurrence
     *     of this string will be truncated.
     * @return string
     */
    public static function extract( $string, $left = null, $right = null )
    {
        return self::_stringUtilityExtract( $string, $left, $right );
    }

    /**
     * <Split Line Method>
     * Splits a string by the given delimiter and returns
     * an array of the segments.
     * @param type $string The subject to modify.
     * @param type $delimiter (optional) The delimiter to split by.
     *     Default line break.
     * @return array
     */
    public static function split( $string, $delimiter = PHP_EOL )
    {
        return self::_stringUtilitySplitLine( $string, $delimiter );
    }

    /**
     * <Exerpt Method>
     * Creates an exerpt text of the given length from a supplied string,
     * suffixed with the given suffix.
     * @param type $string The string to modify
     * @param type $max_length (optional) The maximum length of the exerpt text.
     *     This method will split at the last space character prior to the max
     *     length minus the length of the suffix, so the return value is never
     *     longer than the given length. If there are no space characters, the
     *     string will be chopped off at exactly the max length minus the length
     *     of the suffix.
     * @param string $suffix (optional) The suffix string to append at the
     *     end of the exerpt.The length of the suffix will be accounted for
     *     in the return value. Default "...".
     * @return string
     */
    public static function exerpt( $string, $max_length = 300, $suffix = '...' )
    {
        return self::_stringUtilityExerpt( $string, $max_length, $suffix );
    }

    /**
     * <String Interpolation Method>
     * Replaces markers in a string with values from a set
     * of provided stringable values. The delimiter and wrapping for the
     * value injection can be arbitrarily defined.
     *
     * The parameters received to inject may be any scalar value,
     * a resource stream, an object with the __toString method,
     * or any instance of \Psr\Http\Message\StreamInterface
     *
     * --------
     *
     * @example
     * $string = "this [inject] [inject2] [inject3].";
     * echo $method::interpolate($string, array(
     *     'inject' => 'is an example',
     *     'inject2' =>  of an interpolated',
     *     'inject3' => 'string'
     * );
     *
     * Outputs:
     * this is an example of an interpolated string.
     *
     * --------
     *
     * @param string $string The string to modify
     * @param array $context (optional) An associative array of replacements,
     *     where the key is the marker to replace, and the value is the
     *     replacement (any scalar value, null, a readable resource, or an
     *     instance of \Psr\Http\Message\StreamInterface)
     * @param string $search_left (optional) The left delimiter.
     *     Default left bracket "[".
     * @param string $search_right (optional) The right delimiter.
     *     Default right bracket "]".
     * @param string $left_wrap (optional) If provided, will prefix
     *     all values injected by this value.
     * @param string $right_wrap (optional) If provided, will suffix
     *     all values injected by this value.
     * @return string
     */
    public static function interpolate( $string, $context = array(),
        $search_left = '[', $search_right = ']', $left_wrap = null,
        $right_wrap = null )
    {
        return self::_stringUtilityInterpolate( $string, $context, $search_left,
                $search_right, $left_wrap, $right_wrap );
    }

}
