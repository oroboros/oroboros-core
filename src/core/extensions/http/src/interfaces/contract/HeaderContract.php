<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\interfaces\contract;

/**
 * <HTTP Header Utility Contract Interface>
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface HeaderContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract
{

    /**
     * <Header Constructor>
     * Constructs an object representation of a header
     * @param string $name the case insensitive header name
     * @param string|array $values Represents the value of the header. May be a string or an array. If the header can have multiple values and a string is passed, the string will be split based on the defined separator.
     * @param string $separator (optional) designates the correct separator for headers that carry multiple values. Defaults to a comma if not supplied.
     * @param string $type (optional) designates whether the specified header is a request or response header. Assumes response if not defined otherwise.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     * @link https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
     */
    public function __construct( $name, $value, $type = 'response',
        $separator = ',' );

    /**
     * Hooks into native PHP string language constructs,
     * and returns the string representation of the header value.
     * @return string
     */
    public function __toString();

    /**
     * Returns the name of the header.
     * @return string
     */
    public function getName();

    /**
     * Returns the value(s) of the header, represented as an array.
     * @return array
     */
    public function getValue();

    /**
     * Returns only the header value, contencated as a string.
     * @return string
     */
    public function getValueString();

    /**
     * Replaces the existing value with the supplied value.
     * @param string|array $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    public function setValue( $value );

    /**
     * Adds an additional value to append to the header value.
     * This method will not replace existing values.
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    public function appendValue( $value );

    /**
     * Sends the header.
     * This will replace the same header if it has already been sent.
     * @return void
     */
    public function send();
}
