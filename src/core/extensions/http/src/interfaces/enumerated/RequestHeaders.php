<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\interfaces\enumerated;

/**
 * <Request Header Api>
 * This is a list of valid request headers, established non-standard headers,
 * and their generic message body.
 * This can be used as a validator for a request consumer,
 * and also works well for quickly building correct headers with REST calls.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @since 0.2.4-alpha
 */
interface RequestHeaders
extends \oroboros\core\interfaces\enumerated\BaseEnum
{

    /**
     * -----------------------------------------------
     *             Standard Headers
     * -----------------------------------------------
     */
    const REQUEST_HEADER_ACCEPT = 'Accept';
    const REQUEST_HEADER_ACCEPT_CHARSET = 'Accept-Charset';
    const REQUEST_HEADER_ACCEPT_ENCODING = 'Accept-Encoding';
    const REQUEST_HEADER_ACCEPT_LANGUAGE = 'Accept-Language';
    const REQUEST_HEADER_ACCEPT_DATETIME = 'Accept-Datetime';
    const REQUEST_HEADER_ACCESS_CONTROL_REQUEST_METHOD = 'Access-Control-Request-Method';
    const REQUEST_HEADER_ACCESS_CONTROL_REQUEST_HEADERS = 'Access-Control-Request-Headers';
    const REQUEST_HEADER_AUTHORIZATION = 'Authorization';
    const REQUEST_HEADER_CACHE_CONTROL = 'Cache-Control';
    const REQUEST_HEADER_CONNECTION = 'Connection';
    const REQUEST_HEADER_COOKIE = 'Cookie';
    const REQUEST_HEADER_CONTENT_LENGTH = 'Content-Length';
    const REQUEST_HEADER_CONTENT_MD5 = 'Content-MD5';
    const REQUEST_HEADER_CONTENT_TYPE = 'Content-Type';
    const REQUEST_HEADER_DATE = 'Date';
    const REQUEST_HEADER_EXPECT = 'Expect';
    const REQUEST_HEADER_FORWARDED = 'Forwarded';
    const REQUEST_HEADER_FROM = 'From';
    const REQUEST_HEADER_HOST = 'Host';
    const REQUEST_HEADER_IF_MATCH = 'IF-Match';
    const REQUEST_HEADER_IF_MODIFIED_SINCE = 'If-Modified-Since';
    const REQUEST_HEADER_IF_NONE_MATCH = 'If-None-Match';
    const REQUEST_HEADER_IF_RANGE = 'If-Range';
    const REQUEST_HEADER_IF_UNMODIFIED_SINCE = 'If-Unmodified-Since';
    const REQUEST_HEADER_MAX_FORWARDS = 'Max-Forwards';
    const REQUEST_HEADER_ORIGIN = 'Origin';
    const REQUEST_HEADER_PRAGMA = 'Pragma';
    const REQUEST_HEADER_PROXY_AUTHENTICATION = 'Proxy-Authentication';
    const REQUEST_HEADER_RANGE = 'Range';
    const REQUEST_HEADER_REFERER = 'Referer';
    const REQUEST_HEADER_TE = 'TE';
    const REQUEST_HEADER_USER_AGENT = 'User-Agent';
    const REQUEST_HEADER_UPGRADE = 'Upgrade';
    const REQUEST_HEADER_VIA = 'Via';
    const REQUEST_HEADER_WARNING = 'Warning';

    /**
     * -----------------------------------------------
     *            Nonstandard Headers
     * -----------------------------------------------
     */
    const REQUEST_HEADER_X_REQUESTED_WITH = 'X-Requested-With';
    const REQUEST_HEADER_DNT = 'DNT';
    const REQUEST_HEADER_X_FORWARDED_FOR = 'X-Forwarded-For';
    const REQUEST_HEADER_X_FORWARDED_HOST = 'X-Forwarded-Host';
    const REQUEST_HEADER_X_FORWARDED_PROTO = 'X-Forwarded-Proto';
    const REQUEST_HEADER_FRONT_END_HTTPS = 'Front-End-Https';
    const REQUEST_HEADER_X_HTTP_METHOD_OVERRIDE = 'X-HTTP-Method-Override';
    const REQUEST_HEADER_X_ATT_DEVICEID = 'X-ATT-Deviceid';
    const REQUEST_HEADER_X_WAP_PROFILE = 'x-wap-profile';
    const REQUEST_HEADER_PROXY_CONNECTION = 'Proxy-Connection';
    const REQUEST_HEADER_X_UIDH = 'X-UIDH';
    const REQUEST_HEADER_X_CSRF_TOKEN = 'X-Csrf-Token';
    const REQUEST_HEADER_X_REQUEST_ID = 'X-Request-ID';
    const REQUEST_HEADER_X_CORRELATION_ID = 'X-Correlation-ID';

}
