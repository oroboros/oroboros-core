<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\interfaces\enumerated;

/**
 * <Response Headers>
 * This is a list of valid response headers, and their default message body.
 * This can be used to validate REST response headers, and to generate
 * outbound request headers that are semantically correct.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @since 0.2.4-alpha
 */
interface ResponseHeaders
extends \oroboros\core\interfaces\enumerated\BaseEnum
{

    /**
     * -----------------------------------------------
     *             Standard Headers
     * -----------------------------------------------
     */
    const RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = 'Access-Control-Allow-Origin';
    const RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = 'Access-Control-Allow-Credentials';
    const RESPONSE_HEADER_ACCESS_CONTROL_EXPOSE_HEADERS = 'Access-Control-Expose-Headers';
    const RESPONSE_HEADER_MAX_AGE = 'Access-Control-Max-Age';
    const RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_METHODS = 'Access-Control-Allow-Methods';
    const RESPONSE_HEADER_ACCESS_CONTROL_ALLOW_HEADERS = 'Access-Control-Allow-Headers';
    const RESPONSE_HEADER_ACCEPT_PATCH = 'Accept-Patch';
    const RESPONSE_HEADER_ACCEPT_RANGES = 'Accept-Ranges';
    const RESPONSE_HEADER_AGE = 'Age';
    const RESPONSE_HEADER_ALLOW = 'Allow';
    const RESPONSE_HEADER_ALT_SVC = 'Alt-Svc';
    const RESPONSE_HEADER_CACHE_CONTROL = 'Cache-Control';
    const RESPONSE_HEADER_CONNECTION = 'Connection';
    const RESPONSE_HEADER_CONTENT_DISPOSITION = 'Content-Disposition';
    const RESPONSE_HEADER_CONTENT_ENCODING = 'Content-Encoding';
    const RESPONSE_HEADER_CONTENT_LANGUAGE = 'Content-Language';
    const RESPONSE_HEADER_CONTENT_LENGTH = 'Content-Length';
    const RESPONSE_HEADER_CONTENT_LOCATION = 'Content-Location';
    const RESPONSE_HEADER_CONTENT_MD5 = 'Content-MD5';
    const RESPONSE_HEADER_CONTENT_RANGE = 'Content-Range';
    const RESPONSE_HEADER_CONTENT_TYPE = 'Content-Type';
    const RESPONSE_HEADER_DATE = 'Date';
    const RESPONSE_HEADER_ETAG = 'ETag';
    const RESPONSE_HEADER_EXPIRES = 'Expires';
    const RESPONSE_HEADER_LAST_MODIFIED = 'Last-Modified';
    const RESPONSE_HEADER_LINK = 'Link';
    const RESPONSE_HEADER_LOCATION = 'Location';
    const RESPONSE_HEADER_P3P = 'P3P';
    const RESPONSE_HEADER_PRAGMA = 'Pragma';
    const RESPONSE_HEADER_PROXY_AUTHENTICATE = 'Proxy-Authenticate';
    const RESPONSE_HEADER_PUBLIC_KEY_PINS = 'Public-Key-Pins';
    const RESPONSE_HEADER_RETRY_AFTER = 'Retry-After';
    const RESPONSE_HEADER_SERVER = 'Server';
    const RESPONSE_HEADER_SET_COOKIE = 'Set-Cookie';
    const RESPONSE_HEADER_STRICT_TRANSPORT_SECURITY = 'Strict-Transport-Security';
    const RESPONSE_HEADER_TRAILER = 'Trailer';
    const RESPONSE_HEADER_TRANSFER_ENCODING = 'Transfer-Encoding';
    const RESPONSE_HEADER_TK = 'Tk';
    const RESPONSE_HEADER_UPGRADE = 'Upgrade';
    const RESPONSE_HEADER_VARY = 'Vary';
    const RESPONSE_HEADER_VIA = 'Via';
    const RESPONSE_HEADER_WARNING = 'Header-Warning';
    const RESPONSE_HEADER_WWW_AUTHENTICATE = 'WWW-Authenticate';
    const RESPONSE_HEADER_X_FRAME_OPTIONS = 'X-Frame-Options';

    /**
     * -----------------------------------------------
     *            Nonstandard Headers
     * -----------------------------------------------
     */
    const RESPONSE_HEADER_CONTENT_SECURITY_POLICY = 'Content-Security-Policy';
    const RESPONSE_HEADER_X_CONTENT_SECURITY_POLICY = 'X-Content-Security-Policy';
    const RESPONSE_HEADER_X_WEBKIT_CSP = 'X-WebKit-CSP';
    const RESPONSE_HEADER_REFRESH = 'Refresh';
    const RESPONSE_HEADER_STATUS = 'Status';
    const RESPONSE_HEADER_UPGRADE_INSECURE_REQUESTS = 'Upgrade-Insecure-Requests';
    const RESPONSE_HEADER_X_CONTENT_DURATION = 'X-Content-Duration';
    const RESPONSE_HEADER_X_CONTENT_TYPE_OPTIONS = 'X-Content-Type-Options';
    const RESPONSE_HEADER_X_POWERED_BY = 'X-Powered-By';
    const RESPONSE_HEADER_X_REQUEST_ID = 'X-Request-ID';
    const RESPONSE_HEADER_X_CORRELATION_ID = 'X-Correlation-Id';
    const RESPONSE_HEADER_X_UA_COMPATIBLE = 'X-UA-Compatible';
    const RESPONSE_HEADER_X_XSS_PROTECTION = 'X-XSS-Protection';

}
