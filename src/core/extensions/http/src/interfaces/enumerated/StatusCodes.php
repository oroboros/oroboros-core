<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\interfaces\enumerated;

/**
 * <Oroboros HTTP Status Code Interface>
 * This file provides consistent definitions for HTTP status codes.
 * This file exposes  HTTP status codes to your class as class constants.
 * This file will be updated periodically with future releases to adhere
 *  to the current W3C specifications.
 * @note <INCOMPLETE LIST. Finish before building REST engine!>
 */
interface StatusCodes extends \oroboros\core\interfaces\enumerated\BaseEnum
{
    /**
     * ---------------- 1xx Informational -----------------
     * This class of status code indicates a provisional response,
     * consisting only of the Status-Line and optional headers, and
     * is terminated by an empty line. There are no required headers
     * for this class of status code. Since HTTP/1.0 did not define
     * any 1xx status codes, servers MUST NOT send a 1xx response to
     * an HTTP/1.0 client except under experimental conditions.
     *
     * A client MUST be prepared to accept one or more 1xx status responses
     * prior to a regular response, even if the client does not expect
     * a 100 (Continue) status message. Unexpected 1xx status responses
     * MAY be ignored by a user agent.
     *
     * Proxies MUST forward 1xx responses, unless the connection between
     * the proxy and its client has been closed, or unless the proxy
     * itself requested the generation of the 1xx response. (For example,
     * if a proxy adds a "Expect: 100-continue" field when it forwards a
     * request, then it need not forward the corresponding 100 (Continue)
     * response(s).)
     * ----------------------------------------------------
     */

    /**
     * 100 Continue
     */
    const STATUS_100 = 100;
    const STATUS_100_TITLE = 'Continue';
    const HTTP_CONTINUE = self::STATUS_100;

    /**
     * 101 Switching Protocols
     */
    const STATUS_101 = 101;
    const STATUS_101_TITLE = 'Switching Protocols';
    const HTTP_SWITCHING_PROTOCOLS = self::STATUS_101;

    /**
     * 102 Processing (WebDAV)
     */
    const STATUS_102 = 102;
    const STATUS_102_TITLE = 'Processing';
    const HTTP_PROCESSING = self::STATUS_102;

    /**
     * -------------------- 2xx Success -------------------
     * This class of status code indicates that further action needs
     * to be taken by the user agent in order to fulfill the request.
     * The action required MAY be carried out by the user agent without
     * interaction with the user if and only if the method used in the
     * second request is GET or HEAD. A client SHOULD detect infinite
     * redirection loops, since such loops generate network traffic for
     * each redirection.
     *
     * Note: previous versions of this specification recommended a
     * maximum of five redirections. Content developers should be aware
     * that there might be clients that implement such a fixed
     * limitation.
     * ----------------------------------------------------
     */

    /**
     * 200 OK
     */
    const STATUS_200 = 200;
    const STATUS_200_TITLE = 'OK';
    const HTTP_OK = self::STATUS_200;

    /**
     * 201 Created
     */
    const STATUS_201 = 201;
    const STATUS_201_TITLE = 'Created';
    const HTTP_CREATED = self::STATUS_201;

    /**
     * 202 Accepted
     */
    const STATUS_202 = 202;
    const STATUS_202_TITLE = 'Accepted';
    const HTTP_ACCEPTED = self::STATUS_202;

    /**
     * 203 Non-Authoritative Information
     */
    const STATUS_203 = 203;
    const STATUS_203_TITLE = 'Non-Authoritative Information';
    const HTTP_NON_AUTHORITATIVE = self::STATUS_203;

    /**
     * 204 No Content
     */
    const STATUS_204 = 204;
    const STATUS_204_TITLE = 'No Content';
    const HTTP_NO_CONTENT = self::STATUS_204;

    /**
     * 205 Reset Content
     */
    const STATUS_205 = 205;
    const STATUS_205_TITLE = 'Reset Content';
    const HTTP_RESET_CONTENT = self::STATUS_205;

    /**
     * 206 Partial Content
     */
    const STATUS_206 = 206;
    const STATUS_206_TITLE = 'Partial Content';
    const HTTP_PARTIAL_CONTENT = self::STATUS_206;

    /**
     * 207 Multi-Status
     */
    const STATUS_207 = 207;
    const STATUS_207_TITLE = 'Multi-Status';
    const HTTP_MULTI_STATUS = self::STATUS_207;

    /**
     * 208 Multi-Status
     */
    const STATUS_208 = 208;
    const STATUS_208_TITLE = 'Already Reported';
    const HTTP_ALREADY_REPORTED = self::STATUS_208;

    /**
     * 226 IM Used
     */
    const STATUS_226 = 208;
    const STATUS_226_TITLE = 'IM Used';
    const HTTP_IM_USED = self::STATUS_226;

    /**
     * ----------------- 3xx Redirection ------------------
     * This class of status code indicates that further action needs
     * to be taken by the user agent in order to fulfill the request.
     * The action required MAY be carried out by the user agent without
     * interaction with the user if and only if the method used in the
     * second request is GET or HEAD. A client SHOULD detect infinite
     * redirection loops, since such loops generate network traffic for
     * each redirection.
     *
     * Note: previous versions of this specification recommended a
     * maximum of five redirections. Content developers should be aware
     * that there might be clients that implement such a fixed
     * limitation.
     * ----------------------------------------------------
     */

    /**
     * 300 Multiple Choices
     */
    const STATUS_300 = 300;
    const STATUS_300_TITLE = 'Multiple Choices';
    const HTTP_MULTIPLE_CHOICES = self::STATUS_300;

    /**
     * 301 Moved Permanently
     */
    const STATUS_301 = 301;
    const STATUS_301_TITLE = 'Moved Permanently';
    const HTTP_MOVED_PERMANENTLY = self::STATUS_301;

    /**
     * 302 Found
     */
    const STATUS_302 = 305;
    const STATUS_302_TITLE = 'Found';
    const HTTP_FOUND = self::STATUS_302;

    /**
     * 303 See Other
     */
    const STATUS_303 = 303;
    const STATUS_303_TITLE = 'See Other';
    const HTTP_SEE_OTHER = self::STATUS_303;

    /**
     * 304 Not Modified
     */
    const STATUS_304 = 304;
    const STATUS_304_TITLE = 'Not Modified';
    const HTTP_NOT_MODIFIED = self::STATUS_304;

    /**
     * 305 Use Proxy
     */
    const STATUS_305 = 305;
    const STATUS_305_TITLE = 'Use Proxy';
    const HTTP_USE_PROXY = self::STATUS_305;

    /**
     * 306 Unused
     */
    const STATUS_306 = 306;
    const STATUS_306_TITLE = 'Unused';
    const HTTP_UNUSED = self::STATUS_306;

    /**
     * 307 Temporary Redirect
     */
    const STATUS_307 = 307;
    const STATUS_307_TITLE = 'Temporary Redirect';
    const HTTP_TEMPORARY_REDIRECT = self::STATUS_307;

    /**
     * 308 Permanent Redirect (experimental)
     */
    const STATUS_308 = 308;
    const STATUS_308_TITLE = 'Permanent Redirect';
    const HTTP_PERMANENT_REDIRECT_EXPERIMENTAL = self::STATUS_308;

    /**
     * -------------- 4xx Client Errors --------------
     * The 4xx class of status code is intended for cases in which
     * the client seems to have erred. Except when responding to a
     * HEAD request, the server SHOULD include an entity containing
     * an explanation of the error situation, and whether it is a
     * temporary or permanent condition. These status codes are
     * applicable to any request method. User agents SHOULD display
     * any included entity to the user.
     *
     * If the client is sending data, a server implementation using
     * TCP SHOULD be careful to ensure that the client acknowledges
     * receipt of the packet(s) containing the response, before the
     * server closes the input connection. If the client continues
     * sending data to the server after the close, the server's TCP
     * stack will send a reset packet to the client, which may erase
     * the client's unacknowledged input buffers before they can be
     * read and interpreted by the HTTP application.
     * -----------------------------------------------
     */

    /**
     * 400 Bad Request
     */
    const STATUS_400 = 400;
    const STATUS_400_TITLE = 'Bad Request';
    const HTTP_BAD_REQUEST = self::STATUS_400;

    /**
     * 401 Unauthorized
     */
    const STATUS_401 = 401;
    const STATUS_401_TITLE = 'Unauthorized';
    const HTTP_UNAUTHORIZED = self::STATUS_401;

    /**
     * 402 Payment Required
     */
    const STATUS_402 = 402;
    const STATUS_402_TITLE = 'Payment Required';
    const HTTP_PAYMENT_REQUIRED = self::STATUS_402;

    /**
     * 403 Forbidden
     */
    const STATUS_403 = 403;
    const STATUS_403_TITLE = 'Forbidden';
    const HTTP_FORBIDDEN = self::STATUS_403;

    /**
     * 404 Not Found
     */
    const STATUS_404 = 404;
    const STATUS_404_TITLE = 'Not Found';
    const HTTP_NOT_FOUND = self::STATUS_404;

    /**
     * 405 Method Not Allowed
     */
    const STATUS_405 = 405;
    const STATUS_405_TITLE = 'Method Not Allowed';
    const HTTP_METHOD_NOT_ALLOWED = self::STATUS_405;

    /**
     * 406 Not Acceptable
     */
    const STATUS_406 = 406;
    const STATUS_406_TITLE = 'Not Acceptable';
    const HTTP_NOT_ACCEPTABLE = self::STATUS_406;

    /**
     * 407 Proxy Authentication Required
     */
    const STATUS_407 = 407;
    const STATUS_407_TITLE = 'Proxy Authentication Required';
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = self::STATUS_407;

    /**
     * 411 Length Required
     */
    const STATUS_411 = 411;
    const STATUS_411_TITLE = 'Length Required';
    const HTTP_LENGTH_REQUIRED = self::STATUS_411;

    /**
     * 412 Precondition Failed
     */
    const STATUS_412 = 412;
    const STATUS_412_TITLE = 'Precondition Failed';
    const HTTP_PRECONDITION_FAILED = self::STATUS_412;

    /**
     * 413 Request Entity Too Large
     */
    const STATUS_413 = 413;
    const STATUS_413_TITLE = 'Request Entity Too Large';
    const HTTP_REQUEST_ENTITY_TOO_LARGE = self::STATUS_413;

    /**
     * 414 Request-URI Too Long
     */
    const STATUS_414 = 414;
    const STATUS_414_TITLE = 'Request-URI Too Long';
    const HTTP_REQUEST_URI_TOO_LONG = self::STATUS_414;

    /**
     * 415 Unsupported Media Type
     */
    const STATUS_415 = 415;
    const STATUS_415_TITLE = 'Unsupported Media Type';
    const HTTP_UNSUPPORTED_MEDIA_TYPE = self::STATUS_415;

    /**
     * 416 Requested Range Not Satisfiable
     */
    const STATUS_416 = 416;
    const STATUS_416_TITLE = 'Requested Range Not Satisfiable';
    const HTTP_RANGE_REQUEST_NOT_SATISFIABLE = self::STATUS_416;

    /**
     * 417 Expectation Failed
     */
    const STATUS_417 = 417;
    const STATUS_417_TITLE = 'Expectation Failed';
    const HTTP_EXPECTATION_FAILED = self::STATUS_417;

    /**
     * 418 I'm a teapot
     */
    const STATUS_418 = 418;
    const STATUS_418_TITLE = 'I\'m a teapot';
    const HTTP_IM_A_TEAPOT = self::STATUS_418;

    /**
     * 420 Enhance Your Calm
     */
    const STATUS_420 = 420;
    const STATUS_420_TITLE = 'Enhance Your Calm';
    const HTTP_ENHANCE_YOUR_CALM = self::STATUS_420;

    /**
     * 422 Unprocessable Entity
     */
    const STATUS_422 = 422;
    const STATUS_422_TITLE = 'Unprocessable Entity';
    const HTTP_UNPROCESSABLE_ENTITY = self::STATUS_422;

    /**
     * 423 Locked
     */
    const STATUS_423 = 423;
    const STATUS_423_TITLE = 'Locked';
    const HTTP_LOCKED = self::STATUS_423;

    /**
     * 424 Failed Dependency
     */
    const STATUS_424 = 424;
    const STATUS_424_TITLE = 'Failed Dependency';
    const HTTP_FAILED_DEPENDENCY = self::STATUS_424;

    /**
     * 425 Reserved for WebDAV
     */
    const STATUS_425 = 425;
    const STATUS_425_TITLE = 'Reserved for WebDAV';
    const HTTP_RESERVED_FOR_WEBDAV = self::STATUS_425;

    /**
     * 426 Upgrade Required
     */
    const STATUS_426 = 426;
    const STATUS_426_TITLE = 'Upgrade Required';
    const HTTP_UPGRADE_REQUIRED = self::STATUS_426;

    /**
     * 428 Precondition Required
     */
    const STATUS_428 = 428;
    const STATUS_428_TITLE = 'Precondition Required';
    const HTTP_PRECONDITION_REQUIRED = self::STATUS_428;

    /**
     * 429 Too Many Requests
     */
    const STATUS_429 = 429;
    const STATUS_429_TITLE = 'Too Many Requests';
    const HTTP_TOO_MANY_REQUESTS = self::STATUS_429;

    /**
     * 431 Request Header Fields Too Large
     */
    const STATUS_431 = 431;
    const STATUS_431_TITLE = 'Request Header Fields Too Large';
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = self::STATUS_431;

    /**
     * 444 No Response
     */
    const STATUS_444 = 444;
    const STATUS_444_TITLE = 'No Response';
    const HTTP_NO_RESPONSE = self::STATUS_444;

    /**
     * 449 Retry With
     */
    const STATUS_449 = 449;
    const STATUS_449_TITLE = 'Retry With';
    const HTTP_RETRY_WITH = self::STATUS_449;

    /**
     * 450 Blocked by Windows Parental Controls
     */
    const STATUS_450 = 450;
    const STATUS_450_TITLE = 'Blocked by Windows Parental Controls';
    const HTTP_PARENTAL_CONTROLS = self::STATUS_450;

    /**
     * 451 Unavailable For Legal Reasons
     */
    const STATUS_451 = 451;
    const STATUS_451_TITLE = 'Unavailable For Legal Reasons';
    const HTTP_UNAVAILABLE_FOR_LEGAL_REASONS = self::STATUS_451;

    /**
     * 499 Client Closed Request
     */
    const STATUS_499 = 499;
    const STATUS_499_TITLE = 'Client Closed Request';
    const HTTP_CLIENT_CLOSED_REQUEST = self::STATUS_499;

    /**
     * -------------- 5xx Server Errors --------------
     * Response status codes beginning with the digit "5"
     * indicate cases in which the server is aware that
     * it has erred or is incapable of performing the request.
     * Except when responding to a HEAD request, the server
     * SHOULD include an entity containing an explanation of
     * the error situation, and whether it is a temporary or
     * permanent condition. User agents SHOULD display any
     * included entity to the user. These response codes are
     * applicable to any request method.
     * -----------------------------------------------
     */

    /**
     * 500 Server Error
     */
    const STATUS_500 = 500;
    const STATUS_500_TITLE = 'Server Error';
    const HTTP_SERVER_ERROR = self::STATUS_500;

    /**
     * 501 Not Implemented
     */
    const STATUS_501 = 501;
    const STATUS_501_TITLE = 'Not Implemented';
    const HTTP_NOT_IMPLEMENTED = self::STATUS_501;

    /**
     * 502 Bad Gateway
     */
    const STATUS_502 = 502;
    const STATUS_502_TITLE = 'Bad Gateway';
    const HTTP_BAD_GATEWAY = self::STATUS_502;

    /**
     * 503 Service Unavailable
     */
    const STATUS_503 = 503;
    const STATUS_503_TITLE = 'Service Unavailable';
    const HTTP_SERVICE_UNAVAILABLE = self::STATUS_503;

    /**
     * 504 Gateway Timeout
     */
    const STATUS_504 = 504;
    const STATUS_504_TITLE = 'Gateway Timeout';
    const HTTP_GATEWAY_TIMEOUT = self::STATUS_504;

    /**
     * 505 HTTP Version Not Supported
     */
    const STATUS_505 = 505;
    const STATUS_505_TITLE = 'HTTP Version Not Supported';
    const HTTP_VERSION_NOT_SUPPORTED = self::STATUS_505;

    /**
     * 506 Variant Also Negotiates
     */
    const STATUS_506 = 506;
    const STATUS_506_TITLE = 'Variant Also Negotiates';
    const HTTP_VARIANT_ALSO_NEGOTIATES = self::STATUS_506;

    /**
     * 507 Insufficient Storage
     */
    const STATUS_507 = 507;
    const STATUS_507_TITLE = 'Insufficient Storage';
    const HTTP_INSUFFICIENT_STORAGE = self::STATUS_507;

    /**
     * 508 Loop Detected
     */
    const STATUS_508 = 508;
    const STATUS_508_TITLE = 'Loop Detected';
    const HTTP_LOOP_DETECTED = self::STATUS_508;

    /**
     * 509 Bandwidth Limit Exceeded
     */
    const STATUS_509 = 509;
    const STATUS_509_TITLE = 'Bandwidth Limit Exceeded';
    const HTTP_BANDWIDTH_LIMIT_EXCEEDED = self::STATUS_509;

    /**
     * 510 Not Extended
     */
    const STATUS_510 = 510;
    const STATUS_510_TITLE = 'Not Extended';
    const HTTP_NOT_EXTENDED = self::STATUS_510;

    /**
     * 511 Network Authentication Required
     */
    const STATUS_511 = 511;
    const STATUS_511_TITLE = 'Network Authentication Required';
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED = self::STATUS_511;

    /**
     * 598 Network read timeout error
     */
    const STATUS_598 = 598;
    const STATUS_598_TITLE = 'Network read timeout error';
    const HTTP_NETWORK_READ_TIMEOUT_ERROR = self::STATUS_598;

    /**
     * 599 Network connect timeout error
     */
    const STATUS_599 = 599;
    const STATUS_599_TITLE = 'Network connect timeout error';
    const HTTP_NETWORK_CONNECT_TIMEOUT_ERROR = self::STATUS_599;

}
