<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\traits;

/**
 * <Header Trait>
 * Provides a simple means for representing a header as an object.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
trait HeaderTrait
{

    /**
     * Represents common headers as defined by http specification
     * and common non-standard headers with wide useage.
     *
     * Headers not existing in this array will be marked as custom,
     * but will not be excluded from creation.
     * @var array
     */
    private static $_headers_valid = array();

    /**
     * Represents whether the header is a request or response header.
     * @var string
     */
    private $_header_type = 'response';

    /**
     * Represents whether the header is a known existing header.
     * If the system could not determine it's identity, this will be true.
     * @var bool
     */
    private $_header_is_custom = false;

    /**
     * Represents the correct header name.
     * @var string
     */
    private $_header_name;

    /**
     * Represents the values supplied for the header.
     * @var array
     */
    private $_header_value = array();

    /**
     * Represents the correct separator for the header,
     * if it can contain multiple values.
     * @var string
     */
    private $_header_value_separator = ',';

    /**
     * Represents whether the header has been properly initialized.
     * @var bool
     */
    private $_header_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the header contract
     *
     * @satisfies \oroboros\http\interfaces\contract\HeaderContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Header Constructor>
     * Constructs an object representation of a header
     * @param string $name the case insensitive header name
     * @param string|array $values Represents the value of the header. May be a string or an array. If the header can have multiple values and a string is passed, the string will be split based on the defined separator.
     * @param string $separator (optional) designates the correct separator for headers that carry multiple values. Defaults to a comma if not supplied.
     * @param string $type (optional) designates whether the specified header is a request or response header. Assumes response if not defined otherwise.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     * @link https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
     */
    public function __construct( $name, $value, $type = 'response',
        $separator = ',' )
    {
        $this->_initializeHeader( $name, $value, $type, $separator );
    }

    /**
     * Hooks into native PHP string language constructs,
     * and returns the string representation of the header value.
     * @return string
     */
    public function __toString()
    {
        return $this->getName() . ': ' . $this->getValueString();
    }

    /**
     * Returns the name of the header.
     * @return string
     */
    public function getName()
    {
        return $this->_header_name;
    }

    /**
     * Returns the value(s) of the header, represented as an array.
     * @return array
     */
    public function getValue()
    {
        return $this->_header_value;
    }

    /**
     * Returns only the header value, contencated as a string.
     * @return string
     */
    public function getValueString()
    {
        return implode( $this->_header_value_separator . ' ', $this->getValue() );
    }

    /**
     * Replaces the existing value with the supplied value.
     * @param string|array $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    public function setValue( $value )
    {
        $this->_setHeaderValue( $value );
    }

    /**
     * Adds an additional value to append to the header value.
     * This method will not replace existing values.
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    public function appendValue( $value )
    {
        $this->_appendAdditionalHeaderValue( $value );
    }

    /**
     * Sends the header.
     * This will replace the same header if it has already been sent.
     * @return void
     */
    public function send()
    {
        header( (string) $this );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Header Initialization>
     * Creates the internal values representing the header.
     * @param string $name the case insensitive header name
     * @param string|array $values Represents the value of the header. May be a string or an array. If the header can have multiple values and a string is passed, the string will be split based on the defined separator.
     * @param string $separator (optional) designates the correct separator for headers that carry multiple values. Defaults to a comma if not supplied.
     * @param string $type (optional) designates whether the specified header is a request or response header. Assumes response if not defined otherwise.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    protected function _initializeHeader( $name, $value, $type = 'response',
        $separator = ',' )
    {
        $this->_getValidHeaders();
        $this->_setHeaderName( $name );
        $this->_setHeaderType( $type );
        $this->_setHeaderSeparator( $separator );
        $this->_setHeaderValue( $value );
        $this->_checkHeaderCustom();
        $this->_header_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Checks if the supplied name is a valid type.
     * @param string $name
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    private function _validateHeaderName( $name )
    {
        if ( !is_string( $name ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $name ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Checks if the supplied value is a valid type.
     * @param string|array $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    private function _validateHeaderValue( $value )
    {
        if ( !is_string( $value ) && !is_array( $value ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string|array', gettype( $value ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Checks if the supplied type is a valid.
     * @param string $type
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    private function _validateHeaderType( $type )
    {
        if ( !is_string( $type ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $type ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Checks if the supplied separator is a valid.
     * @param string $separator
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter was passed
     */
    private function _validateHeaderSeparator( $separator )
    {
        if ( !is_string( $separator ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $separator ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Sets the internal name for the header.
     * @param string $name
     * @return void
     */
    private function _setHeaderName( $name )
    {
        $this->_validateHeaderName( $name );
        $this->_header_name = $name;
    }

    /**
     * Sets the internal type for the header.
     * @param string $name
     * @return void
     */
    private function _setHeaderType( $type )
    {
        $this->_validateHeaderType( $type );
        $this->_header_type = $type;
    }

    /**
     * Sets the separator used for the header if it can contain multiple values.
     * @param string $separator
     * @return void
     */
    private function _setHeaderSeparator( $separator = ',' )
    {
        $this->_validateHeaderSeparator( $separator );
        $this->_header_value_separator = $separator;
    }

    /**
     * Sets the internal value(s) for the header.
     * @param string|array $value
     * @return void
     */
    private function _setHeaderValue( $value )
    {
        $this->_validateHeaderValue( $value );
        $header = $value;
        if ( is_string( $value ) )
        {
            $header = array();
            $tmp = explode( $this->_header_value_separator, $value );
            foreach ( $tmp as
                $val )
            {
                $header[] = trim( $val );
            }
        }
        $this->_header_value = $header;
    }

    /**
     * Adds an additional header value to the internal index of values.
     * @param string $value
     * @return void
     */
    private function _appendAdditionalHeaderValue( $value )
    {
        $this->_validateHeaderValue( $value );
        $values = $value;
        if ( is_string( $value ) )
        {
            $values = array();
            $tmp = explode( $this->_header_value_separator, $value );
            foreach ( $tmp as
                $val )
            {
                $values[] = trim( $val );
            }
        }
        foreach ( $values as
            $v )
        {
            if ( !in_array( $v, $this->_header_value ) )
            {
                $this->_header_value[] = $v;
            }
        }
    }

    /**
     * Checks if the header is known or not, and sets the appropriate boolean flag.
     * @return void
     */
    private function _checkHeaderCustom()
    {
        foreach ( self::$_headers_valid[$this->_header_type] as
            $header )
        {
            if ( strtolower( $this->_header_name ) === strtolower( $header ) )
            {
                $this->_header_is_custom = false;
                return;
            }
        }
        $this->_header_is_custom = true;
    }

    /**
     * Obtains a list of known headers.
     * This is set statically, and will only be performed
     * the first time the object instantiates.
     *
     * These values are fixed enumerations read from interfaces,
     * and do not change.
     * @return void
     */
    private function _getValidHeaders()
    {
        if ( !array_key_exists( 'request', self::$_headers_valid ) )
        {
            self::$_headers_valid['request'] = \oroboros\enum\http\RequestHeaders::valid();
        }
        if ( !array_key_exists( 'response', self::$_headers_valid ) )
        {
            self::$_headers_valid['response'] = \oroboros\enum\http\ResponseHeaders::valid();
        }
    }

}
