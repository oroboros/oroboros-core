<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\traits;

/**
 * <Protocol Trait>
 * Provides a simple means for representing an HTTP protocol as an object.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
trait ProtocolTrait
{
    /**
     * Represents valid HTTP request methods.
     * If HTTP/2 becomes prominent and supported at a later date,
     * it will be added here also.
     * @var array
     */
    private $_valid_protocols = array(
        '1.0',
        '1.1',
    );

    /**
     * Represents the current http protocol.
     * @var string
     */
    private $_protocol;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\http\interfaces\contract\ProtocolContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Protocol Constructor>
     * Creates an object representation of an http protocol
     * @param string $protocol (optional) defaults to HTTP/1.1
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a non-valid http protocol is passed
     */
    public function __construct( $protocol = '1.1' )
    {
        $this->_initializeHttpProtocol($protocol);
    }

    /**
     * Hooks into native PHP string language constructs
     * and provides the object value as a string.
     * @return string
     */
    public function __toString()
    {
        return $this->_protocol;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Protocol Initialization>
     * Initializes the http protocol as an object.
     * @param string $protocol
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a non-valid http protocol is passed
     */
    protected function _initializeHttpProtocol( $protocol = '1.1' )
    {
        if ( !in_array( $protocol, $this->_valid_protocols ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Invalid protocol passed. Valid values are [%s].',
                implode( ', ', $this->_valid_protocols ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_protocol = $protocol;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
}
