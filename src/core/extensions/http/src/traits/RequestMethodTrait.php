<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\traits;

/**
 * <Request Method Trait>
 * Provides a simple means for representing a request method as an object.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4
 * @since 0.2.4-alpha
 * @link https://tools.ietf.org/html/rfc7231#section-4 Specifies the standard HTTP request methods
 * @link https://tools.ietf.org/html/rfc5789#section-2 Specifies the HTTP PATCH request method
 */
trait RequestMethodTrait
{

    /**
     * Represents the valid http request methods.
     * @var array
     */
    private static $_request_methods_valid = array(
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'OPTIONS',
        'TRACE',
        'HEAD',
        'CONNECT',
        'PATCH'
    );

    /**
     * Represents the current request method.
     * @var string
     */
    private $_request_method;

    /**
     * Represents whether the request method has been correctly initialized.
     * @var bool
     */
    private $_request_method_is_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Request Method Constructor>
     * Instantiates an object representing a valid http request method.
     * @param string $method (optional) If provided, must be a valid http request method. Defaults to GET if not provided.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid request method is passed
     */
    public function __construct( $method = 'GET' )
    {
        $this->_initializeRequestMethod( $method );
    }

    /**
     * Hooks into native PHP string language constructs,
     * and provides a string value to represent this object.
     * @return string
     */
    public function __toString()
    {
        return $this->_request_method;
    }

    /**
     * Returns an array of valid http request methods.
     * @return array
     */
    public static function valid()
    {
        return self::$_request_methods_valid;
    }

    /**
     * Returns a boolean determination as to whether the provided
     * value is a valid http request method.
     * @param string $method
     * @return bool
     */
    public static function isValid( $method )
    {
        return in_array($method, self::$_request_methods_valid);
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Request Method Initialization>
     * Initializes the request method object with the provided value.
     * @param string $method
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid request method is passed
     */
    protected function _initializeRequestMethod( $method )
    {
        $this->_setRequestMethod( $method );
        $this->_request_method_is_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Validates the provided request method
     * @param string $method
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid request method is passed
     */
    private function _validateRequestMethod( $method )
    {
        if ( !self::isValid( $method ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( '',
                (is_string( $method )
                    ? $method
                    : gettype( $method ) ),
                'string: ' . implode( ', ', self::$_request_methods_valid ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    private function _setRequestMethod( $method )
    {
        $this->_validateRequestMethod( $method );
        $this->_request_method = strtoupper( $method );
    }

}
