<?php

namespace oroboros\log;

/**
 *
 */
final class LogManager
    extends \oroboros\log\abstracts\AbstractLogManager
    implements \oroboros\log\interfaces\api\LogApi
{

    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_LOGGER_MANAGER;

}
