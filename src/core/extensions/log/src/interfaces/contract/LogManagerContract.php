<?php

/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\interfaces\contract;

/**
 * <Oroboros Core Log Manager Contract>
 * This interface enforces the methods required for a valid log manager.
 * These methods may be manually honored, or may be satisfied by using the
 * corresponding library trait. Traits that extend upon this functionality
 * should be expected to implement this for you.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage environment
 * @version 0.2.5
 * @since 0.2.5
 */
interface LogManagerContract
extends \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract,
 \oroboros\core\interfaces\contract\libraries\LibraryContract,
 \Psr\Log\LoggerInterface,
 \Psr\Log\LoggerAwareInterface
{

    /**
     * Sets a logger instance on the object
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param string $name (optional) An optional slug to identify the logger as. This will be automatically determined if it is not supplied.
     * @return null
     */
    public function setLogger( \Psr\Log\LoggerInterface $logger, $name = null );

    /**
     * Returns a list of all of the current loggers scope keys that are
     * currently in the LogManagers pool of workers.
     * @return array
     */
    public function listLoggers();

    /**
     * Enables a specific logger logging by its worker scope.
     * Has no effect if it was already enabled or an invalid scope is passed.
     * @param string $logger
     * @return void
     */
    public function enableLogger( $logger );

    /**
     * Disables a specific logger from logging by its worker scope.
     * Has no effect if it was already disabled or an invalid scope is passed.
     * @param string $logger
     * @return void
     */
    public function disableLogger( $logger );

    /**
     * Flushes buffered logs to content. If [$return] is true,
     * it will return an array of the content, with the key being
     * the logger scope that generated the content, and the value
     * being the output.
     * If [$return] is false, the content will instead be sent to whatever
     * stream or source it is attached to, and nothing will be returned.
     * @param $return (optional) whether or not to return the content as an array (default true)
     * @return array|void
     */
    public function flushLogs( $return = true );

    /**
     * Tells loggers that buffer logs instead of logging in realtime
     * to erase their buffer. This DOES NOT erase acutal log files.
     * @return void
     */
    public function clearLogs();

    /**
     * Enables a specific log level being logged.
     * If the level is not valid, the method will simply return false.
     * @param string $level
     * @return bool
     */
    public function enableLogLevel( $level );

    /**
     * Disables a specific log level from being logged.
     * If the level is not valid, the method will simply return false.
     * @param string $level
     * @return bool
     */
    public function disableLogLevel( $level );

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function emergency( $message, array $context = array() );

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function alert( $message, array $context = array() );

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function critical( $message, array $context = array() );

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function error( $message, array $context = array() );

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function warning( $message, array $context = array() );

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function notice( $message, array $context = array() );

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function info( $message, array $context = array() );

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function debug( $message, array $context = array() );

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log( $level, $message, array $context = array() );
}
