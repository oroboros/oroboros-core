<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\traits;

/**
 * <Log Writer Trait>
 * This trait provides methods required to write to logs.
 * Logger writers deliver the actual message to its destination,
 * without regard to formatting concerns or message content.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr3
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\log\interfaces\contract\LogWriterContract
 */
trait LogWriterTrait
{

    use \oroboros\core\traits\patterns\behavioral\WorkerTrait;

    /**
     * Represents the stream resource or handle for writing to.
     * This may be a Psr-7 StreamWrapper, or it may be a resource.
     * In some cases it may also be another object such as \PDO
     * or similar, depending on the nature of the writer.
     *
     * It is left to the individual writer to understand what its
     * resource is and how to interact with it.
     * @var object|resource
     */
    private $_logger_writer_resource;

    /**
     * Represents a stream resource to a temp stream, using php://temp.
     * This is set if the provided settings have the use_buffer key set to true.
     * if this is the case, the stream will be copied to the resource when the
     * flush method is called, and will be truncated if the clear method
     * is called.
     * @var resource
     */
    private $_logger_writer_buffer;

    /**
     * Represents the settings passed in during instantiation.
     * If no settings are provided, then this will be set to an empty array.
     * LogWriter settings are immutable after instantiation.
     * Changing the settings requires creating a new LogWriter.
     * @var array
     */
    private $_logger_writer_settings = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\log\interfaces\contract\LogWriterContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Log Writer Constructor>
     * Oroboros Core log writers have a standardized constructor.
     * Settings are nullable, but the log writer will generally
     * not do anything if it does not have settings injected.
     *
     * The Logger will inject the correct parameters in the normal
     * context of logger usage in this system, but will only do so for loggers
     * that honor the Oroboros Core logger contract in addition
     * to \Psr\Log\LoggerInterface.
     *
     * @param array $settings (optional)
     * @param resource $destination (optional)
     */
    public function __construct( $settings = null, $destination = null )
    {
        $class = get_class( $this );
        $this->_setWorkerCategory( defined( $class . '::OROBOROS_CLASS_TYPE' )
                ? $class::OROBOROS_CLASS_TYPE
                : false  );
        $this->_setWorkerScope( defined( $class . '::OROBOROS_CLASS_SCOPE' )
                ? $class::OROBOROS_CLASS_SCOPE
                : false  );
        $this->_logWriterSetSettings( $settings );
        if ( array_key_exists( 'use_buffer', $this->_logger_writer_settings ) &&
            $this->_logger_writer_settings['use_buffer'] )
        {
            $this->_logger_writer_buffer = $this->_logWriterGetBufferHandle();
        }
        $this->_logWriterSetResource( $destination );
    }

    /**
     * Close the resource handle if it still exists.
     * If the buffer exists still at this point and has not been flushed,
     * it will be copied to the output resource at this point.
     */
    public function __destruct()
    {
        $this->flush( false );
        if ( is_resource( $this->_logger_writer_resource ) )
        {
            fclose( $this->_logger_writer_resource );
        }
    }

    /**
     * <Log Writer Write Method>
     * Writes the message to the endpoint.
     * @param string $message
     * @return void
     */
    public function write( $message )
    {
        if ( is_resource( $this->_logger_writer_buffer ) )
        {
            fwrite( $this->_logger_writer_buffer, $message );
        } else
        {
            $res = fwrite( $this->_logger_writer_resource, $message );
        }
    }

    /**
     * <Log Writer Flush Method>
     * Flushes the output buffer, if it is buffered.
     * This will send the content to the defined resource.
     * @return void
     */
    public function flush( $return = true )
    {
        if ( is_resource( $this->_logger_writer_buffer ) )
        {
            rewind( $this->_logger_writer_buffer );
            if ( $return )
            {
                $content = stream_get_contents( $this->_logger_writer_buffer );
                fclose( $this->_logger_writer_buffer );
                return $content;
            }
            if ( is_resource( $this->_logger_writer_resource ) )
            {
                stream_copy_to_stream( $this->_logger_writer_buffer,
                    $this->_logger_writer_resource );
            }
            fclose( $this->_logger_writer_buffer );
        }
    }

    /**
     * <Log Writer Clear Method>
     * Clears the output buffer, if buffering is possible.
     * This will discard all buffered messages.
     * @return voind
     */
    public function clear()
    {
        if ( is_resource( $this->_logger_writer_buffer ) )
        {
            ftruncate( $this->_logger_writer_buffer, 0 );
            fclose( $this->_logger_writer_buffer );
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Gets the settings passed in from instantiation.
     * Settings are immutable after instantiation, but this
     * method can retrieve them from child classes.
     * @return array
     */
    protected function _getSettings()
    {
        return \oroboros\core\utilities\core\CoreConfig::get( 'settings', 'core' )['log']['writer'];
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the settings
     * @param array $settings
     */
    private function _logWriterSetSettings( array $settings = null )
    {
        $this->_logger_writer_settings = array_replace_recursive( $this->_logger_writer_settings,
            $settings );
    }

    /**
     * Creates a stream resource pointer, or sets a provided one as the pointer,
     * and wraps it in a Psr7 StreamWrapper.
     * @param resource $stream (optional) Must be a stream resource if provided. If not provided, the writer will open a pointer to the default log file location.
     * @throws \oroboros\core\utilities\exception\log\InvalidArgumentException if the provided stream is not null and is not a resource
     * @return void
     */
    private function _logWriterSetResource( $stream = null )
    {
        if (
        //covers existing file that is writable
            (!is_null( $stream ) && is_file( $stream ) && is_writable( $stream ) )
            //covers a file that does not exist, but it's parent directory is writable
            || (!is_null( $stream ) && is_dir( realpath( dirname( $stream ) ) )
            && is_writable( realpath( dirname( $stream ) ) )
            && !is_file( $stream ) ) )
        {
            $stream = fopen( $stream, 'a+b' );
        }
        if ( !is_null( $stream ) && !is_resource( $stream ) )
        {
            $this->_validate( 'type-of', $stream, 'resource', true );
        }
        if ( is_null( $stream ) )
        {
            $this->_logger_writer_resource = $this->_logWriterGetLogHandle();
        } else
        {
            fseek( $stream, 0, SEEK_END );
            $this->_logger_writer_resource = $stream;
        }
    }

    private function _logWriterGetBufferHandle()
    {
        $sh = fopen( 'php://temp', 'a+b' );
        return $sh;
    }

    /**
     * Attempts to create a new error log file if one does not already exist.
     * If one can be created, it will create the corresponding log header if
     * the settings designate that one should be made. Failure to create a
     * logfile will raise an exception.
     * @return resource The stream wrapper for the new error log file.
     */
    private function &_logWriterGetLogHandle()
    {
        if ( \oroboros\environment\interfaces\enumerated\CoreEnvironment::ERROR_LOG )
        {
            //An existing logfile is present. Use that one.
            return fopen( \oroboros\environment\interfaces\enumerated\CoreEnvironment::ERROR_LOG,
                'wb' );
        }
        $dir = \oroboros\environment\interfaces\enumerated\CoreEnvironment::TEMP_DIRECTORY;
        $file = $this->_logger_writer_settings['default_error_logfile'];
        if ( !$dir || !is_writable( $dir ) )
        {
            $dir = \oroboros\environment\interfaces\enumerated\CoreEnvironment::LOCAL_TMP_DIRECTORY;
        }
        if ( !$dir || !is_writable( $dir ) )
        {
            throw new \oroboros\core\utilities\exception\log\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                sprintf( 'Log directory %sis not writable.',
                    ( $dir )
                        ? "$dir "
                        : null  ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        } elseif ( is_file( $dir . $file ) && !is_writable( $dir . $file ) )
        {
            throw new \oroboros\core\utilities\exception\log\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                sprintf( 'Log file [%s] exists but is not writable.',
                    $dir . $file ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $make_header = (!is_file( $dir . $file ) && array_key_exists( 'use_header',
                $this->_logger_writer_settings ) && $this->_logger_writer_settings['use_header'] );
        $resource = fopen( $dir . $file, 'a+b' );
        if ( $make_header )
        {
            $this->_logWriterMakeFileHeader( $resource );
        }
        return $resource;
    }

    private function _logWriterMakeFileHeader( $resource )
    {
        $header = $this->_logger_writer_settings['header_title'] . PHP_EOL;
        if ( $this->_logger_writer_settings['header_timestamp'] )
        {
            $time = new \DateTime();
            $header .= sprintf( 'Created at: %s' . PHP_EOL,
                date_format( $time,
                    $this->_logger_writer_settings['timestamp_format'] ) );
        }
        $header .= '=====================================' . PHP_EOL . PHP_EOL;
        if ( $this->_logger_writer_settings['use_buffer'] )
        {
            fwrite( $this->_logger_writer_buffer, $header );
        } else
        {
            fwrite( $resource, $header );
        }
    }

}
