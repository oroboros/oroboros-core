<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\message\interfaces\contract;

/**
 * <Response Contract Interface>
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage response
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
interface ResponseContract
extends MessageContract,
 \Psr\Http\Message\ResponseInterface
{

    /**
     * <Message Constructor>
     * Constructs a new instance of a Psr-7 message wrapper.
     * @param scalar|resource|\Psr\Http\Message\StreamInterface $body This is the only required value for the constructor, and represents a message body as a scalar value, resource pointer, or instance of a stream interface.
     * @param array $headers (optional) If provided, represents an array of headers to construct with the message.
     * @param string $protocol (optional) If provided, the HTTP request protocol will be set to this value. Default value is "1.1"
     * @param int $code (optional) If provided, the HTTP status code. If not provided, defaults to 200.
     * @param string $reason (optional) If provided, the HTTP status code reason. If not provided, uses the default for the code
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if any passed parameters do not validate
     */
    public function __construct( $body, array $headers = null, $protocol = null,
        $code = null, $reason = null );

    /**
     * Gets the response status code.
     *
     * The status code is a 3-digit integer result code of the server's attempt
     * to understand and satisfy the request.
     *
     * @return int Status code.
     */
    public function getStatusCode();

    /**
     * Return an instance with the specified status code and, optionally, reason phrase.
     *
     * If no reason phrase is specified, implementations MAY choose to default
     * to the RFC 7231 or IANA recommended reason phrase for the response's
     * status code.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated status and reason phrase.
     *
     * @see http://tools.ietf.org/html/rfc7231#section-6
     * @see http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @param int $code The 3-digit integer result code to set.
     * @param string $reasonPhrase The reason phrase to use with the
     *     provided status code; if none is provided, implementations MAY
     *     use the defaults as suggested in the HTTP specification.
     * @return static
     * @throws \InvalidArgumentException For invalid status code arguments.
     */
    public function withStatus( $code, $reasonPhrase = '' );

    /**
     * Gets the response reason phrase associated with the status code.
     *
     * Because a reason phrase is not a required element in a response
     * status line, the reason phrase value MAY be empty. Implementations MAY
     * choose to return the default RFC 7231 recommended reason phrase (or those
     * listed in the IANA HTTP Status Code Registry) for the response's
     * status code.
     *
     * @see http://tools.ietf.org/html/rfc7231#section-6
     * @see http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @return string Reason phrase; must return an empty string if none present.
     */
    public function getReasonPhrase();
}
