<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\message\traits;

/**
 * <Response Trait>
 * Provides a set of methods to represent a Psr-7 response stream.
 *
 * Representation of an outgoing, server-side response.
 *
 * Per the HTTP specification, this interface includes properties for
 * each of the following:
 *
 * - Protocol version
 * - Status code and reason phrase
 * - Headers
 * - Message body
 *
 * Responses are considered immutable; all methods that might change state MUST
 * be implemented such that they retain the internal state of the current
 * message and return an instance that contains the changed state.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr7
 * @version 0.2.4
 * @since 0.2.4-alpha
 * @satisfies \oroboros\message\interfaces\contract\ResponseContract
 * @satisfies \Psr\Http\Message\ResponseInterface
 */
trait ResponseTrait
{

    /**
     * Provides Psr-7 message wrapper functionality.
     */
    use \oroboros\message\traits\MessageTrait;

    /**
     * Represents the internal status code object
     * @var \oroboros\http\StatusCode
     */
    private $_response_code;

    /**
     * Represents whether the request has been correctly initialized.
     * @var bool
     */
    private $_response_is_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the request contract
     *
     * @satisfies \oroboros\message\interfaces\contract\ResponseContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Message Constructor>
     * Constructs a new instance of a Psr-7 message wrapper.
     * @param scalar|resource|\Psr\Http\Message\StreamInterface $body This is the only required value for the constructor, and represents a message body as a scalar value, resource pointer, or instance of a stream interface.
     * @param array $headers (optional) If provided, represents an array of headers to construct with the message.
     * @param string $protocol (optional) If provided, the HTTP request protocol will be set to this value. Default value is "1.1"
     * @param int $code (optional) If provided, the HTTP status code. If not provided, defaults to 200.
     * @param string $reason (optional) If provided, the HTTP status code reason. If not provided, uses the default for the code
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if any passed parameters do not validate
     */
    public function __construct( $body, array $headers = null, $protocol = null,
        $code = null, $reason = null )
    {
        $this->_initializeResponse( $body, $headers, $protocol, $code, $reason );
    }

    /**
     * Gets the response status code.
     *
     * The status code is a 3-digit integer result code of the server's attempt
     * to understand and satisfy the request.
     *
     * @return int Status code.
     */
    public function getStatusCode()
    {
        return $this->_response_code->getCode();
    }

    /**
     * Return an instance with the specified status code and, optionally, reason phrase.
     *
     * If no reason phrase is specified, implementations MAY choose to default
     * to the RFC 7231 or IANA recommended reason phrase for the response's
     * status code.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated status and reason phrase.
     *
     * @see http://tools.ietf.org/html/rfc7231#section-6
     * @see http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @param int $code The 3-digit integer result code to set.
     * @param string $reasonPhrase The reason phrase to use with the
     *     provided status code; if none is provided, implementations MAY
     *     use the defaults as suggested in the HTTP specification.
     * @return static
     * @throws \InvalidArgumentException For invalid status code arguments.
     */
    public function withStatus( $code, $reasonPhrase = '' )
    {
        $values = $this->_getMessageValues();
        $obj = $this->_getResponseStatusCodeObject( $code, $reasonPhrase );
        $values['code'] = $obj->getCode();
        $values['reason'] = ( ( $reasonPhrase === '' )
            ? ( $this->_response_code->getCode() === $code
            ? $this->_response_code->getMessage()
            : $obj->getMessage() )
            : $reasonPhrase );
        return $this->_instantiateMessageNewInstance( $values );
    }

    /**
     * Gets the response reason phrase associated with the status code.
     *
     * Because a reason phrase is not a required element in a response
     * status line, the reason phrase value MAY be empty. Implementations MAY
     * choose to return the default RFC 7231 recommended reason phrase (or those
     * listed in the IANA HTTP Status Code Registry) for the response's
     * status code.
     *
     * @see http://tools.ietf.org/html/rfc7231#section-6
     * @see http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @return string Reason phrase; must return an empty string if none present.
     */
    public function getReasonPhrase()
    {
        return $this->_response_code->getMessage();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Response Initialization Method>
     * Creates a ready instance of a Psr-7 response message stream.
     * @param mixed $body
     * @param resource|string $source
     * @param array $headers
     * @param string $protocol
     * @return void
     */
    protected function _initializeResponse( $body, array $headers = null,
        $protocol = null, $code = null, $reason = null )
    {
        $this->_initializeMessage( $body, $headers, $protocol );
        if ( is_null( $code ) )
        {
            $code = 200;
        }
        $this->_setResponseCode( $code );
        if ( is_null( $reason ) )
        {
            $reason = $this->_response_code->getMessage();
        }
        $this->_setResponseReason( $reason );
        $this->_response_is_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the response code if it is valid.
     * @param int $code
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid code is passed
     */
    private function _setResponseCode( $code )
    {
        $this->_response_code = $this->_getResponseStatusCodeObject( $code );
    }

    /**
     * Sets the response reason.
     * @param string $reason
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid reason is passed
     */
    private function _setResponseReason( $reason )
    {
        $this->_response_code->setMessage( $reason );
    }

    /**
     * Returns an array of the existing message values for this object,
     * for substitution when returning instances that must retain object immutability.
     * @return array
     */
    private function _getMessageValues()
    {
        $values = array(
            'body' => $this->_stream,
            'protocol' => (string) $this->_message_protocol,
            'headers' => $this->getHeaders(),
            'code' => $this->_response_code->getCode(),
            'reason' => $this->_response_code->getMessage()
        );
        return $values;
    }

    /**
     * Returns a status code object.
     * @param type $code
     * @param type $message
     * @return \oroboros\http\StatusCode
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid code is passed
     */
    private function _getResponseStatusCodeObject( $code, $message = '' )
    {
        $obj = new \oroboros\http\StatusCode( $code );
        if ( $message !== '' )
        {
            $obj->setMessage( $message );
        }
        return $obj;
    }

    /**
     * Creates a new instance based off of the default parameters,
     * substituting any values passed in.
     * @param array $params
     * @return \oroboros\message\traits\ResponseTrait
     */
    private function _instantiateMessageNewInstance( array $params = null )
    {
        $values = $this->_getMessageValues();
        if ( !is_null( $params ) )
        {
            foreach ( $params as
                $key =>
                $value )
            {
                $values[$key] = $value;
            }
        }
        $class = get_class( $this );
        $instance = new $class( $values['body'], $values['headers'],
            $values['protocol'], $values['code'], $values['reason'] );
        return $instance;
    }

}
