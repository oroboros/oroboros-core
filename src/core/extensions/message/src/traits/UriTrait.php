<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\message\traits;

/**
 * <Uri Trait>
 * Provides a set of methods to honor the Psr-7 Uri Interface.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr7
 * @version 0.2.4
 * @since 0.2.4-alpha
 * @satisfies \oroboros\message\interfaces\contract\UriContract
 * @satisfies \Psr\Http\Message\UriInterface
 */
trait UriTrait
{

    use \oroboros\core\traits\libraries\LibraryTrait;


    private static $_regex = array(
        'path' => '/(?:[^a-zA-Z0-9_\-\.~!\$&\'\(\)\*\+,;=%:@\/]++|%(?![A-Fa-f0-9]{2}))/',
        'queryAndFragment' => '/(?:[^a-zA-Z0-9_\-\.~!\$&\'\(\)\*\+,;=%:@\/\?]++|%(?![A-Fa-f0-9]{2}))/',
    );
    private static $_replaceQuery = [
        '=' => '%3D',
        '&' => '%26' ];

    /** @var string Uri scheme. */
    private $_scheme = '';

    /** @var string Uri user info. */
    private $_userInfo = '';

    /** @var string Uri host. */
    private $_host = '';

    /** @var int|null Uri port. */
    private $_port;

    /** @var string Uri path. */
    private $_path = '';

    /** @var string Uri query string. */
    private $_query = '';

    /** @var string Uri fragment. */
    private $_fragment = '';
    private $_defaultPorts = array(
        'http' => 80,
        'https' => 443,
        'ftp' => 21,
        'gopher' => 70,
        'nntp' => 119,
        'news' => 119,
        'telnet' => 23,
        'tn3270' => 23,
        'imap' => 143,
        'pop' => 110,
        'ldap' => 389,
    );

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Uri Constructor>
     * Creates a new uri wrapper based on the supplied uri parameter.
     * @param string $uri
     */
    public function __construct( $uri )
    {
        $this->_initializeUri( $uri );
    }

    /**
     * Retrieve the scheme component of the URI.
     * If no scheme is present, this method MUST return an empty string.
     * The value returned MUST be normalized to lowercase, per RFC 3986
     * Section 3.1.
     * The trailing ":" character is not part of the scheme and MUST NOT be
     * added.
     * @see https://tools.ietf.org/html/rfc3986#section-3.1
     * @return string The URI scheme.
     */
    public function getScheme()
    {
        return $this->_scheme;
    }

    /**
     * Retrieve the authority component of the URI.
     * If no authority information is present, this method MUST return an empty
     * string.
     * The authority syntax of the URI is:
     * <pre>
     * [user-info@]host[:port]
     * </pre>
     * If the port component is not set or is the standard port for the current
     * scheme, it SHOULD NOT be included.
     * @see https://tools.ietf.org/html/rfc3986#section-3.2
     * @return string The URI authority, in "[user-info@]host[:port]" format.
     */
    public function getAuthority()
    {
        $authority = $this->_host;
        if ( $this->_userInfo !== '' )
        {
            $authority = $this->_userInfo . '@' . $authority;
        }
        if ( $this->_port !== null )
        {
            $authority .= ':' . $this->_port;
        }
        return $authority;
    }

    /**
     * Retrieve the user information component of the URI.
     * If no user information is present, this method MUST return an empty
     * string.
     * If a user is present in the URI, this will return that value;
     * additionally, if the password is also present, it will be appended to the
     * user value, with a colon (":") separating the values.
     * The trailing "@" character is not part of the user information and MUST
     * NOT be added.
     * @return string The URI user information, in "username[:password]" format.
     */
    public function getUserInfo()
    {
        return $this->_userInfo;
    }

    /**
     * Retrieve the host component of the URI.
     * If no host is present, this method MUST return an empty string.
     * The value returned MUST be normalized to lowercase, per RFC 3986
     * Section 3.2.2.
     * @see http://tools.ietf.org/html/rfc3986#section-3.2.2
     * @return string The URI host.
     */
    public function getHost()
    {
        return $this->_host;
    }

    /**
     * Retrieve the port component of the URI.
     * If a port is present, and it is non-standard for the current scheme,
     * this method MUST return it as an integer. If the port is the standard port
     * used with the current scheme, this method SHOULD return null.
     * If no port is present, and no scheme is present, this method MUST return
     * a null value.
     * If no port is present, but a scheme is present, this method MAY return
     * the standard port for that scheme, but SHOULD return null.
     * @return null|int The URI port.
     */
    public function getPort()
    {
        return $this->_port;
    }

    /**
     * Retrieve the path component of the URI.
     * The path can either be empty or absolute (starting with a slash) or
     * rootless (not starting with a slash). Implementations MUST support all
     * three syntaxes.
     * Normally, the empty path "" and absolute path "/" are considered equal as
     * defined in RFC 7230 Section 2.7.3. But this method MUST NOT automatically
     * do this normalization because in contexts with a trimmed base path, e.g.
     * the front controller, this difference becomes significant. It's the task
     * of the user to handle both "" and "/".
     * The value returned MUST be percent-encoded, but MUST NOT double-encode
     * any characters. To determine what characters to encode, please refer to
     * RFC 3986, Sections 2 and 3.3.
     * As an example, if the value should include a slash ("/") not intended as
     * delimiter between path segments, that value MUST be passed in encoded
     * form (e.g., "%2F") to the instance.
     * @see https://tools.ietf.org/html/rfc3986#section-2
     * @see https://tools.ietf.org/html/rfc3986#section-3.3
     * @return string The URI path.
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * Retrieve the query string of the URI.
     * If no query string is present, this method MUST return an empty string.
     * The leading "?" character is not part of the query and MUST NOT be
     * added.
     * The value returned MUST be percent-encoded, but MUST NOT double-encode
     * any characters. To determine what characters to encode, please refer to
     * RFC 3986, Sections 2 and 3.4.
     * As an example, if a value in a key/value pair of the query string should
     * include an ampersand ("&") not intended as a delimiter between values,
     * that value MUST be passed in encoded form (e.g., "%26") to the instance.
     * @see https://tools.ietf.org/html/rfc3986#section-2
     * @see https://tools.ietf.org/html/rfc3986#section-3.4
     * @return string The URI query string.
     */
    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * Retrieve the fragment component of the URI.
     * If no fragment is present, this method MUST return an empty string.
     * The leading "#" character is not part of the fragment and MUST NOT be
     * added.
     * The value returned MUST be percent-encoded, but MUST NOT double-encode
     * any characters. To determine what characters to encode, please refer to
     * RFC 3986, Sections 2 and 3.5.
     * @see https://tools.ietf.org/html/rfc3986#section-2
     * @see https://tools.ietf.org/html/rfc3986#section-3.5
     * @return string The URI fragment.
     */
    public function getFragment()
    {
        return $this->_fragment;
    }

    /**
     * Return an instance with the specified scheme.
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified scheme.
     * Implementations MUST support the schemes "http" and "https" case
     * insensitively, and MAY accommodate other schemes if required.
     * An empty scheme is equivalent to removing the scheme.
     * @param string $scheme The scheme to use with the new instance.
     * @return static A new instance with the specified scheme.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException for invalid or unsupported schemes.
     */
    public function withScheme( $scheme )
    {
        if ( !array_key_exists( $scheme, $this->_defaultPorts ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            'Invalid scheme specified. Thrown at: '
            . __LINE__ . ' of ' . __METHOD__,
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $args = $this->_getObjectArguments();
        $args['scheme'] = $scheme;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return an instance with the specified user information.
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified user information.
     * Password is optional, but the user information MUST include the
     * user; an empty string for the user is equivalent to removing user
     * information.
     * @param string $user The user name to use for authority.
     * @param null|string $password The password associated with $user.
     * @return static A new instance with the specified user information.
     */
    public function withUserInfo( $user, $password = null )
    {
        $args = $this->_getObjectArguments();
        $args['user'] = $user;
        $args['pass'] = $password;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return an instance with the specified host.
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified host.
     * An empty host value is equivalent to removing the host.
     * @param string $host The hostname to use with the new instance.
     * @return static A new instance with the specified host.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException for invalid hostnames.
     */
    public function withHost( $host )
    {
        $args = $this->_getObjectArguments();
        $args['host'] = $host;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return an instance with the specified port.
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified port.
     * Implementations MUST raise an exception for ports outside the
     * established TCP and UDP port ranges.
     * A null value provided for the port is equivalent to removing the port
     * information.
     * @param null|int $port The port to use with the new instance; a null value
     *     removes the port information.
     * @return static A new instance with the specified port.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException for invalid ports.
     */
    public function withPort( $port )
    {
        $args = $this->_getObjectArguments();
        $args['port'] = $port;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return an instance with the specified path.
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified path.
     * The path can either be empty or absolute (starting with a slash) or
     * rootless (not starting with a slash). Implementations MUST support all
     * three syntaxes.
     * If the path is intended to be domain-relative rather than path relative then
     * it must begin with a slash ("/"). Paths not starting with a slash ("/")
     * are assumed to be relative to some base path known to the application or
     * consumer.
     * Users can provide both encoded and decoded path characters.
     * Implementations ensure the correct encoding as outlined in getPath().
     * @param string $path The path to use with the new instance.
     * @return static A new instance with the specified path.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException for invalid paths.
     */
    public function withPath( $path )
    {
        $args = $this->_getObjectArguments();
        $args['path'] = $path;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return an instance with the specified query string.

     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified query string.
     * Users can provide both encoded and decoded query characters.
     * Implementations ensure the correct encoding as outlined in getQuery().
     * An empty query string value is equivalent to removing the query string.
     * @param string $query The query string to use with the new instance.
     * @return static A new instance with the specified query string.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException for invalid query strings.
     */
    public function withQuery( $query )
    {
        $args = $this->_getObjectArguments();
        $args['query'] = $query;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return an instance with the specified URI fragment.
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified URI fragment.
     * Users can provide both encoded and decoded fragment characters.
     * Implementations ensure the correct encoding as outlined in getFragment().
     * An empty fragment value is equivalent to removing the fragment.
     * @param string $fragment The fragment to use with the new instance.
     * @return static A new instance with the specified fragment.
     */
    public function withFragment( $fragment )
    {
        $args = $this->_getObjectArguments();
        $args['fragment'] = $fragment;
        $instance = $this->_composeNewUriDefaultClass( $args );
        return $instance;
    }

    /**
     * Return the string representation as a URI reference.
     * Depending on which components of the URI are present, the resulting
     * string is either a full URI or relative reference according to RFC 3986,
     * Section 4.1. The method concatenates the various components of the URI,
     * using the appropriate delimiters:
     * - If a scheme is present, it MUST be suffixed by ":".
     * - If an authority is present, it MUST be prefixed by "//".
     * - The path can be concatenated without delimiters. But there are two
     *   cases where the path has to be adjusted to make the URI reference
     *   valid as PHP does not allow to throw an exception in __toString():
     *     - If the path is rootless and an authority is present, the path MUST
     *       be prefixed by "/".
     *     - If the path is starting with more than one "/" and no authority is
     *       present, the starting slashes MUST be reduced to one.
     * - If a query is present, it MUST be prefixed by "?".
     * - If a fragment is present, it MUST be prefixed by "#".
     *
     * @see http://tools.ietf.org/html/rfc3986#section-4.1
     * @return string
     */
    public function __toString()
    {
        return $this->_composeComponents(
                $this->_scheme, $this->_userInfo, $this->_host, $this->_port,
                $this->_path, $this->_query, $this->_fragment
        );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     *
     * @param array $args
     * @param array $flags
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    protected function _initializeUri( $uri )
    {
        $parts = parse_url( (string) $uri );
        if ( $parts === false )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            "Unable to parse URI: {$uri}",
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::EXCEPTION_CODE_LIBRARY_FAILURE );
        }
        $this->_parseUriParameters( $parts );
    }

    /**
     * Returns a new instance of the defined default class, as defined in the
     * class constant: [NEW_URI_DEFAULT_CLASS]. Arguments for initialization may be
     * passed into this method to update the class.
     * @param array $args (optional) Arguments used to create the new class instance.
     * @param array $flags (optional) Placeholder for future flag processing
     * @return \Psr\Http\Message\UriInterface The new class instance.
     * @throws \oroboros\core\utilities\exception\Exception Thrown if the current class implementing this trait is not an instance of \Psr\Http\Message\UriInterface
     */
    protected function _composeNewUriDefaultClass( array $args = array() )
    {
        $classname = get_class( $this );
        $new_uri = $this->_composeComponents( $args['scheme'],
            $args['user'] . ( isset( $args['pass'] )
            ? ':' . $args['pass']
            : null), $args['host'], $args['port'], $args['path'],
            $args['query'], $args['fragment'] );
        $class = new $classname( $new_uri );
        if ( !$class instanceof \Psr\Http\Message\UriInterface )
        {
            throw new \oroboros\core\utilities\exception\Exception( 'Instance of ['
            . $classname . '] is not a valid implementation of'
            . ' interface: \\Psr\\Http\\Message\\UriInterface',
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::EXCEPTION_CODE_LIBRARY_FAILURE );
        }
        $class->_initializeUri( $new_uri );
        return $class;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    private function _getObjectArguments()
    {
        $args = array(
            'uri' => $this->getHost(),
            'authority' => $this->getAuthority(),
            'user' => $this->_userInfo,
            'scheme' => $this->getScheme(),
            'host' => $this->getHost(),
            'port' => $this->getPort(),
            'path' => $this->getPath(),
            'query' => $this->getQuery(),
            'fragment' => $this->getFragment(),
        );
        return $args;
    }

    private function _parseUriParameters( $uri )
    {
        $this->_scheme = isset( $uri['scheme'] )
            ? $this->_filterScheme( $uri['scheme'] )
            : '';
        $this->_userInfo = isset( $uri['user'] )
            ? $uri['user']
            : '';
        $this->_host = isset( $uri['host'] )
            ? $this->_filterHost( $uri['host'] )
            : '';
        $this->_port = isset( $uri['port'] )
            ? $this->_filterPort( $uri['port'] )
            : null;
        $this->_path = isset( $uri['path'] )
            ? $this->_filterPath( $uri['path'] )
            : '';
        $this->_query = isset( $uri['query'] )
            ? $this->_filterQueryAndFragment( $uri['query'] )
            : '';
        $this->_fragment = isset( $uri['fragment'] )
            ? $this->_filterQueryAndFragment( $uri['fragment'] )
            : '';
        if ( isset( $uri['pass'] ) )
        {
            $this->_userInfo .= ':' . $uri['pass'];
        }
    }

    /**
     * @param string $scheme
     * @return string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the scheme is invalid.
     */
    private function _filterScheme( $scheme )
    {
        if ( !is_string( $scheme ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            'Scheme must be a string. Thrown at: '
            . __LINE__ . ' of ' . __METHOD__,
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        return strtolower( $scheme );
    }

    /**
     * @param string $host
     * @return string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the host is invalid.
     */
    private function _filterHost( $host )
    {
        if ( !is_string( $host ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            'Host must be a string. Thrown at: '
            . __LINE__ . ' of ' . __METHOD__,
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        return strtolower( $host );
    }

    /**
     * @param int|null $port
     * @return int|null
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the port is invalid.
     */
    private function _filterPort( $port )
    {
        if ( $port === null )
        {
            return null;
        }
        $port = (int) $port;
        if ( 1 > $port || 0xffff < $port )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Invalid port: %d. Must be between 1 and 65535', $port )
            . '. Thrown at: ' . __LINE__ . ' of ' . __METHOD__,
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $port;
    }

    /**
     * Filters the path of a URI
     * @param string $path
     * @return string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the path is invalid.
     */
    private function _filterPath( $path )
    {
        if ( !is_string( $path ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException( 'Path must be a string' );
        }
        return preg_replace_callback(
            self::$_regex['path'],
            [
            $this,
            '_rawurlencodeMatchZero' ], $path
        );
    }

    /**
     * Filters the query string or fragment of a URI.
     * @param string $str
     * @return string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the query or fragment is invalid.
     */
    private function _filterQueryAndFragment( $str )
    {
        if ( !is_string( $str ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException( 'Query and fragment must be a string' );
        }
        return preg_replace_callback(
            self::$_regex['queryAndFragment'],
            [
            $this,
            '_rawurlencodeMatchZero' ], $str
        );
    }

    private function _rawurlencodeMatchZero( array $match )
    {
        return rawurlencode( $match[0] );
    }

    private function _validateState()
    {
        if ( $this->host === '' && ($this->scheme === 'http' || $this->scheme ===
            'https') )
        {
            $this->host = static::HTTP_DEFAULT_HOST;
        }
        if ( $this->getAuthority() === '' )
        {
            if ( 0 === strpos( $this->path, '//' ) )
            {
                throw new \oroboros\core\utilities\exception\InvalidArgumentException( 'The path of a URI without an authority must not start with two slashes "//"' );
            }
            if ( $this->scheme === '' && false !== strpos( explode( '/',
                        $this->path, 2 )[0], ':' ) )
            {
                throw new \oroboros\core\utilities\exception\InvalidArgumentException( 'A relative URI must not have a path beginning with a segment containing a colon' );
            }
        } elseif ( isset( $this->path[0] ) && $this->path[0] !== '/' )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException( 'The path of a URI with an authority must start with a slash "/" or be empty' );
        }
    }

    /**
     * Composes a URI reference string from its various components.
     * PSR-7 UriInterface treats an empty component the same as a missing component as
     * getQuery(), getFragment() etc. always return a string. This explains the slight
     * difference to RFC 3986 Section 5.3.
     *
     * Another adjustment is that the authority separator is added even when the authority is missing/empty
     * for the "file" scheme. This is because PHP stream functions like `file_get_contents` only work with
     * `file:///myfile` but not with `file:/myfile` although they are equivalent according to RFC 3986. But
     * `file:///` is the more common syntax for the file scheme anyway (Chrome for example redirects to
     * that format).
     *
     * @param string $scheme
     * @param string $authority
     * @param string $path
     * @param string $query
     * @param string $fragment
     *
     * @return string Returns the full semantically correct uri if the request is http. If the request is cli, it returns the top level fully qualified path to the script originally called.
     * @final
     * @link https://tools.ietf.org/html/rfc3986#section-5.3
     */
    private function _composeComponents( $scheme, $user, $host, $port, $path,
        $query, $fragment )
    {
        //Check for CLI specific requests
        if (
            ( in_array( $host,
                array(
                '127.0.0.1',
                '::1' ) ) && ( in_array( $path,
                array(
                '',
                '/',
                null ) ) ) )
            || ( in_array( $path,
                array(
                '127.0.0.1',
                '::1' ) ) && ( in_array( $host,
                array(
                '',
                null ) ) ) )
        )
        {
            //This is a CLI request. Return the original script name.
            //This will be neccessary for shell scripts that need to reference
            //the real path of the file referenced when providing passthroughs
            //to the internal workings of the system on the PHP side.
            return $_SERVER['SCRIPT_NAME'];
        }
        $uri = '';
        // weak type checks to also accept null until we can add scalar type hints
        if ( $user != '' || $scheme === 'file' )
        {
            $uri .= $user . '@';
        }
        $uri .= $host;
        if ( !is_null( $port ) && $port != '' )
        {
            $uri .= ':' . $port;
        }
        if ( $path != '' )
        {
            $uri = rtrim( $uri, '/' ) . '/' . ltrim( $path, '/' );
        }
        if ( $query != '' )
        {
            $uri .= '?' . $query;
        }
        if ( $fragment != '' )
        {
            $uri .= '#' . $fragment;
        }
        if ( $scheme != '' )
        {
            $uri = $scheme . '://' . $uri;
        }
        return $uri;
    }

}
