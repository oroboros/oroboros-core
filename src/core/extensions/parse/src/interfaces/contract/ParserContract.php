<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\parse\interfaces\contract;

/**
 * <Parser Utility Contract Interface>
 * This contract interface designates the methods required for a valid parser.
 * Parsers are tasked with converting data to a common array format,
 * and packaging it into a container.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage parser
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface ParserContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract
{

    /**
     * <Parser Constructor>
     * The parser should receive the subject it is intended to parse upon construction.
     * The subject may be any of the following:
     *
     * - A string
     * - A readable stream resource
     * - An array
     * - An instance of \oroboros\collection\interfaces\contract\CollectionContract
     * - A readable file
     *
     * This pointer will be stored internally and NOT parsed until requested,
     * or the parse method is manually called.
     * This prevents excessive memory overhead on large data sets.
     *
     * @param mixed $subject
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an unobtainable subject is passed.
     */
    public function __construct( $subject );

    /**
     * <Parse Method>
     * This method may be manually called to parse data into memory internally.
     * This step does not need to be manually taken, as obtaining the
     * parsed content should call this method automatically if it has
     * not been called already. It may be called manually if an operation
     * is expected to access the return data set numerous times to prevent
     * the weight of re-parsing the data multiple times.
     * @return void
     * @throws \oroboros\core\utilities\exception\DomainException if an error during parsing occurs due to an invalid data format
     */
    public function parse();

    /**
     * <Cast Method>
     * Casts the parsed data to the expected output type and
     * returns the resulting format as a string.
     *
     * This is useful when originally supplied with an array or container,
     * and it needs to be expressed in the correct format.
     *
     * @return string
     * @throws \oroboros\core\utilities\exception\DomainExceptionif the supplied data array or container was not originally a valid format for the expected type.
     */
    public function cast();

    /**
     * <Reset Method>
     * This method releases the local copy of the parsed data, and sets the parse flag to false.
     * This can be called on large data sets to release them back to their source,
     * which is useful to conserve memory if the source is a resource or file,
     * and the data set is significant.
     *
     * This method should never throw an error of any sort.
     *
     * @return void
     */
    public function reset();

    /**
     * <Parsed Data Getter Method>
     * This method will obtain a copy of the parsed data set.
     * If it has not already been parsed, it will be parsed when
     * this method is called. In the event that it is not parsed when
     * this is called, the default behavior is to release the data back
     * to it's source after returning the parsed data. If you would like
     * to retain the parsed data in memory in this object to avoid the
     * weight of re-parsing it, pass true into this method, and it will
     * be retained. It may then be released manually by calling reset().
     *
     * @param bool $retain if true, the data will be retained in memory if it was not already after parsing. Default is false. Has no effect if the data is already parsed.
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if an error during parsing occurs due to an invalid data format
     */
    public function getParsed( $retain = false );

    /**
     * <Unparsed Data Getter Method>
     * This method will return the original source provided unmodified by this object.
     * This does not guarantee that the source was not externally modified if it is a
     * resource or a file, but this object will consider it's original source
     * immutable within it's own scope.
     * @return mixed
     */
    public function getOriginal();
}
