<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\parse\traits;

/**
 * <Docblock Parser Trait>
 * Parses docblock comments into a readable array of values,
 * respecting properties set in the comments.
 *
 * Classes implementing this trait MUST set their
 * class constant CLASS_SCOPE to the following value:
 *
 * \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_PARSER_DOCBLOCK
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage parser
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\parser\ParserContract
 */
trait ClassConstantDocBlockTrait
{

    use DocBlockTrait;

    /**
     * <Class Constant Docblock Parse Logic Method>
     * This method parses a class or interface, and extracts an array of
     * docblock comments from the class constants.
     * @param string $data
     * @param mixed $options (optional) no-op
     * @return array
     * @throws \oroboros\core\utilities\exception\DomainException if the provided data set is not valid for the declared scope of the parser.
     */
    protected function _parseData( $data, $options = null )
    {
        $docblocks = self::_classConstantDocBlockGetDocblocks( $data );
        try
        {
            $comments = self::_classConstantDocBlockGetDocblocks( $data );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            $e->getMessage(), $e->getCode(), $e );
        }
        return $comments;
    }

    /**
     * <Docblock Cast Logic Method>
     * This method casts an api array to an array of semantically correct
     * class constant docblock comments, with constant declarations equal to
     * the keys in fullcaps and canonicalized with underscores.
     *
     * The constant value will be null by default if no value key is provided.
     * If a value key is provided, it will not render back into the original docblock output.
     *
     * This can be used for programmatically generating docblock comments
     * for functions, methods, properties, constants, etc.
     *
     * @param \oroboros\core\interfaces\contract\libraries\collection\ContainerContract $data
     * @param mixed $options (optional) no-op
     * @return string
     */
    protected function _castData( $data, $options = null )
    {
        $src = '';
        foreach ( $data as
            $constant =>
            $comment )
        {
            $value = 'null';
            if ( array_key_exists( 'value', $comment ) )
            {
                $value = $comment['value'];
                unset( $comment['value'] );
            }
            $const = self::_classConstantDocBlockCastConstant( $constant,
                    $value );
            $src .= $this->_castDocBlockData( new \oroboros\collection\Collection( $comment ) )
                . PHP_EOL . $const . PHP_EOL . PHP_EOL;
        }
        return $src;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Parses the class and returns the docblock comments for the class constants
     * in the same format as the DocBlock. The value of the class constant
     * will be set under the key "value" in the result for reverse parsing back
     * into valid constants with identical docblock comments.
     * @param string $class
     * @return array
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a subject that cannot have class constants is passed
     */
    private static function _classConstantDocBlockGetDocblocks( $class )
    {
        try
        {
            $reflector = new \ReflectionClass( $class );
            if ( $reflector->isTrait() )
            {
                throw new \ReflectionException( 'Traits cannot have class constants' );
            }
        } catch ( \ReflectionException $e )
        {
            //this is not a class or interface,
            //and cannot have class constants.
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'class|interface',
                (is_string( $class ) && trait_exists( $class )
                    ? 'trait'
                    : gettype( $class ) ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS,
            $e );
        }
        //Result is valid, parse the content.
        $content = file_get_contents( $reflector->getFileName() );
        $tokens = token_get_all( $content );
        $comments = self::_classConstantDocBlockParseTokens( $tokens );
        $enum = new \oroboros\enum\InterfaceEnumerator( $class );
        $results = array();
        foreach ( $comments as
            $constant =>
            $comment )
        {
            $parser = new \oroboros\parse\DocBlock( $comment );
            $results[$constant] = $parser->getParsed()->toArray();
            $results[$constant]['value'] = $enum[$constant];
        }
        return $results;
    }

    /**
     * Parses the tokens from the file contents,
     * and returns the docblock comments for class
     * constants.
     * @param array $tokens
     * @return array
     */
    private static function _classConstantDocBlockParseTokens( $tokens )
    {
        $doc = null;
        $isConst = false;
        $comments = array();
        foreach ( $tokens as
            $token )
        {
            if ( count( $token ) <= 1 )
            {
                continue;
            }

            list($tokenType, $tokenValue) = $token;

            switch ( $tokenType )
            {
                // ignored tokens
                case T_WHITESPACE:
                case T_COMMENT:
                    break;

                case T_DOC_COMMENT:
                    $doc = $tokenValue;
                    break;

                case T_CONST:
                    $isConst = true;
                    break;

                case T_STRING:
                    if ( $isConst )
                    {
                        $comments[$tokenValue] = $doc;
                    }
                    $doc = null;
                    $isConst = false;
                    break;

                // all other tokens reset the parser
                default:
                    $doc = null;
                    $isConst = false;
                    break;
            }
        }
        return $comments;
    }

    /**
     * Casts a constant back to a PHP declaration from the provided values.
     * Honors defined constants, references to class constants,
     * integers, booleans, and nulls correctly.
     * @param string $constant
     * @param string $value
     * @return string
     */
    private static function _classConstantDocBlockCastConstant( $constant,
        $value )
    {
        if (
            ( preg_match( \oroboros\regex\interfaces\enumerated\Regex::REGEX_NAMESPACE,
                $value ) ) //do not string wrap class constants
            || (!defined( $value ) //do not string wrap known constants
            && !( in_array( $value,
                array(
                'TRUE',
                'true',
                'FALSE',
                'false',
                'NULL',
                'null' ) ) ) //do not string wrap valid language constructs
            && !( (string) intval( $value ) === $value ) //do not string wrap integers
            ) )
        {
            $value = '"' . str_replace( '\\', '\\\\', $value ) . '"';
        }
        return 'const ' . $constant . ' = ' . $value . ';';
    }
}
