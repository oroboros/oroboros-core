<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\parse\traits;

/**
 * <Docblock Parser Trait>
 * Parses docblock comments into a readable array of values,
 * respecting properties set in the comments.
 *
 * Classes implementing this trait MUST set their
 * class constant CLASS_SCOPE to the following value:
 *
 * \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_PARSER_DOCBLOCK
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage parser
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\parser\ParserContract
 */
trait DocBlockTrait
{

    use ParserTrait;

    /**
     * <Docblock Parse Logic Method>
     * This method parses a docblock comment string into an array.
     * @param string $data
     * @param mixed $options (optional) no-op
     * @return array
     * @throws \oroboros\core\utilities\exception\DomainException if the provided data set is not valid for the declared scope of the parser.
     */
    protected function _parseData( $data, $options = null )
    {
        $data_array = $this->_docBlockParserCleanupString( $data );
        return array(
            'comment' => $this->_docBlockParserGetTextComment( $data_array ),
            'attributes' => $this->_docBlockParserGetAttributes( $data_array )
        );
    }

    /**
     * <Docblock Cast Logic Method>
     * This method casts an array to a semantically correct docblock comment.
     * This can be used for programmatically generating docblock comments
     * for functions, methods, properties, constants, etc.
     *
     * @param \oroboros\core\interfaces\contract\libraries\collection\ContainerContract $data
     * @param mixed $options (optional) no-op
     * @return string
     */
    protected function _castData( $data, $options = null )
    {
        return $this->_castDocBlockData( $data );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Cleans up the docblock comment and returns an array separated by line,
     * with the prefixing comment designations and whitespace removed.
     * Secondary asterisks in valid comment lines will be retained in the event
     * they are meant to designate markdown.
     *
     * @param string $data
     * @return array
     */
    private function _docBlockParserCleanupString( $data )
    {
        $data_array = explode( PHP_EOL, $data );
        foreach ( $data_array as
            $key =>
            $value )
        {
            $value = trim( $value, ' ' );
            if ( strpos( $value, '*' ) === 0 )
            {
                $value = trim( substr( $value, 1 ), ' ' );
            }
            if ( $value === '' )
            {
                //retain line breaks
                $value = PHP_EOL;
            }
            if ( in_array( $value,
                    array(
                    '/*',
                    '/**',
                    '/' ) ) )
            {
                unset( $data_array[$key] );
            } else
            {
                $data_array[$key] = $value;
            }
        }
        return $data_array;
    }

    /**
     * Extracts the initial comment subset from
     * the cleaned up docblock comment array.
     * @param array $data
     * @return string
     */
    private function _docBlockParserGetTextComment( $data )
    {
        $comment_array = array();
        foreach ( $data as
            $line )
        {
            if ( strpos( $line, '@' ) === 0 )
            {
                //This is the end of the comment. All subsequent lines are parameter comments.
                break;
            }
            $comment_array[] = $line;
        }
        $comment = trim( implode( PHP_EOL, $comment_array ), PHP_EOL );
        //shorten redundant line breaks
        while ( strpos( $comment, PHP_EOL . PHP_EOL . PHP_EOL ) !== false )
        {
            $comment = str_replace( PHP_EOL . PHP_EOL . PHP_EOL,
                PHP_EOL . PHP_EOL, $comment );
        }
        return $comment;
    }

    /**
     * Returns an array of attributes in the docblock comment.
     * @param array $data
     * @return array
     */
    private function _docBlockParserGetAttributes( $data )
    {
        $attributes = array();
        $params = array();
        $is_subline = $last_key = false;
        //Remove initial comment
        foreach ( $data as
            $key =>
            $line )
        {
            if ( strpos( $line, '@' ) === false )
            {
                unset( $data[$key] );
                continue;
            }
            break;
        }
        //parse comments
        foreach ( $data as
            $line )
        {
            if ( !$is_subline && !strpos( $line, '@' ) === 0 )
            {
                //not an attribute
                continue;
            } elseif ( $is_subline && strpos( $line, '@' ) === 0 )
            {
                //end of previous subline
                $is_subline = false;
            }
            $key = $is_subline
                ? $last_key
                : substr( $line, 1, strpos( $line, ' ' ) - 1 );
            if ( !array_key_exists( $key, $attributes ) )
            {
                $attributes[$key] = array();
            }
            $last_key = $key;
            $subkey = ($is_subline
                ? count( $attributes[$key] ) - 1
                : count( $attributes[$key] ) );
            if ( !array_key_exists( $subkey, $attributes[$key] ) )
            {
                $attributes[$key][$subkey] = substr( $line,
                    strpos( $line, ' ' ) + 1 );
            } elseif ( array_key_exists( $subkey, $attributes[$key] ) && $key !==
                'param' )
            {
                $attributes[$key][$subkey] .= PHP_EOL . $line;
            }
            //special consideration has to be given to params
            if ( $key === 'param' && !$is_subline )
            {
                $attributes[$key][$subkey] = $this->_docBlockParserParseParam( $attributes[$key][$subkey] );
            } elseif ( $key === 'param' && $is_subline )
            {
                //package the extra lines of the param comment
                if ( !array_key_exists( 'comment', $attributes[$key][$subkey] ) )
                {
                    $attributes[$key][$subkey]['comment'] = '';
                }
                $attributes[$key][$subkey]['comment'] .= PHP_EOL . $line;
            }
            $is_subline = true;
        }
        return $attributes;
    }

    /**
     * Parses a raw param into a correctly formatted one.
     * @param string $param_raw
     * @return array
     */
    private function _docBlockParserParseParam( $param_raw )
    {
        $param = $type = $comment = '';
        $param_array = explode( ' ', $param_raw );
        foreach ( $param_array as
            $key =>
            $value )
        {
            if ( strpos( $value, '$' ) === 0 )
            {
                $param = ltrim( $value, '$' );
                if ( $key > 0 )
                {
                    $type = $param_array[$key - 1];
                    unset( $param_array[$key - 1] );
                }
                for ( $i = 1;
                    $i <= $key;
                    $i++ )
                {
                    unset( $param_array[$i] );
                }
                $comment = implode( ' ', $param_array );
                break;
            }
        }
        $result = array();
        if ( $param !== '' )
        {
            $result['name'] = $param;
        }
        if ( $type !== '' )
        {
            $result['type'] = $type;
        }
        if ( $comment !== '' )
        {
            $result['comment'] = $comment;
        }
        return $result;
    }

    /**
     * Casts an array back to a param declaration
     * @param array $param
     * @return string
     */
    private function _docBlockParserCastParam( $param )
    {
        $string = '';
        if ( array_key_exists( 'type', $param ) )
        {
            $string .= $param['type'] . ' ';
        }
        if ( array_key_exists( 'name', $param ) )
        {
            $string .= '$' . $param['name'] . ' ';
        }
        if ( array_key_exists( 'comment', $param ) )
        {
            if ( strpos( $param['comment'], PHP_EOL ) !== false )
            {
                $comment_array = explode( PHP_EOL, $param['comment'] );
                $string .= array_shift( $comment_array ) . PHP_EOL;
                foreach ( $comment_array as
                    $line )
                {
                    $string .= ' *     ' . $line . PHP_EOL;
                }
            } else
            {
                //normal comment line
                $string .= $param['comment'];
            }
        }
        $string = trim( $string, ' ' . PHP_EOL );
        return $string === ''
            ? null
            : $string;
    }

    /**
     * Casts doc block comment data back to a docblock string.
     * @param type $data
     * @return string
     */
    protected function _castDocBlockData( $data )
    {
        $result = '/**' . PHP_EOL;
        $prefix = ' * ';
        if ( $data->has( 'comment' ) )
        {
            foreach ( explode( PHP_EOL, $data['comment'] ) as
                $line )
            {
                $result .= $prefix . $line . PHP_EOL;
            }
        }
        if ( $data->has( 'attributes' ) )
        {
            foreach ( $data['attributes'] as
                $attribute =>
                $values )
            {
                foreach ( $values as
                    $subkey =>
                    $value )
                {
                    $result .= $prefix . '@' . $attribute . ' ' . ($attribute ===
                        'param'
                        ? $this->_docBlockParserCastParam( $value )
                        : implode( PHP_EOL . $prefix . '    ',
                            explode( PHP_EOL, $value ) ) ) . PHP_EOL;
                }
            }
        }
        $result .= ' */';
        return $result;
    }

}
