<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\parse\traits;

/**
 * <JSON Parser Trait>
 * Parses JSON data into a readable array of values.
 * This is a pretty simple operation, and this is only really necessary
 * for use in conjunction with a command pattern or similar.
 *
 * Classes implementing this trait MUST set their
 * class constant CLASS_SCOPE to the following value:
 *
 * \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_PARSER_JSON
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage parser
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\parser\ParserContract
 */
trait JsonTrait
{

    use ParserTrait;

    /**
     * <JSON Parse Logic Method>
     * This method parses a docblock comment string into an array.
     * @param string $data
     * @param mixed $options (optional) Any json parsing options
     * @return array
     * @throws \oroboros\core\utilities\exception\DomainException if the provided data set is not valid for the declared scope of the parser.
     */
    protected function _parseData( $data, $options = null )
    {
        if ( is_null( $options ) )
        {
            $options = 0;
        }
        try
        {
            $data = json_decode( $data, 1, 512, $options );
            $errcode = json_last_error();
            $errmsg = json_last_error_msg();
            if ( $errcode !== JSON_ERROR_NONE )
            {
                throw new Exception( $errmsg, $errcode );
            }
            return $data;
        } catch ( \Exception $e )
        {
            throw new \oroboros\core\utilities\exception\DomainException( sprintf( 'Parse error at [%s]. Error: [%s], with code: [%s]',
                __METHOD__, $e->getMessage(), $e->getCode() ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR,
            $e );
        }
    }

    /**
     * <JSON Cast Logic Method>
     * This method casts an array to a semantically correct docblock comment.
     * This can be used for programmatically generating docblock comments
     * for functions, methods, properties, constants, etc.
     *
     * @param \oroboros\core\interfaces\contract\libraries\collection\ContainerContract $data
     * @param mixed $options (optional) Any json casting options
     * @return string
     */
    protected function _castData( $data, $options = null )
    {
        if ( is_null( $options ) )
        {
            $options = 0;
        }
        try
        {
            $data = json_encode( $data->toArray(), $options );
            $errcode = json_last_error();
            $errmsg = json_last_error_msg();
            if ( $errcode !== JSON_ERROR_NONE )
            {
                throw new Exception( $errmsg, $errcode );
            }
            return $data;
        } catch ( \Exception $e )
        {
            throw new \oroboros\core\utilities\exception\DomainException( sprintf( 'Parse error at [%s]. Error: [%s], with code: [%s]',
                __METHOD__, $e->getMessage(), $e->getCode() ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR,
            $e );
        }
    }

}
