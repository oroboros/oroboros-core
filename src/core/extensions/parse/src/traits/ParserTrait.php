<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\parse\traits;

/**
 * <Parser Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage parser
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\parser\ParserContract
 */
trait ParserTrait
{
    use \oroboros\core\traits\core\BaselineTrait {
        \oroboros\core\traits\core\BaselineTrait::__construct as private _baselineConstruct;
    }
    /**
     * Represents the data source of the content to parse.
     * @var mixed
     */
    private $_parser_subject;

    /**
     * Represents the type of the subject, which determines
     * how the parser obtains the data from it.
     * @var string
     */
    private $_parser_subject_type;

    /**
     * Represents the parseable types of subject.
     * @var array
     */
    private static $_parser_valid_subject_types = array(
        'array',
        'collection',
        'string',
        'resource',
        'stream',
        'file'
    );

    /**
     * Represents the scope of the parser.
     * Parsers shipped with Oroboros Core use types defined in the ClassScopeApi
     * @see \oroboros\core\interfaces\enumerated\scope\ClassScope
     * @var string
     */
    private $_parser_scope = null;

    /**
     * Represents whether the content is currently stored in memory.
     * This can be released and re-parsed to save runtime memory on
     * large data sets as needed.
     * @var bool
     */
    private $_parser_is_parsed = false;

    /**
     * Represents whether the object has initialized as a parser cleanly.
     * @var bool
     */
    private $_parser_is_initialized = false;

    /**
     * Represents the parsed data set. This value will be an array
     * if the data has been parsed, or null if it is not currently parsed.
     * @var array
     */
    private $_parser_parsed = null;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\utilities\parser\ParserContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Parser Constructor>
     * The parser should receive the subject it is intended to parse upon construction.
     * The subject may be any of the following:
     *
     * - A string
     * - A readable stream resource
     * - An array
     * - An instance of \oroboros\collection\interfaces\contract\CollectionContract
     * - A readable file
     *
     * This pointer will be stored internally and NOT parsed until requested,
     * or the parse method is manually called.
     * This prevents excessive memory overhead on large data sets.
     *
     * @param mixed $subject
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an unobtainable subject is passed.
     */
    public function __construct( $subject )
    {
        $this->_initializeParser( $subject );
    }

    /**
     * <Parse Method>
     * This method may be manually called to parse data into memory internally.
     * This step does not need to be manually taken, as obtaining the
     * parsed content should call this method automatically if it has
     * not been called already. It may be called manually if an operation
     * is expected to access the return data set numerous times to prevent
     * the weight of re-parsing the data multiple times.
     * @return void
     * @param mixed $options (optional) Any parsing options usable by the individual parser
     * @throws \oroboros\core\utilities\exception\DomainException if an error during parsing occurs due to an invalid data format
     */
    public function parse( $options = null )
    {
        $this->_parserParse( $this->_parser_subject, $options );
    }

    /**
     * <Cast Method>
     * Casts the parsed data to the expected output type and
     * returns the resulting format as a string.
     *
     * This is useful when originally supplied with an array or container,
     * and it needs to be expressed in the correct format.
     *
     * The casted format does not need to be a 1:1 direct equivalent of the
     * original data set, but it MUST express all of the same expected
     * data equally.
     *
     * This method MUST provide all of the same information unmodified,
     * but MAY optimize the schema to express the exact same data more
     * efficiently if that is possible in the scope of the current parser.
     *
     * @return string
     * @param mixed $options (optional) Any casting options usable by the individual parser
     * @throws \oroboros\core\utilities\exception\DomainExceptionif the supplied data array or container was not originally a valid format for the expected type.
     */
    public function cast( $options = null )
    {
        return $this->_parserCast($options);
    }

    /**
     * <Reset Method>
     * This method releases the local copy of the parsed data, and sets the parse flag to false.
     * This can be called on large data sets to release them back to their source,
     * which is useful to conserve memory if the source is a resource or file,
     * and the data set is significant.
     *
     * This method should never throw an error of any sort.
     *
     * @return void
     */
    public function reset()
    {
        $this->_parserReset();
    }

    /**
     * <Parsed Data Getter Method>
     * This method will obtain a copy of the parsed data set.
     * If it has not already been parsed, it will be parsed when
     * this method is called. In the event that it is not parsed when
     * this is called, the default behavior is to release the data back
     * to it's source after returning the parsed data. If you would like
     * to retain the parsed data in memory in this object to avoid the
     * weight of re-parsing it, pass true into this method, and it will
     * be retained. It may then be released manually by calling reset().
     *
     * @param bool $retain if true, the data will be retained in memory if it was not already after parsing. Default is false. Has no effect if the data is already parsed.
     * @param mixed $options (optional) Any parsing options usable by the individual parser
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if an error during parsing occurs due to an invalid data format
     */
    public function getParsed( $retain = false, $options = null )
    {
        $reset = false;
        if ( !$this->_parser_is_parsed )
        {
            $reset = true;
            $this->parse($options);
        }
        $value = $this->_parser_parsed;
        if ( $reset && !$retain )
        {
            $this->reset();
        }
        return $value;
    }

    /**
     * <Unparsed Data Getter Method>
     * This method will return the original source provided unmodified by this object.
     * This does not guarantee that the source was not externally modified if it is a
     * resource or a file, but this object will consider it's original source
     * immutable within it's own scope.
     * @return mixed
     */
    public function getOriginal()
    {
        return $this->_parser_subject;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    protected function _initializeParser( $subject )
    {
        $this->_setParserScope( self::OROBOROS_CLASS_SCOPE );
        if (
            !( is_object( $subject ) && ( $subject instanceof \Psr\Http\Message\StreamInterface ) )
            && !( is_object( $subject ) && ( $subject instanceof \oroboros\collection\interfaces\contract\CollectionContract ) )
            && !( is_string( $subject ) )
            && !( is_resource( $subject ) )
            && !( is_array( $subject ) )
        )
        {
            \oroboros\Oroboros::log( 'warning',
                'Failed to load parser {class} because the given {subject} is not a valid format. Valid formats are {formats}.',
                array(
                'class' => get_class( $this ),
                'formats' => 'string|array|resource|\\Psr\\Http\\Message\\StreamInterface|\\oroboros\\collection\\interfaces\\contract\\CollectionContract',
                'subject' => is_scalar( $subject )
                    ? $subject
                    : ( is_null( $subject )
                        ? 'null'
                        : ( is_object( $subject )
                            ? get_class( $subject )
                            : gettype( $subject ) ) )
            ) );
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_PARAMETERS_MESSAGE,
                'string|array|resource|\\Psr\\Http\\Message\\StreamInterface|\\oroboros\\core\\contract\\libraries\\container\\CollectionContract',
                is_object( $subject )
                    ? get_class( $subject )
                    : gettype( $subject )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS
            );
        }
        $this->_parser_subject = $subject;
        $this->_parser_is_initialized = true;
    }

    /**
     * <Parser Scope Setter>
     * This method MUST be called immediately in the constructor.
     * The purpose of this method is to update the parser to deal
     * with its specific scope (eg: json, xml, csv, html, etc).
     * @param string $scope
     * @throws \oroboros\core\utilities\exception\LogicException if the scope for the object is already set
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided scope is not a string
     */
    protected function _setParserScope( $scope )
    {
        //Insure the scope was not already declared.
        if ( !is_null( $this->_parser_scope ) )
        {
            throw new \oroboros\core\utilities\exception\LogicException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_MESSAGE,
                __METHOD__, 'parser scope cannot be overridden once it is set' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC
            );
        }
        //insure the scope is a string.
        if ( !is_string( $scope ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $scope ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_parser_scope = $scope;
    }

    /**
     * This method must be overridden with logic to parse the specified format.
     * It will be called by internals of the class to complete the parse operation,
     * and then the correct packaging of the data into a container will be done after that.
     * This method should return an array, and throw the specified exception below
     * if it cannot complete parsing. The incoming data will always be a string.
     *
     * In order for the parser to correctly handle it's given scope,
     * extensions upon this trait should provide this method in order to
     * complete the conversion task. The source data will be obtained and
     * passed into this method prior to this, so this method only needs to be
     * concerned with the actual conversion, regardless of what source was provided
     * (eg: file, stream, resource, string, etc).
     * @param string $data
     * @return array
     * @throws \oroboros\core\utilities\exception\DomainException if the provided data set is not valid for the declared scope of the parser.
     */
    abstract protected function _parseData( $data, $options = null );

    /**
     * This method must be overridden with logic to cast to the specified format.
     * It will be called by internals of the class to complete the cast operation,
     * and will be fed a collection instance that must be cast to the specified data
     * format for the parser. If it is not possible to cast the data to the specified
     * format correctly, then this method MUST raise a DomainException.
     *
     * The collection instance represents an array of values.
     * It may be iterated directly and provides some support for searching
     * the array to alleviate redundant code, or you may retrieve the
     * original array from it by using $data->toArray(). The collection
     * itself is NOT an expression of the data, it is just a wrapper for
     * the data, and no reference to the collection should be included in
     * the parsed output.
     *
     * @param \oroboros\collection\interfaces\contract\CollectionContract
     * @return string
     * @throws \oroboros\core\utilities\exception\DomainException if the supplied data array or container was not originally a valid format for the expected type. Any DomainException is sufficient, the internal provided one is suggested to avoid possible outside collision.
     */
    abstract protected function _castData( $data, $options = null );

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    private function _parserReset()
    {
        $this->_parser_parsed = null;
        $this->_parser_is_parsed = false;
    }

    /**
     * Parses the subject and creates a collection of the result.
     * If the subject is already an array or collection, it will not be modified,
     * though arrays will be wrapped in a collection object.
     *
     * @param type $subject
     * @param mixed $options (optional) Any parsing options usable by the individual parser
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     * @throws \oroboros\core\utilities\exception\DomainException if the subject is not a parsable source format, of if the data is not valid for the declared scope of the parser.
     */
    private function _parserParse( $subject, $options = null )
    {
        //If already a container, return as passed.
        if ( is_object( $subject )
            && ( $subject instanceof \oroboros\collection\interfaces\contract\CollectionContract ) )
        {
            $this->_parser_parsed = $subject;
            return;
        }
        //If a stream interface, fetch the stream contents and parse the result.
        if ( is_object( $subject )
            && ( $subject instanceof \Psr\Http\Message\StreamInterface ) )
        {
            $this->_parser_parsed = $this->_parserConvertStream( $subject );
            return;
        }
        //Two string use cases
        if ( is_string( $subject ) )
        {
            //If this is a file, fetch the contents of the file and parse the result.
            if ( is_file( $subject ) )
            {
                $this->_parser_parsed = $this->_parserConvertFile( $subject );
                return;
            }
            //If not a file, parse as a string.
            $this->_parser_parsed = $this->_parserConvertString( $subject );
            return;
        }
        //If already an array, wrap in a container and return the result.
        if ( is_array( $subject ) )
        {
            $this->_parser_parsed = $this->_parserConvertArray( $subject );
            return;
        }
        if ( is_resource( $subject ) )
        {
            $this->_parser_parsed = $this->_parserConvertResource( $subject );
            return;
        }
        //No other valid instances
        throw new \oroboros\core\utilities\exception\DomainException(
        sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_PARAMETERS_MESSAGE,
            'string|array|resource|\\Psr\\Http\\Message\\StreamInterface|\\oroboros\\collection\\interfaces\\contract\\CollectionContract',
            is_object( $subject )
                ? get_class( $subject )
                : gettype( $subject )  ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS
        );
    }

    /**
     * Casts the current subject back to the expected format.
     * If the subject is not already parsed, it will parse it first.
     * This is only really needed if an array was supplied that needs
     * to be cast to the desired format. In most other cases you will
     * get an equivalent to what you originally supplied, though it may
     * apply some cleanup depending on the original format, and might be
     * used to optimize a data set as well.
     *
     * @return string
     * @param mixed $options (optional) Any casting options usable by the individual parser
     * @throws \oroboros\core\utilities\exception\DomainException if the subject is not a castable for the declared scope of the parser.
     */
    private function _parserCast( $options = null )
    {
        if ( !$this->_parser_is_parsed )
        {
            $this->parse();
            $data = $this->_parser_parsed;
            $this->reset();
        } else
        {
            $data = $this->_parser_parsed;
        }
        return $this->_castData( $data, $options );
    }

    /**
     * Converts an array into a collection.
     * @param array $subject
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if the provided subject is not an array
     */
    private function _parserConvertArray( $subject )
    {
        if ( !is_array( $subject ) )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_PARSER_FAILURE_MESSAGE,
                get_class( $this ), 'subject is not a valid array' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR
            );
        }
        return new \oroboros\collection\Collection( $subject );
    }

    /**
     * Parses a string, and converts it into a collection.
     * All other parser methods that actually require parsing eventually
     * point to this method.
     * @param string $subject
     * @param mixed $options (optional) Any casting options usable by the individual parser
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if the provided subject does not adhere to the specified format of the parser
     */
    private function _parserConvertString( $subject, $options = null )
    {
        if ( !is_string( $subject ) )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_PARSER_FAILURE_MESSAGE,
                get_class( $this ), 'subject is not a string' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR
            );
        }
        return new \oroboros\collection\Collection( $this->_parseData( $subject, $options = null ) );
    }

    /**
     * Parses a resource, and converts it into a collection.
     * @param string $subject
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if the provided subject does not adhere to the specified format of the parser
     */
    private function _parserConvertResource( $subject )
    {
        if ( !is_resource( $subject ) )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_PARSER_FAILURE_MESSAGE,
                get_class( $this ), 'subject is not a valid resource' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR
            );
        }
        $stream = new \oroboros\message\Stream( $subject );
        return $this->_parserConvertString( (string) $stream );
    }

    /**
     * Parses a stream wrapper, and converts it into a collection.
     * @param string $subject
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if the provided subject does not adhere to the specified format of the parser
     */
    private function _parserConvertStream( $subject )
    {
        if ( !( $subject instanceof \Psr\Http\Message\StreamInterface ) )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_PARSER_FAILURE_MESSAGE,
                get_class( $this ),
                sprintf( 'subject is not a valid collection. must be an instance of %s',
                    '\\Psr\\Http\\Message\\StreamInterface' ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR
            );
        }
        //The string method will handle the result, which will be cast
        //to a string as its parameter as per the Psr-7 spec.
        return $this->_parserConvertString( (string) $subject );
    }

    /**
     * Parses the contents of a file, and converts it into a collection.
     * @param string $subject A valid file that is readable.
     * @return \oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\DomainException if the provided subject does not adhere to the specified format of the parser
     */
    private function _parserConvertFile( $subject )
    {
        if ( !is_file( $subject ) )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_PARSER_FAILURE_MESSAGE,
                get_class( $this ), 'subject is not a valid file' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR
            );
        }
        //cannot parse an unreadable file
        if ( !is_readable( $subject ) )
        {
            throw new \oroboros\core\utilities\exception\DomainException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_PARSER_FAILURE_MESSAGE,
                get_class( $this ), 'subject file is not readable' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_DATA_SCHEMA_PARSER_ERROR
            );
        }
        return $this->_parserConvertString( file_get_contents( $subject ) );
    }

}
