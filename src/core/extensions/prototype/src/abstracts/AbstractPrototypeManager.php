<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\prototype\abstracts;

/**
 * <Abstract Prototyper>
 * Defines the core methods for the prototyper.
 * This class is used in conjunction with factories
 * for prototyping multiple objects quickly.
 *
 * Any object requested in a factory that has the prototypical interface will
 * first be checked against the prototyper for an existing template,
 * and request a clone instead of creating a new object.
 *
 * If the object does not exist, it will be created,
 * but will also be registered with the prototyper if it
 * has the prototypical interface, so it will not need to
 * be created again.
 *
 * It is important to note that if a class has the prototypical interface,
 * it MUST have all of it's initial parameterization complete on it's first
 * instantiation, because it's constructor will not fire again.
 *
 * Additional instantiation must then be done in the __clone() method,
 * which fires every time a new object is created from the prototype.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractPrototypeManager
    extends \oroboros\core\abstracts\libraries\AbstractLibrary
    implements \oroboros\prototype\interfaces\contract\PrototypeManagerContract
{

    use \oroboros\prototype\traits\PrototypeManagerTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\LibraryClassTypes::CLASS_TYPE_LIBRARY_MANAGER;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_PROTOTYPE_MANAGER;
    const OROBOROS_API = '\\oroboros\\prototype\\interfaces\\api\\PrototypeApi';

}
