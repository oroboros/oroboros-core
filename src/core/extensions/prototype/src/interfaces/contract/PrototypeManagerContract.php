<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\prototype\interfaces\contract;

/**
 * <Prototype Manager Contract Interface>
 * Enforces methods for providing a simplified, robust way to manage multiple
 * scoped prototypers and handle prototype setting and getting by identifying
 * key rather than full class name.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @category prototype
 * @package oroboros/core
 * @subpackage prototype
 * @version 0.2.5
 * @since 0.2.5
 */
interface PrototypeManagerContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract,
 \oroboros\core\interfaces\contract\patterns\creational\PrototypicalContract,
 \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract
{
    /**
     * <Protype Manager Initialization Method>
     * Initializes the prototype manager and gets it ready for use.
     * @param type $params (optional) Any custom parameters to supply to the prototype manager
     * @param type $dependencies (optional) A valid prototyper template, or any starting prototypers to initialize with.
     * @param type $flags (optional) Any flags that the prototyper is expected to recognize
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If any invalid parameter, dependency, or flag is supplied
     * @throws \oroboros\core\utilities\exception\RuntimeException If initialization cannot resolve
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null );

    /**
     * <Prototype Manager Class Check Method>
     * Checks if the given class has a prototypical instance registered.
     * @param string|object $class A class name or object to check for a
     *     prototypical instance of within the registered prototypers, or the
     *     specified prototyper if the second parameter is passed.
     * @param type $prototyper (optional) If supplied, will check only within
     *     the specified prototyper if that prototyper exists in the registry
     *     of prototyper instances.
     * @return bool
     */
    public function hasPrototype( $class, $prototyper = null );

    /**
     * <Prototype Manager Prototypical Check Method>
     * Reurns a boolean determination as to whether a given class name
     * or object is prototypically compatible.
     * @param type $class
     * @return bool
     */
    public function checkIfPrototypical( $class );

    /**
     * <Prototype Manager Prototyper Setter Method>
     * Sets an externally supplied prototyper into the manager's prototyper registry.
     * @param string $id
     * @param \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract $prototyper
     * @param type $replace (optional) If true, will replace the current instance of the prototyper if it exists. Default false.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided prototyper does not honor its contract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to set an internal default prototyper
     * @throws \oroboros\core\utilities\exception\RuntimeException If replace is false and the given id already exists
     */
    public function setPrototyper( $id, $prototyper, $replace = false );

    /**
     * <Prototype Manager New Prototyper Instance Setter Method>
     * Creates a new prototyper instance using the template prototyper.
     * @param string $id The id for the new prototyper
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to set an internal default prototyper
     * @throws \oroboros\core\utilities\exception\RuntimeException If replace is false and the given id already exists
     */
    public function addPrototyper( $id );

    /**
     * <Prototype Manager Prototyper Getter Method>
     * Returns a clone of a currently held prototyper if it exists, or false if it does not exist.
     * @param string $id The slug identifier of the prototyper to return
     * @return bool|\oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     */
    public function getPrototyper( $id );

    /**
     * <Prototype Manager Prototyper Check Method>
     * Returns a boolean determination as to whether a given id is
     * associated with a currently held prototyper.
     * @param string $id
     * @return bool
     */
    public function hasPrototyper( $id );

    /**
     * <Prototype Manager Prototyper List Method>
     * Returns a list of the current slug identifiers for
     * all currently held prototypers.
     * @return array
     */
    public function listPrototypers();

    /**
     * <Prototype Manager Prototyper Reset Method>
     * Resets a prototyper by the slug identifier of the instance currently
     * held by the prototype manager.
     * @param type $id The slug identifier of the prototyper to reset
     * @return void
     */
    public function resetPrototyper( $prototyper );

    /**
     * <Prototype Manager Prototyper Unsetter Method>
     * Deletes a current prototyper instance if it exists.
     * @param string $id
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to delete the default prototyper
     */
    public function deletePrototyper( $id );

    /**
     * <Prototype Manager Prototypical Setter Method>
     * Sets an object as a prototypical template for prototype operations.
     * @param string $id
     * @param \oroboros\core\interfaces\contract\patterns\creational\PrototypicalContract $object
     * @param string $prototyper (optional) If supplied, will set the given
     *     instance into a specific prototyper instance. If not supplied, will
     *     set into the default prototyper.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given prototyper does not exist
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given object is not prototypical
     */
    public function setPrototypical( $id, $object, $prototyper = null );

    /**
     * <Prototype Manager Prototyical Instance Getter Method>
     * Returns a prototypical clone of a given prototypical id,
     * or false if not found.
     * @param string $id The slug identifier of the prototypical instance
     *     to retrieve.
     * @param string $prototyper (optional) If supplied, will check the
     *     given prototyper for the specified instance. If not supplied,
     *     will check the default prototyper instance.
     * @return bool|\oroboros\core\interfaces\contract\patterns\creational\PrototyicalContract
     */
    public function getPrototypical( $id, $prototyper = null );

    /**
     * <Prototype Manager Prototypical Instance Check Method>
     * Checks if a given slug identifier has an instance stored in the
     * given prototyper instance.
     * @param string $id The slug identifier of the
     *     prototyical instance to check.
     * @param string $prototyper (optional) If supplied, will check within
     *     the given prototyper. If not supplied, will check within the
     *     default prototyper.
     * @return bool|string Returns the class name of the given id if it exists,
     *     otherwise returns false.
     */
    public function hasPrototypical( $id, $prototyper = null );

    /**
     * <Prototype Manager Prototypical List Method>
     * Returns a list of all prototypical instances currently held in all
     * prototypers currently registered. If the optional prototyper parameter
     * is passed, the check will occur only within the given prototyper,
     * and return an associative array of the slug identifiers as keys and
     * the class names of the prototypical instances as values.
     *
     * If the prototyper parameter is not supplied, all prototypical instances
     * will be returned in an associative array, where the key is the slug
     * identifier of the prototyper, and the value is an associative array
     * with keys corresponding to the slug identifiers of the prototypical
     * instances, and values corresponding to their class names.
     *
     * If a prototyper is specified that does not exist, returns false.
     *
     * @param string $prototyper (optional)
     * @return array|bool
     */
    public function listPrototypical( $prototyper = null );

    /**
     * <Prototype Manager Prototypical Unsetter Method>
     * Unsets a given prototypical instance from the specified prototyper.
     * If the optional prototyper parameter is supplied, it will unset the
     * given id from that prototyper. If not supplied, it will unset it from
     * the default prototyper.
     *
     * @param string $id The slug identifier of the prototypical
     *     instance to remove.
     * @param string $prototyper (optional) If supplied, will unset the
     *     prototypical instance from the specified prototyper. If not supplied,
     *     will unset it from the default prototyper.
     * @return void
     */
    public function deletePrototypical( $id, $prototyper = null );

    /**
     * <Prototype Manager Reset Method>
     * Resets the prototype manager to its default state,
     * which will also reset or remove all of its redundant
     * prototypers. The default prototyper will be restored
     * to an empty state.
     * @return void
     */
    public function reset();
}
