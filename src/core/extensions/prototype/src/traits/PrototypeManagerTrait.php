<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\prototype\traits;

/**
 * <Prototype Manager Trait>
 * Provides a simplified api for prototyping objects,
 * and categorizing prototypers by independent scopes.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage prototype
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\libraries\prototype\PrototypeManagerContract
 */
trait PrototypeManagerTrait
{

    use \oroboros\core\traits\patterns\behavioral\ManagerTrait
    {
        \oroboros\core\traits\patterns\behavioral\ManagerTrait::initialize as private manager_initialize;
    }
    use \oroboros\core\traits\patterns\creational\PrototypicalTrait;

    /**
     * Represents the template object for new prototypers. This will be populated
     * automatically during initialization if not provided through dependency
     * injection.
     * @var \oroboros\core\interfaces\contract\libraries\prototype\PrototypeContract
     */
    private $_prototyper_template;

    /**
     * Represents the internal registry of currently active prototyper instances.
     * @var \oroboros\collection\interfaces\contract\CollectionContract
     */
    private $_prototyper_instances;

    /**
     * Represents the identifying keys for the current set of prototyper instances.
     * @var \oroboros\collection\interfaces\contract\CollectionContract
     */
    private $_prototyper_slug_identifiers;

    /**
     * Represents the slug identifiers of the prototypical instance slug identifiers,
     * and which prototyper contains the correct instance of them.
     * @var \oroboros\collection\interfaces\contract\CollectionContract
     */
    private $_prototypical_slug_index;

    /**
     * Represents the contract interface that
     * insures prototypical compatibility.
     *
     * Any class implementing the __clone magic method
     * can implement this interface successfully to enable
     * prototypical functionality for it as a valid subject.
     *
     * @var string
     */
    private $_prototype_prototypical_contract = '\\oroboros\\core\\interfaces\\contract\\patterns\\creational\\PrototypicalContract';

    /**
     * Represents the valid prototypical class that the prototyper
     * prototypes instances of.
     * @var string
     */
    private $_prototype_prototypical_class = __CLASS__;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\libraries\prototype\PrototypeManagerContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Protype Manager Initialization Method>
     * Initializes the prototype manager and gets it ready for use.
     * @param type $params (optional) Any custom parameters to supply to the prototype manager
     * @param type $dependencies (optional) A valid prototyper template, or any starting prototypers to initialize with.
     * @param type $flags (optional) Any flags that the prototyper is expected to recognize
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If any invalid parameter, dependency, or flag is supplied
     * @throws \oroboros\core\utilities\exception\RuntimeException If initialization cannot resolve
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineSetParameterPersistence( true );
        $this->_baselineSetDependencyPersistence( true );
        $this->_baselineSetFlagPersistence( true );
        $this->_registerPrototypeTask( '_prototypeManagerCleanupClone' );
        $this->_baselineRegisterInitializationTask( '_prototypeManagerPostInitialization' );
        $this->_baselineRegisterDestructorTask( '_prototypeManagerDestructorGarbageCollection' );
        $this->manager_initialize( $params, $dependencies, $flags );
    }

    /**
     * <Prototype Manager Class Check Method>
     * Checks if the given class has a prototypical instance registered.
     * @param string|object $class A class name or object to check for a
     *     prototypical instance of within the registered prototypers, or the
     *     specified prototyper if the second parameter is passed.
     * @param type $prototyper (optional) If supplied, will check only within
     *     the specified prototyper if that prototyper exists in the registry
     *     of prototyper instances.
     * @return bool
     */
    public function hasPrototype( $class, $prototyper = null )
    {
        ;
    }

    /**
     * <Prototype Manager Prototypical Check Method>
     * Reurns a boolean determination as to whether a given class name
     * or object is prototypically compatible.
     * @param type $class
     * @return bool
     */
    public function checkIfPrototypical( $class )
    {
        return $this->_validate( 'instance-of', $class,
                '\\oroboros\\prototype\\interfaces\\contract\\PrototypicalContract' );
    }

    /**
     * <Prototype Manager Prototyper Setter Method>
     * Sets an externally supplied prototyper into the manager's prototyper registry.
     * @param string $id
     * @param \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract $prototyper
     * @param type $replace (optional) If true, will replace the current instance of the prototyper if it exists. Default false.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided prototyper does not honor its contract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to set an internal default prototyper
     * @throws \oroboros\core\utilities\exception\RuntimeException If replace is false and the given id already exists
     */
    public function setPrototyper( $id, $prototyper, $replace = false )
    {
        if ( $id === '__default' )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Cannot override the default'
                    . ' prototyper after initialization. Please provide a '
                    . 'template prototyper instance in the initialization '
                    . 'parameters if you wish to use a different object as the '
                    . 'default prototyper.', __METHOD__ ),
                self::_getExceptionCode( 'logic-bad-parameters' ) );
        }
        try
        {
            $this->_prototypeManagerAddPrototyper( $id, $prototyper );
        } catch ( \oroboros\core\utilities\exception\RuntimeException $e )
        {
            if ( !$replace )
            {
                throw $e;
            }
            $this->deletePrototyper( $id );
            $this->_prototypeManagerAddPrototyper( $id, $prototyper );
        }
    }

    /**
     * <Prototype Manager New Prototyper Instance Setter Method>
     * Creates a new prototyper instance using the template prototyper.
     * @param string $id The id for the new prototyper
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to set an internal default prototyper
     * @throws \oroboros\core\utilities\exception\RuntimeException If replace is false and the given id already exists
     */
    public function addPrototyper( $id )
    {
        $this->setPrototyper( $id, clone $this->_prototyper_template );
    }

    /**
     * <Prototype Manager Prototyper Getter Method>
     * Returns a clone of a currently held prototyper if it exists,
     * or false if it does not exist.
     * @param string $id The slug identifier of the prototyper to return
     * @return bool|\oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     */
    public function getPrototyper( $id )
    {
        self::_validate( 'type-of', $id, 'string', true );
        if ( !$this->_prototyper_instances->has( $id ) )
        {
            return false;
        }
        return clone $this->_prototyper_instances[$id];
    }

    /**
     * <Prototype Manager Prototyper Check Method>
     * Returns a boolean determination as to whether a given id is
     * associated with a currently held prototyper.
     * @param string $id
     * @return bool
     */
    public function hasPrototyper( $id )
    {
        return is_string( $id ) && $this->_prototyper_instances->has( $id );
    }

    /**
     * <Prototype Manager Prototyper List Method>
     * Returns a list of the current slug identifiers for
     * all currently held prototypers.
     * @return array
     */
    public function listPrototypers()
    {
        return array_keys( $this->_prototyper_slug_identifiers->toArray() );
    }

    /**
     * <Prototype Manager Prototyper Reset Method>
     * Resets a prototyper by the slug identifier of the instance currently
     * held by the prototype manager.
     * @param type $id The slug identifier of the prototyper to reset
     * @return void
     */
    public function resetPrototyper( $prototyper )
    {

    }

    /**
     * <Prototype Manager Prototyper Unsetter Method>
     * Deletes a current prototyper instance if it exists.
     * @param string $id
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to delete the default prototyper
     */
    public function deletePrototyper( $id )
    {
        if ( $id === '__default' )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Cannot delete the default'
                    . ' prototyper.', __METHOD__ ),
                self::_getExceptionCode( 'logic-bad-parameters' ) );
        }
    }

    /**
     * <Prototype Manager Prototypical Setter Method>
     * Sets an object as a prototypical template for prototype operations.
     * @param string $id
     * @param \oroboros\core\interfaces\contract\patterns\creational\PrototypicalContract $object
     * @param string $prototyper (optional) If supplied, will set the given
     *     instance into a specific prototyper instance. If not supplied, will
     *     set into the default prototyper.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given prototyper does not exist
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given object is not prototypical
     */
    public function setPrototypical( $id, $object, $prototyper = null )
    {

    }

    /**
     * <Prototype Manager Prototyical Instance Getter Method>
     * Returns a prototypical clone of a given prototypical id,
     * or false if not found.
     * @param string $id The slug identifier of the prototypical instance
     *     to retrieve.
     * @param string $prototyper (optional) If supplied, will check the
     *     given prototyper for the specified instance. If not supplied,
     *     will check the default prototyper instance.
     * @return bool|\oroboros\core\interfaces\contract\patterns\creational\PrototyicalContract
     */
    public function getPrototypical( $id, $prototyper = null )
    {

    }

    /**
     * <Prototype Manager Prototypical Instance Check Method>
     * Checks if a given slug identifier has an instance stored in the
     * given prototyper instance.
     * @param string $id The slug identifier of the
     *     prototyical instance to check.
     * @param string $prototyper (optional) If supplied, will check within
     *     the given prototyper. If not supplied, will check within the
     *     default prototyper.
     * @return bool|string Returns the class name of the given id if it exists,
     *     otherwise returns false.
     */
    public function hasPrototypical( $id, $prototyper = null )
    {

    }

    /**
     * <Prototype Manager Prototypical List Method>
     * Returns a list of all prototypical instances currently held in all
     * prototypers currently registered. If the optional prototyper parameter
     * is passed, the check will occur only within the given prototyper,
     * and return an associative array of the slug identifiers as keys and
     * the class names of the prototypical instances as values.
     *
     * If the prototyper parameter is not supplied, all prototypical instances
     * will be returned in an associative array, where the key is the slug
     * identifier of the prototyper, and the value is an associative array
     * with keys corresponding to the slug identifiers of the prototypical
     * instances, and values corresponding to their class names.
     *
     * If a prototyper is specified that does not exist, returns false.
     *
     * @param string $prototyper (optional)
     * @return array|bool
     */
    public function listPrototypical( $prototyper = null )
    {

    }

    /**
     * <Prototype Manager Prototypical Unsetter Method>
     * Unsets a given prototypical instance from the specified prototyper.
     * If the optional prototyper parameter is supplied, it will unset the
     * given id from that prototyper. If not supplied, it will unset it from
     * the default prototyper.
     *
     * @param string $id The slug identifier of the prototypical
     *     instance to remove.
     * @param string $prototyper (optional) If supplied, will unset the
     *     prototypical instance from the specified prototyper. If not supplied,
     *     will unset it from the default prototyper.
     * @return void
     */
    public function deletePrototypical( $id, $prototyper = null )
    {

    }

    /**
     * <Prototype Manager Reset Method>
     * Resets the prototype manager to its default state,
     * which will also reset or remove all of its redundant
     * prototypers. The default prototyper will be restored
     * to an empty state.
     * @return void
     */
    public function reset()
    {

    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the directory category to the environment scope.
     * @return void
     */
    protected function _managerSetDirectorCategory()
    {
        $this->_setDirectorCategory( \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_PROTOTYPE_WORKER );
    }

    /**
     * Defines the default parameters.
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return array(
            'template' => new \oroboros\prototype\Prototyper(null, null, self::getCompiledFlags()),
        );
    }

    /**
     * Defines the valid types of the parameters.
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array(
            'template' => $this->_prototype_prototypical_contract,
        );
    }

    /**
     * Creates a default prototyper instance if none is supplied.
     * @return array
     */
    protected function _baselineSetDependenciesDefault()
    {
        return array();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Insures that the cloned copy does not have any hanging data that
     * is not relevant to its expected operation.
     *
     * This method automatically fires when any clone operation occurs.
     *
     * @return void
     * @internal
     */
    private function _prototypeManagerCleanupClone()
    {
        $this->_object_fingerprint = null;
        $this->_baselineGetObjectFingerprint();
    }

    /**
     * Performs the post initialization operations for the prototype manager.
     *
     * This method automatically fires immediately after initialization.
     *
     * @return void
     * @internal
     */
    private function _prototypeManagerPostInitialization()
    {
        if ( is_null( $this->_prototyper_instances ) )
        {
            $this->_prototyper_instances = new \oroboros\collection\Collection();
        }
        if ( is_null( $this->_prototyper_slug_identifiers ) )
        {
            $this->_prototyper_slug_identifiers = new \oroboros\collection\Collection();
        }
        if ( is_null( $this->_prototypical_slug_index ) )
        {
            $this->_prototypical_slug_index = new \oroboros\collection\Collection();
        }
        $this->_prototypeManagerSetInjectedTemplate();
        $this->_prototypeManagerSetDefaultPrototyperTemplate();
        $this->_prototypeManagerSetDefaultPrototyper();
//        d( 'Initialization operation', $this );
    }

    /**
     * Releases memory and dependent resources accrued during
     * the lifecycle of the object.
     *
     * This method automatically fires when the destructor is called.
     *
     * @return void
     * @internal
     */
    private function _prototypeManagerDestructorGarbageCollection()
    {
//        d( 'Destructor operation', $this );
    }

    /**
     * Sets a new prototyper in the internal prototyper registry.
     * @param string $id
     * @param \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract $prototyper
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid id is supplied
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid prototyper is supplied
     * @throws \oroboros\core\utilities\exception\RuntimeException If a prototyper is already defined for the supplied key
     * @internal
     */
    private function _prototypeManagerAddPrototyper( $id, $prototyper )
    {
        self::_validate( 'type-of', $id, 'string', true );
        self::_validate( 'instance-of', $prototyper,
            $this->_prototype_prototypical_contract,
            true );
        if ( $this->_prototyper_instances->has( $id ) )
        {
            throw self::_getException( 'runtime-exception',
                sprintf( 'Error encountered at [%s]. Key [%s] is already defined.',
                    __METHOD__, $id ),
                self::_getExceptionCode( 'logic-ambiguous-reference' )
            );
        }
        $this->_prototyper_instances[$id] = $prototyper;
        $this->_prototyper_slug_identifiers[$id] = $prototyper->getFingerprint();
    }

    /**
     * Deletes a current prototyper instance if it exists.
     * @param string $id
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @internal
     */
    private function _prototypeManagerDeletePrototyper( $id )
    {
        self::_validate( 'type-of', $id, 'string', true );
        if ( $this->_prototyper_instances->has( $id ) )
        {
            unset( $this->_prototyper_instances[$id] );
            unset( $this->_prototyper_slug_identifiers[$id] );
        }
    }

    /**
     * Provides the baseline internal default prototyper. If it is already set
     * and matches the settings, the existing instance will be returned.
     *
     * @return \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract
     * @interal
     */
    private function _prototypeManagerSetDefaultPrototyperTemplate()
    {
        if ( !is_null( $this->_baselineGetDependency( 'template' ) ) )
        {
            //If a valid prototyper template was injected,
            //this will be implicitly used.
            return;
        }
        $template_class = $this->_baselineGetParameter( 'template' );
        $template_current = $this->_prototyper_template;
        if ( is_null( $template_current ) || !get_class( $template_current ) ===
            ltrim( $template_class, '\\' ) )
        {
            $template = new $template_class();
            self::_validate( 'instance-of', $template,
                $this->_prototype_prototypical_contract,
                true );
            $this->_prototyper_template = $template;
        }
    }

    private function _prototypeManagerSetDefaultPrototyper()
    {
        $default = clone $this->_prototyper_template;
        $this->_prototypeManagerAddPrototyper( '__default', $default );
    }

    /**
     * Sets the injected template prototyper if one was supplied.
     * @return void
     * @internal
     */
    private function _prototypeManagerSetInjectedTemplate()
    {
        $template_supplied = $this->_baselineGetDependency( 'template' );
        if ( !is_null( $template_supplied ) )
        {
            $this->_prototyper_template = $template_supplied;
        }
    }

}
