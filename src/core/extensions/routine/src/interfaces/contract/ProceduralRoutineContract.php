<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\routine\interfaces\contract;

/**
 * <Procedural Routine Contract Interface>
 * Procedural routines wrap non-object oriented executional php constructs.
 * This allows them to be approached in an object oriented manner,
 * for compatibility with legacy architecture that does not follow
 * an object oriented paradigm.
 *
 * Appropriate use cases for this construct include wrapping procedural
 * or imperative style files that accomplish a defined task, but
 * assume control of operation when doing so, and do not play nice
 * with object oriented builds.
 *
 * These can then be wrapped inside a safe object,
 * that will execute them in a safe manner upon demand,
 * thereby decoupling their assumption of control with
 * their standard execution approach. This is most useful
 * in refactoring legacy code into a modern, object oriented build,
 * in a way that does not disrupt its purpose, but allows for
 * implementation of a different aproach to order of execution
 * than was originally intended in a flexible way.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage routines
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface ProceduralRoutineContract
extends RoutineContract
{

}
