<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\stream\interfaces\contract;

/**
 * <File Stream Utility Contract Interface>
 * This contract interface designates methods required to create a PHP
 * stream resource that can be registered with the native stream_wrapper_register,
 * and interact with native PHP functions like fopen, fread, fwrite, etc.
 *
 * This particular contract adds additional file functionality that may be
 * considered unsafe in some other contexts.
 *
 * This should not be confused with the Psr-7 StreamInterface, which accepts a
 * resource as a parameter for construction. Objects successfully implementing
 * this contract interface should be valid parameters for Psr-7 StreamInterface
 * implementers after they have been registered with stream_wrapper_register.
 *
 * This is a very low level construct. You should avoid extending
 * this functionality unless you are well acquainted with PHP internals.
 *
 * If you are going to utilize this interface, you must be very careful
 * to safely null all functions that are unsafe in the scope that you
 * are working with the stream. Read the doc comments very carefully
 * and make sure you understand what you are doing. If you are not sure,
 * use one of the provided options that are put together safely.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\stream\interfaces\contract\StreamContract
 */
interface FileStreamContract
extends StreamContract
{
    /**
     * <resource context>
     * This property MUST be declared,
     * and MUST be set to public.
     * @var resource
     */
    //public $context;

    /**
     * <Stream Resource Rename>
     * This method is called in response to rename().
     *
     * Should attempt to rename $path_from to $path_to
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support renaming files.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path_from The URL to the current file.
     * @param string $path_to The URL which the path_from should be renamed to.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function rename( $path_from, $path_to );

    /**
     * <Stream Resource Feof>
     * This method is called in response to feof().
     *
     * Warning: When reading the whole file
     * (for example, with file_get_contents()),
     * PHP will call streamWrapper::stream_read()
     * followed by streamWrapper::stream_eof()
     * in a loop but as long as streamWrapper::stream_read()
     * returns a non-empty string, the
     * return value of streamWrapper::stream_eof() is ignored.
     *
     * @return bool Should return TRUE if the read/write position is at the end of the stream and if no more data is available to be read, or FALSE otherwise.
     */
    public function stream_eof();

    /**
     * <Stream Resource Lock>
     * This method is called in response to flock(), when file_put_contents()
     * (when flags contains LOCK_EX), stream_set_blocking() and when closing
     * the stream (LOCK_UN).
     *
     * operation is one of the following:
     *
     * * LOCK_SH to acquire a shared lock (reader).
     * * LOCK_EX to acquire an exclusive lock (writer).
     * * LOCK_UN to release a lock (shared or exclusive).
     * * LOCK_NB if you don't want flock() to block while locking. (not supported on Windows)
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param mode $operation See comment above
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_lock( $operation );

    /**
     * <Stream Resource Open>
     * This method is called immediately after the wrapper
     * is initialized (f.e. by fopen() and file_get_contents()).
     *
     * The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path Specifies the URL that was passed to the original function.
     * @param string $mode The mode used to open the file, as detailed for fopen().
     * @param int $options Holds additional flags set by the streams API. It can hold one or more of the following values OR'd together.
     * @param string &$opened_path If the path is opened successfully, and STREAM_USE_PATH is set in options, opened_path should be set to the full path of the file/resource that was actually opened.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_open($path , $mode , $options , &$opened_path);

    /**
     * <Stream Resource Tell>
     * Will respond to truncation, e.g., through ftruncate().
     *
     * @param int $new_size The new size.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_truncate( $new_size );

    /**
     * <Stream Resource Metadata Setter>
     * This method is called to set metadata on the stream.
     * It is called when one of the following functions is
     * called on a stream URL:
     *
     * * touch()
     * * chmod()
     * * chown()
     * * chgrp()
     *
     * Please note that some of these operations may not be available on your system.
     *
     * $option will be one of:
     *
     * * STREAM_META_TOUCH (The method was called in response to touch())
     * * STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
     * * STREAM_META_OWNER (The method was called in response to chown())
     * * STREAM_META_GROUP_NAME (The method was called in response to chgrp())
     * * STREAM_META_GROUP (The method was called in response to chgrp())
     * * STREAM_META_ACCESS (The method was called in response to chmod())
     *
     * $value will differ based on the value of $option:
     *
     * * STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
     * * STREAM_META_OWNER_NAME or STREAM_META_GROUP_NAME: The name of the owner user/group as string.
     * * STREAM_META_OWNER or STREAM_META_GROUP: The value owner user/group argument as integer.
     * * STREAM_META_ACCESS: The argument of the chmod() as integer.
     *
     * @param string $path The file path or URL to set metadata. Note that in the case of a URL, it must be a :// delimited URL. Other URL forms are not supported.
     * @param int $option See comment above
     * @param mixed $value See comment above
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_metadata( $path, $option, $value );

    /**
     * <Stream Resource Writer>
     * This method is called in response to fwrite().
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note If the return value is greater the length of data, E_WARNING will be emitted and the return value will truncated to its length.
     * @note Remember to update the current position of the stream by number of bytes that were successfully written.
     * @param string $data Should be stored into the underlying stream (If there is not enough room in the underlying stream, store as much as possible).
     * @return int Should return the number of bytes that were successfully stored, or 0 if none could be stored.
     */
    public function stream_write( $data );

    /**
     * <Stream Resource Unlink>
     * This method is called in response to unlink().
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support removing files.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     * @param string $path The file URL which should be deleted.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function unlink( $path );
}
