<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\stream\interfaces\contract;

/**
 * <Stream Utility Contract Interface>
 * This contract interface designates methods required to create a PHP
 * stream resource that can be registered with the native stream_wrapper_register,
 * and interact with native PHP functions like fopen, fread, fwrite, etc.
 *
 * This should not be confused with the Psr-7 StreamInterface, which accepts a
 * resource as a parameter for construction. Objects successfully implementing
 * this contract interface should be valid parameters for Psr-7 StreamInterface
 * implementers after they have been registered with stream_wrapper_register.
 *
 * This is a very low level construct. You should avoid extending
 * this functionality unless you are well acquainted with PHP internals.
 *
 * If you are going to utilize this interface, you must be very careful
 * to safely null all functions that are unsafe in the scope that you
 * are working with the stream. Read the doc comments very carefully
 * and make sure you understand what you are doing. If you are not sure,
 * use one of the provided options that are put together safely.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface StreamContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract
{
    /**
     * <resource context>
     * This property MUST be declared,
     * and MUST be set to public.
     * @var resource
     */
    //public $context;

    /**
     * <Stream Resource Constructor>
     * This is called by PHP internals
     * immediately before streamWrapper::stream_open().
     */
    public function __construct();

    /**
     * <Stream Resource Destructor>
     * The resource should clean up and release
     * any memory it has allocated when this fires.
     *
     * This is called by PHP internals
     * immediately before streamWrapper::stream_flush().
     */
    public function __destruct();

    /**
     * <Stream Resource Cast>
     * This method is called in response to stream_select().
     *
     * @param int $cast_as Can be STREAM_CAST_FOR_SELECT when stream_select() is calling stream_cast() or STREAM_CAST_AS_STREAM when stream_cast() is called for other uses.
     * @return resource Should return the underlying stream resource used by the wrapper, or FALSE.
     */
    public function stream_cast( $cast_as );

    /**
     * <Stream Resource Fclose>
     * This method is called in response to fclose().
     * @return void
     */
    public function stream_close();

    /**
     * <Stream Resource Feof>
     * This method is called in response to feof().
     *
     * Warning: When reading the whole file
     * (for example, with file_get_contents()),
     * PHP will call streamWrapper::stream_read()
     * followed by streamWrapper::stream_eof()
     * in a loop but as long as streamWrapper::stream_read()
     * returns a non-empty string, the
     * return value of streamWrapper::stream_eof() is ignored.
     *
     * @return bool Should return TRUE if the read/write position is at the end of the stream and if no more data is available to be read, or FALSE otherwise.
     */
    public function stream_eof();

    /**
     * <Stream Resource Flush>
     * This method is called in response to fflush() and when the stream
     * is being closed while any unflushed data has been written to it before.
     *
     * If you have cached data in your stream but not yet stored it into
     * the underlying storage, you should do so now.
     *
     * If not implemented, FALSE is assumed as the return value.
     *
     * @return bool Should return TRUE if the cached data was successfully stored (or if there was no data to store), or FALSE if the data could not be stored.
     */
    public function stream_flush();

    /**
     * <Stream Resource Lock>
     * This method is called in response to flock(), when file_put_contents()
     * (when flags contains LOCK_EX), stream_set_blocking() and when closing
     * the stream (LOCK_UN).
     *
     * operation is one of the following:
     *
     * * LOCK_SH to acquire a shared lock (reader).
     * * LOCK_EX to acquire an exclusive lock (writer).
     * * LOCK_UN to release a lock (shared or exclusive).
     * * LOCK_NB if you don't want flock() to block while locking. (not supported on Windows)
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param mode $operation See comment above
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_lock( $operation );

    /**
     * <Stream Resource Open>
     * This method is called immediately after the wrapper
     * is initialized (f.e. by fopen() and file_get_contents()).
     *
     * The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path Specifies the URL that was passed to the original function.
     * @param string $mode The mode used to open the file, as detailed for fopen().
     * @param int $options Holds additional flags set by the streams API. It can hold one or more of the following values OR'd together.
     * @param string &$opened_path If the path is opened successfully, and STREAM_USE_PATH is set in options, opened_path should be set to the full path of the file/resource that was actually opened.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_open($path , $mode , $options , &$opened_path);

    /**
     * <Stream Resource Read>
     * This method is called in response to fread() and fgets().
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * streamWrapper::stream_eof() is called directly after calling streamWrapper::stream_read() to check if EOF has been reached. If not implemented, EOF is assumed.
     *
     * @note Remember to update the read/write position of the stream (by the number of bytes that were successfully read).
     * @note If the return value is longer then count an E_WARNING error will be emitted, and excess data will be lost.
     * @param int $count How many bytes of data from the current position should be returned.
     * @return string If there are less than count bytes available, return as many as are available. If no more data is available, return either FALSE or an empty string.
     */
    public function stream_read( $count );

    /**
     * <Stream Resource Seek>
     * This method is called in response to fseek().
     *
     * The read/write position of the stream should be updated
     * according to the offset and whence.
     *
     * Upon success, streamWrapper::stream_tell() is called directly
     * after calling streamWrapper::stream_seek().
     * If streamWrapper::stream_tell() fails, the return value to the
     * caller function will be set to FALSE
     *
     * Not all seeks operations on the stream will result in this function
     * being called. PHP streams have read buffering enabled by default
     * (see also stream_set_read_buffer()) and seeking may be done by
     * merely moving the buffer pointer.
     *
     * $whence has the following possible values:
     *
     * * SEEK_SET - Set position equal to offset bytes.
     * * SEEK_CUR - Set position to current location plus offset.
     * * SEEK_END - Set position to end-of-file plus offset.
     *
     * @note If not implemented, FALSE is assumed as the return value.
     * @param int $offset The stream offset to seek to.
     * @param int $whence See comment above.
     * @return bool Return TRUE if the position was updated, FALSE otherwise.
     */
    public function stream_seek( $offset, $whence = SEEK_SET );

    /**
     * <Stream Resource Option Setter>
     * This method is called to set options on the stream.
     *
     * $option can have the following values:
     *
     * * STREAM_OPTION_BLOCKING (The method was called in response to stream_set_blocking())
     * * STREAM_OPTION_READ_TIMEOUT (The method was called in response to stream_set_timeout())
     * * STREAM_OPTION_WRITE_BUFFER (The method was called in response to stream_set_write_buffer())
     *
     *
     * $arg1 varies based on the value passed for $option as follows:
     *
     * * STREAM_OPTION_BLOCKING: requested blocking mode (1 meaning block 0 not blocking).
     * * STREAM_OPTION_READ_TIMEOUT: the timeout in seconds.
     * * STREAM_OPTION_WRITE_BUFFER: buffer mode (STREAM_BUFFER_NONE or STREAM_BUFFER_FULL).
     *
     *
     * $arg2 varies based on the value passed for $option as follows:
     *
     * * STREAM_OPTION_BLOCKING: This option is not set.
     * * STREAM_OPTION_READ_TIMEOUT: the timeout in microseconds.
     * * STREAM_OPTION_WRITE_BUFFER: the requested buffer size.
     *
     * @param int $option See comment above.
     * @param int $arg1 See comment above.
     * @param int $arg2 See comment above.
     * @return bool Returns TRUE on success or FALSE on failure. If option is not implemented, FALSE should be returned.
     */
    public function stream_set_option( $option, $arg1, $arg2 );

    /**
     * <Stream Resource Stat>
     * This method is called in response to fstat().
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @see stat()
     * @return array See stat()
     */
    public function stream_stat();

    /**
     * <Stream Resource Tell>
     * This method is called in response to fseek()
     * to determine the current position.
     *
     * @return int Should return the current position of the stream.
     */
    public function stream_tell();

    /**
     * <Stream Resource Writer>
     * This method is called in response to fwrite().
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note If the return value is greater the length of data, E_WARNING will be emitted and the return value will truncated to its length.
     * @note Remember to update the current position of the stream by number of bytes that were successfully written.
     * @param string $data Should be stored into the underlying stream (If there is not enough room in the underlying stream, store as much as possible).
     * @return int Should return the number of bytes that were successfully stored, or 0 if none could be stored.
     */
    public function stream_write( $data );

    /**
     * <Stream Resource Url Stat>
     * This method is called in response to all stat() related functions, such as:
     *
     * * chmod() (only when safe_mode is enabled)
     * * copy()
     * * fileperms()
     * * fileinode()
     * * filesize()
     * * fileowner()
     * * filegroup()
     * * fileatime()
     * * filemtime()
     * * filectime()
     * * filetype()
     * * is_writable()
     * * is_readable()
     * * is_executable()
     * * is_file()
     * * is_dir()
     * * is_link()
     * * file_exists()
     * * lstat()
     * * stat()
     * * SplFileInfo::getPerms()
     * * SplFileInfo::getInode()
     * * SplFileInfo::getSize()
     * * SplFileInfo::getOwner()
     * * SplFileInfo::getGroup()
     * * SplFileInfo::getATime()
     * * SplFileInfo::getMTime()
     * * SplFileInfo::getCTime()
     * * SplFileInfo::getType()
     * * SplFileInfo::isWritable()
     * * SplFileInfo::isReadable()
     * * SplFileInfo::isExecutable()
     * * SplFileInfo::isFile()
     * * SplFileInfo::isDir()
     * * SplFileInfo::isLink()
     * * RecursiveDirectoryIterator::hasChildren()
     *
     *
     * $flags can have the following values
     * (these may be added to in later PHP versions):
     *
     * * STREAM_URL_STAT_LINK For resources with the ability to link to other resource (such as an HTTP Location: forward, or a filesystem symlink). This flag specified that only information about the link itself should be returned, not the resource pointed to by the link. This flag is set in response to calls to lstat(), is_link(), or filetype().
     * * STREAM_URL_STAT_QUIET If this flag is set, your wrapper should not raise any errors. If this flag is not set, you are responsible for reporting errors using the trigger_error() function during stating of the path.
     *
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     * @param string $path The file path or URL to stat. Note that in the case of a URL, it must be a :// delimited URL. Other URL forms are not supported.
     * @param int $flags See comment above.
     * @return array Should return as many elements as stat() does. Unknown or unavailable values should be set to a rational value (usually 0).
     */
    public function url_stat( $path, $flags );
}
