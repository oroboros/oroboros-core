<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\stream\traits;

/**
 * <File Stream Trait>
 * Provides methods for creating a valid stream resource object that
 * can be integrated into PHP internals directly.
 *
 * This trait grants optimized functionality for working with files.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage streams
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\stream\interfaces\contract\FileStreamContract
 */
trait FileStreamTrait
{

    use StreamTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\stream\interfaces\contract\FileStreamContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Stream Resource Rename>
     * This method is called in response to rename().
     *
     * Should attempt to rename $path_from to $path_to
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support renaming files.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path_from The URL to the current file.
     * @param string $path_to The URL which the path_from should be renamed to.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function rename( $path_from, $path_to )
    {

    }

    /**
     * <Stream Resource Feof>
     * This method is called in response to feof().
     *
     * Warning: When reading the whole file
     * (for example, with file_get_contents()),
     * PHP will call streamWrapper::stream_read()
     * followed by streamWrapper::stream_eof()
     * in a loop but as long as streamWrapper::stream_read()
     * returns a non-empty string, the
     * return value of streamWrapper::stream_eof() is ignored.
     *
     * @return bool Should return TRUE if the read/write position is at the end of the stream and if no more data is available to be read, or FALSE otherwise.
     */
    public function stream_eof()
    {

    }

    /**
     * <Stream Resource Lock>
     * This method is called in response to flock(), when file_put_contents()
     * (when flags contains LOCK_EX), stream_set_blocking() and when closing
     * the stream (LOCK_UN).
     *
     * operation is one of the following:
     *
     * * LOCK_SH to acquire a shared lock (reader).
     * * LOCK_EX to acquire an exclusive lock (writer).
     * * LOCK_UN to release a lock (shared or exclusive).
     * * LOCK_NB if you don't want flock() to block while locking. (not supported on Windows)
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param mode $operation See comment above
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_lock( $operation )
    {

    }

    /**
     * <Stream Resource Open>
     * This method is called immediately after the wrapper
     * is initialized (f.e. by fopen() and file_get_contents()).
     *
     * The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path Specifies the URL that was passed to the original function.
     * @param string $mode The mode used to open the file, as detailed for fopen().
     * @param int $options Holds additional flags set by the streams API. It can hold one or more of the following values OR'd together.
     * @param string &$opened_path If the path is opened successfully, and STREAM_USE_PATH is set in options, opened_path should be set to the full path of the file/resource that was actually opened.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_open( $path, $mode, $options, &$opened_path )
    {

    }

    /**
     * <Stream Resource Tell>
     * Will respond to truncation, e.g., through ftruncate().
     *
     * @param int $new_size The new size.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_truncate( $new_size )
    {

    }

    /**
     * <Stream Resource Metadata Setter>
     * This method is called to set metadata on the stream.
     * It is called when one of the following functions is
     * called on a stream URL:
     *
     * * touch()
     * * chmod()
     * * chown()
     * * chgrp()
     *
     * Please note that some of these operations may not be available on your system.
     *
     * $option will be one of:
     *
     * * STREAM_META_TOUCH (The method was called in response to touch())
     * * STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
     * * STREAM_META_OWNER (The method was called in response to chown())
     * * STREAM_META_GROUP_NAME (The method was called in response to chgrp())
     * * STREAM_META_GROUP (The method was called in response to chgrp())
     * * STREAM_META_ACCESS (The method was called in response to chmod())
     *
     * $value will differ based on the value of $option:
     *
     * * STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
     * * STREAM_META_OWNER_NAME or STREAM_META_GROUP_NAME: The name of the owner user/group as string.
     * * STREAM_META_OWNER or STREAM_META_GROUP: The value owner user/group argument as integer.
     * * STREAM_META_ACCESS: The argument of the chmod() as integer.
     *
     * @param string $path The file path or URL to set metadata. Note that in the case of a URL, it must be a :// delimited URL. Other URL forms are not supported.
     * @param int $option See comment above
     * @param mixed $value See comment above
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_metadata( $path, $option, $value )
    {

    }

    /**
     * <Stream Resource Writer>
     * This method is called in response to fwrite().
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note If the return value is greater the length of data, E_WARNING will be emitted and the return value will truncated to its length.
     * @note Remember to update the current position of the stream by number of bytes that were successfully written.
     * @param string $data Should be stored into the underlying stream (If there is not enough room in the underlying stream, store as much as possible).
     * @return int Should return the number of bytes that were successfully stored, or 0 if none could be stored.
     */
    public function stream_write( $data )
    {

    }

    /**
     * <Stream Resource Unlink>
     * This method is called in response to unlink().
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support removing files.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     * @param string $path The file URL which should be deleted.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function unlink( $path )
    {

    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
}
