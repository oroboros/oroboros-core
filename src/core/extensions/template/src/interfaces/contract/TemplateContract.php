<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\template\interfaces\contract;

/**
 * <Template Contract Interface>
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage
 * @version
 * @since
 */
interface TemplateContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{

    /**
     * <Template Render Method>
     * Directs the template to render fully, using the currently set data.
     * Initialization method must be called before this point. If [$params]
     * is not empty, will call [queue($params, $flags)] before rendering.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return void
     * @since 0.0.2a
     */
    public function render( $params = null, $flags = null );

    /**
     * <Template Master Reset Method>
     * Calls all individual reset methods. Restores template data to default
     * settings. If [$params] is supplied, it will call [queue($params, $flags)]
     * after the reset operation is complete.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return void
     * @since 0.0.2a
     */
    public function reset( $params = null, $flags = null );

    /**
     * <Template Part Retrieval Method>
     * Returns a rendered template part. If data is needed to fill
     * out the template, it should be supplied in [$params]. If any
     * special conditions may change the way that the template part
     * is laid out, it should be passed in [$flags].
     * @param string $part (required) The template part name.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return string The completed template markup
     * @since 0.0.2a
     */
    public function part( $part, array $params = array(), array $flags = array() );

    /**
     * <Template Bulk Queue Method>
     * Calls the setter method for any known data type that the
     * template handles. When possible, queues additional data
     * non-destructively. If [$params] is empty or not provided,
     * has no effect. Queue values should be key => value pairs.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return void
     * @since 0.0.2a
     */
    public function queue( $params = null, $flags = null );
}
