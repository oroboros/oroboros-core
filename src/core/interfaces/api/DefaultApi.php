<?php
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\api;

/**
 * <Oroboros Default API>
 * This class provides a null API if another API is
 * not attached to an evaluated class.
 *
 * Do not attach this to your class, It is already
 * used if [\Classname::OROBOROS_API === FALSE], which is the
 * default defined in the base class.
 *
 * @note API Interfaces MAY declare public methods
 * @note API Interfaces that define public methods will have those methods automatically listed in their API Info when [\oroboros\Oroboros::codex()->api([$instanceOfClass|(string) $validClassname]);] is used
 * @see \oroboros\Oroboros
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface DefaultApi
extends BaseApi
{

}
