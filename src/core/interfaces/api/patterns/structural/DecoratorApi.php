<?php

/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\api\patterns\structural;

/**
 * <Decorator Api Interface>
 * This interface represents the api interface for decorator patterns
 * within Oroboros Core. It designates all related classes, interfaces,
 * and traits used to satisfy the requirements of the decorator, and any related
 * constructs that can be accessed to provide related information.
 *
 * This api is most easily provided by accessing the codex entry [decorator].
 *
 * ----------------
 *
 * PURPOSE
 *
 * ----------------
 *
 * An Api Interface is used to declare a family of related classes, and what
 * they are meant to satisfy. It also declares any dependencies, and any
 * interoperability that can occur with it, or any additional apis that it provides.
 *
 * This information is used internally to validate the execution plan.
 *
 * This information can be indexed by checking the Codex
 *
 * ----------------
 *
 * USAGE
 *
 * ----------------
 *
 * Effective useage of this interface construct without collisions requires
 * that these interfaces ONLY BE IMPLEMENTED BY CONCRETE, FINAL CLASSES
 *
 * The traits and abstractions of this system are built to honor the
 * considerations if these without directly implementing them.
 *
 * Concrete classes can implement them as proof that they honor their api.
 * Internally, the system will FAVOR the object for jobs related to it's
 * DECLARED API when it has MULTIPLE AVAILABLE OPTIONS that all honor a scope.
 *
 * DECLARE THE API IN THE CONCRETE CLASS if it is not met by inheritance
 * (traits cannot accomplish this for you. You must either declare this
 * yourself or inherit it from one of our base classes).
 *
 * const OROBOROS_API = '\\oroboros\\core\\interfaces\\api\\adapters\AdapterApi'; //honors the adapter api
 *
 * Api Interfaces serve as an index of how class type and class scope
 * relate to specific api use cases.
 * This information is available by checking the Codex
 *
 * @see \oroboros\core\codex\Codex
 *
 * Which can also be done by any class using the codex trait
 *
 * @see \oroboros\core\traits\codex\CodexTrait
 *
 * Or by extending the abstract
 *
 * @see \oroboros\core\abstracts\codex\Codex
 *
 * ----------------
 *
 * CONSIDERATIONS
 * 
 * ----------------
 *
 * These interfaces DO enforce methods, for objects to report their api.
 * This condition can be satisfied by including the api trait in your class.
 * Most traits use this one, so it is likely already available if you are
 * implementing any other trait.
 *
 * @see \oroboros\core\traits\api\ApiTrait
 *
 * These interfaces DO declare numerous constants.
 * There is a low probability of constant collision with external codebases.
 * If this causes an issue, wrap the object that implements
 * the api in one that doesn't, and use a pass-through to obtain it's values.
 *
 * There is a trait that can accomplish this strict enumeration based off of
 * any interface attached to a class that uses it, which can also filter
 * results by prefix or suffix of the constant name. It's super handy for
 * indexing these in any class that uses them.
 *
 * @see \oroboros\enum\traits\EnumTrait
 *
 * There are also sets of provided defaults under the concrete namespace
 *
 * @see \oroboros\enum
 *
 * @satisfies \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_API_VALID
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/api_interface.md
 * @category api-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
interface DecoratorApi
    extends StructuralPatternApi
{
    /**
     * Designates the type of this Api Interface, and what realm of
     * responsibilities it is classified as.
     */
    const API_TYPE = 'patterns';

    /**
     * Determines the focused goal within the api type.
     * The Api Scope reveals the underlying goal of this specific Api Interface,
     * and what the specific purpose of this collection of classes, traits,
     * and interfaces is meant to collectively accomplish.
     */
    const API_SCOPE = 'structural';

    /**
     * Designates a parent package or namespace that this Api adheres to.
     * This value should be false if no such parent exists,
     * but this constant should still be declared.
     */
    const API_PARENT_NAMESPACE = false;

    /**
     * Designates the codex index for this Api, which can be used to reference
     * this collection of classes, traits, and interfaces as a related family
     * working toward a specific goal.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CODEX = 'decorator';

    /**
     * Designates the abstract class used to satisfy \oroboros\core\interfaces\contract\patterns\structural\DecoratorContract.
     * This class may be extended to satisfy the requirements of the decorator contract.
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\DecoratorContract
     */
    const DECORATOR_ABSTRACT_CLASS = '\\oroboros\\core\\abstracts\\patterns\\structural\\AbstractDecorator';

    /**
     * Designates the interface contract used to enforce the methods of the decorator.
     *
     * Any other class successfully implementing this contract can be substituted
     * interchangeably throughout this system, and can be designated to be used
     * internally entirely in place of the existing default.
     *
     * @extends \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract
     */
    const DECORATOR_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\patterns\\structural\\DecoratorContract';

    /**
     * Designates the trait used to satisfy
     * \oroboros\core\interfaces\contract\patterns\structural\DecoratorTrait.
     *
     * This trait provides all of the methods required to satisfy the contract
     * interface, and may be used on any class where it's methods are not
     * colliding in order to make it compliant with this contract.
     *
     * Any other class successfully using this trait without method collision can
     * implement the aforementioned contract without issue, making it a
     * recognizable internal substitution in this system without additional effort.
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\DecoratorContract
     */
    const DECORATOR_TRAIT = '\\oroboros\\core\\traits\\patterns\\structural\\DecoratorTrait';

    /**
     * Designates the abstract class used to satisfy \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract.
     * This class may be extended to satisfy the requirements of the decoratee contract.
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract
     */
    const DECORATEE_ABSTRACT_CLASS = '\\oroboros\\core\\abstracts\\patterns\\structural\\AbstractDecoratee';

    /**
     * Designates the interface contract used to enforce the methods of the decoratee.
     *
     * Any other class successfully implementing this contract can be substituted
     * interchangeably throughout this system, and can be designated to be used
     * internally entirely in place of the existing default.
     *
     * @extends \oroboros\core\interfaces\contract\patterns\structural\StructuralContract
     */
    const DECORATEE_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\patterns\\structural\\DecorateeContract';

    /**
     * Designates the trait used to satisfy
     * \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract.
     *
     * This trait provides all of the methods required to satisfy the contract
     * interface, and may be used on any class where it's methods are not
     * colliding in order to make it compliant with this contract.
     *
     * Any other class successfully using this trait without method collision can
     * implement the aforementioned contract without issue, making it a
     * recognizable internal substitution in this system without additional effort.
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract
     */
    const DECORATEE_TRAIT = '\\oroboros\\core\\traits\\patterns\\structural\\DecorateeTrait';
}
