<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\api\standards;

/**
 * <Standards Api Interface>
 * This interface represents the root level programming standards api interface.
 *
 * This interface is a meta construct, and should not be directly placed on any class.
 * External interfaces meant to enforce programmatic standards such as Psr's have
 * their Apis extend from this interface. It's only purpose is to act as a meta construct
 * designating a child interface as an extension of an external standard.
 *
 * If you introduce a set of standards that you would like indexed by the Codex,
 * your Api for them should extend this interface. No other api interfaces
 * should extend this interface.
 *
 * @satisfies \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_API_VALID
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.2.5
 */
interface StandardsApi
extends \oroboros\core\interfaces\api\BaseApi
{
    //no-op
}
