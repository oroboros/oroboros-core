<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract;

/**
 * <Oroboros Core Contract Interface>
 * The Core Contract Interface represents the global Oroboros core api.
 * This interface represents the baseline contract for the front-most api.
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.4-alpha
 * @see \oroboros\Oroboros
 * @see \oroboros\core\traits\OroborosTrait
 * @see \oroboros\core\interfaces\api\CoreApi
 */
interface OroborosContract
extends \oroboros\core\interfaces\contract\extensions\ExtendableContract
{

    /**
     * <Initialization>
     * This is the initialization method for the global accessor.
     * It will load the default dependencies of the system and insure
     * that they are available for further requests. This is typically
     * called during the bootload routine, and does not need to be
     * called again. However, for unit testing or customization,
     * you may reinitialize with specified parameters at any time.
     *
     * An internal registry of initializations will be kept, so in the
     * event of an error or mis-configuration or unexpected results,
     * some record of when/where the reinitialization can be easily
     * determined and corrected.
     *
     * This method will take three optional parameters.
     *
     * $dependencies is an optional associative array of dependencies for
     * dependency injection. If any of these are passed, it will
     * check that they honor the corresponding contract, and if so,
     * use them in place of it's defaults. This is useful for unit
     * testing or custom configuration. Any parameters that do not
     * implement the expected contract interface will cause an exception
     * to be raised and break initialization. The system will not make
     * any effort to catch this exception, as it will lead to potentially
     * unstable behavior if it is allowed to proceed.
     *
     * $options is an optional associative array of settings or configuration
     * details to pass into initialization. These correspond to the keys listed
     * in the conf/settings_default.json file. You may override these in bulk by
     * creating a file named settings.json and overriding any keys that you wish
     * to provide other defaults for. You only need to override the keys you want
     * to change defaults for, as the result will be array_merge'd with the default,
     * so any defaults that are not provided are retained. You may also provide a
     * settings file outside of the project directory structure by designating
     * its fully qualified path by defining the constant OROBOROS_BOOTLOAD_SETTINGS
     * as its fully qualified file path. The file must be in json format, but may
     * exist anywhere in your filesystem. You may also set an environment
     * variable with this same name in place of defining the constant, and if
     * such an environment variable exists, its value will be used to define
     * the constant in place of the default.
     *
     * $flags is an optional array of bootload flags that may be passed into
     * initialization. No flags are used by default, but any available in the
     * BootloadFlag enumerated interface may be used. These will modify behavior
     * of the system as described in their block comment in the enumerated api
     * interface. If reinitialization occurs, these flags will all be cleared
     * if they are not explicitly passed again. You may override these in bulk
     * by defining a constant named OROBOROS_BOOTLOAD_FLAGS as the fully
     * qualified path to a json file containing an array of the key names of
     * the flags you wish to pass. Only the flags in the BootloadFlag interface
     * will be honored, and all others will be silently discarded.
     * You may also accomplish this by declaring an environment variable
     * of the same name, which will be used in place of the constant if
     * such an environment variable exists.
     *
     * @param array $dependencies
     * @param array $options
     * @param array $flags
     * @return void
     */
    public static function initialize( array $dependencies = array(),
        array $options = array(), array $flags = array() );
}
