<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\core;

/**
 * <Static Baseline Contract>
 * This interface designates the baseline methods provided as
 * part of the baseline object api. Almost every single class in
 * Oroboros uses this contract or extends from it, with the exception of
 * a very small handful of utility classes that are used in baseline
 * initialization.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage internal
 * @version 0.2.5
 * @since 0.2.5
 */
interface StaticBaselineContract
extends CoreContract
{

    /**
     * <Static Baseline Initialization Method>
     * Initializes a class in the static scope, so that related configurations,
     * dependencies, flags, and settings are accessible across a range of child
     * classes. This also allows for extension logic to be immediately accessible
     * to all child classes if extensions are used.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * Stateful static initialization should only ever occur in a
     * dedicated base object that does not have a deep inheritance chain,
     * and only in very specialized cases. The functionality is not outright
     * prevented, however it does require several overrides to enable, because
     * its use in such a manner is highly discouraged to avoid introducing
     * instability in your code. Internal Oroboros logic always disables this
     * functionality, but you may roll your own classes using this trait that
     * implement it pretty easily if need be.
     *
     * @param type $params (optional) Any parameters that need to be accessible across all child objects.
     * @param type $dependencies (optional) Any dependencies that need to be accessible across all child objects.
     * @param type $flags (optional) Any flags that need to be accessible across all child objects.
     * @return void
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null );

    /**
     * <Static Baseline Initialization Check Method>
     * Returns a boolean determination as to whether the class
     * is statically initialized. This parameter is only ever set by the
     * _staticBaselineInitialize method if it has fully resolved at least
     * one time, and indicates that all internal static properties are
     * available to all child objects of the class.
     * @return bool
     */
    public static function isStaticInitialized();

    /**
     * <Static Baseline Static Type Context Getter Method>
     * Returns the default declared runtime class type context.
     * If the CLASS_TYPE class constant is not set on this object,
     * it will instead fall back to the static compiled class type,
     * which will in turn return either a compiled class type or false.
     *
     * @return string|bool
     */
    public static function getCompiledType();

    /**
     * <Static Baseline Static Scope Context Getter Method>
     * Returns the default declared runtime class scope context.
     * If the CLASS_SCOPE class constant is not set on this object,
     * it will instead fall back to the static compiled class scope,
     * which will in turn return either a compiled class scope or false.
     *
     * @return string|bool
     */
    public static function getCompiledScope();

    /**
     * <Static Baseline Static Api Context Getter Method>
     * Returns the default declared runtime class api context.
     * If the API class constant is not set on this object,
     * it will instead fall back to the static compiled class api,
     * which will in turn return either a compiled class api or false.
     *
     * @return string|bool
     */
    public static function getCompiledApi();

    /**
     * <Static Baseline Public Flag Setter Method>
     * Sets a given flag, if flag setting is allowed,
     * if it passes the whitelist if one exists, and
     * if it is a scalar value.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If setting flags is not allowed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given flag is not scalar, or if a whitelist is defined and
     *     the flag is not in it.
     */
    public static function setCompiledFlag( $flag );

    /**
     * <Static Baseline Public Flag Unsetter Method>
     * Removes a given flag, if it exists and unsetting flags is allowed.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed.
     */
    public static function unsetCompiledFlag( $flag );

    /**
     * <Static Baseline Flag Check Method>
     * Returns a boolean determination as to whether a given flag exists
     * in the current objects flag set.
     * @param scalar $flag
     * @return bool
     */
    public static function checkCompiledFlag( $flag );

    /**
     * <Static Baseline Flag Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public static function getCompiledFlags();

    /**
     * <Static Baseline Parameter Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public static function getCompiledParameters();

    /**
     * <Static Baseline Dependency Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public static function getCompiledDependencies();

    /**
     * <Static Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledRequiredParameters();

    /**
     * <Static Baseline Required Dependency Getter Method>
     * Returns a list of all required dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledRequiredDependencies();

    /**
     * <Static Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledRequiredFlags();

    /**
     * <Static Baseline Valid Parameters Getter Method>
     * Returns a list of all valid parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledValidParameters();

    /**
     * <Static Baseline Valid Dependencies Getter Method>
     * Returns a list of all valid dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledValidDependencies();

    /**
     * <Static Baseline Valid Flags Getter Method>
     * Returns a list of all required flags.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledValidFlags();

    /**
     * <Static Baseline Static Fingerprint Getter Method>
     * Returns the underlying parent class fingerprint.
     * This value does not change during runtime.
     *
     * @return string|bool
     */
    public static function getCompiledFingerprint();

    /**
     * <Static Baseline Active State Child Object Tracker Method>
     * Returns an associative array of all child Baseline objects that currently
     * have active state and extend from the class implementing this trait,
     * provided they use the BaselineTrait and implement the BaselineContract.
     *
     * This trait does not retain references to the actual objects, only their
     * fingerprint and absolute classname. There is not a way to get the specific
     * object directly from this logic without some access to the actual object
     * itself. This method is only provided as a meta value to determine realtime
     * weight of child objects in existence at any given time, and validate their
     * specific fingerprints to rule out tampering or spoofing.
     *
     * @return array
     */
    public static function getActiveInstanceFingerprints();
}
