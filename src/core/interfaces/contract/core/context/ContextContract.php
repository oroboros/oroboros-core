<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\core\context;

/**
 * <Context Contract>
 * This interface designates the methods required for an object to represent
 * a contextual reference object.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage internal
 * @version 0.2.5
 * @since 0.2.5
 */
interface ContextContract
extends \oroboros\core\interfaces\contract\core\CoreContract
{

    /**
     * <Context Constructor>
     * Creates a contextual value, with an optional
     * type parameter for additional filtering.
     *
     * This object can only be created with
     * scalar or stringable values.
     *
     * @param scalar $context Also accepts an object with __toString
     * @param scalar $value Also accepts an object with __toString
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a context is passed that is not scalar or a stringable object
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a value is passed that is not scalar or a stringable object
     */
    public function __construct( $context, $value );

    /**
     * <Context String Casting Method>
     * Casts the value to a string.
     * @return string
     */
    public function __toString();

    /**
     * <Context Context Getter Method>
     * Returns the context.
     * @return scalar
     */
    public function getContext();

    /**
     * <Context Context Check Method>
     * Checks if a given context matches the internally set context.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared context.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other contexts will always return false if they are non-scalar.
     * If the internal context is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     */
    public function hasContext( $context );

    /**
     * <Context Value Getter Method>
     * Returns the value.
     * @return scalar
     */
    public function getValue();

    /**
     * <Context Type Check Method>
     * Checks if a given value matches the internally set value.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared value.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal value is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $value
     * @return bool
     */
    public function hasValue( $value );

    /**
     * <Context Type Getter Method>
     * Returns the type.
     * @return scalar
     */
    public function getType();

    /**
     * <Context Type Check Method>
     * Checks if a given type matches the internally set type.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared type.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal type is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $type
     * @return bool
     */
    public function hasType( $type );

    /**
     * <Context Category Getter Method>
     * Returns the category.
     * @return scalar
     */
    public function getCategory();

    /**
     * <Context Category Check Method>
     * Checks if a given category matches the internally set category.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared type.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal type is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $category
     * @return bool
     */
    public function hasCategory( $category );

    /**
     * <Context Sub-Category Getter Method>
     * Returns the sub-category.
     * @return scalar
     */
    public function getSubcategory();

    /**
     * <Context Sub-Category Check Method>
     * Checks if a given sub-category matches the internally set sub-category.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared sub-category.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal sub-category is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $subcategory
     * @return bool
     */
    public function hasSubcategory( $subcategory );
}
