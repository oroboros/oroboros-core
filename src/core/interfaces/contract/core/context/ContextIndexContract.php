<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\core\context;

/**
 * <Context Index Contract>
 * This interface designates the methods required for an object to represent
 * an index of creatable context objects.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage internal
 * @version 0.2.5
 * @since 0.2.5
 */
interface ContextIndexContract
extends \oroboros\core\interfaces\contract\core\CoreContract
{

    /**
     * <Context Index Getter Method>
     * Returns a list of the index of context objects that the index can create.
     * @return array
     */
    public function listContexts();

    /**
     * <Context Index Context Object Instance Setter>
     * Sets a given context object class into the internal index if it matches
     * the check for contract, type, category, and subcategory. Returns true if
     * the object was set into the index, and false otherwise.
     * @param type $context
     * @return bool Returns true if the context was added, and false otherwise.
     */
    public function setContext( $context );

    /**
     * <Context Index Context Object Check Method>
     * Returns a boolean determination as to whether
     * the given context exists in the context index.
     * @param string|object|\oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     */
    public function hasContext( $context );

    /**
     * <Context Index Context Object Validation Method>
     * Checks a given context object or class name, and returns a
     * boolean determination as to whether it can be successfully
     * added to the context index.
     *
     * This method does not add the context class, it only returns a
     * designation as to whether it can be added.
     *
     * @param string|object|\oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     */
    public function isContextValid( $context );

    /**
     * <Context Index Context Object Set Getter Method>
     * Creates a set of all valid context indexes that the provided context
     * and value can resolve against. If no instances are contained in the
     * index that the provided arguments can resolve against, the method will
     * return false. Otherwise it returns an array of all valid context object
     * instances that could be created by the provided parameters.
     *
     * This method is non-blocking.
     *
     * @param scalar $context The context of the context objects to create
     * @param scalar $value The value of the context objects to create
     * @return bool|array Returns false if no contexts could be made,
     *     otherwise returns an array of the context objects
     */
    public function getContextInstanceSet( $context, $value );

    /**
     * <Context Index Context Object Getter Method>
     * Creates a single context object instance from the given parameters,
     * if the index contains the specified instance.
     * @param string|object|\oroboros\core\interfaces\contract\core\context\ContextContract $instance
     *     Should resolve to the classname of a context object class contained
     *     in the context index. this may be a context object, string of the
     *     class name, or an object that can be cast to a string, but the
     *     provided class name of whichever is given must resolve to a
     *     class name that the index contains.
     * @param scalar $context The context of the context objects to create
     * @param scalar $value The value of the context objects to create
     * @return bool|\oroboros\core\interfaces\contract\core\context\ContextContract
     * @throws \oroboros\core\utilities\exception\InvalidArugmentException
     *     If an instance is given that is not known to the context index
     */
    public function getContextInstance( $instance, $context, $value );
}
