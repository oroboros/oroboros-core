<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\extensions;

/**
 * <Extension Contract Interface>
 * Enforces a set of methods to allow the class to extend other extendable classes.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @category core
 * @package oroboros/core
 * @subpackage extensions
 * @version 0.2.5
 * @since 0.2.5
 */
interface ExtensionContract
extends \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
{

    /**
     * <Extension Context Getter>
     * This method is used to determine the context of the extension by
     * the ExtensionManager, and which class it is intended to extend.
     * @return string|bool Returns false if no context is set,
     *     otherwise returns the context.
     */
    public static function getContext();

    /**
     * <Extension Identifier Getter>
     * Returns the Id associated with the extension.
     * @return string
     */
    public static function getExtensionId();

    /**
     * <Extension Index Check Method>
     * Returns a boolean determination as to whether the given index
     * is registered for the extension.
     * @param string $index
     * @return bool
     */
    public static function hasIndex( $index );

    /**
     * <Extension Method Index Getter>
     * Returns a list of the commands within the given index.
     * If the index is not registered, returns false.
     * @param string $index The index to check within
     * @return array|bool Returns false if the index is not registered,
     *     otherwise returns an array of the valid commands
     *     within the index.
     */
    public static function getIndex( $index );

    /**
     * <Extension Index List Getter>
     * Returns an array of all indexes the extension has declared.
     * @return array
     */
    public static function getIndexes();

    /**
     * <Extension Command Check Method>
     * Returns a boolean determination as to whether the given index
     * contains the specified method. If no method is supplied, will return
     * whether the index has a default method.
     * @param string $index The index to check for the method within
     * @param string $method (optional) If not supplied, will return whether
     *     the index has a default. Default not supplied.
     * @return bool
     */
    public static function hasMethod( $index, $method = null );
}
