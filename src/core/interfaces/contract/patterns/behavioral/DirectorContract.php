<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns\behavioral;

/**
 * <Director Pattern Contract Interface>
 * Enforces the methods required for a class to be recognized as a valid Director
 * that can aggregate work and results to and from a pool of Workers.
 *
 * Directors are also inherently Workers, and can be managed
 * by broader scope Directors as needed.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @see \oroboros\core\traits\patterns\behavioral\DirectorTrait
 */
interface DirectorContract
extends WorkerContract
{

    /**
     * <Director Category Getter>
     * This determines the category of work that the Director expects
     * workers to adhere to. As Directors are also Workers, this may
     * not be the same category as their Worker category. If a Director
     * has no director category, it will refuse all workers passed to
     * it and be unable to delegate anything.
     * @return string|bool Returns false if the director category was not set, otherwise returns the director category.
     */
    public function getDirectorCategory();

    /**
     * <Director Available Worker Scope Getter>
     * This will return an array of all of the scopes within the category
     * that the Director has a worker to delegate work to.
     * @return array
     */
    public function getWorkerScopes();

    /**
     * <Director Worker Scope Check>
     * This will return a boolean determination as to whether a
     * given scope is covered by an already validated worker.
     * @param string $scope
     * @return bool
     */
    public function checkWorkerScope( $scope );

    /**
     * <Worker Setter>
     * This is the dependency injection method for Worker objects.
     * This method will allow the Director to receive a Worker,
     * and will check if the worker category is congruent with
     * the director category, and that the worker has a scope
     * that it can fulfill. The Director does not care what the
     * Workers scope is, only that it has one.
     *
     * Workers lacking either of these will be rejected and cause
     * the method to return false. Workers that fulfill these will
     * be retained and will cause the method to return true.
     *
     * Workers provided with a scope that matches an existing scope MUST
     * replace the existing worker.
     *
     * @param \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract $worker
     * @return bool
     */
    public function setWorker( \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract $worker );
}
