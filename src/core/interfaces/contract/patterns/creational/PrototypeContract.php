<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns\creational;

/**
 * <Prototype Creational Pattern Contract Interface>
 * The prototype pattern is concerned with creating numerous objects
 * quickly by cloning a base template. This avoids initialization weight
 * from occurring again, and allows for the distribution of numerous objects
 * at considerably less memory weight.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface PrototypeContract
extends \oroboros\core\interfaces\contract\patterns\PatternContract
{

    /**
     * Gets an instance of a prototypical object, if this object has a template for it.
     * @param string $type A fully qualified classname
     * @return bool|object returns false if the object did not exist, otherwise returns an instance of the object
     * @throws \oroboros\core\utilities\exception\LogicException if the requested class failed it's clone operation. You've got a bug if you see this.
     */
    public function getPrototype( $type );

    /**
     * Returns a new instance of a prototyped object
     * @param type $class An instance of $this::PROTOTYPICAL_INTERFACE
     * @return object
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if given a non-prototypical class, or parameter that cannot resolve into a class
     * @throws \oroboros\core\utilities\exception\UnexpectedValueException if the given object already exists in the prototype registry.
     */
    public function setPrototype( $class );

    /**
     * unsets an existing prototype
     * @param string $class A fully qualified classname (use get_class($class))
     * @return bool true if the object was unset, false if it was not set
     */
    public function unSetPrototype( $class );

    /**
     * Checks if the given class object can be prototyped by this object.
     * @param object $class
     * @return bool
     */
    public function checkIfPrototypical( $class );

    /**
     * Checks if the specified class is registered as a prototype
     * @param string $class a fully qualified classname
     * @return bool true if has an instance of the class, false if not
     */
    public function checkPrototype( $class );

    /**
     * Returns a list of all currently registered prototypes
     * @return array
     */
    public function listPrototypes();

    /**
     * Resets the prototype registry to empty
     * @return type
     */
    public function reset();
}
