<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\utilities\exception;

/**
 * <Exception Utility Contract Interface>
 * This contract interface designates general Oroboros core exceptions.
 * All further exception contracts extend from this interface if they
 * are a part of the Oroboros core architecture.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 * @see \oroboros\core\traits\utilities\exception\ExceptionTrait
 */
interface ExceptionContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract,
 \oroboros\core\interfaces\enumerated\exception\ExceptionCode,
 \oroboros\core\interfaces\enumerated\exception\ExceptionMessage
{

    /**
     * <Oroboros Exception Constructor>
     * Provides the default exception constructor,
     * and sets hooks for internal error handler analysis of core exceptions.
     *
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionMessage
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionCode
     *
     * @param string $message The exception message.
     * @param int $code The status code.
     * @param \Exception $previous (optional) The previous exception, if provided.
     */
    public function __construct( $message = '', $code = 0,
        \Exception $previous = null );
}
