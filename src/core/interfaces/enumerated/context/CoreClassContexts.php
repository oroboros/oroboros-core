<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\context;

/**
 * <Oroboros Core Class Context Api>
 * Designates the class contexts used for various oroboros core classes
 * 3rd-parties may add their own additional scopes through the module director.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface CoreClassContexts
extends ClassContextBase
{

    /**
     * <Core Context>
     * Designates that the class is a core internal class.
     *
     * Core internals are used to prepare the system for use,
     * and are generally not extendable or overrideable.
     *
     * They typically do not provide abstraction or allow
     * substitution, and are intended to be used as is.
     */
    const CLASS_CONTEXT_CORE = '::core::';

    /**
     * <Pattern Context>
     * Designates that the class is a design pattern.
     *
     * Design patterns provide streamlined answers to typical logic
     * considerations to allow for simplified abstraction and
     * scalable functionality.
     *
     * Design patterns typically present a generic approach to typical
     * logical considerations. They do not make assumptions about why
     * they are used, but only deliver abstract functionality to provide
     * a means to accomplish a standard sort of task to child classes.
     */
    const CLASS_CONTEXT_PATTERN = '::pattern::';

    /**
     * <Utility Context>
     * Designates that the class is a utility.
     *
     * Utilities are classes that perform quick operations or
     * extend PHP baseline classes.
     *
     * They are typically stateless
     * and do not maintain internal data unless they extend upon
     * functionality that does, and in the case where they do,
     * they only retain enough internal data to properly report
     * on their lower level state.
     */
    const CLASS_CONTEXT_UTILITY = '::utility::';

    /**
     * <Library Context>
     * Designates that the class is a library.
     *
     * Libraries perform a subset of related work. They may maintain
     * internal state, depending on the nature of the work they are
     * designated to perform.
     *
     * Libraries do not provide opinion about execution unless
     * explicitly told to do so.
     */
    const CLASS_CONTEXT_LIBRARY = '::library::';

    /**
     * <Routine Context>
     * Designates that the class is a routine wrapper.
     *
     * Routine wrappers act as an identifier for code that assumes
     * control of operation. They are tasked with standardizing
     * the execution of a subsection of independent code, and
     * reporting and/or correcting any changes that code makes
     * that would otherwise be disruptive to system operation.
     *
     * Routines should be expected to assume control of operation when run,
     * and should be capable of restoring the runtime state to its pre-run
     * standard at the end of execution. They also should be able to impart
     * beforehand whether or not the subsection of code they wrap will
     * termintate execution before returning control back to Oroboros,
     * so that shutdown operations can be queued prior to execution.
     *
     * Procedural code, embedded subsystems, and 3rd party applications
     * controlled by Oroboros should all be registered as routines.
     */
    const CLASS_CONTEXT_ROUTINE = '::routine::';

    /**
     * <Adapter Context>
     * Designates that the class is an adapter.
     *
     * Adapters provide a layer of application with an external protocol.
     * Interaction with a database or the command line for example would
     * occur through the use of an adapter.
     *
     * Adapters maintain state needed to determine the external status of
     * the functionality they wrap, and parameters or state that has
     * previously been passed to the external protocol.
     */
    const CLASS_CONTEXT_ADAPTER = '::adapter::';

    /**
     * <Model Context>
     * Designates that the class is a model.
     *
     * Models perform business logic. They are tasked with executing
     * code required to make application level changes to data, run reports,
     * pull or push data to endpoints, or otherwise deliver the functionality
     * required to make the system accomplish its underlying goals.
     *
     * Models maintain state in accordance with the data they are tracking,
     * and a runtime history of the changes and operations that have
     * occurred in relation to that data.
     *
     * Models typically do not provide opinion, but do perform work.
     * They do what they are told without regard to why they are told
     * to do so.
     */
    const CLASS_CONTEXT_MODEL = '::model::';

    /**
     * <Controller Context>
     * Designates that the class is a controller.
     *
     * Controllers provide opinion. They are tasked with decision making logic,
     * and the aggregation of work conditionally to other classes that
     * represent the internal operations that correspond to those decisions.
     * In general, they do all of the decision making and do none of the work.
     *
     * Controllers maintain state in accordance with their decision making
     * process, and how far through the execution plan they have progressed
     * toward resolving the request they are tasked with. They also may retain
     * records or properties pulled from other classes that designate a
     * portion of the work they are tasked with resolving.
     */
    const CLASS_CONTEXT_CONTROLLER = '::controller::';

    /**
     * <View Context>
     * Designates that the class is a view.
     *
     * Views represent the presentation layer of logic. They are tasked with
     * formatting and submitting information in a way that is understandable
     * by the consumer of the request they are tasked with honoring.
     *
     * Views do not make decisions. They only display the information they
     * are provided with to the consumer, and only execute logic required
     * to standardize the information they have been provided with into a
     * valid consumable resource for the endpoint.
     *
     * Typically, views only maintain state required to resolve the data they
     * have received into the desired output. They will release this state
     * back to their default once their execution has resolved, and cleanup
     * any loose internal properties they have accrued throughout the process
     * of resolution.
     */
    const CLASS_CONTEXT_VIEW = '::view::';

    /**
     * <Extension Context>
     * Designates that the class is an extension.
     *
     * Extensions provide additional callable methods to existing logic
     * through abstraction. They are used to provide new methodology to
     * existing classes that otherwise would not exist.
     *
     * Whether or not an extension maintains state depends on the class
     * that it is extending. It should always follow the same stateful
     * approach as its base.
     */
    const CLASS_CONTEXT_EXTENSION = '::extension::';

    /**
     * <Module Context>
     * Designates that the class is a module.
     *
     * Modules provide additional internal steps to logic resolution.
     * In contrast to extensions, they do not provide new methods,
     * but instead extend the operations internally of existing methods.
     *
     * A module might add additional hooks to a class the would not
     * otherwise exist, or additional parsing steps to a class that
     * otherwise would not exist.
     *
     * Modules typically act as sub-utilities, being used by existing classes
     * in expected locations to extend upon their output logic. They are
     * generally stateless, and fire their extended logic when called from
     * specific hook or filter points defined in their parent class.
     */
    const CLASS_CONTEXT_MODULE = '::module::';

    /**
     * <Component Context>
     * Designates that the class is a component.
     *
     * Components provide additional output options to classes using existing
     * logic. They are tasked with providing additional formatting to an
     * existing subset of information without interfering with the process
     * of generating that information.
     *
     * An example of a component might be a class that applies console coloring
     * to cli output that would not otherwise be produced in the output messages
     * of its parent, or a class that adds additional html attributes to
     * existing markup generated by its parent.
     */
    const CLASS_CONTEXT_COMPONENT = '::component::';

    /**
     * <Third Party Context>
     * Designates that a class is a third party class that does not
     * conform to the expected oroboros context model.
     *
     * All classes that are not oroboros classes, standard PHP classes, and
     * do not explicitly declare their context use this context.
     *
     * This context does not make any assumption about the nature of the class
     * using it, and does not assume that the class is automatically safe or
     * unsafe for any specific designation.
     */
    const CLASS_CONTEXT_THIRDPARTY = '::thirdparty::';

    /**
     * <PHP Context>
     * Designates that a class is a standard PHP class.
     *
     * Classes that are shipped with PHP use this class context.
     * This designates that they are available in all PHP environments
     * that correspond to their minimum and maximum version.
     *
     * In general, these classes will typically be wrapped by a utility
     * designed to standardize their usage through direct extension,
     * or through some sort of adapter that interracts with them in
     * an expected way when they cannot or should not be directly
     * extended upon.
     */
    const CLASS_CONTEXT_PHP = '::php::';

}
