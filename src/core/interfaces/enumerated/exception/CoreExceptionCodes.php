<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Core Exception Code API>
 * This interface declares all of the exception codes used to designate a
 * failure on account of improper use of core logic. These codes can grant
 * some contextual information about what manner of logic failure occurred
 * in the event that an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface CoreExceptionCodes
extends ExceptionBase
{
    const RANGE_ERROR_CORE_MIN = 10000;
    const RANGE_ERROR_CORE_MAX = 10999;
    /**
     * PHP Errors
     *
     * ----------------
     *
     * Generic or expected errors that can't be handled,
     */
    const ERROR_CORE = 10000; //Generic Oroboros core errors.
    const ERROR_CORE_CONTRACT_INVALID = 10001; //The contract interface is not honored.
    const ERROR_CORE_ENUMERATE_INVALID = 10002; //The enumerated interface is not honored.
    const ERROR_CORE_API_INVALID = 10003; //The api interface is not honored.
    const ERROR_CORE_CONTRACT_MISCONFIGURED = 10004; //The contract interface does not account for all public methods of the class.
    const ERROR_CORE_ENUMERATE_MISCONFIGURED = 10005; //The enumerated interface does not declare expected values.
    const ERROR_CORE_API_MISCONFIGURED = 10006; //The api interface does not declare the expected api values, or does not account for all objects in its api.
    const ERROR_CORE_CONTRACT_MISSING = 10007; //A reference is made to a nonexistent contract interface.
    const ERROR_CORE_ENUMERATE_MISSING = 10008; //A reference is made to a nonexistent enumerated interface.
    const ERROR_CORE_API_MISSING = 10009; //A reference is made to a nonexistent api interface.
    const ERROR_CORE_CLASS_INVALID = 10010; //An attempt to use a class in an invalid scope occured.
    const ERROR_CORE_CLASS_MISCONFIGURED = 10011; //A class cannot resolve correctly because its runtime initialization logic is not valid.
    const ERROR_CORE_CLASS_MISSING = 10012; //An expected class could not be found, and this exception was thrown to prevent a fatal error.
    const ERROR_CORE_INTERFACE_INVALID = 10013; //An attempt to use an interface in an invalid scope occured.
    const ERROR_CORE_INTERFACE_MISCONFIGURED = 10014; //An interface cannot resolve correctly because its runtime initialization logic is not valid.
    const ERROR_CORE_INTERFACE_MISSING = 10015; //An expected interface could not be found, and this exception was thrown to prevent a fatal error.
    const ERROR_CORE_ABSTRACT_INVALID = 10016; //An attempt to use an abstract class in an invalid scope occured.
    const ERROR_CORE_ABSTRACT_MISCONFIGURED = 10017; //An abstract class cannot resolve correctly because its runtime initialization logic is not valid.
    const ERROR_CORE_ABSTRACT_MISSING = 10018; //An expected abstract class could not be found, and this exception was thrown to prevent a fatal error.
    const ERROR_CORE_TRAIT_INVALID = 10019; //An attempt to use a trait in an invalid scope occured.
    const ERROR_CORE_TRAIT_MISCONFIGURED = 10020; //A trait cannot resolve correctly because its runtime initialization logic is not valid.
    const ERROR_CORE_TRAIT_MISSING = 10021; //An expected trait could not be found, and this exception was thrown to prevent a fatal error.

    //Class Contextual Failure Messages

    const ERROR_CORE_CORE_FAILURE = 10100; //Generic Oroboros core errors.
    const ERROR_CORE_ROUTINE_FAILURE = 10150; //Generic Oroboros core errors.
    const ERROR_CORE_UTILITY_FAILURE = 10200; //Generic Oroboros core errors.
    const ERROR_CORE_PATTERN_FAILURE = 10250; //Generic Oroboros core errors.
    const ERROR_CORE_LIBRARY_FAILURE = 10300; //Generic Oroboros core errors.
    const ERROR_CORE_CONTROLLER_FAILURE = 10350; //Generic Oroboros core errors.
    const ERROR_CORE_MODEL_FAILURE = 10400; //Generic Oroboros core errors.
    const ERROR_CORE_VIEW_FAILURE = 10450; //Generic Oroboros core errors.
    const ERROR_CORE_ADAPTER_FAILURE = 10500; //Generic Oroboros core errors.
    const ERROR_CORE_MODULE_FAILURE = 10550; //Generic Oroboros core errors.
    const ERROR_CORE_EXTENSION_FAILURE = 10600; //Generic Oroboros core errors.
    const ERROR_CORE_COMPONENT_FAILURE = 10650; //Generic Oroboros core errors.
}
