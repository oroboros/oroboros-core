<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Data Exception Code API>
 * This interface declares all of the exception codes used to designate a
 * failure on account of data integrity, source, or operation. These codes can grant
 * some contextual information about what manner of logic failure occurred
 * in the event that an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface DataExceptionCodes
extends ExceptionBase
{
    const RANGE_ERROR_DATA_MIN = 11000;
    const RANGE_ERROR_DATA_MAX = 11999;
    /**
     * PHP Errors
     *
     * ----------------
     *
     * Generic or expected errors that can't be handled,
     */
    const ERROR_DATA = 11000; //Generic Oroboros data errors.
    const ERROR_DATA_FILESYSTEM = 11100; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_NOT_READABLE = 11110; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_NOT_WRITABLE = 11120; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_NOT_EXECUTABLE = 11130; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_FILE_MISSING = 11140; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_FILE_WRONG_FORMAT = 11150; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_FILE_CORRUPT = 11160; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_DIRECTORY_MISSING = 11170; //Generic Oroboros filesystem errors.
    const ERROR_DATA_FILESYSTEM_DIRECTORY_EMPTY = 11180; //Generic Oroboros filesystem errors.
    const ERROR_DATA_ENDPOINT = 11200; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_MISSING = 11210; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_UNAUTHORIZED = 11220; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_UNAVAILABLE = 11230; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_INVALID = 11240; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_PARTIAL = 11250; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_NON_RESPONSIVE = 11260; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_REFUSED = 11270; //Generic Oroboros endpoint errors.
    const ERROR_DATA_ENDPOINT_UNKNOWN = 11280; //Generic Oroboros endpoint errors.
    const ERROR_DATA_DATABASE = 11300; //Generic Oroboros database errors.
    const ERROR_DATA_CACHE = 11400; //Generic Oroboros cache errors.
    const ERROR_DATA_CACHE_INVALID = 11410; //Generic Oroboros cache errors.
    const ERROR_DATA_CACHE_MISSING = 11420; //Generic Oroboros cache errors.
    const ERROR_DATA_CACHE_CORRUPT= 11430; //Generic Oroboros cache errors.
    const ERROR_DATA_CACHE_UNEXPECTED_VALUE = 11440; //Generic Oroboros cache errors.
    const ERROR_DATA_CACHE_UNAVAILABLE = 11450; //Generic Oroboros cache errors.
    const ERROR_DATA_CACHE_REFUSED = 11460; //Generic Oroboros cache errors.
    const ERROR_DATA_CONFIGURATION = 11500; //Generic Oroboros configuration errors.
    const ERROR_DATA_SCHEMA = 11600; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_INVALID = 11610; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_UNSUPPORTED = 11620; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_UNKNOWN = 11630; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_CORRUPTED = 11640; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_READER_ERROR = 11650; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_WRITER_ERROR = 11660; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_PARSER_ERROR = 11670; //Generic Oroboros schema errors.
    const ERROR_DATA_SCHEMA_CASTER_ERROR = 11680; //Generic Oroboros schema errors.
    const ERROR_DATA_TYPE = 11700; //Generic Oroboros data type errors.
    const ERROR_DATA_TYPE_INVALID = 11710; //Generic Oroboros data type errors.
    const ERROR_DATA_TYPE_UNKNOWN = 11720; //Generic Oroboros data type errors.
    const ERROR_DATA_TYPE_WRONG_CONTEXT = 11730; //Generic Oroboros data type errors.
    const ERROR_DATA_TYPE_UNEXPECTED = 11740; //Generic Oroboros data type errors.
    const ERROR_DATA_TYPE_INCOMPATIBLE = 11750; //Generic Oroboros data type errors.
    const ERROR_DATA_VALIDATION = 11800; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_FORMAT = 11810; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_INSTANCE = 11811; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_FORMATTER = 11820; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_KEY = 11830; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_VALUE = 11840; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_SCHEMA = 11850; //Generic Oroboros data validation errors.
    const ERROR_DATA_VALIDATION_INVALID_DATA_TYPE = 11860; //Generic Oroboros data validation errors.
    const ERROR_DATA_IO = 11900; //Generic Oroboros data read/write errors.
    const ERROR_DATA_IO_LOAD_FAILURE = 11910; //Generic Oroboros data read/write errors.
    const ERROR_DATA_IO_SAVE_FAILURE = 11920; //Generic Oroboros data read/write errors.
    const ERROR_DATA_IO_UPDATE_FAILURE = 11930; //Generic Oroboros data read/write errors.
    const ERROR_DATA_IO_DELETE_FAILURE = 11940; //Generic Oroboros data read/write errors.
    const ERROR_DATA_IO_PARSE_FAILURE = 11950; //Generic Oroboros data read/write errors.
    const ERROR_DATA_IO_CAST_FAILURE = 11960; //Generic Oroboros data read/write errors.

}
