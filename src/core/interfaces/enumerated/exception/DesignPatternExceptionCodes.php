<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Exception Code API>
 * This interface declares all of the exception codes used by design pattern
 * implementations throughout the system. These codes can grant some contextual
 * information about what manner of logic failure occurred in the event that
 * an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface DesignPatternExceptionCodes
extends ExceptionBase
{
    const RANGE_ERROR_PATTERN_MIN = 12000;
    const RANGE_ERROR_PATTERN_MAX = 12999;

    /**
     * 12000 - 12099: Generic pattern errors
     */

    const ERROR_PATTERN = 12000;
    const ERROR_PATTERN_FAILURE_CHAIN_OF_RESPONSIBILITY = 12001;
    const ERROR_PATTERN_FAILURE_DIRECTOR = 12002;
    const ERROR_PATTERN_FAILURE_MANAGER = 12003;
    const ERROR_PATTERN_FAILURE_WORKER = 12004;
    const ERROR_PATTERN_FAILURE_BUILDER = 12005;
    const ERROR_PATTERN_FAILURE_REGISTRY = 12006;
    const ERROR_PATTERN_FAILURE_STATIC_REGISTRY = 12007;
    const ERROR_PATTERN_FAILURE_STATE = 12008;
    const ERROR_PATTERN_FAILURE_COMMAND = 12009;
    const ERROR_PATTERN_FAILURE_INTERPRETER = 12010;
    const ERROR_PATTERN_FAILURE_ITERATOR = 12011;
    const ERROR_PATTERN_FAILURE_MEDIATOR = 12012;
    const ERROR_PATTERN_FAILURE_MEMENTO = 12013;
    const ERROR_PATTERN_FAILURE_NULL_OBJECT = 12014;
    const ERROR_PATTERN_FAILURE_OBSERVER = 12015;
    const ERROR_PATTERN_FAILURE_STRATEGY = 12016;
    const ERROR_PATTERN_FAILURE_TEMPLATE = 12017;
    const ERROR_PATTERN_FAILURE_VISITOR = 12018;
    const ERROR_PATTERN_FAILURE_ABSTRACT_FACTORY = 12019;
    const ERROR_PATTERN_FAILURE_FACTORY = 12020;
    const ERROR_PATTERN_FAILURE_OBJECT_POOL = 12021;
    const ERROR_PATTERN_FAILURE_PROTOTYPE = 12022;
    const ERROR_PATTERN_FAILURE_SINGLETON = 12023;
    const ERROR_PATTERN_FAILURE_ADAPTER = 12024;
    const ERROR_PATTERN_FAILURE_BRIDGE = 12025;
    const ERROR_PATTERN_FAILURE_COMPOSITE = 12026;
    const ERROR_PATTERN_FAILURE_DECORATOR = 12027;
    const ERROR_PATTERN_FAILURE_FACADE = 12028;
    const ERROR_PATTERN_FAILURE_FLYWEIGHT = 12029;
    const ERROR_PATTERN_FAILURE_PRIVATE_CLASS_DATA = 12030;
    const ERROR_PATTERN_FAILURE_PROXY = 12031;
    const ERROR_PATTERN_FAILURE_DOMAIN_MODEL = 12032;
    const ERROR_PATTERN_FAILURE_ACTIVE_OBJECT = 12033;
    const ERROR_PATTERN_FAILURE_BALKING_PATTERN = 12034;
    const ERROR_PATTERN_FAILURE_BARRIER = 12035;
    const ERROR_PATTERN_FAILURE_BINDING_PROPERTIES = 12036;
    const ERROR_PATTERN_FAILURE_DOUBLE_CHECKED_LOCKING = 12037;
    const ERROR_PATTERN_FAILURE_EVENT_BASED_ASYNCHRONOUS = 12038;
    const ERROR_PATTERN_FAILURE_GUARDED_SUSPENSION = 12039;
    const ERROR_PATTERN_FAILURE_JOIN = 12040;
    const ERROR_PATTERN_FAILURE_LOCK = 12041;
    const ERROR_PATTERN_FAILURE_MONITOR = 12042;
    const ERROR_PATTERN_FAILURE_PROACTOR = 12043;
    const ERROR_PATTERN_FAILURE_REACTOR = 12044;
    const ERROR_PATTERN_FAILURE_READ_WRITE_LOCK = 12045;
    const ERROR_PATTERN_FAILURE_SCHEDULER = 12046;
    const ERROR_PATTERN_FAILURE_THREAD_POOL = 12047;
    const ERROR_PATTERN_FAILURE_THREAD_LOCAL_STORAGE = 12048;
    const ERROR_PATTERN_FAILURE_FRONT_CONTROLLER = 12049;
    const ERROR_PATTERN_FAILURE_INTERCEPTOR = 12050;
    const ERROR_PATTERN_FAILURE_MVC = 12051;
    const ERROR_PATTERN_FAILURE_ADR = 12052;
    const ERROR_PATTERN_FAILURE_ECS = 12053;
    const ERROR_PATTERN_FAILURE_N_TIER = 12054;
    const ERROR_PATTERN_FAILURE_SPECIFICATION = 12055;
    const ERROR_PATTERN_FAILURE_PUBLISH_SUBSCRIBE = 12056;
    const ERROR_PATTERN_FAILURE_NAKED_OBJECT = 12057;
    const ERROR_PATTERN_FAILURE_SERVICE_LOCATOR = 12058;
    const ERROR_PATTERN_FAILURE_ACTIVE_RECORD = 12059;
    const ERROR_PATTERN_FAILURE_IDENTITY_MAP = 12060;
    const ERROR_PATTERN_FAILURE_DATA_ACCESS_OBJECT = 12061;
    const ERROR_PATTERN_FAILURE_DATA_TRANSFER_OBJECT = 12062;
    const ERROR_PATTERN_FAILURE_INVERSION_OF_CONTROL = 12063;
    const ERROR_PATTERN_FAILURE_BLACKBOARD = 12064;
    const ERROR_PATTERN_FAILURE_BUSINESS_DELEGATE = 12065;
    const ERROR_PATTERN_FAILURE_COMPOSITE_ENTITY = 12066;
    const ERROR_PATTERN_FAILURE_DEPENDENCY_INJECTION = 12067;
    const ERROR_PATTERN_FAILURE_INTERCEPTING_FILTER = 12068;
    const ERROR_PATTERN_FAILURE_LAZY_LOADING = 12069;
    const ERROR_PATTERN_FAILURE_MOCK_OBJECT = 12070;
    const ERROR_PATTERN_FAILURE_SERVANT = 12071;
    const ERROR_PATTERN_FAILURE_TWIN = 12072;
    const ERROR_PATTERN_FAILURE_TYPE_TUNNEL = 12073;
    const ERROR_PATTERN_FAILURE_METHOD_CHAINING = 12074;
    const ERROR_PATTERN_FAILURE_BLOCKCHAIN = 12075;

    /**
     * 12100 - 12199: Core implementation errors
     */

    /**
     * 12200 - 12299: Core extension implementation errors
     */

    /**
     * 12300 - 12999: Reserved for expansion
     */

}
