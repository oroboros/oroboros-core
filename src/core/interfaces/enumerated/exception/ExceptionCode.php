<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Exception Code API>
 * This interface declares all of the exception codes used by the system. It may
 * be implemented to provide easy access to error code levels for checking or throwing
 * system compliant exceptions.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface ExceptionCode
extends ExceptionBase,
 CoreExceptionCodes,
 DataExceptionCodes,
 PhpExceptionCodes,
 InitializationExceptionCodes,
 LogicExceptionCodes,
 SecurityExceptionCodes,
 DesignPatternExceptionCodes
{

}
