<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Logic Exception Code API>
 * This interface declares all of the exception codes used to designate a
 * failure on account of logic collisions through faulty implementation,
 * abstraction, or usage of otherwise stable underlying logic. These codes
 * can grant some contextual information about what manner of logic failure
 * occurred in the event that an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface LogicExceptionCodes
extends ExceptionBase
{
    const RANGE_ERROR_LOGIC_MIN = 19000;
    const RANGE_ERROR_LOGIC_MAX = 19999;
    /**
     * Logic Errors
     *
     * ----------------
     *
     * Collision or misuse of otherwise stable functionality.
     */
    const ERROR_LOGIC = 19000; //Logic Error - An error generated from sloppy programming logic, like infinite recursion or dividing by zero. These should usually be anticipated and handled before they happen, and indicate that a patch is needed.
    const ERROR_LOGIC_BAD_PARAMETERS = 19001; //Logic Error - A parameter was supplied, but is malformed.
    const ERROR_LOGIC_MISSING_PARAMETERS = 19002; //Logic Error - A required parameter was not supplied.
    const ERROR_LOGIC_AMBIGUOUS_REFERENCE = 19003; //Logic Error - More than one option is available and only one is acceptable.
    const ERROR_LOGIC_UNEXPECTED_RETURN_VALUE = 19004; //Logic Error - The return format did not conform to expectation.
    const ERROR_LOGIC_ASSUMPTION_OF_CONTROL = 19005; //Logic Error - A sub-process hijacked program control in an unexpected way.
    const ERROR_LOGIC_INCOMPATIBILITY = 19006; //Logic Error - A class or object is not compatible with the methodology it is attempting to implement.
    const ERROR_LOGIC_DEPRECATION = 19007; //Logic Error - A method or function is not supported or is slated for removal.
    const ERROR_LOGIC_ASSUMPTION_OF_LANGUAGE_FEATURE = 19008; //Logic Error - A method or function is assumed to exist when it does not, and this is not handled prior to the logic being encountered.
    const ERROR_LOGIC_ASSUMPTION_OF_DEPENDENCY_AVAILABILITY = 19009; //Logic Error - An external extension, module, or library is assumed to exist when it does not, and this is not accounted for prior to usage.
    const ERROR_LOGIC_ASSUMPTION_OF_VALUE = 19010; //Logic Error - An variable or property is expected to have a specified value but does not.
    const ERROR_LOGIC_ASSUMPTION_OF_TYPE = 19011; //Logic Error - An variable or property is expected to have a specified type but does not.
    const ERROR_LOGIC_ASSUMPTION_OF_MUTABILITY = 19012; //Logic Error - An immutable object is assumed to be mutable.
    const ERROR_LOGIC_ASSUMPTION_OF_SCOPE = 19013; //Logic Error - An expected scope is not honored, because it is broader or narrower than the implemented usage expects.
    const ERROR_LOGIC_INFINITE_RECURSION = 19014; //Logic Error - Arecursive loop does not have an exit point.
    const ERROR_LOGIC_SELF_REFERENTIAL_LOOP = 19015; //Logic Error - A recursive loop does not have an exit point, and occurs across multiple methods.
    const ERROR_LOGIC_BLINDLY_TRUSTED_API = 19016; //Logic Error - A method or function is expected to exist without any specific effort to validate that it does, such as checking by an interface or parent class.
    const ERROR_LOGIC_COMPILE_ERROR = 19017; //Logic Error - A function, class, trait, or interface could not properly compile because of syntax errors or invalid extension.
    const ERROR_LOGIC_UNREACHABLE_CODE = 19018; //Logic Error - A block of code exists that can never be reached, such as additional code beyond a return statement, or something in an if (false) block. This indicates that it should be either commented out or removed.
    const ERROR_LOGIC_ASSUMPTION_OF_ENVIRONMENT = 19019; //Logic Error - A specific runtime environment is expected, but is not presently available, such as trying to use CLI functionality from an HTTP request or vice versa, or trying to leverage a platform specific feature when not on that platform.
    const ERROR_LOGIC_CLASS_NOT_FOUND = 19020; //Logic Error - A specific class, trait or interface is needed to continue, cannot be autoloaded, and does not exist in any known location.
    const ERROR_LOGIC_UNDECLARED_VARIABLE = 19021; //Logic Error - A non-existent variable or class property is referenced without being declared prior to the reference.
    const ERROR_LOGIC_BAD_VISIBILITY_REFERENCE = 19022; //Logic Error - A method call to a method that cannot be reached in the current scope exists.
    const ERROR_LOGIC_ASSUMPTION_OF_SCHEMA = 19023; //Logic Error - A complicated schema is blindly trusted to exist and fails because it is not defined as expected, and is not validated prior to reference.
    const ERROR_LOGIC_AMBIGUOUS_RESPONSIBILITY = 19024; //Logic Error - A given piece of logic has too many responsibilities.
    const ERROR_LOGIC_EXCESSIVE_COMPLEXITY = 19025; //Logic Error - A straightforward operation is overengineered to the point of being impossible to follow.
    const ERROR_LOGIC_CODE_DUPLICATION = 19026; //Logic Error - A piece of logic is redeclared that exists and is reachable elsewhere, and no modification to the logic was presented.
    const ERROR_LOGIC_LANGUAGE_ABUSE = 19027; //Logic Error - A piece of logic is used in a way that abuses language features for impractible purposes, such as conditionally declaring a class that is not specifically a compatibility patch.
    const ERROR_LOGIC_IMPROPER_VALUE_MUTATION = 19028; //Logic Error - A value or property is modified in a way that makes it unusable for its intended purpose, or is mutated by a piece of logic that has no business modifying it.
    const ERROR_LOGIC_ASSUMPTION_OF_INITIALIZATION = 19029; //Logic Error - A piece of logic is attempting to leverage a feature that is not available until after initialization has occurred.
    const ERROR_LOGIC_MISSING_DECLARATION = 19030; //Logic Error - An function, constant, or method that is expected to be declared is not, meaning that the logic being interacted with does not honor its expressed api correctly.

}
