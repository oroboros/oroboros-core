<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Exception Code API>
 * This interface declares all of the exception codes used to designate a
 * failure on account of typical PHP usage in the context of
 * vanilla PHP methodology. These codes can grant some contextual
 * information about what manner of logic failure occurred in the event that
 * an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface PhpExceptionCodes
extends ExceptionBase
{
    const RANGE_ERROR_PHP_MIN = 13000;
    const RANGE_ERROR_PHP_MAX = 13999;
    /**
     * PHP Errors
     *
     * ----------------
     *
     * Generic or expected errors that can't be handled,
     */
    const ERROR_PHP = 13000; //PHP Errors, vanilla exceptions that can't be resolved, etc.
    const ERROR_PHP_METHOD_FAILURE = 13001; //Method Failure.
    const ERROR_PHP_INSTANTIATION_FAILURE = 13002; //Instantiation Failure.
    const ERROR_PHP_DESTRUCTOR_FAILURE = 13003; //Destruct Failure.
    const ERROR_PHP_BAD_PARAMETERS = 13004; //Bad parameters.
    const ERROR_PHP_BAD_METHOD_CALL = 13005; //Bad method call.
    const ERROR_PHP_BAD_FUNCTION_CALL = 13006; //Bad function call.
    const ERROR_PHP_WRONG_INSTANCE_SUPPLIED = 13007; //Wrong instance passed.
    const ERROR_PHP_WRONG_SCHEMA_PROVIDED = 13008; //Wrong schema provided (Missing expected keys in array, etc).
    const ERROR_PHP_NOT_INITIALIZED = 13009; //Not initialized.
    const ERROR_PHP_INVOCATION_FAILURE = 13010; //Invocation failed.
    const ERROR_PHP_CALL_FAILURE = 13011; //Call failed.
    const ERROR_PHP_CLONE_FAILURE = 13012; //Clone failed.
    const ERROR_PHP_SERIALIZATION_FAILURE = 13013; //Serialization failed.
    const ERROR_PHP_UNSERIALIZATION_FAILURE = 13014; //Unserialization failed.
    const ERROR_PHP_SLEEP_FAILURE = 13015; //Sleep failed.
    const ERROR_PHP_WAKEUP_FAILURE = 13016; //Wakeup failed.
    const ERROR_PHP_GETTER_FAILURE = 13017; //Get failed.
    const ERROR_PHP_SETTER_FAILURE = 13018; //Set failed.
    const ERROR_PHP_TO_ARRAY_FAILURE = 13019; //To Array failed. Placeholder for if the language ever supports this, and may be used through internal abstraction.
    const ERROR_PHP_CALLSTATIC_FAILURE = 13020; //CallStatic failed.
    const ERROR_PHP_IS_SET_FAILURE = 13021; //IsSet failed.
    const ERROR_PHP_UN_SET_FAILURE = 13022; //UnSet failed.
    const ERROR_PHP_DEBUG_INFO_FAILURE = 13023; //DebugInfo failed.
    const ERROR_PHP_KEY_NOT_FOUND = 13024; //DebugInfo failed.
    const ERROR_PHP_COMPILE_ERROR = 13025; //Compiler error.
    const ERROR_PHP_MISSING_PHP_EXTENSION = 13026; //Missing PHP extension.
    const ERROR_PHP_DEPRECATED_FEATURE = 13027; //The requested functionality is deprecated, and will throw warnings.
    const ERROR_PHP_REMOVED_FEATURE = 13028; //The requested functionality has been removed in the current PHP version.
    const ERROR_PHP_INCOMPATIBLE_FEATURE = 13029; //The requested functionality is not yet available in the current PHP version.
    const ERROR_PHP_SYNTAX = 13030; //Syntax error.

}
