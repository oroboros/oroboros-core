<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Exception Code API>
 * This interface declares all of the exception codes used to designate a
 * failure on account of security violation. These codes can grant some contextual
 * information about what manner of logic failure occurred in the event that
 * an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface SecurityExceptionCodes
extends ExceptionBase
{
    const RANGE_ERROR_SECURITY_MIN = 17000;
    const RANGE_ERROR_SECURITY_MAX = 17999;
    /**
     * PHP Errors
     *
     * ----------------
     *
     * Generic or expected errors that can't be handled,
     */
    const ERROR_SECURITY = 17000; //Security Error - An error with a security protocol. Usually thrown because of an intentional, malicious attempt to break things by a 3rd party.
    const ERROR_SECURITY_LOCKED_RESOURCE = 17100; //Security Error - An attempt was made to access a locked resource in the codebase.
    const ERROR_SECURITY_LOCKED_FILE = 17110; //Security Error - An attempt was made to access a locked resource in the filebase.
    const ERROR_SECURITY_LOCKED_TABLE = 17120; //Security Error - An attempt was made to access a locked table in the database.
    const ERROR_SECURITY_LOCKED_COLUMN = 17121; //Security Error - An attempt was made to access a locked column in the database.
    const ERROR_SECURITY_LOCKED_OBJECT = 17122; //Security Error - An attempt was made to access a locked object.
    const ERROR_SECURITY_LOCKED_EVENT = 17123; //Security Error - An attempt was made to access a locked event.
    const ERROR_SECURITY_LOCKED_ENTITY = 17124; //Security Error - An attempt was made to access a locked entity.
    const ERROR_SECURITY_LOCKED_JOB = 17125; //Security Error - An attempt was made to access a locked job.
    const ERROR_SECURITY_LOCKED_POLICY = 17126; //Security Error - An attempt was made to access a locked policy.
    const ERROR_SECURITY_LOCKED_ROUTE = 17127; //Security Error - An attempt was made to access a locked route.
    const ERROR_SECURITY_LOCKED_COMMAND = 17128; //Security Error - An attempt was made to access a locked command.

}
