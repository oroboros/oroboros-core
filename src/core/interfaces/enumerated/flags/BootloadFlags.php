<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\flags;

/**
 * <Bootload Flag Enumerated Api Interface>
 * This is the master interface for core bootload flags.
 * These flags may be passed in to the base accessor to
 * modify bootload behavior.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage flags
 * @version 0.2.5
 * @since 0.2.5
 * @see oroboros\Oroboros
 * @see oroboros\core\traits\Oroboros
 * @see oroboros\core\interfaces\contract\OroborosContract
 */
interface BootloadFlags
extends FlagBase
{
    /**
     * Designates that the runtime process is a test,
     * and only relevant resources should be loaded.
     */
    const FLAG_UNIT_TEST = '::unit_test::';

    /**
     * Designates that additional debugging
     * information should be shown and/or collected.
     */
    const FLAG_DEBUG = '::debug::';

    /**
     * Designates that extensions, modules, and other 3rd party additions
     * should be treated as unsafe, and not allowed to make changes
     * whenever applicable.
     */
    const FLAG_SANDBOX = '::sandbox::';

    /**
     * Production server, all errors and
     * server details should be opaque to the client.
     * Typically, this designates the following behavior:
     * @note Errors are not displayed on screen.
     * @note Alpha features are disabled.
     * @note Beta features may be disabled, depending on app settings.
     * @note Errors with level ALERT or EMERGENCY alert the maintainer immediately.
     */
    const FLAG_PRODUCTION = '::production::';

    /**
     * Development server, additional statistics
     * and information displayed about performance
     * and errors is ok.
     */
    const FLAG_DEVELOPMENT = '::development::';

    /**
     * Designates that the current execution should be handled as a dryrun.
     * This will be passed along to all manager classes to indicate not to
     * make changes, and only collect statistics on what would have changed.
     */
    const FLAG_DRYRUN = '::dryrun::';

    /**
     * Designates that the current execution should monitored for performance.
     * This enables functionality that collects data on the execution time of
     * any task that runs, and will produce log debug entries on the microtime
     * duration of all operations after execution.
     */
    const FLAG_BENCHMARK = '::benchmark::';

}
