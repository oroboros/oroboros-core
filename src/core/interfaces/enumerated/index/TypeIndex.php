<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\index;

/**
 * <Type Index Enumerated Api Interface>
 * Provides an enumerated index of all type enumerated interfaces
 * that ship with Oroboros Core.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
interface TypeIndex
extends IndexBase
{
    const INDEX_BASE_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\TypeBase';
    const INDEX_BASE_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ClassTypeBase';
    const INDEX_MASTER_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ClassType';
    const INDEX_ADAPTER_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\AdapterClassTypes';
    const INDEX_COMPONENT_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ComponentClassTypes';
    const INDEX_CONTROLLER_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ControllerClassTypes';
    const INDEX_CORE_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\CoreClassTypes';
    const INDEX_EXTENSION_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ExtensionClassTypes';
    const INDEX_LIBRARY_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\LibraryClassTypes';
    const INDEX_MODEL_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ModelClassTypes';
    const INDEX_MODULE_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ModuleClassTypes';
    const INDEX_DESIGN_PATTERN_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\PatternClassTypes';
    const INDEX_ROUTINE_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\RoutineClassTypes';
    const INDEX_UTILITY_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\UtilityClassTypes';
    const INDEX_VIEW_CLASS_TYPE = '\\oroboros\\core\\interfaces\\enumerated\\type\\ViewClassTypes';
}
