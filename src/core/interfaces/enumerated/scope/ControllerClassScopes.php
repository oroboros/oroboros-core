<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Controller Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for controller class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ControllerClassScopes
extends ClassScopeBase
{
    /**
     * <Controller>
     *
     * ----------------
     *
     * A controller is tasked with making all of the decisions,
     * and ideally should also do none of the work.
     * Controllers job is to interpret a request and execute
     * the corresponding execution plan. Individual tasks within that
     * execution plan should be considered out of scope for controllers
     * to be directly executing unless they are extremely simple
     * and non-blocking.
     *
     * Primarily, the Controller should be designating all work to sub-objects,
     * and only deciding which ones to load and what to order them to do.
     *
     * This separation is not enforced, but the system heavily implies that it
     * should be followed, and it's entire structure is intended to
     * accomplish this. Failing to take this approach will cause you roadblocks
     * later, but if you have a good reason for doing it that way, then by all
     * means do so, nothing will inherently prevent it.
     */

    /**
     * <Generic Controller>
     * This classification designates only that the class is a controller object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_CONTROLLER = "::controller::";
    const CLASS_SCOPE_CONTROLLER_ABSTRACT = '::abstract-controller::';
    const CLASS_SCOPE_CONTROLLER_NULL = '::null-controller::';
    const CLASS_SCOPE_CONTROLLER_AJAX = '::ajax-controller::';
    const CLASS_SCOPE_CONTROLLER_PUBLIC = '::public-controller::';
    const CLASS_SCOPE_CONTROLLER_SECURITY = '::security-controller::';
    const CLASS_SCOPE_CONTROLLER_ERROR = '::error-controller::';
    const CLASS_SCOPE_CONTROLLER_ADMIN = '::admin-controller::';
    const CLASS_SCOPE_CONTROLLER_SYSTEM = '::system-controller::';
    const CLASS_SCOPE_CONTROLLER_CGI = '::cgi-controller::';
    const CLASS_SCOPE_CONTROLLER_FRONT = '::front-controller::';
    const CLASS_SCOPE_CONTROLLER_REST = '::rest-controller::';
    const CLASS_SCOPE_CONTROLLER_HTTP = '::http-controller::';
    const CLASS_SCOPE_CONTROLLER_SOCKET = '::socket-controller::';
    const CLASS_SCOPE_CONTROLLER_PROXY = '::proxy-controller::';
    const CLASS_SCOPE_CONTROLLER_MODULE = '::module-controller::';
    const CLASS_SCOPE_CONTROLLER_EXTENSION = '::extension-controller::';
    const CLASS_SCOPE_CONTROLLER_DEBUG = '::debug-controller::';
    const CLASS_SCOPE_CONTROLLER_TEST = '::test-controller::';

}
