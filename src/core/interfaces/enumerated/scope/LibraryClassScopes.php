<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Library Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for library class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface LibraryClassScopes
extends ClassScopeBase
{
    /**
     * <Library>
     *
     * ----------------
     *
     * Libraries are multi-purpose classes that cover a specific area of focus,
     * and provide functionality to accomplish it easily. The majority of other
     * classes distributed in this system are subsets of libraries, or extend
     * from them.
     *
     * Libraries ideally should do all of the work and make
     * none of the decisions. They are the inverse of the Controller
     * (which makes all of the decisions and does none of the work).
     *
     * What designates something as a library, is that it does not
     * provide opinion, and does not fall into a more focused
     * class definition, and also is not inherently a wrapper for
     * external functionality (though it may interact with one or
     * multiple of these to do it's job).
     *
     * Libraries typically have a singular, narrow scope of focus, and are
     * always the default option for accomplishing tasks within that focus.
     * If this is not the case, they are at least an option on the list for
     * accomplishing tasks in that area of focus (eg a MySQL library and a
     * PostGRES library might be used interchangeably, but they both only
     * deal with databases).
     */

    /**
     * <Generic Library>
     * This classification designates only that the class is a library object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY = '::library::';

    /**
     * A library intended to be extended
     */
    const CLASS_SCOPE_LIBRARY_ABSTRACT = '::abstract-library::';

    /**
     * A library that satisfies an expected condition and performs no operation.
     */
    const CLASS_SCOPE_LIBRARY_NULL = '::null-library::';

    /**
     * A library that handles adapters
     */
    const CLASS_SCOPE_LIBRARY_ADAPTER = '::adapter-library::';

    /**
     * <Auth>
     *
     * ----------------
     *
     * Auth objects make decisions about whether a request is valid or not.
     * They will make a judgment, but do not execute any action for having
     * made it, they just make the verdict available. Asking for a verdict
     * is in scope for auth objects, using that verdict to change the flow
     * of logic is out of scope.
     *
     * Auth objects are often paired with a Validator, but this prerequisite
     * is situational, so it is not enforced. As long as they internally
     * resolve the verdict, make no change to state outside their scope,
     * and make the verdict available, they are doing their job correctly.
     *
     * Controllers should use Auth objects to make complex determinations
     * instead of putting it inline, so that the controller code represents
     * more the process of action per decision than the actual determination.
     * This approach is not enforced, but it does keep code clean and readable,
     * and keeps the single-responsibility principle intact.
     */

    /**
     * <Generic Auth>
     * This classification designates only that the class is a auth object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_AUTH = '::auth-library::';
    const CLASS_SCOPE_LIBRARY_AUTH_ABSTRACT = '::auth-abstract-library::';
    const CLASS_SCOPE_LIBRARY_AUTH_NULL = '::auth-null-library::';
    const CLASS_SCOPE_LIBRARY_AUTH_DEFAULT = '::auth-default-library::';

    /**
     * A library that provides or extends autoloading
     * @note Psr-4, Psr-0, classmap, and schema mapping are supported
     */
    const CLASS_SCOPE_LIBRARY_AUTOLOAD = '::autoload-library::';
    const CLASS_SCOPE_LIBRARY_AUTOLOAD_ABSTRACT = '::autoload-abstract-library::';
    const CLASS_SCOPE_LIBRARY_AUTOLOAD_MANAGER = '::autoload-manager-library::';
    const CLASS_SCOPE_LIBRARY_AUTOLOAD_PSR0 = '::autoload-psr0-library::';
    const CLASS_SCOPE_LIBRARY_AUTOLOAD_PSR4 = '::autoload-psr4-library::';
    const CLASS_SCOPE_LIBRARY_AUTOLOAD_CLASSMAP = '::autoload-classmap-library::';
    const CLASS_SCOPE_LIBRARY_AUTOLOAD_SCHEMA = '::autoload-schema-library::';

    /**
     * <Bootstrap>
     *
     * ----------------
     *
     * Bootstrap classes are libraries that prepare the system for execution.
     * They are tasked with loading prerequisite files, defining the environment,
     * instantiating the router, determining the correct controller and method
     * from the router, then executing the request. Bootstrap classes within this
     * system typically get called from a routine, but can be instantiated and
     * run independently also. Keep in mind that this is a low level class,
     * and it is going to assume control when you execute it unless you pass
     * in flags to make it perform differently. As such, it is generally
     * inappropriate to use a bootstrap object further on in the order of
     * operations, unless you take explicit care to circumvent it's assumption
     * of control.
     */

    /**
     * <Generic Bootstrap>
     * This classification designates only that the class is a bootstrap object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP = '::bootstrap-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_ABSTRACT = '::bootstrap-abstract-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_NULL = '::bootstrap-null-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_DEFAULT = '::bootstrap-default-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_NULLDEBUG = '::bootstrap-nulldebug-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_DRYRUNWEB = '::bootstrap-dryrunweb-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_DRYRUNCGI = '::bootstrap-dryruncgi-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_UNITTEST = '::bootstrap-test-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_PROFILE = '::bootstrap-profile-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_BENCHMARK = '::bootstrap-benchmark-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_WEBPRODUCTION = '::bootstrap-webproduction-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_WEBSTAGING = '::bootstrap-webstaging-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_WEBDEV = '::bootstrap-webdev-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_HTTP_PRODUCTION = '::bootstrap-http-production-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_HTTP_STAGING = '::bootstrap-http-staging-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_HTTP_DEV = '::bootstrap-http-dev-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_CGI_PRODUCTION = '::bootstrap-cgi-production-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_CGI_STAGING = '::bootstrap-cgi-staging-library::';
    const CLASS_SCOPE_LIBRARY_BOOTSTRAP_CGI_DEV = '::bootstrap-cgi-dev-library::';

    /**
     * <Cache>
     *
     * ----------------
     *
     * Cache objects buffer data in a way that may or may not reflect
     * it's exact current state (depending on designation).
     * The purpose of a Cache object is to provide a default
     * set when a piece of information is requested heavily and
     * repeatedly, so that it does not become overly burdensome
     * to performance to get it.
     *
     * The frustration of Caches is when they become out-of-sync with
     * the literal data they represent, and present a wrong dataset.
     * In many cases this is unavoidable, and a threshold of tolerance
     * needs to be designated. In other cases, this is blocking, and
     * the data ALWAYS needs to be represented accurately.
     *
     * The Oroboros implementation of Cache objects follows the Psr-6
     * specification, and will shortly be upgraded to also support the
     * Psr-16 specification as well. As with most other assets in this system,
     * it can be interchangeably used with any other package that honors
     * it's specification correctly.
     */

    /**
     * <Generic Cache>
     * This classification designates only that the class is a cache object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     * @note Must follow Psr-6 spec to be acceptable
     */
    const CLASS_SCOPE_LIBRARY_CACHE = '::cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_MANAGER = '::cache-manager-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_ITEM = '::cache-item-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_POOL = '::cache-pool-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_EXCEPTION = '::cache-exception-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_ABSTRACT = '::abstract-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_NULL = '::null-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_DEFAULT = '::memory-default-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_AUTH = '::auth-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_OBJECT = '::object-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_ADAPTER = '::adapter-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_REQUEST = '::request-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_RESPONSE = '::response-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_GENERIC = '::memory-generic-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_DISK = '::disk-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_MEMORY = '::memory-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_SHARED = '::shared-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_DATA = '::data-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_SERIAL = '::serial-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_BUFFER = '::buffer-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_SESSION = '::session-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_MEMCACHE = '::memcache-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_SERViCE = '::service-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_SERViCE_CDN = '::service-cdn-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_SERViCE_PROXY = '::service-proxy-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_CLIENT = '::client-cache-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_CLIENT_REQUEST = '::client-browser-cache-request-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_CLIENT_RESPONSE = '::client-browser-cache-response-library::';
    const CLASS_SCOPE_LIBRARY_CACHE_CLIENT_BROWSER = '::client-browser-cache-library::';

    /**
     * <Codex>
     *
     * ----------------
     *
     * The Codex is an index of runtime class information.
     * Its task is to provide a library of known classes,
     * index them by keyword, and provide information about
     * them upon request. It is primarilty used for supplying
     * scope information to other classes so they can enforce it,
     * but also works well for debugging, providing detailed
     * api information, and benchmarking counts of currently
     * active instances of objects, and how many of them are unique.
     * It is essentially the system catalog for this system.
     */

    /**
     * <Generic Codex>
     * This classification designates only that the class is a codex,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_CODEX = '::codex-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_INDEX = '::codex-index-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_ENTRY = '::codex-entry-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_ABSTRACT = '::codex-abstract-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_NULL = '::codex-null-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_DEFAULT = '::codex-default-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_GENERIC = '::codex-generic-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_CORE = '::codex-core-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_LEXICON = '::codex-lexicon-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_LEXICON_ARCHIVE = '::codex-lexicon-archive-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_LEXICON_INDEX = '::codex-lexicon-index-library::';
    const CLASS_SCOPE_LIBRARY_CODEX_LEXICON_ENTRY = '::codex-lexicon-entry-library::';

    /**
     * A library that provides configuration functionality
     */
    const CLASS_SCOPE_LIBRARY_CONFIG = '::config-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_ABSTRACT = '::config-abstract-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_CORE = '::config-core-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_COMPOSER = '::config-composer-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_PHPUNIT = '::config-phpunit-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_CODECOV = '::config-codecov-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_APIGEN = '::config-apigen-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_OROBOROS = '::config-oroboros-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_TRAVIS = '::config-travis-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_BITBUCKET_PIPELINES = '::config-bitbucket-pipelines-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_CODEX = '::config-codex-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_BUILD = '::config-build-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_APPLICATION = '::config-application-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_PACKAGE = '::config-package-library::';
    const CLASS_SCOPE_LIBRARY_CONFIG_PACKAGE_MAP = '::config-package-map-library::';

    /**
     * A library that provides container objects
     * @note Must follow Psr-11 spec to be acceptable
     */
    const CLASS_SCOPE_LIBRARY_CONTAINER = '::container-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_ABSTRACT = '::container-abstract-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION = '::container-collection-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_ABSTRACT = '::container-collection-abstract-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_DEFAULT = '::container-collection-default-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_ENUMERATED = '::container-collection-enumerated-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_READONLY = '::container-collection-readonly-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_ARCHIVAL = '::container-collection-archival-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_DIRECTORY = '::container-collection-directory-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_FILES = '::container-collection-files-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_NAMESPACE = '::container-collection-namespace-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_CLOSURE = '::container-collection-closure-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_API = '::container-collection-api-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_CACHE = '::container-collection-cache-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_SERVICE = '::container-collection-service-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_EXCEPTION = '::container-collection-exception-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_DATASET = '::container-collection-dataset-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_SCHEMA = '::container-collection-schema-library::';
    const CLASS_SCOPE_LIBRARY_CONTAINER_COLLECTION_SEARCH = '::container-collection-search-library::';

    /**
     * <Data Object>
     *
     * ----------------
     *
     * Data objects are just containers for information.
     * By themselves, they make no decisions about that information,
     * and only act as a container. To be stable, they must be able
     * to reproduce their original result format from whatever they
     * parse it into, and be able to do so even if changes are made.
     * For simple data objects, this can be done with php internals.
     * For complex data objects, this should be done by using a
     * Parser class.
     */

    /**
     * <Generic Data Object>
     * This classification designates only that the class is a data object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT = '::data-object-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_ABSTRACT = '::data-object-abstract-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_NULL = '::data-object-null-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_READONLY = '::data-object-readonly-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_WRITEONLY = '::data-object-writeonly-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_EXECUTEONLY = '::data-object-executeonly-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_READWRITE = '::data-object-readwrite-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_READEXECUTE = '::data-object-readexecute-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_WRITEEXECUTE = '::data-object-writeexecute-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_OPEN = '::data-object-open-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_CONFIGURABLE = '::data-object-configurable-library::';
    const CLASS_SCOPE_LIBRARY_DATA_OBJECT_HOOK = '::data-object-hook-library::';

    /**
     * A library that provides read-only information functionality
     */
    const CLASS_SCOPE_LIBRARY_DICTIONARY = '::dictionary-library::';
    const CLASS_SCOPE_LIBRARY_DICTIONARY_ABSTRACT = '::dictionary-abstract-library::';
    const CLASS_SCOPE_LIBRARY_DICTIONARY_FILE_TYPES = '::dictionary-file-types-library::';
    const CLASS_SCOPE_LIBRARY_DICTIONARY_PHP_CLASSES = '::dictionary-php-classes-library::';
    const CLASS_SCOPE_LIBRARY_DICTIONARY_LANGUAGE_TRANSLATION = '::dictionary-language-translation-library::';

    /**
     * A library that provides domain name system functionality
     */
    const CLASS_SCOPE_LIBRARY_DNS = '::dns-library::';
    const CLASS_SCOPE_LIBRARY_DNS_DOMAIN = '::dns-domain-library::';
    const CLASS_SCOPE_LIBRARY_DNS_DOMAIN_IPV4 = '::dns-ipv4-library::';
    const CLASS_SCOPE_LIBRARY_DNS_DOMAIN_IPV6 = '::dns-ipv6-library::';
    const CLASS_SCOPE_LIBRARY_DNS_DOMAIN_LOCALHOST = '::dns-localhost-library::';

    /**
     * A library that provides debug functionality
     */
    const CLASS_SCOPE_LIBRARY_DEBUG = '::debug-library::';
    const CLASS_SCOPE_LIBRARY_DEBUG_BACKTRACE = '::debug-backtrace-library::';
    const CLASS_SCOPE_LIBRARY_DEBUG_BENCHMARK = '::debug-benchmark-library::';
    const CLASS_SCOPE_LIBRARY_DEBUG_LISKOV = '::debug-liskov-library::';
    const CLASS_SCOPE_LIBRARY_DEBUG_SUBSTITUTION = '::debug-substitution-library::';
    const CLASS_SCOPE_LIBRARY_DEBUG_VAR_DUMP = '::debug-var-dump-library::';

    /**
     * <Entity>
     *
     * ----------------
     *
     * An Entity is any sort of unique object bound to a concrete identity.
     * Users are entities, as are their accounts, and any other construct
     * that has a defined and reproducible identity.
     *
     * Entity classes are tasked with maintaining the correct association
     * between various data points that collectively constitute the entity,
     * and maintaining their integrity and current state.
     *
     * Entities will typically interact with one or more Records,
     * and determine the association and synchronization between
     * those records internally, and present a public api for
     * interacting with or modifying their state.
     */

    /**
     * <Generic Entity>
     * This classification designates only that the class is an entity object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_ENTITY = '::entity-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_ABSTRACT = '::entity-abstract-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_COLLECTION = '::entity-collection-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_MANAGER = '::entity-manager-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_NULL = '::entity-null-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_USER = '::entity-user-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_SITE = '::entity-site-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_APP = '::entity-app-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_DATA = '::entity-data-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_RECORD = '::entity-record-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_SERVICE = '::entity-service-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_ADAPTER = '::entity-adapter-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_REPORT = '::entity-report-library::';
    const CLASS_SCOPE_LIBRARY_ENTITY_CONFIGURATION = '::entity-config-library::';

    /**
     * <Enum>
     *
     * ----------------
     *
     * Enums provide a flat set of fixed values that are immutable.
     * Enums are generally used internally as reflectors for api interfaces,
     * granting a simple method of quickly indexing for a valid fixed parameter.
     * Enums are always read-only, and build their data set internally in contrast
     * to data-objects, which are packaged from externally defined data.
     * Enums must have all of their data on hand at instantiation without
     * any additional external direction, so they can be consistently relied
     * upon to report their data accurately 100% of the time.
     */

    /**
     * <Generic Enum>
     * This classification designates only that the class is an enumerator object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_ENUM = '::enum-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_ABSTRACT = '::enum-abstract-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_NULL = '::enum-null-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_DEFAULT = '::enum-default-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_STATIC = '::enum-static-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_INTERFACE = '::enum-interface-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_VALIDATOR = '::enum-value-validator-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_API = '::enum-api-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_CODEX = '::enum-codex-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_VALIDATION = '::enum-validation-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_UTILITY = '::enum-utility-library::';
    const CLASS_SCOPE_LIBRARY_ENUM_ENVIRONMENT = '::enum-environment-library::';

    /**
     * A library that provides environment information functionality
     */
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT = '::environment-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_MANAGER = '::environment-manager-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_SERVER = '::environment-server-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_PHP = '::environment-php-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_BROWSER = '::environment-browser-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_CLI = '::environment-cli-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_REQUEST = '::environment-request-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_INSTALLATION = '::environment-installation-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_CORE = '::environment-core-library::';
    const CLASS_SCOPE_LIBRARY_ENVIRONMENT_SUBSYSTEM = '::environment-subsystem-library::';

    /**
     * <Error>
     *
     * ----------------
     *
     * Error objects represent a thrown error,
     * or a class that manages them and provides
     * execution control when errors arise.
     *
     * Native PHP Exceptions are errors,
     * as are any class that manages them.
     */

    /**
     * <Generic Error Handler>
     * This classification designates only that the class is an error handler object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    //Error Handling
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_ABSTRACT = '::error-handler-abstract-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_NULL = '::error-handler-null-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_DEFAULT = '::error-handler-default-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_GENERIC = '::error-handler-generic-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_FATAL = '::error-handler-fatal-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_SECURITY = '::error-handler-security-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_EMERGENCY = '::error-handler-emergency-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_ALERT = '::error-handler-alert-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_CRITICAL = '::error-handler-critical-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_ERROR = '::error-handler-error-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_WARNING = '::error-handler-warning-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_NOTICE = '::error-handler-notice-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_INFO = '::error-handler-info-library::';
    const CLASS_SCOPE_LIBRARY_ERROR_HANDLER_DEBUG = '::error-handler-debug-library::';

    /**
     * <Event>
     *
     * ----------------
     *
     * Events are classes that designate a point of interest
     * during execution, or any time sensitive task that needs
     * to run at a specific interval, date, or time.
     * Like flags, events are primarily tasked with making a
     * determination as to whether they currently apply or not,
     * but also need to be able to report the details of the event
     * to anything that requests more information about it.
     */

    /**
     * <Generic Event>
     * This classification designates only that the class is an event object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_EVENT = '::event-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_ABSTRACT = '::event-abstract-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_COLLECTION = '::event-collection-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_MANAGER = '::event-manager-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_NULL = '::event-null-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_ERROR = '::event-error-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_FLAG = '::event-flag-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_INITIALIZATION = '::event-initialization-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_DEBUG = '::event-debug-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_COMPLETION = '::event-completion-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_BUILD = '::event-build-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_SHUTDOWN = '::event-shutdown-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_CODEX = '::event-codex-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_ROUTING = '::event-routing-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_SERVICE = '::event-service-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_ENTITY = '::event-entity-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_SERVER = '::event-server-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_STREAM = '::event-stream-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_DATA = '::event-data-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_PARSER = '::event-parser-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_JOB = '::event-job-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_SECURITY = '::event-security-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_CONTROLLER = '::event-controller-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_MODEL = '::event-model-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_VIEW = '::event-view-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_ADAPTER = '::event-adapter-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_LIBRARY = '::event-library-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_MODULE = '::event-module-library::';
    const CLASS_SCOPE_LIBRARY_EVENT_EXTENSION = '::event-extension-library::';

    /**
     * A library that provides extension functionality
     */
    const CLASS_SCOPE_LIBRARY_EXTENDABLE = '::extendable-library::';
    const CLASS_SCOPE_LIBRARY_EXTENSION = '::extension-library::';
    const CLASS_SCOPE_LIBRARY_EXTENSION_CONFIG = '::extension-config-library::';
    const CLASS_SCOPE_LIBRARY_EXTENSION_MANAGER = '::extension-manager-library::';
    const CLASS_SCOPE_LIBRARY_EXTENSION_EXTENDABLE = '::extension-extendable-library::';

    /**
     * A library that provides filebase functionality
     */
    const CLASS_SCOPE_LIBRARY_FILE = '::file-library::';
    const CLASS_SCOPE_LIBRARY_FILE_POINTER = '::file-pointer-library::';
    const CLASS_SCOPE_LIBRARY_FILE_UPLOAD = '::file-upload-library::';
    const CLASS_SCOPE_LIBRARY_FILE_COLLECTION = '::file-collection-library::';
    const CLASS_SCOPE_LIBRARY_FILE_MANAGER = '::file-manager-library::';
    const CLASS_SCOPE_LIBRARY_FILE_DIRECTORY = '::file-directory-library::';
    const CLASS_SCOPE_LIBRARY_FILE_DIRECTORY_MANAGER = '::file-directory-manager-library::';

    /**
     * <Flags>
     *
     * ----------------
     *
     * Flags are modifiers passed to trigger altered behavior
     * at instantiation or initialization of an object.
     * They represent a direct order prior to ready-state
     * for that object to operate along custom guidelines
     * for it's remaining lifespan. They cannot be altered
     * after initialization without fully reinitializing.
     *
     * Flags typically represent edge-case behavior that
     * is expected but not common. For example, a database
     * adapter might take a FLAG_DRYRUN parameter on initialization,
     * which would mean that it does not alter any data even if
     * it is asked to, and instead returns an accurate count of
     * what would have been altered if the request was completed.
     *
     * Flag integration has to be done on a case by case basis,
     * however all classes that extend from the base compatibility
     * trait have methods available to them to leverage this behavior
     * if desired.
     *
     * Valid flags for a concrete object should
     * be defined in its api interface.
     *
     * Flag objects represent the actual conditional value of the flag,
     * and as such their task is pretty much only to determine whether
     * or not they currently apply. This is already heavily abstracted
     * out and generally does not require any additional code aside
     * from defining constants to set scope.
     */

    /**
     * <Generic Flag>
     * This classification designates only that the class is a flag object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_FLAG = '::flag-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_ABSTRACT = '::flag-abstract-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_NULL = '::flag-null-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_DEFAULT = '::flag-default-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_ARGUMENT = '::flag-argument-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_ADAPTER = '::flag-adapter-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_AUTH = '::flag-auth-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_BOOTSTRAP = '::flag-bootstrap-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_BENCHMARK = '::flag-benchmark-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_CONTROLLER = '::flag-controller-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_DATA = '::flag-data-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_DEBUG = '::flag-debug-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_DRYRUN = '::flag-dryrun-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_ENTITY = '::flag-entity-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_ENVIRONMENT = '::flag-environment-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_EVENT = '::flag-event-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_EXECUTEONLY = '::flag-executeonly-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_EXTENSION = '::flag-extension-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_INITIALIZATION = '::flag-initialization-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_JOB = '::flag-job-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_LOCK = '::flag-lock-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_MODEL = '::flag-model-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_MODULE = '::flag-module-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_READONLY = '::flag-readonly-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_RESOURCE = '::flag-resource-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_RESPONSE = '::flag-response-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_REQUEST = '::flag-request-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_ROUTINE = '::flag-routine-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_SECURITY = '::flag-security-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_SERVICE = '::flag-service-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_STREAM = '::flag-stream-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_SYSTEM = '::flag-system-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_TEMPLATE = '::flag-template-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_TEST = '::flag-test-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_VALIDATION = '::flag-validation-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_VIEW = '::flag-view-library::';
    const CLASS_SCOPE_LIBRARY_FLAG_WRITEONLY = '::flag-writeonly-library::';

    /**
     * A library that provides hook functionality
     */
    const CLASS_SCOPE_LIBRARY_HOOK = '::hook-library::';
    const CLASS_SCOPE_LIBRARY_HOOK_ABSTRACT = '::hook-abstract-library::';
    const CLASS_SCOPE_LIBRARY_HOOK_PARSER = '::hook-parser-library::';

    /**
     * A library that provides an index of available options
     */
    const CLASS_SCOPE_LIBRARY_INDEX = '::index-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_ABSTRACT = '::index-abstract-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_NULL = '::index-null-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_INTERFACES = '::index-interfaces-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_TRAITS = '::index-traits-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CLASSES = '::index-classes-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CONTAINERS = '::index-containers-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_ARCHIVES = '::index-archives-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_COMMANDS = '::index-commands-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_OBJECTS = '::index-objects-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_INDEXES = '::index-indexes-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CODEX = '::index-codex-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_LEXICON = '::index-lexicon-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CORE_INTERNALS = '::index-core-internals-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_MODULES = '::index-modules-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_COMPONENTS = '::index-components-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_EXTENSIONS = '::index-extensions-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_UTILITIES = '::index-utilities-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_LIBRARIES = '::index-libraries-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_ADAPTERS = '::index-adapters-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_MODELS = '::index-models-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_VIEWS = '::index-views-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CONTROLLERS = '::index-controllers-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_PATTERNS = '::index-routes-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_ROUTINES = '::index-routines-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CONFIGS = '::index-configs-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_FLAGS = '::index-flags-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_HOOKS = '::index-hooks-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_DATA = '::index-data-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_CONNECTIONS = '::index-connections-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_SERVICES = '::index-services-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_ERRORS = '::index-errors-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_OPERATIONS = '::index-operations-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_STATISTICS = '::index-statistics-library::';
    const CLASS_SCOPE_LIBRARY_INDEX_REPORTS = '::index-reports-library::';

    /**
     * <Job>
     *
     * ----------------
     *
     * A Job class represents a specific task, and its order of operations.
     * The task of job classes is to execute a specifically defined task and
     * see it to completion, manage it's errors, and appropriately clean up if
     * something goes wrong, rolling back to the last acceptable state when
     * this happens. They must also provide a Report object that verifies the
     * status of their execution, and whether or not it resolved correctly.
     *
     * This may be confused with an event.
     * A Job is the object that resolves the actual roadmap and execution method for a task.
     * An Event is used as a container for an active job instance of that Job running.
     */

    /**
     * <Generic Job>
     * This classification designates only that the class is a job object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_JOB = '::job-library::';
    const CLASS_SCOPE_LIBRARY_JOB_ABSTRACT = '::job-abstract-library::';
    const CLASS_SCOPE_LIBRARY_JOB_COLLECTION = '::job-collection-library::';
    const CLASS_SCOPE_LIBRARY_JOB_MANAGER = '::job-manager-library::';
    const CLASS_SCOPE_LIBRARY_JOB_NULL = '::job-null-library::';
    const CLASS_SCOPE_LIBRARY_JOB_CRON = '::job-cron-library::';
    const CLASS_SCOPE_LIBRARY_JOB_CGI = '::job-cgi-library::';
    const CLASS_SCOPE_LIBRARY_JOB_QUEUE = '::job-queue-library::';
    const CLASS_SCOPE_LIBRARY_JOB_WEB = '::job-web-library::';
    const CLASS_SCOPE_LIBRARY_JOB_SERVICE = '::job-service-library::';

    /**
     * A library that provides or extends logging
     * @note Must follow Psr-3 spec to be acceptable
     */
    const CLASS_SCOPE_LIBRARY_LOGGER = '::log-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_ABSTRACT = '::log-abstract-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_MANAGER = '::log-manager-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_MANAGER_ABSTRACT = '::log-abstract-manager-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_LOGGER = '::log-logger-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_NULL = '::log-null-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_FILE = '::log-file-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_PLAINTEXT = '::log-plaintext-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CONSOLE = '::log-console-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_DATABASE = '::log-database-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_JSON = '::log-json-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_HTML = '::log-html-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_XML = '::log-xml-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CSV = '::log-csv-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER = '::log-writer-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER_ABSTRACT = '::log-abstract-writer-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER_FILE = '::log-writer-file-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER_NULL = '::log-writer-null-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER_CLI = '::log-writer-cli-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER_SERVICE = '::log-writer-service-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_WRITER_MYSQL = '::log-writer-mysql-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CONTEXT = '::log-context-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CONTEXT_ABSTRACT = '::log--abstract-context-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CONTEXT_BACKTRACE = '::log-context-backtrace-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CONTEXT_ORIGIN = '::log-context-origin-library::';
    const CLASS_SCOPE_LIBRARY_LOGGER_CONTEXT_TIMESTAMP = '::log-context-timestamp-library::';

    /**
     * A library that provides message handling functionality
     */
    const CLASS_SCOPE_LIBRARY_MESSAGE = '::message-library::';
    const CLASS_SCOPE_LIBRARY_MESSAGE_ABSTRACT = '::message-abstract-library::';
    const CLASS_SCOPE_LIBRARY_MESSAGE_CGI = '::message-cgi-library::';
    const CLASS_SCOPE_LIBRARY_MESSAGE_HTTP = '::message-http-library::';

    /**
     * A library that provides prototyping functionality
     */
    const CLASS_SCOPE_LIBRARY_PROTOTYPE = '::prototype-library::';
    const CLASS_SCOPE_LIBRARY_PROTOTYPE_MANAGER = '::prototype-manager-library::';
    const CLASS_SCOPE_LIBRARY_PROTOTYPE_WORKER = '::prototype-worker-library::';
    const CLASS_SCOPE_LIBRARY_PROTOTYPE_EXCEPTION = '::prototype-exception-library::';

    /**
     * A library that provides parsing functionality
     */
    const CLASS_SCOPE_LIBRARY_PARSER = '::parser-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_ABSTRACT = '::parser-abstract-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_DOCBLOCK = '::parser-docblock-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_DOCBLOCK_CLASS_CONSTANT = '::parser-class-constant-docblock-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_JSON = '::parser-json-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_XML = '::parser-xml-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_HTML = '::parser-html-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_INI = '::parser-ini-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_YAML = '::parser-yaml-library::';
    const CLASS_SCOPE_LIBRARY_PARSER_CSV = '::parser-csv-library::';

    /**
     * <Promise>
     *
     * ----------------
     *
     * Promises are objects returned synchronously, which represent values
     * from asynchronous calls. These allow the program to proceed with
     * execution until such time as the value of the promise becomes required.
     * They represent an effort to smoothe out a lot of the typical issues
     * that arise when mixing synchronous local logic with asynchronous
     * remote requests.
     *
     * Promises as they are implemented in Oroboros core follow the Promise/A+
     * specification, which is a universal approach but typically the most
     * represented in Javascript, where it's usage is pretty much standard.
     *
     * Any language that executes asynchronous calls can benefit from
     * this approach, and minimize ambiguity about responses by doing so.
     */

    /**
     * <Generic Promise>
     * This classification designates only that the class is a promise object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_PROMISE = '::promise-library::';
    const CLASS_SCOPE_LIBRARY_PROMISE_ABSTRACT = '::promise-abstract-library::';
    const CLASS_SCOPE_LIBRARY_PROMISE_NULL = '::promise-null-library::';
    const CLASS_SCOPE_LIBRARY_PROMISE_DEFAULT = '::promise-default-library::';
    const CLASS_SCOPE_LIBRARY_PROMISE_GENERIC = '::promise-generic-library::';

    /**
     * <Record>
     *
     * ----------------
     *
     * A record is a reflection of one specific entry in some datastore.
     * That datastore may be a database, flatfile, memory, or a binding to a
     * specific result in a remote service api, but it always represents
     * one specific unique record.
     *
     * Unlike an Entity, a Record has no inherent cross-association with any
     * related records that would collectively constitute a fully qualified
     * entity.
     *
     * If for example you have a three related tables in a database that
     * constitute all of the details about a user, then there are three
     * record objects for that user, and one Entity that owns all three
     * and understands how they correlate.
     *
     * Records are bound to one specific source, and represent one
     * specific entry in that souce when instantiated.
     *
     * If more than one object needs to work with the same Record,
     * it should be passed by reference so that operations are
     * universally understood, and no other instances become
     * out-of-sync with the current state of that record.
     */

    /**
     * <Generic Record>
     * This classification designates only that the class is a record object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_RECORD = '::record-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_ABSTRACT = '::record-abstract-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_NULL = '::record-null-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_SQL = '::record-sql-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_SERIAL = '::record-serial-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_CSV = '::record-csv-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_XML = '::record-xml-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_JSON = '::record-json-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_INI = '::record-ini-library::';
    const CLASS_SCOPE_LIBRARY_RECORD_YAML = '::record-yaml-library::';

    /**
     * <Request>
     *
     * ----------------
     *
     * A request object represents a request to the server by a client,
     * or a request from the server to another server.
     *
     * Requests from a client become read only after they initialize.
     * Outbound requests become read only when they are executed.
     *
     * Both can return an editable copy of themselves,
     * but the original object remains unchanged after it's execution point.
     */

    /**
     * <Generic Request>
     * This classification designates only that the class is a request object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_REQUEST = '::request-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_ABSTRACT = '::request-abstract-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_SERVER = '::request-server-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_NULL = '::request-null-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_HTTP = '::request-http-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_REST = '::request-rest-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_AJAX = '::request-ajax-library::';
    const CLASS_SCOPE_LIBRARY_REQUEST_WEBSOCKET = '::request-websocket-library::';

    /**
     * <CGI Request>
     * This is the default for command line interaction.
     * It assumes script execution by default, and will
     * be expected not to modify output content to STDOUT
     * or STDERR in any way.
     *
     * This is a general purpose scope for command line interaction.
     * It should generally be used by default, unless you know that
     * the request always comes from a human user. This scope will
     * not maintain interractive state with the command line, and
     * will proceed as if each command should have a clean exit
     * after execution.
     */
    const CLASS_SCOPE_LIBRARY_REQUEST_CGI = '::request-cgi-library::';

    /**
     * <Console Request>
     * This also designates command line useage.
     * Unlike the default CGI request, this designates
     * that the request came from an active user,
     * and will allow efforts to apply additional colors,
     * spacing, or interactive input directives.
     *
     * Classes that expect ongoing state with the
     * command line session must use this scope.
     */
    const CLASS_SCOPE_LIBRARY_REQUEST_CONSOLE = '::request-console-library::';

    /**
     * <Response>
     *
     * ----------------
     *
     * Response objects represent a response to a request,
     * either the outbound response after execution
     * (which is by default owned by the currently active View),
     * or a response from a remote service.
     *
     * These follow the same principle as Request objects of
     * becoming immutable after execution, except in the case of
     * a Response to the client, execution is typically the last
     * thing that happens before shutdown, so only shutdown methods
     * will encounter it in an immutable state.
     * In contrast, Response objects that are received from service
     * calls are immutable as soon as they are received and parsed
     * correctly.
     */

    /**
     * <Generic Response>
     * This classification designates only that the class is a response object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_RESPONSE = '::response-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_ABSTRACT = '::response-abstract-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_DEFAULT = '::response-default-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_NULL = '::response-null-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_SUCCESS = '::response-success-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_ERROR = '::response-error-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_UNAUTHORIZED = '::response-unauthorized-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_UNIMPLEMENTED = '::response-unimplemented-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_REDIRECT = '::response-redirect-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_TIMEOUT = '::response-timeout-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_MISSING = '::response-notfound-library::';
    const CLASS_SCOPE_LIBRARY_RESPONSE_SECURITY = '::response-security-library::';

    /**
     * <Router>
     *
     * ----------------
     *
     * Routers make decisions about which classes and methods handle requests.
     * Their only task is to map a request to an execution plan,
     * and run the execution plan when requested.
     *
     * In theory this is pretty simple, but achieving this
     * dynamically in a flexible and scalable way is actually
     * quite complex under the hood.
     *
     * The Oroboros router schema is pretty standard and easily extensible.
     * However it can be substituted with pretty much any other router also,
     * provided you resolve the mapping conditions of that router against
     * the provided interface.
     */

    /**
     * <Generic Router>
     * This classification designates only that the class is a router object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_ROUTER = '::router-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_ABSTRACT = '::router-abstract-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_NULL = '::router-null-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_CGI = '::router-cgi-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_HTTP = '::router-http-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_AJAX = '::router-ajax-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_SOCKET = '::router-socket-library::';
    //scopes of individual routes
    const CLASS_SCOPE_LIBRARY_ROUTER_ROUTE_ABSTRACT = '::router-route-abstract-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_ROUTE_NULL = '::router-route-null-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_ROUTE_CGI = '::router-route-cgi-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_ROUTE_HTTP = '::router-route-http-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_ROUTE_AJAX = '::router-route-ajax-library::';
    const CLASS_SCOPE_LIBRARY_ROUTER_ROUTE_SOCKET = '::router-route-socket-library::';

    /**
     * A library that provides routine handling functionality
     */
    const CLASS_SCOPE_LIBRARY_ROUTINE = '::routine-library::';
    const CLASS_SCOPE_LIBRARY_ROUTINE_ABSTRACT = '::routine-abstract-library::';

    /**
     * <Security>
     *
     * ----------------
     *
     * Security objects as a whole can be used anywhere,
     * but they enforce explicit scope, and cannot be used
     * in the wrong context. Security objects are tasked with
     * preventing unauthorized access or exploits. Their task
     * is to notice irregularities, create, send, and recieve
     * security advisories, and alter the execution plan in some
     * cases as neccessary to prevent damage.
     */

    /**
     * <Generic Security>
     * This classification designates only that the class is a security object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_SECURITY = '::security-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_ABSTRACT = '::security-abstract-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_NULL = '::security-null-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_REQUEST = '::security-request-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_HACK = '::security-hack-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_AUTH = '::security-auth-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_SESSION = '::security-session-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_DATA = '::security-data-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_FILE = '::security-file-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_SERVICE = '::security-service-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_SERVER = '::security-server-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_PROXY = '::security-proxy-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_CLUSTER = '::security-cluster-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_SOURCE = '::security-source-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_ACCESS = '::security-access-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_DDOS = '::security-ddos-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_MAINTENANCE = '::security-maintenance-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_PRODUCTION = '::security-production-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_STAGING = '::security-staging-library::';
    const CLASS_SCOPE_LIBRARY_SECURITY_DEVELOPMENT = '::security-dev-library::';

    /**
     * <Service>
     *
     * ----------------
     *
     * A service is a containerized wrapper for an external Api
     * that holds no execution on the same process as the
     * current runtime. Services are a subset of adapters,
     * that specifically deal with outbound requests.
     *
     * Their task is to understand, interpret, provide,
     * and execute a remote defined Api, and return a result that
     * is in a uniform format for the internal system to understand.
     * They may leverage any number of Data-Objects, Parsers,
     * Records, Validators, etc to accomplish this.
     */

    /**
     * <Generic Service>
     * This classification designates only that the class is a service object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_SERVICE = '::service-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_ABSTRACT = '::service-abstract-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_NULL = '::service-null-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_LOCAL = '::service-local-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_REMOTE = '::service-remote-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_REMOTE_AUTHENTICATED = '::service-authenticated-remote-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_REMOTE_UNAUTHENTICATED = '::service-unauthenticated-remote-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_CGI = '::service-cgi-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_CGI_JOB = '::service-cgi-job-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_CGI_JOB_CRON = '::service-cgi-cron-job-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_CGI_CONSOLE = '::service-cgi-console-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_DATA = '::service-data-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_DATA_AUTH = '::service-data-auth-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_DATA_INDEX = '::service-data-index-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_DATA_STREAM = '::service-data-stream-library::';
    const CLASS_SCOPE_LIBRARY_SERVICE_DATA_STORE = '::service-data-store-library::';

    /**
     * A library that provides stream or message support
     * @note Must follow Psr-7 spec to be acceptable
     */
    const CLASS_SCOPE_LIBRARY_STREAM = '::stream-library::';
    const CLASS_SCOPE_LIBRARY_STREAM_RESOURCE = '::stream-resource-library::';
    const CLASS_SCOPE_LIBRARY_STREAM_WRAPPER = '::stream-wrapper-library::';

    /**
     * <Template>
     *
     * ----------------
     *
     * A template simplifies the reproduction of content.
     * These are most often used by Views, but may be used by anything
     * that needs to abstract out injection of data into some kind of
     * boilerplate syntax. Not all templates are automatically
     * associated with views, but the majority of them are.
     */

    /**
     * <Generic Template>
     * This classification designates only that the class is a template object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_LIBRARY_TEMPLATE = '::template-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_ABSTRACT = '::template-abstract-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_NULL = '::template-null-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_PLAINTEXT = '::template-plaintext-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_CONSOLE = '::template-console-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_HTML = '::template-html-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_XML = '::template-xml-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_AJAX = '::template-ajax-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_JSON = '::template-json-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_CSV = '::template-csv-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_SQL = '::template-sql-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_JAVASCRIPT = '::template-javascript-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_CSS = '::template-css-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_JPG = '::template-jpg-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_GIF = '::template-gif-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_MP3 = '::template-mp3-library::';
    const CLASS_SCOPE_LIBRARY_TEMPLATE_MP4 = '::template-mp4-library::';

    /**
     * A library that provides theme handling functionality
     */
    const CLASS_SCOPE_LIBRARY_THEME = '::theme-library::';

    /**
     * A library that provides uri handling functionality
     */
    const CLASS_SCOPE_LIBRARY_URI = '::uri-library::';
    const CLASS_SCOPE_LIBRARY_URI_ABSTRACT = '::uri-abstract-library::';
    const CLASS_SCOPE_LIBRARY_URI_HTTP = '::uri-http-library::';
    const CLASS_SCOPE_LIBRARY_URI_CLI = '::uri-cli-library::';
    const CLASS_SCOPE_LIBRARY_URI_IPV4 = '::uri-ipv4-library::';
    const CLASS_SCOPE_LIBRARY_URI_IPV6 = '::uri-ipv6-library::';
    const CLASS_SCOPE_LIBRARY_URI_FILE = '::uri-file-library::';
    const CLASS_SCOPE_LIBRARY_URI_SOCKET = '::uri-socket-library::';

    /**
     * A library that provides utilities.
     * Individual utilities should not use this,
     * but their aggregators and managers can
     * if it treats them as workers.
     */
    const CLASS_SCOPE_LIBRARY_UTILITY = '::utility-library::';

    /**
     * A library that provides container objects
     * @note Must follow Psr-13 spec to be acceptable
     */
    const CLASS_SCOPE_LIBRARY_LINK = '::link-library::';

    /**
     * A library provided as a worker.
     * Use this if the worker does not have a more appropriate scope definition.
     * @note Workers can only be requested by a manager class that has registered them as a valid worker instance.
     */
    const CLASS_SCOPE_LIBRARY_WORKER = '::worker-library::';

    /**
     * A library that provides a manager for other related objects
     * @note These libraries must define their valid worker instances, and can request any worker class that satisfies that condition, as long as it is in their scope.
     */
    const CLASS_SCOPE_LIBRARY_MANAGER = '::manager-library::';

    /**
     * A private module library.
     * @note This scope tells the factories and prototyper not to honor requests for the object if they are not part of it's parent module package.
     */
    const CLASS_SCOPE_LIBRARY_MODULE = '::module-library::';

}
