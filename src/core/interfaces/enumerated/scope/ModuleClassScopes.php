<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Module Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for component class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ModuleClassScopes
extends ClassScopeBase
{
    /**
     * <Module>
     *
     * ----------------
     *
     * Components add enhanced output to existing return results in a way that
     * does not invalidate the original output. Components within the context of
     * Oroboros are designed to enrich return data with additional context,
     * theme, or templating data, without altering the underlying intention of
     * the output. An example of a component might be a class that colorizes
     * cli console output, adds SERP tags to existing html, or binds additional
     * content hooks into output content for external content injection.
     *
     * In Oroboros, Components are typically bound to an explicit
     * output type (eg: html, plaintext, etc), and also are usually designated
     * to work in an explicit scope (public facing site, command line api, etc).
     */

    /**
     * <Generic Module>
     * This classification designates only that the class is a module object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_MODULE = "::module::";

    /**
     * <Abstract Module>
     * Provides a basis for modular extension for other classes,
     * but is not usable by itself.
     */
    const CLASS_SCOPE_MODULE_ABSTRACT = '::abstract-module::';

    /**
     * <Null Module>
     * Registers as a module into a class and fires on its hooks and filters,
     * but does not alter any information or perform any additional operations.
     */
    const CLASS_SCOPE_MODULE_NULL = '::null-module::';
    const CLASS_SCOPE_MODULE_SYSTEM = '::system-module::';
    const CLASS_SCOPE_MODULE_CORE = '::core-module::';
    const CLASS_SCOPE_MODULE_THIRDPARTY = '::thirdparty-module::';

}
