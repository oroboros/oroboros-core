<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Utility Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for utility class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface UtilityClassScopes
extends ClassScopeBase
{
    /**
     * <Utility>
     *
     * ----------------
     *
     * A utility is an external and independent, directly integrated class.
     * These are used when they wrap something too straightforward and
     * simplistic to warrant an Adapter, or when the class needs
     * to be directly extended.
     *
     * All helper classes should either be utilities or traits.
     * (Use a trait if you want to be able to override behavior internally,
     * use an adapter if it does it's full job as is).
     *
     * The approach between utilities and adapters is largely preferential.
     * You may use either or, or a mix of both. Internally, complex operation
     * is always designated to an adapter, and simple one-off tasks are
     * designated to a utility. Knowing the appropriate distinction is
     * based on use case, and no opinion about your preference is enforced,
     * other than that you ascribe to the appropriate paradigm for whichever you use.
     *
     * Example:
     *
     * //Utilities are direct children of 3rd-party constructs,
     * //or standalone constructs that should be capable of fully
     * //fulfilling their scope without external resources.
     * class Foo extends \DateTime {}  //This is a utility.
     *
     * //Adapters wrap 3rd-party resources internally,
     * //and are internal classes themselves.
     * //They abstract out complex interaction,
     * //and provide a simple public api to accomplish it.
     * class Bar {
     *     private $_foo;
     *     public function __construct() {
     *         $this->_foo = new Foo();
     *     }
     *
     *     public function setTime($time = 'NOW') {
     *         return $this->_foo->setTime($time);
     *     }
     * }
     */

    /**
     * <Generic Utility>
     * This classification designates only that the class is a utility object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_UTILITY = "::utility::";
    const CLASS_SCOPE_UTILITY_ABSTRACT = "::abstract-utility::";
    const CLASS_SCOPE_UTILITY_NULL = '::null-utility::';
    const CLASS_SCOPE_UTILITY_MATH = '::math-utility::';
    const CLASS_SCOPE_UTILITY_FORMATTING = '::formatting-utility::';
    const CLASS_SCOPE_UTILITY_VALIDATION = '::validation-utility::';
    const CLASS_SCOPE_UTILITY_OUTPUT = '::output-utility::';
    const CLASS_SCOPE_UTILITY_DATA = '::data-utility::';
    const CLASS_SCOPE_UTILITY_SECURITY = '::security-utility::';
    const CLASS_SCOPE_UTILITY_PARSER = '::parse-utility::';

    /**
     * <Generic Error>
     * This classification designates only that the class is an error object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_UTILITY_ERROR = '::error::';
    const CLASS_SCOPE_UTILITY_ERROR_ABSTRACT = '::error-abstract::';
    const CLASS_SCOPE_UTILITY_ERROR_HANDLER = '::error-handler::';
    const CLASS_SCOPE_UTILITY_ERROR_HANDLER_ABSTRACT = '::error-handler-abstract::';
    const CLASS_SCOPE_UTILITY_ERROR_NULL = '::error-null::';
    const CLASS_SCOPE_UTILITY_ERROR_DEFAULT = '::error-default::';
    const CLASS_SCOPE_UTILITY_ERROR_GENERIC = '::error-generic::';
    const CLASS_SCOPE_UTILITY_ERROR_EXCEPTION = '::error-exception::';
    const CLASS_SCOPE_UTILITY_ERROR_BADFUNCTIONCALL = '::error-badfunctioncall::';
    const CLASS_SCOPE_UTILITY_ERROR_BADMETHODCALL = '::error-badmethodcall::';
    const CLASS_SCOPE_UTILITY_ERROR_DOMAIN = '::error-domain::';
    const CLASS_SCOPE_UTILITY_ERROR_INVALIDARGUMENT = '::error-invalidargument::';
    const CLASS_SCOPE_UTILITY_ERROR_LENGTH = '::error-length::';
    const CLASS_SCOPE_UTILITY_ERROR_LOGIC = '::error-logic::';
    const CLASS_SCOPE_UTILITY_ERROR_OUTOFBOUNDS = '::error-outofbounds::';
    const CLASS_SCOPE_UTILITY_ERROR_OUTOFRANGE = '::error-outofrange::';
    const CLASS_SCOPE_UTILITY_ERROR_OVERFLOW = '::error-overflow::';
    const CLASS_SCOPE_UTILITY_ERROR_RANGE = '::error-range::';
    const CLASS_SCOPE_UTILITY_ERROR_RUNTIME = '::error-runtime::';
    const CLASS_SCOPE_UTILITY_ERROR_UNDERFLOW = '::error-underflow::';
    const CLASS_SCOPE_UTILITY_ERROR_UNEXPECTEDVALUE = '::error-unexpectedvalue::';

    /**
     * <Parser>
     *
     * ----------------
     *
     * A parser is tasked with the read/write operation against
     * a defined content type. It's job is to translate a data
     * structure into a PHP internal object or array, and to be
     * able to recreate the same semantically correct data structure
     * from the same or modified data. Parsers abstract out external
     * data formats. The more parsers you have, the more universalized
     * your I/O will be.
     */

    /**
     * <Generic Parser>
     * This classification designates only that the class is a parser object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_PARSER = '::parser::';
    const CLASS_SCOPE_PARSER_ABSTRACT = "::abstract-parser::";
    const CLASS_SCOPE_PARSER_NULL = '::null-parser::';
    const CLASS_SCOPE_PARSER_SQL = '::sql-parser::';
    const CLASS_SCOPE_PARSER_SERIAL = '::serial-parser::';
    const CLASS_SCOPE_PARSER_CSV = '::csv-parser::';
    const CLASS_SCOPE_PARSER_XML = '::xml-parser::';
    const CLASS_SCOPE_PARSER_HTML = '::parser-html::';
    const CLASS_SCOPE_PARSER_JSON = '::json-parser::';
    const CLASS_SCOPE_PARSER_INI = '::ini-parser::';
    const CLASS_SCOPE_PARSER_YAML = '::yaml-parser::';
    const CLASS_SCOPE_PARSER_DOCBLOCK = '::docblock-parser::';
    const CLASS_SCOPE_PARSER_DOCBLOCK_CLASS_CONSTANTS = '::class-constant-docblock-parser::';

    /**
     * <Stream>
     *
     * ----------------
     *
     * A stream represents a wrapper for a specific data feed that needs to be
     * read, written, or passed along elsewhere.
     * Streams minimize the memory overhead of sets of data by allowing them to
     * be read like a tape, where only the current pointer on the tape is
     * taking up any memory at runtime. This allows you to handle very large
     * files, resources, or memory objects with little to no performance
     * impact on the application runtime.
     *
     * Oroboros's implementation of streams is based on
     * the Psr\Http\StreamWrapper standard (Psr-7), though it
     * is used extensively throughout the system, not just for messages.
     */

    /**
     * <Generic Stream>
     * This classification designates only that the class is a stream object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_STREAM_RESOURCE = "::stream::";
    const CLASS_SCOPE_STREAM_RESOURCE_ABSTRACT = "::abstract-stream::";
    const CLASS_SCOPE_STREAM_RESOURCE_NULL = '::null-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_DEFAULT = '::default-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_BUFFER = '::buffer-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_DATA = '::data-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_MEMORY = '::memory-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_PROXY = '::proxy-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_REQUEST = '::request-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_RESPONSE = '::response-stream::';
    const CLASS_SCOPE_STREAM_RESOURCE_SERVICE = '::service-stream::';


    /**
     * <Hooks>
     *
     * ----------------
     *
     * A hook is a scalar marker in some kind of data that designates that it
     * should be automatically replaced with a known value or the result of a
     * callback when it is encountered.
     *
     * This allows post-processing of data to be used in any designated scope.
     * This is similar to Wordpress's concept of a shortcode, but this
     * implementation is flexible enough to be used anywhere (including in
     * database results, json files, etc), Allows for customized definition
     * of what constitutes a Hook, respects scope, auth, and visibility, and
     * by default nulls out all existing hooks that do not have an associated
     * output registered for them, which prevents unresolved hooks from polluting
     * the source they are encountered in. Hooks also have a debug switch, which
     * will wrap whatever content they replace in a marker that designates
     * where/why it was inserted. As hooks can be fired recursively, this is
     * very handy for debugging root problems quickly while using them.
     *
     * Hooks are used somewhat internally, and it is not enforced that you use
     * them to interact with this system. They are provided for convenience and
     * granular post-processing, to prevent having to make retroactive changes
     * to lower level structure when edge cases arise.
     *
     * You can create your own case-specific hook classes by extending AbstractHook.
     * @see \oroboros\core\abstracts\hooks\AbstractHook
     *
     * There is also an interface and trait provided to accomplish this
     * without direct class inheritance.
     */

    /**
     * <Generic Hook>
     * This classification designates only that the class is a hook object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_HOOK = "::hook::";
    const CLASS_SCOPE_HOOK_ABSTRACT = "::abstract-hook::";
    const CLASS_SCOPE_HOOK_NULL = "::null-hook::";
    const CLASS_SCOPE_HOOK_SYSTEM = "::system-hook::";
    const CLASS_SCOPE_HOOK_API = "::api-hook::";
    const CLASS_SCOPE_HOOK_ADAPTER = "::adapter-hook::";
    const CLASS_SCOPE_HOOK_PROMISE = "::promise-hook::";
    const CLASS_SCOPE_HOOK_SERVICE = "::service-hook::";
    const CLASS_SCOPE_HOOK_JOB = "::job-hook::";
    const CLASS_SCOPE_HOOK_CODEX = "::codex-hook::";
    const CLASS_SCOPE_HOOK_CONTROLLER = "::controller-hook::";
    const CLASS_SCOPE_HOOK_MODEL = "::model-hook::";
    const CLASS_SCOPE_HOOK_VIEW = "::view-hook::";
    const CLASS_SCOPE_HOOK_ENTITY = "::entity-hook::";
    const CLASS_SCOPE_HOOK_EVENT = "::event-hook::";
    const CLASS_SCOPE_HOOK_ROUTINE = "::routine-hook::";
    const CLASS_SCOPE_HOOK_CACHE = "::cache-hook::";
    const CLASS_SCOPE_HOOK_MESSAGE = "::message-hook::";
    const CLASS_SCOPE_HOOK_REQUEST = "::request-hook::";
    const CLASS_SCOPE_HOOK_RESPONSE = "::response-hook::";

}
