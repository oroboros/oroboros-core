<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <View Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for view class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ViewClassScopes
extends ClassScopeBase
{
    /**
     * <View>
     *
     * ----------------
     * 
     * Views present information to the end user. They do not make decisions
     * or perform logic aside from deciding how to render what they are
     * given correctly.
     *
     * In Oroboros, Views are typically bound to an explicit
     * output format (eg: plaintext, html, json, xml, csv, etc).
     *
     * Views can also instantiate and manage other views,
     * so a granular approach to content output management
     * is possible by doing this. Scoped views always set their headers
     * correctly and can validate their content on demand.
     *
     * For Views that need to be more generalized, there are
     * default implementations of the AbstractView, HttpView,
     * and CgiView, which are scoped to anything, all web requests,
     * and all command line interaction respectively.
     *
     * This provides the flexibility of having general purpose
     * Views if desired, but again, neither approach is enforced.
     */

    /**
     * <Generic View>
     * This classification designates only that the class is a view object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_VIEW = "::view::";
    const CLASS_SCOPE_VIEW_ABSTRACT = '::abstract-view::';
    const CLASS_SCOPE_VIEW_NULL = '::null-view::';
    const CLASS_SCOPE_VIEW_PLAINTEXT = '::plaintext-view::';
    const CLASS_SCOPE_VIEW_CONSOLE = '::console-view::';
    const CLASS_SCOPE_VIEW_HTML = '::html-view::';
    const CLASS_SCOPE_VIEW_XML = '::xml-view::';
    const CLASS_SCOPE_VIEW_AJAX = '::ajax-view::';
    const CLASS_SCOPE_VIEW_JSON = '::json-view::';
    const CLASS_SCOPE_VIEW_CSV = '::csv-view::';
    const CLASS_SCOPE_VIEW_SQL = '::sql-view::';
    const CLASS_SCOPE_VIEW_JAVASCRIPT = '::javascript-view::';
    const CLASS_SCOPE_VIEW_CSS = '::css-view::';
    const CLASS_SCOPE_VIEW_JPG = '::jpg-view::';
    const CLASS_SCOPE_VIEW_GIF = '::gif-view::';
    const CLASS_SCOPE_VIEW_MP3 = '::mp3-view::';
    const CLASS_SCOPE_VIEW_MP4 = '::mp4-view::';

}
