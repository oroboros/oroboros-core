<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Class Type Api>
 * Defines the class types recognized by the oroboros system.
 * This is the master class type interface, and contains all other valid class
 * types by extension. Queries against a class type of unknown origin can be
 * accomplished by enumerating this api interface. It is suggested to enumerate
 * the specific api interface that is scoped to the expected class type when
 * possible to maintain performance, though this one contains all of them that
 * ship with core.
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system. All system classes can be expected to have one of
 * these types and honor it's specification explicitly. Custom extensions of
 * these must honor their Api, but otherwise no rigid useage is directly
 * enforced for external child classes (only for core internal classes).
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface ClassType
extends AdapterClassTypes,
 ComponentClassTypes,
 ControllerClassTypes,
 CoreClassTypes,
 ExtensionClassTypes,
 LibraryClassTypes,
 ModelClassTypes,
 ModuleClassTypes,
 PatternClassTypes,
 RoutineClassTypes,
 UtilityClassTypes,
 ViewClassTypes
{

}
