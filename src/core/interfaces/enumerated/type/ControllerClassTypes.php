<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Controller Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as controllers. All system classes designated
 * as controllers can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ControllerClassTypes
extends ClassTypeBase
{

    /**
     * <Controller Class Types>
     *
     * ----------------
     * 
     * A controller is tasked with making all of the decisions,
     * and ideally should also do none of the work.
     * Controllers job is to interpret a request and execute
     * the corresponding execution plan. Individual tasks within that
     * execution plan should be considered out of scope for controllers
     * to be directly executing unless they are extremely simple
     * and non-blocking.
     *
     * Primarily, the Controller should be designating all work to sub-objects,
     * and only deciding which ones to load and what to order them to do.
     *
     * This separation is not enforced, but the system heavily implies that it
     * should be followed, and it's entire structure is intended to
     * accomplish this. Failing to take this approach will cause you roadblocks
     * later, but if you have a good reason for doing it that way, then by all
     * means do so, nothing will inherently prevent it.
     */
    const CLASS_TYPE_CONTROLLER = "::controller::";

}
