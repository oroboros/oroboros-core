<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Extension Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as extensions. All system classes designated
 * as extensions that ship with core or are official extensions of it can be
 * expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ExtensionClassTypes
extends ClassTypeBase
{

    /**
     * <Extension Class Types>
     *
     * ----------------
     *
     * Extensions add additional public facing methods to various classes
     * through overloading or decoration. Extensions within the context of
     * Oroboros are designed to add more methods for external use in a way
     * that operates as if it were a native method of a class, but can
     * access class properties and methods of the class they extend as if
     * they shared the class scope through normal inheritance.
     *
     * In Oroboros, Extensions are typically bound to an explicit
     * parent class (eg: core, codex, etc).
     */
    const CLASS_TYPE_EXTENSION = "::extension::";

    /**
     * <Extendable Class Types>
     *
     * ----------------
     *
     * Extensions add additional public facing methods to various classes
     * through overloading or decoration. An extendable class type designates
     * that the class can receive extensions to add to its own public facing api.
     *
     * In Oroboros, Extensions are typically bound to an explicit
     * parent class (eg: core, codex, etc).
     */
    const CLASS_TYPE_EXTENDABLE = "::extendable::";

}
