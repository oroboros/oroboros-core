<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Library Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as libraries. All system classes designated
 * as libraries can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface LibraryClassTypes
extends ClassTypeBase
{

    /**
     * <Library Class Types>
     *
     * ----------------
     *
     * Libraries are multi-purpose classes that cover a specific area of focus,
     * and provide functionality to accomplish it easily. The majority of other
     * classes distributed in this system are subsets of libraries, or extend
     * from them.
     *
     * Libraries ideally should do all of the work and make
     * none of the decisions. They are the inverse of the Controller
     * (which makes all of the decisions and does none of the work).
     *
     * What designates something as a library, is that it does not
     * provide opinion, and does not fall into a more focused
     * class definition, and also is not inherently a wrapper for
     * external functionality (though it may interact with one or
     * multiple of these to do it's job).
     *
     * Libraries typically have a singular, narrow scope of focus, and are
     * always the default option for accomplishing tasks within that focus.
     * If this is not the case, they are at least an option on the list for
     * accomplishing tasks in that area of focus (eg a MySQL library and a
     * PostGRES library might be used interchangeably, but they both only
     * deal with databases).
     */
    const CLASS_TYPE_LIBRARY = "::library::";

    /**
     * Designates a library concerned with authorization.
     */
    const CLASS_TYPE_LIBRARY_AUTH = "::auth-library::";

    /**
     * Designates a library concerned with autoloading resources.
     */
    const CLASS_TYPE_LIBRARY_AUTOLOAD = "::autoload-library::";

    /**
     * Designates a library concerned with bootstrapping the system.
     */
    const CLASS_TYPE_LIBRARY_BOOTSTRAP = "::bootstrap-library::";

    /**
     * Designates a library concerned with caching.
     */
    const CLASS_TYPE_LIBRARY_CACHE = "::cache-library::";

    /**
     * Designates a library concerned with codex functionality.
     */
    const CLASS_TYPE_LIBRARY_CODEX = "::codex-library::";

    /**
     * Designates a library concerned with configuration.
     */
    const CLASS_TYPE_LIBRARY_CONFIG = "::config-library::";

    /**
     * Designates a library concerned with containerization of data.
     */
    const CLASS_TYPE_LIBRARY_CONTAINER = "::container-library::";

    /**
     * Designates a library concerned with handling databases and queries.
     */
    const CLASS_TYPE_LIBRARY_DATABASE = "::database-library::";

    /**
     * Designates a library concerned with debugging information and code.
     */
    const CLASS_TYPE_LIBRARY_DEBUG = "::debug-library::";

    /**
     * Designates a library concerned with providing read-only contextual information.
     */
    const CLASS_TYPE_LIBRARY_DICTIONARY = "::dictionary-library::";

    /**
     * Designates a library concerned with handling dns settings.
     */
    const CLASS_TYPE_LIBRARY_DNS = "::dns-library::";

    /**
     * Designates a library concerned with handling entities.
     */
    const CLASS_TYPE_LIBRARY_ENTITY = "::entity-library::";

    /**
     * Designates a library concerned with enumeration.
     */
    const CLASS_TYPE_LIBRARY_ENUM = "::enum-library::";

    /**
     * Designates a library concerned with environment information.
     */
    const CLASS_TYPE_LIBRARY_ENVIRONMENT = "::environment-library::";

    /**
     * Designates a library concerned with handling errors.
     */
    const CLASS_TYPE_LIBRARY_ERROR = "::error-library::";

    /**
     * Designates a library concerned with handling events.
     */
    const CLASS_TYPE_LIBRARY_EVENT = "::event-library::";

    /**
     * Designates a library that can receive extensions.
     */
    const CLASS_TYPE_LIBRARY_EXTENDABLE = "::extendable-library::";

    /**
     * Designates a library concerned with handling extensions.
     */
    const CLASS_TYPE_LIBRARY_EXTENSION = "::extension-library::";

    /**
     * Designates a library concerned with generating objects via the factory pattern.
     */
    const CLASS_TYPE_LIBRARY_FACTORY = "::factory-library::";

    /**
     * Designates a library concerned with file handling.
     */
    const CLASS_TYPE_LIBRARY_FILE = "::file-library::";

    /**
     * Designates a library concerned with flag handling.
     */
    const CLASS_TYPE_LIBRARY_FLAG = "::flag-library::";

    /**
     * Designates a library concerned with hook substitution.
     */
    const CLASS_TYPE_LIBRARY_HOOK = "::hook-library::";

    /**
     * Designates a library concerned with providing available options.
     */
    const CLASS_TYPE_LIBRARY_INDEX = "::index-library::";

    /**
     * Designates a library concerned with running jobs.
     */
    const CLASS_TYPE_LIBRARY_JOB = "::job-library::";

    /**
     * Designates a library concerned with logging.
     */
    const CLASS_TYPE_LIBRARY_LOG = "::log-library::";

    /**
     * Designates a library concerned with managing workers.
     */
    const CLASS_TYPE_LIBRARY_MANAGER = "::manager-library::";

    /**
     * Designates a library concerned with messages.
     */
    const CLASS_TYPE_LIBRARY_MESSAGE = "::message-library::";

    /**
     * Designates a library concerned with prototying objects.
     */
    const CLASS_TYPE_LIBRARY_PROTOTYPE = "::prototype-library::";

    /**
     * Designates a library concerned with requests.
     */
    const CLASS_TYPE_LIBRARY_REQUEST = "::request-library::";

    /**
     * Designates a library concerned with responses.
     */
    const CLASS_TYPE_LIBRARY_RESPONSE = "::response-library::";

    /**
     * Designates a library concerned with routines.
     */
    const CLASS_TYPE_LIBRARY_ROUTER = "::router-library::";

    /**
     * Designates a library concerned with routines.
     */
    const CLASS_TYPE_LIBRARY_ROUTINE = "::routine-library::";

    /**
     * Designates a library concerned with security.
     */
    const CLASS_TYPE_LIBRARY_SECURITY = "::security-library::";

    /**
     * Designates a library concerned with streams.
     */
    const CLASS_TYPE_LIBRARY_STREAM = "::stream-library::";

    /**
     * Designates a library concerned with templates.
     */
    const CLASS_TYPE_LIBRARY_TEMPLATE = "::template-library::";

    /**
     * Designates a library concerned with templates.
     */
    const CLASS_TYPE_LIBRARY_THEME = "::theme-library::";

    /**
     * Designates a library concerned with uris.
     */
    const CLASS_TYPE_LIBRARY_URI = "::uri-library::";

    /**
     * Designates a library concerned with utilities.
     */
    const CLASS_TYPE_LIBRARY_UTILITY = "::utility-library::";

    /**
     * Designates a library concerned with validation.
     */
    const CLASS_TYPE_LIBRARY_VALIDATION = "::validation-library::";

}
