<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Design Pattern Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as design patterns. All system classes designated
 * as design patterns can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface PatternClassTypes
extends ClassTypeBase
{

    /**
     * <Pattern Class Types>
     *
     * ----------------
     * 
     * A pattern is just a design pattern. It provides an unopinionated approach
     * to efficiently performing a specific type of task, without any regard to
     * why that task is neccessary. These are building blocks for robust class
     * functionality. they are never required for use, but speed up ongoing
     * development and scalability drastically if used intelligently. If used
     * recklessly, they overengineer your system into a nightmare.
     *
     * Design patterns are usually provided as an abstract that can be extended,
     * a trait that grants all of their methods independently, and an interface
     * that enforces an agreed upon approach for interoperability.
     *
     * All of the abstract classes for design patterns are very low level,
     * and extending those will require that you provide the logic for all
     * of the actual work manually. Generally, it is better to use the trait
     * as a drop-in solution wherever it is needed, and affix the interface if
     * it needs to be passed around the system (pattern traits ALWAYS satisfy
     * the interface requirements of the same pattern,
     * class constant declarations notwithstanding).
     */
    const CLASS_TYPE_PATTERN = "::pattern::";

}
