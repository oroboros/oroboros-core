<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Utility Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as utilities. All system classes designated
 * as utilities can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface UtilityClassTypes
extends ClassTypeBase
{

    /**
     * <Utility Class Types>
     *
     * ----------------
     *
     * A utility is an external and independent, directly integrated class.
     * These are used when they wrap something too straightforward and
     * simplistic to warrant an Adapter, or when the class needs
     * to be directly extended.
     *
     * All helper classes should either be utilities or traits.
     * (Use a trait if you want to be able to override behavior internally,
     * use an adapter if it does it's full job as is).
     *
     * The approach between utilities and adapters is largely preferential.
     * You may use either or, or a mix of both. Internally, complex operation
     * is always designated to an adapter, and simple one-off tasks are
     * designated to a utility. Knowing the appropriate distinction is
     * based on use case, and no opinion about your preference is enforced,
     * other than that you ascribe to the appropriate paradigm for whichever you use.
     *
     * Example:
     *
     * //Utilities are direct children of 3rd-party constructs,
     * //or standalone constructs that should be capable of fully
     * //fulfilling their scope without external resources.
     * class Foo extends \DateTime {}  //This is a utility.
     *
     * //Adapters wrap 3rd-party resources internally,
     * //and are internal classes themselves.
     * //They abstract out complex interaction,
     * //and provide a simple public api to accomplish it.
     * class Bar {
     *     private $_foo;
     *     public function __construct() {
     *         $this->_foo = new Foo();
     *     }
     *
     *     public function setTime($time = 'NOW') {
     *         return $this->_foo->setTime($time);
     *     }
     * }
     */
    const CLASS_TYPE_UTILITY = "::utility::";
    const CLASS_TYPE_UTILITY_CONTEXT = "::context-utility::";
    const CLASS_TYPE_UTILITY_CORE = "::core-utility::";
    const CLASS_TYPE_UTILITY_DATETIME = "::datetime-utility::";
    const CLASS_TYPE_UTILITY_ERROR = "::error-utility::";
    const CLASS_TYPE_UTILITY_ERROR_HANDLER = "::error-handler-utility::";
    const CLASS_TYPE_UTILITY_EXCEPTION = "::exception-utility::";
    const CLASS_TYPE_UTILITY_FILEBASE = "::filebase-utility::";
    const CLASS_TYPE_UTILITY_FORMAT = "::format-utility::";
    const CLASS_TYPE_UTILITY_HOOK = "::hook-utility::";
    const CLASS_TYPE_UTILITY_HTML = "::html-utility::";
    const CLASS_TYPE_UTILITY_LOGIC = "::logic-utility::";
    const CLASS_TYPE_UTILITY_MATH = "::math-utility::";
    const CLASS_TYPE_UTILITY_PARSER = "::parser-utility::";
    const CLASS_TYPE_UTILITY_REFLECTION = "::reflection-utility::";
    const CLASS_TYPE_UTILITY_REGEX = "::regex-utility::";
    const CLASS_TYPE_UTILITY_RESOURCE = "::resource-utility::";
    const CLASS_TYPE_UTILITY_SESSION = "::session-utility::";
    const CLASS_TYPE_UTILITY_VALIDATION = "::validation-utility::";


}
