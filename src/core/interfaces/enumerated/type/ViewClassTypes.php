<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros View Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as views. All system classes designated
 * as views can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ViewClassTypes
extends ClassTypeBase
{

    /**
     * <View Class Types>
     *
     * ----------------
     * 
     * Views present information to the end user. They do not make decisions
     * or perform logic aside from deciding how to render what they are
     * given correctly.
     *
     * In Oroboros, Views are typically bound to an explicit
     * output format (eg: plaintext, html, json, xml, csv, etc).
     *
     * Views can also instantiate and manage other views,
     * so a granular approach to content output management
     * is possible by doing this. Scoped views always set their headers
     * correctly and can validate their content on demand.
     *
     * For Views that need to be more generalized, there are
     * default implementations of the AbstractView, HttpView,
     * and CgiView, which are scoped to anything, all web requests,
     * and all command line interaction respectively.
     *
     * This provides the flexibility of having general purpose
     * Views if desired, but again, neither approach is enforced.
     */
    const CLASS_TYPE_VIEW = "::view::";

}
