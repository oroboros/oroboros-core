<?php

namespace oroboros\core\patterns\creational;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of FactoryFactory
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class FactoryFactory
    extends \oroboros\core\abstracts\patterns\creational\AbstractFactory
{
//    private $_types = array(
//        "controller" => '\\oroboros\\core\\patterns\\creational\\ControllerFactory',
//        "model" => '\\oroboros\\core\\patterns\\creational\\ModelFactory',
//        "view" => '\\oroboros\\core\\patterns\\creational\\ViewFactory',
//        "library" => '\\oroboros\\core\\patterns\\creational\\LibraryFactory',
//        "adapter" => '\\oroboros\\core\\patterns\\creational\\AdapterFactory',
//        "module" => '\\oroboros\\core\\patterns\\creational\\ModuleFactory',
//        "template" => '\\oroboros\\core\\patterns\\creational\\TemplateFactory',
//        "theme" => '\\oroboros\\core\\patterns\\creational\\ThemeFactory',
//        "app" => '\\oroboros\\core\\patterns\\creational\\AppFactory',
//    );
//
//    public function load($type, array $params = array(), array $flags = array()) {
//        if (!array_key_exists($type, $this->_types)) {
//            throw new \oroboros\core\utilities\exception\Exception("Invalid factory type [" . (string) $type . ']', self::ERROR_LOGIC_BAD_PARAMETERS);
//        }
//        $class = new $this->_types[$type]();
//        $class->initialize($params, $flags);
//        return $class;
//    }
}
