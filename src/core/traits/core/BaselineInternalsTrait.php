<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core;

/**
 * <Oroboros Baseline Internals Trait>
 * This trait provides internal logic for both the
 * BaselineTrait and the StaticBaselineTrait.
 *
 * This trait is only used to prevent code duplication,
 * and does not provide any public methods.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait BaselineInternalsTrait
{
    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Parameter Validation Method>
     * Provides an inline means of quick parameter validation using
     * the baseline Validator class.
     * @see \oroboros\validate\traits\ValidatorTrait
     * @param string $mode A validation mode to use. Full index of these can be
     *     retrieved with self::_validationModes();
     * @param mixed $subject Validation subject to analyze. Varies by mode.
     * @param mixed $valid Parameters to validate against. Varies by mode.
     * @param bool $throw_exception_on_invalid (optional) if true, throws
     *     an automatic InvalidArgumentException if not valid. Default false.
     * @return bool
     */
    protected static function _validate( $mode, $subject, $valid = null,
        $throw_exception_on_invalid = false )
    {
        return \oroboros\validate\Validator::validate( $mode,
                $subject, $valid, $throw_exception_on_invalid );
    }

    /**
     * <Parameter Validation Mode Method>
     * Used to quickly obtain a list of the valid validation modes available.
     * @return array
     */
    protected static function _validationModes()
    {
        return \oroboros\validate\Validator::methods();
    }

    /**
     * <Exception Throw Method>
     * Throws an Oroboros exception by canonicalized keyword.
     * @param type $type (optional) The canonicalized keyword selector
     *     for the exception. Default is the Oroboros standard exception.
     * @param type $message (optional) Standard exception message.
     *     Default is no message.
     * @param type $code (optional) Standard exception code.
     *     May also be a canonicalized string. Default is 0.
     * @param \Exception $previous (optional) An optional previous exception
     *     to pass into the exception stack. Default is null.
     * @return void
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    protected static function _throwException( $type = 'exception',
        $message = '', $code = 0, \Exception $previous = null, $context = array() )
    {
        $bt = debug_backtrace(2);
        $last = array_shift($bt);
        $compiled_context = array(
            "error-line" => $bt[0]['line'],
            "error-method" => $bt[0]['class'] . $bt[0]['type'] . $bt[0]['function'],
            "origin" => '[' . $last['class'] . $last['type'] . $last['function'] . '] line [' . $last['line'] . '] of [' . $last['file'] . ']' ,
            "object-instance" => get_called_class(),
            "compiled-fingerprint" => self::getCompiledFingerprint(),
            "compiled-class" => __CLASS__,
            "backtrace" => $bt,
            "request-mode" => \oroboros\Oroboros::REQUEST_METHOD,
            "uri" => \oroboros\Oroboros::REQUEST_URI,
            "timestamp" => microtime(true),
            "script" => $_SERVER['SCRIPT_FILENAME']
        );
        unset($last);
        unset($bt);
        $context = array_replace($compiled_context, $context);
        return \oroboros\core\utilities\exception\ExceptionUtility::throwException( $type,
            $message, $code, $previous, $context );
    }

    /**
     * <Exception Recast Method>
     * Recasts a given exception as an equivalent that honors its inheritance
     * and is recast as an Oroboros Exception that honors the internal exception
     * contract interface.
     *
     * This method returns the exception but does not throw it.
     *
     * @param \Exception $exception
     * @param string $type (optional) If provided, will recast based on
     *     a canonicalized name
     * @return \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    protected static function _recastException( \Exception $exception,
        $type = null )
    {
        return \oroboros\core\utilities\exception\ExceptionUtility::recastException( $exception,
            $type );
    }

    /**
     * <Exception Generator Method>
     * Throws a generated exception using standardized Oroboros canonicalization.
     * This method uses standardized exception messages found in
     * the ExceptionMessage enumerated interface, and exception codes
     * found in the ExceptionCode enumerated interface.
     *
     * This method returns the exception but does not throw it.
     *
     * @example self::throwException( 'invalid-argument', 'logic-bad-parameters', array( 'string', 'boolean' );
     *
     * @param type $type
     * @param type $slug
     * @param type $params
     * @param \Exception $previous
     * @param array $context
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionCode
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionMessage
     * @return \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    protected static function _getException( $type = 'exception',
        $slug = 'unknown', $params = array(), \Exception $previous = null, $context = array() )
    {
        $bt = debug_backtrace(2);
        $last = array_shift($bt);
        $compiled_context = array(
            "error-line" => $bt[0]['line'],
            "error-method" => $bt[0]['class'] . $bt[0]['type'] . $bt[0]['function'],
            "origin" => '[' . $last['class'] . $last['type'] . $last['function'] . '] line [' . $last['line'] . '] of [' . $last['file'] . ']' ,
            "object-instance" => get_called_class(),
            "compiled-fingerprint" => self::getCompiledFingerprint(),
            "compiled-class" => __CLASS__,
            "backtrace" => $bt,
            "request-mode" => \oroboros\Oroboros::REQUEST_METHOD,
            "uri" => \oroboros\Oroboros::REQUEST_URI,
            "timestamp" => microtime(true),
            "script" => $_SERVER['SCRIPT_FILENAME']
        );
        unset($last);
        unset($bt);
        $context = array_replace($compiled_context, $context, $params);
        return \oroboros\core\utilities\exception\ExceptionUtility::getException( $type,
                $slug, $params, $previous, $context );
    }

    /**
     * <Exception Code Getter>
     * Returns the designated code for a canonicalized string.
     *
     * @param string $slug The canonicalized exception code identifier
     * @return int Returns 0 if code not found,
     *     otherwise returns the specified code.
     */
    protected static function _getExceptionCode( $slug = 'unknown' )
    {
        return \oroboros\core\utilities\exception\ExceptionUtility::getCode( $slug );
    }

    /**
     * <Exception Message Getter>
     * Returns the designated message for a canonicalized string.
     *
     * @param string $slug The canonicalized exception message identifier
     * @return string Returns "An unknown error has occurred." if code not found,
     *     otherwise returns the designated code for the specified message.
     */
    protected static function _getExceptionMessage( $slug = 'unknown' )
    {
        return \oroboros\core\utilities\exception\ExceptionUtility::getMessage( $slug );
    }

    /**
     * <Exception Identifier Getter>
     * Returns the designated canonicalized identifier for an exception code.
     *
     * @param int $code The exception code to reverse parse.
     * @return bool|string Returns false if code not found, otherwise returns
     * the canonicalized identifier for the code. The same identifier can be
     * used to return the corresponding default message.
     */
    protected static function _getExceptionIdentifier( $code = 0 )
    {
        return \oroboros\core\utilities\exception\ExceptionUtility::getCodeIdentifier( $code );
    }

    /**
     * <Exception Identifier Set Getter>
     * Returns an array of all acceptable canonicalized exception identifiers.
     *
     * @return array
     */
    protected static function _getExceptionCodeIdentifiers()
    {
        return \oroboros\core\utilities\exception\ExceptionUtility::getCodeIdentifiers();
    }

    /**
     * <Exception Identifier Set Getter>
     * Returns an array of all acceptable canonicalized exception identifiers.
     *
     * @return array
     */
    protected static function _getExceptionIdentifiers()
    {
        return \oroboros\core\utilities\exception\ExceptionUtility::getExceptionIdentifiers();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Merges parameters, letting the provided override the existing.
     * If not provided or null, an empty array will be created by
     * default to represent either.
     * @param array $provided
     * @param array $existing
     * @return array
     */
    private static function _baselineInternalsMergeArrayDefaults( $provided = null,
        $existing = null )
    {
        if ( is_null( $provided ) )
        {
            $provided = array();
        }
        if ( is_null( $existing ) )
        {
            $existing = array();
        }
        return array_replace_recursive( $existing, $provided );
    }

    /**
     * Performs extensive type and/or instance checking of provided parameters.
     * @param mixed $evaluate The parameter(s) to evaluate.
     *     If iterable, all parameters will be evaluated against the
     *     corresponding $expected parameter. Otherwise it will be type
     *     checked against the provided expected.
     * @param array $expected If this is not iterable, the evaluate param(s)
     *     must match explicitly. If it is iterable but not associatively keyed,
     *     any match in the set is considered valid. If it is associatively keyed,
     *     matches will be done against the corresponding key.
     * @param bool $skip_extras (optional) If true, additional values that exist
     *     in the evaluate set but not in the expected set will be skipped.
     *     If false, all unexpected keys will cause failure. Default true.
     * @param bool $use_strict_checks (optional) If true, strict type checking
     *     will be used. If false, loose checking will be used. Default false.
     * @param bool $throw_exception_on_failure (optional) If true, an exception
     *     will be raised if any failures occur, but all failures will be
     *     collected for the message. If false, the method will resolve in a
     *     non-blocking way. Default false.
     * @param bool $return_invalid_on_failure (optional) If true, failure will
     *     cause an array of the inconsistent checks instead of false. If false,
     *     only a boolean determination will be returned (true for pass and false
     *     for fail). Default false.
     * @return bool|array
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    private static function _baselineInternalsValidateValues( $evaluate,
        $expected, $skip_extras = true, $use_strict_checks = false,
        $throw_exception_on_failure = false, $return_invalid_on_failure = false )
    {

    }

    /**
     * Validates a given set against the provided whitelist,
     * using the specified mode.
     * @param string $mode
     * @param mixed $parameter
     * @param mixed $valid
     * @param bool $override
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If mismatch occurs
     */
    private static function _baselineInternalsCheckValid( $mode, $parameter,
        $valid, $override = false )
    {
        if ( !$override )
        {
            self::_validate( $mode, $parameter, $valid, true );
        }
    }

    /**
     * Throws a fail exception for bad parameters or dependencies.
     * This method will compile a verbose message of all missing
     * or invalid dependencies/settings/flags before throwing the
     * exception, To prevent the need for repeatedly working through
     * numerous related errors.
     * @param array $missing
     * @param array $invalid
     * @param string $type
     * @param string $method
     * @throws \oroboros\core\utilities\exception\RuntimeException
     */
    private static function _baselineInternalsThrowInvalidException( $missing,
        $invalid, $type, $method )
    {
        if ( !empty( $invalid ) || !empty( $missing ) )
        {
            //Fail initialization on account of bad dependency injection
            $fail_invalid = null;
            $fail_missing = null;
            $msg = 'Error encountered at [%1$s] in instanceof [%4$s]. ';
            if ( !empty( $missing ) )
            {
                $msg .= 'The following required ' . $type . ' were not provided: [%2$s]. ';
                $tmp_array = array();
                foreach ( $missing as
                    $key =>
                    $value )
                {
                    $tmp_array[] = $key . ': ' . $value;
                }
                $fail_missing = implode( ', ', $tmp_array );
            }
            if ( !empty( $invalid ) )
            {
                $msg .= 'The following required ' . $type . ' were invalid: [%3$s].';
                $tmp_array = array();
                foreach ( $invalid as
                    $key =>
                    $value )
                {
                    $tmp_array[] = $key . ': ' . $value;
                }
                $fail_invalid = implode( ', ', $tmp_array );
            }
            $msg = rtrim( $msg, ' ' );
            /**
             * @codeCoverageIgnoreStart
             * This method always throws an exception and cannot fully resolve
             * to PHPUnit standards. It works fine if it gets this far, and
             * it's used all over the place. Does not need further testing,
             * exceptions are rigorously tested elsewhere.
             */
            self::_throwException(
                'runtime-exception',
                sprintf( $msg, $method, $fail_missing, $fail_invalid,
                    get_called_class() ),
                !empty( $invalid )
                    ? 'initialization-dependency-invalid'
                    : 'initialization-missing-dependency'
            );
        }
        /**
         * @codeCoverageIgnoreEnd
         */
    }

    /**
     * <Baseline Internal Logic Validation Method>
     * This method is used to check that the provided internal configuration of
     * the class is correct,and is called during initialization both statically
     * on first initialization, as well as in object initialization.
     *
     * This method detects errors in programming logic, not external usage.
     * If this method throws an error, it means you need to fix your code
     * within the class extending upon this logic itself, not externally.
     *
     * @param string $type parameter|dependency|flag
     * @param type $default The default set the class provides
     * @param type $required The required set the class provides
     * @param type $valid The validation set the class provides
     * @return void
     * @throws \oroboros\core\utilities\exception\LogicException if the
     *     class configuration is bad. This will also raise a log entry of
     *     the alert level, as this class is unusable.
     */
    private static function _baselineInternalsCheckConfiguration( $type,
        $default, $required, $valid, $enabled = false )
    {
        $default_keys = array_keys( $default );
        $valid_keys = array_keys( $valid );
        $required_keys = $required;
        $class = get_called_class();
        $errors = array();
        //Check that provided defaults are valid
        if ( $enabled )
        {

            foreach ( $default_keys as
                $key )
            {
                if ( !in_array( $key, $valid_keys ) )
                {
                    $errors[] = sprintf( 'Default %1$s key [%2$s] provided by [%3$s] does not exist in provided valid %1$s keys',
                        $type, $key, $class );
                }
            }
            //Check that default values are valid
            foreach ( $default as
                $key =>
                $value )
            {
                switch ( $type )
                {
                    case 'parameter':
                        if ( array_key_exists( $key, $valid ) )
                        {
                            if ( is_subclass_of( $value, $valid[$key] ) || class_exists( $valid[$key] ) ||
                                interface_exists( $valid[$key] ) )
                            {
                                if ( !self::_validate( 'instance-of', $value,
                                        $valid[$key] ) )
                                {
                                    $errors[] = sprintf( 'Default %1$s key [%2$s] provided by [%3$s] is not a valid instance of: [%4$s]',
                                        $type, $key, $class, $valid[$key] );
                                }
                            } else
                            {
                                if ( !self::_validate( 'type-of', $value,
                                        $valid[$key] ) )
                                {
                                    $errors[] = sprintf( 'Default %1$s key [%2$s] provided by [%3$s] is not a valid type of: [%4$s]',
                                        $type, $key, $class, $valid[$key] );
                                }
                            }
                        }
                        break;
                    case 'dependency':
                        if ( array_key_exists( $key, $valid ) )
                        {
                            if ( !self::_validate( 'instance-of', $value,
                                    $valid[$key] ) )
                            {
                                $errors[] = sprintf( 'Default %1$s key [%2$s] provided by [%3$s] does not validate against the valid key set: [%4$s]',
                                    $type, $key, $class,
                                    implode( ', ', $valid_keys ) );
                            }
                        }
                        break;
                    case 'flag':
                        if ( !self::_validate( 'any-of', $value, $valid_keys ) )
                        {
                            $errors[] = sprintf( 'Default %1$s key [%2$s] provided by [%3$s] does not validate against the valid key set: [%4$s]',
                                $type, $key, $class,
                                implode( ', ', $valid_keys ) );
                        }
                        break;
                }
            }
            //Check that required keys are valid
            foreach ( $required_keys as
                $key )
            {
                if ( !in_array( $key, $valid_keys ) )
                {
                    $errors[] = sprintf( '%1$s key [%2$s] required by [%3$s] does not exist in provided valid %1$s keys',
                        ucfirst( $type ), $key, $class );
                }
            }
            if ( !empty( $errors ) )
            {
                if ( \oroboros\Oroboros::isInitialized() )
                {
                    \oroboros\Oroboros::log( 'alert',
                        'Class {class} is misconfigured and cannot initialize! '
                        . 'The following errors exist in the implemented logic: {errors}',
                        array(
                        'class' => $class,
                        'errors' => implode( ', ' . PHP_EOL, $errors )
                    ) );
                }
                self::_throwException( 'logic',
                    sprintf( 'Misconfigured initialization class logic encountered in [%s]. '
                        . 'The following errors exist in the provided baseline configuration: [%s].',
                        $class, implode( ', ', $errors ) ),
                    'php-wrong-schema-provided' );
            }
        }
    }

}
