<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core;

/**
 * <Oroboros Baseline Trait>
 * This trait provides common universal utilities required by almost all objects.
 * It represents a baseline foundation for fast development of ongoing object logic.
 *
 * This trait is designed to maximize common object requirement implementation without
 * collisions with external method requirements. As such, all of its public methods
 * passthrough to protected methods that can be called in place of the public ones
 * if a collision occurs, thereby retaining functionality without interfering with
 * 3rd party class extension or interface demands.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\BaselineContract
 */
trait BaselineTrait
{

    /**
     * Inherit the baseline internal logic.
     */
    use BaselineInternalsTrait;

    /**
     * Represents a sha1 hash of the classname and microtime when the
     * fingerprint was generated, creating a unique hash that identifies
     * a specific instance of an object. This can be generated in your
     * constructor by calling _getFingerprint(), or at any time by publicly
     * calling _baselineGetObjectFingerprint(). This trait does not expose any means
     * to alter the fingerprint once it is set, so it should remain consistent
     * once it has been generated for the lifespan of the object.
     * @var string
     */
    private $_object_fingerprint;

    /**
     * Represents whether the object automatically initializes when instantiated.
     * If this is true, then the initialization method will be called on
     * instantiation. If false, it will not be called. If you want to supress
     * automatic initialization, call _baselineDisableAutoInitialize() in your
     * constructor before calling parent::__construct();
     * @var bool
     */
    private $_auto_initialize = true;

    /**
     * Represents whether the baseline initialization has occurred.
     * @var bool
     */
    private $_baseline_initialized = false;

    /**
     * Represents whether initialization may be called again,
     * and override the previous initialization.
     *
     * By default, re-initialization is allowed.
     *
     * This functionality can be overridden by calling the
     * _baselineDisableReinitialization method in your constructor.
     *
     * @var bool
     */
    private $_baseline_allow_reinitialization = true;

    /**
     * Represents whether dependency validation will occur during initialization
     * or parameter setting. If this is true, only parameters existing in the
     * $_baseline_dependencies_valid property will be accepted.
     *
     * By default, dependency setting is not restricted in the object scope.
     *
     * @var bool
     */
    private $_baseline_use_dependency_validation = false;

    /**
     * Represents whether parameter validation will occur during initialization
     * or setting a property. If this is true, only parameters existing in the
     * $_baseline_parameters_valid property will be accepted.
     *
     * By default, parameter setting is not restricted in the object scope.
     *
     * @var bool
     */
    private $_baseline_use_parameter_validation = false;

    /**
     * Represents whether strict type checking is enabled for parameter
     * validation. If parameter validation is enabled and this is also
     * enabled, then only strict checks (===) will be used for validation of
     * parameters. If this is false, then loose checks (==) will be enabled.
     *
     * By default, strict checking is not enabled.
     *
     * @var bool
     */
    private $_baseline_use_parameter_validation_strict_checks = false;

    /**
     * Represents whether flag validation will occur during initialization
     * or flag setting. If this is true, only parameters existing in the
     * $_baseline_flags_valid property will be accepted.
     *
     * By default, flag setting is not restricted in the object scope.
     *
     * @var bool
     */
    private $_baseline_use_flag_validation = false;

    /**
     * Represents whether flags are allowed to be set externally.
     * If true, flags may be passed into the constructor, initialization,
     * and passed via the setFlag method. If false, attempting to set
     * flags will raise an exception.
     *
     * By default, flag setting is not restricted.
     *
     * This functionality may be overridden by calling the _baselinePreventFlagSetting()
     * method, which will prevent all subsequent flags from being set.
     *
     * @var bool
     */
    private $_baseline_allow_flag_setting = true;

    /**
     * Represents whether flags are allowed to be removed externally.
     * If true, flags may be removed via the unsetFlag method. If false,
     * attempting to remove a flag will raise an exception.
     *
     * By default, flag unsetting is not restricted.
     *
     * This functionality may be overridden by calling the _baselinePreventFlagUnSetting()
     * method, which will prevent all subsequent flags from being set.
     *
     * @var bool
     */
    private $_baseline_allow_flag_unsetting = true;

    /**
     * Represents whether parameters persist when reinitialization occurs.
     * If false, all parameters will be taken from the given array when
     * reinitialization occurs. If true, provided parameters will be appended
     * to the existing set if they do not already exist in it and replace
     * existing values if they do, but parameters already set will be otherwise
     * retained.
     *
     * By default, parameters are removed when reinitialization occurs.
     *
     * This functionalty may be toggled by calling
     * _baselineSetParameterPersistence(true)
     * or
     * _baselineSetParameterPersistence(false)
     *
     * @var bool
     */
    private $_baseline_persist_parameters = false;

    /**
     * Represents whether dependencies persist when reinitialization occurs.
     * If false, all dependencies will be taken from the given array when
     * reinitialization occurs. If true, provided dependencies will be appended
     * to the existing set if they do not already exist in it or replace
     * existing values if they do, but dependencies already set will be
     * otherwise retained.
     *
     * By default, dependencies are removed when reinitialization occurs.
     *
     * This functionalty may be toggled by calling
     * _baselineSetDependencyPersistence(true)
     * or
     * _baselineSetDependencyPersistence(false)
     *
     * @var bool
     */
    private $_baseline_persist_dependencies = false;

    /**
     * Represents whether flags persist when reinitialization occurs.
     * If false, all flags will be taken from the given array when
     * reinitialization occurs. If true, provided flags will be appended
     * to the existing set if they do not already exist in it, but flags
     * already set will be retained.
     *
     * By default, flags are removed when reinitialization occurs.
     *
     * This functionalty may be toggled by calling
     * _baselineSetFlagPersistence(true)
     * or
     * _baselineSetFlagPersistence(false)
     *
     * @var bool
     */
    private $_baseline_persist_flags = false;

    /**
     * Represents the baseline dependencies created during initialization
     * or passed in through dependency injection.
     * @var array
     */
    private $_baseline_dependencies = array();

    /**
     * Represents any dependencies that MUST be passed for instantiation to resolve.
     * This can be defined by overriding the method _baselineRequireDependencies()
     * and returning an associative array.
     *
     * By default, no dependencies are required.
     *
     * @var array
     */
    private $_baseline_dependencies_required = array();

    /**
     * Represents the valid dependencies that the object may receive.
     * Dependency injection objects will be rejected if they do not
     * exist in this array.
     *
     * You may update this array by calling the
     * _baselineSetDependenciesValid() method
     *
     * @var array
     */
    private $_baseline_dependencies_valid = array();

    /**
     * Represents the valid dependencies that the object may receive.
     * Dependency injection objects will be rejected if they do not
     * exist in this array.
     *
     * You may update this array by calling the
     * _baselineSetDependenciesDefault() method
     *
     * @var array
     */
    private $_baseline_dependencies_default = array();

    /**
     * Represents the parameters set during initialization.
     * An array is used by default, but this is not enforced,
     * and child classes are free to use any other variable
     * type as their parameter declaration.
     * @var array
     */
    private $_baseline_parameters = array();

    /**
     * Represents any parameters that MUST be passed for instantiation to resolve.
     * This can be defined by overriding the method _baselineRequireParameters()
     * and returning an associative array.
     *
     * By default, no parameters are required.
     *
     * @var array
     */
    private $_baseline_parameters_required = array();

    /**
     * Represents an associative array of valid initialization parameters.
     * If the property $_baseline_use_parameter_validation is true, only
     * parameters that exist in this array will be accepted through injection,
     * and an exception will be raised if any other parameter is given.
     *
     * By default, this is not checked.
     *
     * @var array
     */
    private $_baseline_parameters_valid = array();

    /**
     * Represents an associative array of default parameters set by the object
     * during initialization if not provided through parameter injection.
     *
     * This may be set in child classes by overriding the
     * _baselineSetParametersDefault() method, which will be called during
     * baseline initialization, and returning whatever baseline parameter(s)
     * you wish to set if not provided, typically an array or an abstraction
     * of an array of some sort, although no specific schema for these is
     * enforced at this level.
     *
     * @var array
     */
    private $_baseline_parameters_default = array();

    /**
     * Represents the flags passed in through initialization,
     * or set with the setFlag method.
     * @var type
     */
    private $_baseline_flags = array();

    /**
     * Represents any flags that MUST be passed for instantiation to resolve.
     * This can be defined by overriding the method _baselineRequireFlags()
     * and returning an array.
     *
     * By default, no parameters are required.
     *
     * @var array
     */
    private $_baseline_flags_required = array();

    /**
     * Represents the valid flags that can be set. If the property
     * $_baseline_use_flag_validation is true, then only flags that exist in
     * this array will be allowed to be passed through initialization or the
     * getFlag method. All other flags passed will raise an exception.
     *
     * By default, setting flags is not restricted.
     *
     * @var array
     */
    private $_baseline_flags_valid = array();

    /**
     * Represents any flags set by the object by default on initialization.
     * If any flags exist in this array, they will be provided automatically
     * when the final set of flags to evaluate is created during initialization.
     *
     * No parameters are passed to these methods.
     *
     * By default, no flags are set.
     *
     * @var type
     */
    private $_baseline_flags_default = array();

    /**
     * Represents an array of methods to call immediately prior to
     * completion of initialization. These must exist as valid internals
     * of the class that adds them, must have a visibility of protected or public,
     * and may not be external methods, closures, or callbacks, although they
     * may be static or otherwise.
     *
     * No parameters are passed to these methods.
     *
     * This array is always added to non-destructively, and no methods may be
     * removed from the queue by child classes.
     *
     * @var array
     */
    private $_baseline_initialization_tasks = array();

    /**
     * Represents an array of methods to call from the destructor. These must
     * exist as valid internals of the class that adds them, must have a
     * visibility of protected or public, and may not be external methods,
     * closures, or callbacks, although they may be static or otherwise.
     *
     * This array is always added to non-destructively, and no methods may be
     * removed from the queue by child classes.
     *
     * @var array
     */
    private $_baseline_destructor_tasks = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\core\BaselineContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Standard Constructor>
     * This presents a standardized constructor that universally handles
     * parameter, dependency, and flag injection.
     *
     * For universal constructor compatibility,
     * this method does not declare its arguments,
     * so it can validate to any __construct method
     * declaration in an interface. If this method is
     * overridden, the same functionality can be
     * attained by calling initialize();
     *
     * @param mixed $params
     * @param mixed $dependencies
     * @param mixed $flags
     */
    public function __construct()
    {
        /**
         * object instantiation should not proceed if static
         * initialization has not yet resolved.
         *
         * If this throws an exception, it will be allowed
         * to bubble up without being caught.
         *
         * Object scope will not make any assumptions about parameters
         * that should be passed to static initialization by default.
         */
        if ( ( $this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract ) &&
            !self::_staticBaselineIsInitialized() )
        {
            self::staticInitialize();
        }
        //Set the object fingerprint.
        $this->_baselineGetObjectFingerprint();
        $this->_baselineSetInitializationDefaults();
        if ( $this->_auto_initialize )
        {
            $vars = func_get_args();
            $params = array_key_exists( 0, $vars )
                ? $vars[0]
                : null;
            $dependencies = array_key_exists( 1, $vars )
                ? $vars[1]
                : null;
            $flags = array_key_exists( 2, $vars )
                ? $vars[2]
                : null;
            $this->initialize( $params, $dependencies, $flags );
        }
    }

    /**
     * <Standard Destructor>
     * This presents a standardized destructor that fires cleanup operations.
     */
    public function __destruct()
    {
        $this->_baselineRunDestructorTasks();
        if ( ($this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract ) &&
            ( $this instanceof \oroboros\core\interfaces\contract\core\BaselineContract ) )
        {
            //We only want to do this if the class implements
            //the corresponding interface, otherwise it will
            //cause a fatal error.
            self::_unregisterChildInstance( $this );
        }
    }

    /**
     * <Baseline Initialization Method>
     * Fires the baseline initialization.
     * @param mixed $params Any parameters that should be passed into initialization.
     * @param array $dependencies Any dependencies to inject into initialization.
     * @param array $flags
     * @return $this
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineInitialize( $params, $dependencies, $flags );
        return $this;
    }

    /**
     * <Baseline Runtime Type Context Getter Method>
     * Returns the default declared runtime class type context.
     * If the CLASS_TYPE class constant is not set on this object,
     * it will instead fall back to the static compiled class type,
     * which will in turn return either a compiled class type or false.
     *
     * @return string|bool
     */
    public function getType()
    {
        $const = get_called_class() . '::' . 'OROBOROS_CLASS_TYPE';
        return defined( $const )
            ? constant( $const )
            : self::getCompiledType();
    }

    /**
     * <Baseline Runtime Scope Context Getter Method>
     * Returns the default declared runtime class scope context.
     * If the CLASS_SCOPE class constant is not set on this object,
     * it will instead fall back to the static compiled class scope,
     * which will in turn return either a compiled class scope or false.
     *
     * @return string|bool
     */
    public function getScope()
    {
        $const = get_called_class() . '::' . 'OROBOROS_CLASS_SCOPE';
        return defined( $const )
            ? constant( $const )
            : self::getCompiledScope();
    }

    /**
     * <Baseline Runtime Api Context Getter Method>
     * Returns the default declared runtime class api context.
     * If the API class constant is not set on this object,
     * it will instead fall back to the static compiled class api,
     * which will in turn return either a compiled class api or false.
     *
     * @return string|bool
     */
    public function getApi()
    {
        $const = get_called_class() . '::' . 'OROBOROS_API';
        return defined( $const )
            ? constant( $const )
            : self::getCompiledApi();
    }

    /**
     * <Baseline Public Flag Setter Method>
     * Sets a given flag, if flag setting is allowed,
     * if it passes the whitelist if one exists, and
     * if it is a scalar value.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If setting flags is not allowed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given flag is not scalar, or if a whitelist is defined and
     *     the flag is not in it.
     */
    public function setFlag( $flag )
    {
        $this->_baselineSetFlag( $flag, false );
    }

    /**
     * <Baseline Public Flag Unsetter Method>
     * Removes a given flag, if it exists and unsetting flags is allowed.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed.
     */
    public function unsetFlag( $flag )
    {
        $this->_baselineUnsetFlag( $flag, false );
    }

    /**
     * <Baseline Flag Check Method>
     * Returns a boolean determination as to whether a given flag exists
     * in the current objects flag set.
     * @param scalar $flag
     * @return bool
     */
    public function checkFlag( $flag )
    {
        return $this->_baselineCheckFlag( $flag );
    }

    /**
     * <Baseline Flag Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public function getFlags()
    {
        return $this->_baseline_flags;
    }

    /**
     * <Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getRequiredParameters()
    {
        return $this->_baseline_parameters_required;
    }

    /**
     * <Baseline Required Dependency Getter Method>
     * Returns a list of all required dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getRequiredDependencies()
    {
        return $this->_baseline_dependencies_required;
    }

    /**
     * <Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getRequiredFlags()
    {
        return $this->_baseline_flags_required;
    }

    /**
     * <Baseline Valid Parameters Getter Method>
     * Returns a list of all valid parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getValidParameters()
    {
        return $this->_baseline_parameters_valid;
    }

    /**
     * <Baseline Valid Dependencies Getter Method>
     * Returns a list of all valid dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getValidDependencies()
    {
        return $this->_baseline_dependencies_valid;
    }

    /**
     * <Baseline Valid Flags Getter Method>
     * Returns a list of all required flags.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getValidFlags()
    {
        return $this->_baseline_flags_valid;
    }

    /**
     * <Standard Object Fingerprint Getter>
     * Returns the object fingerprint for the given object instance.
     * The static fingerprint is tracked globally across all instances
     * of a given object, whereas the object fingerprint is tracked
     * on a per-object designation. This method will create a fingerprint
     * if none has already been created, so there is no need to explicitly
     * call the setter from your constructor in most cases.
     * @return string
     */
    public function getFingerprint()
    {
        return $this->_baselineGetObjectFingerprint();
    }

    /**
     * <Standard Initialization Check>
     * Returns a boolean determination as to whether the object has completed
     * its baseline initialization.
     * @return bool
     */
    public function isInitialized()
    {
        return $this->_baseline_initialized;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Auto-Initialize Disable Method>
     * Disables automatic initialization if called in a child class constructor.
     * Sub-children may then call initialization from the constructor normally
     * by calling the initialize method manually if they wish to re-enable this
     * functionality.
     * @return void
     */
    protected function _baselineDisableAutoInitialize()
    {
        $this->_auto_initialize = false;
    }

    /**
     * <Disable Flag Setter Method>
     * Disables public flag setting through the setFlag method,
     * as well as passing additional flags into reinitialization calls.
     * @return void
     */
    protected function _baselinePreventFlagSetting()
    {
        $this->_baseline_allow_flag_setting = false;
    }

    /**
     * <Disable Flag Setter Method>
     * Disables public flag unsetting through the unsetFlag method.
     * @return void
     */
    protected function _baselinePreventFlagUnSetting()
    {
        $this->_baseline_allow_flag_unsetting = false;
    }

    /**
     * <Baseline Parameter Persistence Setter Method>
     * Allows for the persistence of parameters when
     * reinitialization occurs to be toggled on or off.
     * @param bool $opt
     * @return void
     */
    protected function _baselineSetParameterPersistence( $opt )
    {
        $this->_baseline_persist_parameters = ($opt
            ? true
            : false );
    }

    /**
     * <Baseline Dependency Persistence Setter Method>
     * Allows for the persistence of dependencies when
     * reinitialization occurs to be toggled on or off.
     * @param bool $opt
     * @return void
     */
    protected function _baselineSetDependencyPersistence( $opt )
    {
        $this->_baseline_persist_dependencies = ($opt
            ? true
            : false );
    }

    /**
     * <Baseline Flag Persistence Setter Method>
     * Allows for the persistence of flags when
     * reinitialization occurs to be toggled on or off.
     * @param bool $opt
     * @return void
     */
    protected function _baselineSetFlagPersistence( $opt )
    {
        $this->_baseline_persist_flags = ($opt
            ? true
            : false );
    }

    /**
     * <Disable Re-Initialization Method>
     * If this method is called in a constructor,
     * initialization will only be allowed to proceed once.
     * If called at any time after initialization, it will block
     * all subsequent attempts to reinitialize.
     * @return void
     */
    protected function _baselineDisableReinitialization()
    {
        $this->_baseline_allow_reinitialization = false;
    }

    /**
     * <Enable Parameter Validation Method>
     * If called in a constructor, this will force passed parameters
     * to be validated against the internally provided specifications.
     *
     * If called at any other time, reinitialization will be affected.
     *
     * @see _baselineSetParametersValid
     * @param type $use_strict_checks (optional) If true, all dependency checks
     *     will use strict checking instead of loose checking. Default false.
     * @return void
     */
    protected function _baselineEnableParameterValidation( $use_strict_checks = false )
    {
        $this->_baseline_use_parameter_validation = true;
        $this->_baseline_use_parameter_validation_strict_checks = $use_strict_checks
            ? true
            : false;
    }

    /**
     * <Enable Dependency Validation Method>
     * If called in a constructor, this will force passed dependencies
     * to be validated against the internally provided specifications.
     *
     * If called at any other time, reinitialization will be affected.
     *
     * @see _baselineSetDependenciesValid
     * @return void
     */
    protected function _baselineEnableDependencyValidation()
    {
        $this->_baseline_use_dependency_validation = true;
    }

    /**
     * <Enable Flag Validation Method>
     * If called in a constructor, this will force passed flags
     * to be validated against the internally provided specifications.
     *
     * If called at any other time, reinitialization and the
     * setFlag method will be affected.
     *
     * @see _baselineSetFlagsValid
     * @return void
     */
    protected function _baselineEnableFlagValidation()
    {
        $this->_baseline_use_flag_validation = true;
    }

    /**
     * <Baseline Internal Initialization Method>
     * Performs the underlying logic of the baseline initialization.
     * @param mixed $params
     * @param mixed $dependencies
     * @param mixed $flags
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If initialization has already occurred and reinitialization
     *     is disabled.
     */
    protected function _baselineInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        if ( ( $this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract ) &&
            !self::_staticBaselineIsInitialized() )
        {
            /**
             * object instantiation should not proceed if static
             * initialization has not yet resolved.
             *
             * If this throws an exception, it will be allowed
             * to bubble up without being caught.
             *
             * Object scope will not make any assumptions about parameters
             * that should be passed to static initialization by default.
             */
            self::staticInitialize();
        }
        try
        {
            $this->_baselineGetObjectFingerprint();
            $this->_baselineSetInitializationDefaults();
            $this->_baselineCheckReinitializationAllowed();
            $this->_baselineSetParameters( $this->_baselineGetParameters( $params ) );
            $this->_baselineSetDependencies( $this->_baselineGetDependencies( $dependencies ) );
            $this->_baselineSetFlags( $this->_baselineGetFlags( $flags ) );
            $this->_baselineRunInitializationTasks();
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s] in instanceof [%s].' . PHP_EOL . \oroboros\format\StringFormatter::indent('Static initialization could not resolve due to an error:' . PHP_EOL . '%s', 4),
                    __METHOD__, get_called_class(), \oroboros\format\StringFormatter::indent($e->getMessage(), 4), 'initialization', $e ) );
        }
        $this->_baseline_initialized = true;
    }

    /**
     * <Baseline Internal Fingerprint Getter Method>
     * Provides a means of internally fetching the fingerprint,
     * and automatically generating one if it is not already present.
     * If you would like to alter the fingerprinting logic, override
     * this method.
     * @return string
     */
    protected function _baselineGetObjectFingerprint()
    {
        if ( is_null( $this->_object_fingerprint ) )
        {
            $this->_baselineSetFingerprint();
        }
        return $this->_object_fingerprint;
    }

    /**
     * <Baseline Required Parameter Setter Method>
     * May be overridden to provide a set of parameters that
     * are required to be provided for initialization to resolve.
     *
     * Any parameters that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetParametersRequired()
    {
        return array();
    }

    /**
     * <Baseline Valid Parameter Setter Method>
     * May be overridden to provide a validation array for provided parameters.
     * If provided, any provided parameters matching the specified keys MUST
     * resolve to the given type or instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided parameters, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array();
    }

    /**
     * <Baseline Default Parameter Setter Method>
     * May be overridden to provide a set of default parameters.
     * If provided, the given set will act as a baseline set of
     * templated parameters, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * parameters.
     *
     * This allows for parameters to be provided in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return array();
    }

    /**
     * <Baseline Initialization Parameter Setter>
     * This method is called by the internal initialization method,
     * and is the method that resolves parameter setting and checking
     * against the private internals. This method may be overridden
     * to change the way that inbound parameters are handled, or bypass
     * it entirely.
     * @param array $parameters
     * @return void
     */
    protected function _baselineSetParameters( $parameters = null )
    {
        $parameters = $this->_baselineGetParameters( $parameters );
        $this->_baselineValidateParameters( $parameters );
        $this->_baseline_parameters = $parameters;
    }

    /**
     * <Baseline Required Dependency Setter Method>
     * May be overridden to provide a set of dependencies that
     * are required to be provided for initialization to resolve.
     *
     * Any dependencies that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetDependenciesRequired()
    {
        return array();
    }

    /**
     * <Baseline Valid Dependency Setter Method>
     * May be overridden to provide a validation array for provided dependencies.
     * If provided, any provided dependencies matching the specified keys MUST
     * resolve to the given instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided dependencies, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetDependenciesValid()
    {
        return array();
    }

    /**
     * <Baseline Default Dependency Setter Method>
     * May be overridden to provide a set of default dependencies.
     * If provided, the given set will act as a baseline set of
     * templated dependencies, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * dependencies.
     *
     * This allows for dependencies to be injected in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetDependenciesDefault()
    {
        return array();
    }

    /**
     * <Baseline Initialization Dependency Setter>
     * This method is called by the internal initialization method,
     * and is the method that resolves dependency injection and checking
     * against the private internals. This method may be overridden
     * to change the way that inbound dependencies are handled, or bypass
     * it entirely.
     * @param array $dependencies
     * @return void
     */
    protected function _baselineSetDependencies( $dependencies = null )
    {
        $dependencies = $this->_baselineGetDependencies( $dependencies );
        $this->_baselineValidateDependencies( $dependencies );
        $this->_baseline_dependencies = $dependencies;
    }

    /**
     * <Baseline Required Flags Setter Method>
     * May be overridden to provide a set of flags that
     * are required to be provided for initialization to resolve.
     *
     * Any flags that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetFlagsRequired()
    {
        return array();
    }

    /**
     * <Baseline Valid Flag Setter Method>
     * May be overridden to provide a validation array for provided flags.
     * If provided, any provided flags MUST exist in the given array of
     * valid values.
     *
     * The array should be a standard numerically keyed list of valid flags.
     *
     * If a non-empty validation array is given and any flags are passed
     * that do not exist in the validation array, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetFlagsValid()
    {
        return array();
    }

    /**
     * <Baseline Default Flag Setter Method>
     * May be overridden to provide a set of default flags.
     * If provided, the given set will act as a baseline set of
     * templated flags, and public provided parameters will
     * be recursively appended given array, and ignored if they
     * duplicate provided flags.
     *
     * This allows for flags to be provided in a way
     * that appends the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetFlagsDefault()
    {
        return array();
    }

    /**
     * <Baseline Initialization Flag Setter>
     * This method is called by the internal initialization method,
     * and is the method that resolves flag setting and checking
     * against the private internals. This method may be overridden
     * to change the way that inbound flags are handled, or bypass
     * it entirely.
     * @param array $flags
     * @return void
     */
    protected function _baselineSetFlags( $flags = null )
    {
        $flags = $this->_baselineGetFlags( $flags );
        $this->_baselineValidateFlags( $flags );
        $this->_baseline_flags = $flags;
    }

    /**
     * <Baseline Protected Dependency Getter Method>
     * @param string $id
     * @return null|mixed
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string
     */
    protected function _baselineGetParameter( $id )
    {
        self::_validate( 'type-of', $id, 'string', true );
        if ( array_key_exists( $id, $this->_baseline_parameters ) )
        {
            return $this->_baseline_parameters[$id];
        }
        return null;
    }

    /**
     * <Baseline Protected Dependency Setter Method>
     * @param type $id
     * @param type $param
     * @return void
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string
     */
    protected function _baselineSetParameter( $id, $param )
    {
        self::_validate( 'type-of', $id, 'string', true );
        $this->_baseline_parameters[$id] = $param;
    }

    /**
     * <Baseline Protected Dependency Getter Method>
     * @param string $id
     * @return null|object
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string
     */
    protected function &_baselineGetDependency( $id )
    {
        self::_validate( 'type-of', $id, 'string', true );
        if ( array_key_exists( $id, $this->_baseline_dependencies ) )
        {
            return $this->_baseline_dependencies[$id];
        }
        //Must return a reference to a variable,
        //so we will return a null variable.
        $response = null;
        return $response;
    }

    /**
     * <Baseline Protected Dependency Setter Method>
     * @param type $id
     * @param type $dependency
     * @return void
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string or dependency is not an object
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If override is false and the given dependency does not pass
     *     validation
     */
    protected function _baselineSetDependency( $id, $dependency,
        $override = false )
    {
        self::_validate( 'type-of', $id, 'string', true );
        self::_validate( 'object', $id, null, true );
        if ( !$override )
        {
            $this->_baselineCheckDependencyValid( $id, $dependency );
        }
        $this->_baseline_dependencies[$id] = $dependency;
    }

    /**
     * <Baseline Internal Flag Setter Method>
     * This allows internal logic to set flags while bypassing the public lock,
     * so that internal class operations are not impeded by the lock state.
     * @param scalar $flag
     * @param bool $override (optional) If set to false, the same flag
     *     validation checks will fire that do for public flag setting.
     *     Since this is an internal method, the default is true.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed.
     */
    protected function _baselineSetFlag( $flag, $override = true )
    {
        $this->_baselineSetFlagInternal( $flag, $override );
    }

    /**
     * <Baseline Internal Flag Unsetter Method>
     * This allows internal logic to remove flags while bypassing the public lock,
     * so that internal class operations are not impeded by the lock state.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed.
     */
    protected function _baselineUnsetFlag( $flag )
    {
        $this->_baselineUnsetFlagInternal( $flag, true );
    }

    /**
     * <Baseline Flag Check Method>
     * Checks if a given flag is set.
     * @param string $flag The flag to check
     * @return bool Returns true if set, false otherwise
     */
    protected function _baselineCheckFlag( $flag )
    {
        return in_array( $flag, $this->_baseline_flags );
    }

    /**
     * <Baseline Initialization Task Registration Method>
     * Adds an initialization task to the queue to run
     * after initialization completes.
     * @param string $method An accessible method from the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not valid and callable in the current scope
     */
    protected function _baselineRegisterInitializationTask( $method )
    {
        $this->_validate( 'type-of', $method, 'string', true );
        if ( !(method_exists( $this, $method ) && is_callable( array(
                $this,
                $method ) ) ) )
        {
            throw $this->_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'valid callable method in the current scope',
                $method ) );
        }
        if ( !in_array( $method, $this->_baseline_initialization_tasks ) )
        {
            $this->_baseline_initialization_tasks[] = $method;
        }
    }

    /**
     * <Baseline Destructor Task Registration Method>
     * Adds a destructor task to the queue to run
     * after when the object is released from memory.
     * @param string $method An accessible method from the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not valid and callable in the current scope
     */
    protected function _baselineRegisterDestructorTask( $method )
    {
        $this->_validate( 'type-of', $method, 'string', true );
        if ( !(method_exists( $this, $method ) && is_callable( array(
                $this,
                $method ) ) ) )
        {
            throw $this->_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'valid callable method in the current scope',
                $method ) );
        }
        if ( !in_array( $method, $this->_baseline_destructor_tasks ) )
        {
            $this->_baseline_destructor_tasks[] = $method;
        }
    }

    /**
     * <Baseline Destructor Task Unregistration Method>
     * Removes a previously queued destructor task.
     * @param string $method A method registered in the destructor queue.
     * @return void
     */
    protected function _baselineUnRegisterDestructorTask( $method )
    {
        if ( in_array( $method, $this->_baseline_destructor_tasks ) )
        {
            unset( $this->_baseline_destructor_tasks[array_search( $method,
                    $this->_baseline_destructor_tasks )] );
        }
    }

    /**
     * <Baseline Destructor Task List Method>
     * Returns an array of the methods queued to fire
     * when the destructor is called.
     * @return array
     */
    protected function _baselineShowDestructorTasks()
    {
        return $this->_baseline_destructor_tasks;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Object Fingerprint Setter>
     * Sets the object fingerprint.
     * This method should only be fired in a
     * constructor or clone operation, and should
     * only occur one time for any given object
     * instance.
     *
     * The object fingerprint provides a simple way of checking that you
     * are working with a specific instance of a given class, in the case
     * where numerous instances may exist.
     *
     * @return void
     */
    private function _baselineSetFingerprint()
    {
        //Update the static fingerprint if it has not already been generated.
        $hash = sha1( microtime() . get_class( $this ) );
        $this->_object_fingerprint = $hash;
        if ( ( $this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract ) &&
            ( $this instanceof \oroboros\core\interfaces\contract\core\BaselineContract ) )
        {
            //We only want to do this if the class implements
            //the corresponding interface, otherwise it will
            //cause a fatal error.
            self::_staticBaselineGetFingerprint();
            self::_registerChildInstance( $this );
        }
    }

    /**
     * <Baseline Initialization Internal Setup>
     * Sets the initialization default object property values,
     * calling any given overrides that have been provided, or
     * alternately providing nulled values if no overrides exist.
     * @return void
     */
    private function _baselineSetInitializationDefaults()
    {
        $this->_baselinePreflightFlags();
        $this->_baselinePreflightDependencies();
        $this->_baselinePreflightParameters();
    }

    /**
     * <Baseline Internal Parameter Settings Preflight Method>
     * Preflights initialization parameters to insure
     * that the internal class settings are correct.
     * @return void
     */
    private function _baselinePreflightParameters()
    {
        $compiled_parameters_required = array();
        $compiled_parameters_valid = array();
        $compiled_parameters_default = array();
        if ( $this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract )
        {
            $compiled_parameters_required = $this->getCompiledRequiredParameters();
            $compiled_parameters_valid = $this->getCompiledValidParameters();
            $compiled_parameters_default = $this->getCompiledParameters();
        }
        $this->_baseline_parameters_default = array_replace_recursive( $compiled_parameters_default,
            $this->_baselineSetParametersDefault() );
        $this->_baseline_parameters_valid = array_replace_recursive( $compiled_parameters_valid,
            $this->_baselineSetParametersValid() );
        $this->_baseline_parameters_required = array_replace_recursive( $compiled_parameters_required,
            $this->_baselineSetParametersRequired() );
        $this->_baselineInternalsCheckConfiguration( 'parameter',
            $this->_baseline_parameters_default,
            $this->_baseline_parameters_required,
            $this->_baseline_parameters_valid,
            $this->_baseline_use_parameter_validation
        );
    }

    /**
     * <Baseline Internal Dependency Settings Preflight Method>
     * Preflights initialization dependencies to insure
     * that the internal class settings are correct.
     * @return void
     */
    private function _baselinePreflightDependencies()
    {
        $compiled_dependencies_required = array();
        $compiled_dependencies_valid = array();
        $compiled_dependencies_default = array();
        if ( $this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract )
        {
            $compiled_dependencies_required = $this->getCompiledRequiredDependencies();
            $compiled_dependencies_valid = $this->getCompiledValidDependencies();
            $compiled_dependencies_default = $this->getCompiledDependencies();
        }
        $this->_baseline_dependencies_default = array_replace_recursive( $compiled_dependencies_default,
            $this->_baselineSetDependenciesDefault() );
        $this->_baseline_dependencies_valid = array_replace_recursive( $compiled_dependencies_valid,
            $this->_baselineSetDependenciesValid() );
        $this->_baseline_dependencies_required = array_replace_recursive( $compiled_dependencies_required,
            $this->_baselineSetDependenciesRequired() );
        $this->_baselineInternalsCheckConfiguration( 'dependency',
            $this->_baseline_dependencies_default,
            $this->_baseline_dependencies_required,
            $this->_baseline_dependencies_valid,
            $this->_baseline_use_dependency_validation
        );
    }

    /**
     * <Baseline Internal Flag Settings Preflight Method>
     * Preflights initialization flags to insure
     * that the internal class settings are correct.
     * @return void
     */
    private function _baselinePreflightFlags()
    {
        $compiled_flags_required = array();
        $compiled_flags_valid = array();
        $compiled_flags_default = array();
        if ( $this instanceof \oroboros\core\interfaces\contract\core\StaticBaselineContract )
        {
            $compiled_flags_required = $this->getCompiledRequiredFlags();
            $compiled_flags_valid = $this->getCompiledValidFlags();
            $compiled_flags_default = $this->getCompiledFlags();
        }
        $this->_baseline_flags_default = array_replace_recursive( $compiled_flags_default,
            $this->_baselineSetFlagsDefault() );
        $this->_baseline_flags_valid = array_replace_recursive( $compiled_flags_valid,
            $this->_baselineSetFlagsValid() );
        $this->_baseline_flags_required = array_replace_recursive( $compiled_flags_required,
            $this->_baselineSetFlagsRequired() );
        $this->_baselineInternalsCheckConfiguration( 'flag',
            $this->_baseline_flags_default, $this->_baseline_flags_required,
            $this->_baseline_flags_valid, $this->_baseline_use_flag_validation
        );
    }

    /**
     * <Reinitialization Check Method>
     * Checks if reinitialization is allowed, and blocks subsequent
     * initialization attempts if initialization has occurred and
     * reinitializing is disallowed.
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If initialization has already occurred and reinitialization
     *     is disabled.
     */
    private function _baselineCheckReinitializationAllowed()
    {
        if ( !$this->_baseline_allow_reinitialization && $this->_baseline_initialized )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s]. Reinitialization for [%s] is not allowed.',
                    __METHOD__, get_class( $this ) ),
                'initialization-already-initialized' );
        }
    }

    /**
     * <Baseline Internal Flag Setter Logic>
     * Performs the task of checking the validity of a
     * given flag and setting it if allowed.
     * @param scalar $flag The flag to set
     * @param bool $override (optional) if provided, bypasses the
     *     whitelist check. The protected flag setter uses this to
     *     allow internal flag setting regardless of the public lock.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a flag does not exist in the given validation array, or
     *     if it is a non-scalar value.
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed and the override switch
     *     is not set to true.
     */
    private function _baselineSetFlagInternal( $flag, $override = false )
    {
        if ( !$override && !$this->_baseline_allow_flag_setting )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s]. Setting flags for [%s] is not allowed.',
                    __METHOD__, get_class( $this ) ), 'security-locked-resource' );
        }
        $this->_baselineCheckFlagValid( $flag, $override );
        //Duplicate flags are unneccessary.
        if ( in_array( $flag, $this->_baseline_flags ) )
        {
            return;
        }
        $this->_baseline_flags[] = $flag;
    }

    /**
     * <Baseline Internal Flag Un-Setter Logic>
     * Performs the task of checking if unsetting of flags is allowed,
     * and removing it if it is and it exists.
     * @param scalar $flag The flag to unset
     * @param bool $override (optional) if provided, bypasses the whitelist
     *     check. The protected flag setter uses this to allow internal flag
     *     setting regardless of the public lock.
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed, or if a whitelist was defined and
     *     the flag is not in it, and override is not enabled.
     */
    private function _baselineUnsetFlagInternal( $flag, $override = false )
    {
        if ( !$override && !$this->_baseline_allow_flag_unsetting )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s]. Unsetting flags for [%s] is not allowed.',
                    __METHOD__, get_class( $this ) ), 'security-locked-resource' );
        }
        //Remove the flag if it exists
        if ( in_array( $flag, $this->_baseline_flags ) )
        {
            unset( $this->_baseline_flags[array_search( $flag,
                    $this->_baseline_flags )] );
        }
    }

    /**
     * <Baseline Internal Flag Validator Logic>
     * Checks that a given flag is scalar, and that it exists in the
     * whitelisted set if one was defined.
     * @param type $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed, or if a whitelist was defined and
     *     the flag is not in it, and the override switch is not enabled.
     */
    private function _baselineCheckFlagValid( $flag, $override = false )
    {
        self::_validate( 'scalar', $flag, null, true );
        $whitelist = $this->_baseline_flags_valid;
        self::_baselineInternalsCheckValid( 'any-of', $flag, $whitelist,
            $override );
    }

    /**
     * <Baseline Internal Parameter Validator Logic>
     * Checks that a given parameter valid.
     * @param type $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed.
     */
    private function _baselineCheckParameterValid( $key, $parameter,
        $override = false )
    {
        $whitelist = $this->_baseline_parameters_valid;
        if ( class_exists( $whitelist[$key] ) || interface_exists( $whitelist[$key] ) )
        {
            self::_baselineInternalsCheckValid( 'instance-of', $parameter,
                $whitelist[$key], $override );
        } elseif ( is_string( $whitelist[$key] ) && in_array( $whitelist[$key],
                self::_validationModes() ) )
        {
            self::_baselineInternalsCheckValid( $whitelist[$key], $parameter,
                true, $override );
        } else
        {
            self::_baselineInternalsCheckValid( 'type-of', $parameter,
                $whitelist[$key], $override );
        }
    }

    /**
     * <Baseline Internal Dependency Validator Logic>
     * Checks that a given parameter valid.
     * @param type $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid dependency is passed.
     */
    private function _baselineCheckDependencyValid( $key, $dependency,
        $override = false )
    {
        $whitelist = $this->_baseline_dependencies_valid;
        self::_baselineInternalsCheckValid( 'instance-of', $dependency,
            $whitelist[$key], $override );
    }

    /**
     * Validates the provided parameters to insure that they match
     * the expected values, and that all required parameters have been
     * provided.
     * @param array $parameters
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If parameter checking is enabled and an invalid parameter is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a required parameter is not given.
     */
    private function _baselineValidateParameters( $parameters )
    {
        if ( $this->_baseline_use_parameter_validation )
        {
            $whitelist = $this->_baseline_parameters_valid;
            $required = $this->_baseline_parameters_required;
            $invalid = array();
            $missing = array();
            //Check valid
            foreach ( $parameters as
                $name =>
                $parameter )
            {
                if ( array_key_exists( $name, $whitelist ) )
                {
                    try
                    {
                        $this->_baselineCheckParameterValid( $name, $parameter,
                            false );
                    } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
                    {
                        $invalid[$name] = $whitelist[$name];
                    }
                }
            }
            //Check missing
            foreach ( $required as
                $require )
            {
                if ( !array_key_exists( $require, $parameters ) )
                {
                    $missing[] = $require;
                }
            }
            if ( !empty( $invalid ) || !empty( $missing ) )
            {
                self::_baselineInternalsThrowInvalidException( $missing,
                    $invalid, 'parameters', __METHOD__ );
            }
        }
    }

    /**
     * Validates the provided dependencies to insure that they match
     * the expected values, and that all required dependencies have been
     * provided.
     * @param array $dependencies
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If dependency checking is enabled and an invalid dependency is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a required dependency is not given.
     */
    private function _baselineValidateDependencies( $dependencies )
    {
        if ( $this->_baseline_use_dependency_validation )
        {
            $whitelist = $this->_baseline_dependencies_valid;
            $required = $this->_baseline_dependencies_required;
            $invalid = array();
            $missing = array();
            //Check valid
            foreach ( $dependencies as
                $name =>
                $dependency )
            {
                if ( array_key_exists( $name, $whitelist ) )
                {
                    try
                    {
                        $this->_baselineCheckDependencyValid( $name,
                            $dependency, false );
                    } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
                    {
                        $invalid[$name] = $required[$name];
                    }
                }
            }
            //Check missing
            foreach ( $required as
                $require )
            {
                if ( !array_key_exists( $require, $dependencies ) )
                {
                    $missing[$require] = $whitelist[$require];
                }
            }
            if ( !empty( $invalid ) || !empty( $missing ) )
            {
                self::_baselineInternalsThrowInvalidException( $missing,
                    $invalid, 'dependencies', __METHOD__ );
            }
        }
    }

    /**
     * Validates the provided flags to insure that they match
     * the expected values, and that all required flags have been
     * provided.
     * @param array $flags
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If flag checking is enabled and an invalid flag is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a required flag is not given.
     */
    private function _baselineValidateFlags( $flags )
    {
        if ( $this->_baseline_use_flag_validation )
        {
            $whitelist = $this->_baseline_flags_valid;
            $required = $this->_baseline_flags_required;
            $invalid = array();
            $missing = array();
            //Check valid
            foreach ( $flags as
                $flag )
            {
                try
                {
                    $this->_baselineCheckFlagValid( $flag );
                } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
                {
                    $invalid[] = $flag;
                }
                if ( !$this->_baseline_allow_flag_setting )
                {
                    self::_throwException( 'runtime',
                        sprintf( 'Error encountered at [%s]. Setting flags for [%s] is not allowed.',
                            __METHOD__, get_class( $this ) ),
                        'security-locked-resource' );
                }
            }
            //Check required
            foreach ( $required as
                $require )
            {
                if ( !in_array( $require, $flags ) )
                {
                    $missing[] = $require;
                }
            }
            if ( !empty( $invalid ) || !empty( $missing ) )
            {
                self::_baselineInternalsThrowInvalidException( $missing,
                    $invalid, 'flags', __METHOD__ );
            }
        }
    }

    /**
     * Returns the merged set of provided dependencies and defaults.
     * @param type $dependencies
     * @return type
     */
    private function _baselineGetDependencies( $dependencies = null )
    {
        return self::_baselineInternalsMergeArrayDefaults( $dependencies,
                $this->_baseline_dependencies_default );
    }

    /**
     * Returns the merged set of provided flags and defaults.
     * @param array $flags
     * @return array
     */
    private function _baselineGetFlags( $flags = null )
    {
        return self::_baselineInternalsMergeArrayDefaults( $flags,
                $this->_baseline_flags_default );
    }

    /**
     * Returns the merged set of provided parameters and defaults,
     * if a mergeable set is provided.
     * @param mixed $params
     * @return mixed
     */
    private function _baselineGetParameters( $params = null )
    {
        return self::_baselineInternalsMergeArrayDefaults( $params,
                $this->_baseline_parameters_default );
    }

    /**
     * Runs any registered initialization tasks
     * immediately prior to initialization.
     * @return void
     */
    private function _baselineRunInitializationTasks()
    {
        foreach ( $this->_baseline_initialization_tasks as
            $initialization_task )
        {
            if ( method_exists( $this, $initialization_task ) && is_callable( array(
                    $this,
                    $initialization_task ) ) )
            {
                $this->$initialization_task();
            }
        }
    }

    /**
     * Runs any cleanup tasks registered for the destructor.
     * @return void
     */
    private function _baselineRunDestructorTasks()
    {
        foreach ( $this->_baseline_destructor_tasks as
            $destructor_task )
        {
            if ( method_exists( $this, $destructor_task ) && is_callable( array(
                    $this,
                    $destructor_task ) ) )
            {
                $this->$destructor_task();
            }
        }
    }

}
