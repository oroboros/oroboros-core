<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core;

/**
 * <Oroboros Static Baseline Trait>
 * This trait provides common universal utilities required by almost
 * all static  classes. It represents a baseline foundation for fast
 * development of ongoing static logic.
 *
 * This trait is designed to maximize common object requirement implementation without
 * collisions with external method requirements. As such, all of its public methods
 * passthrough to protected methods that can be called in place of the public ones
 * if a collision occurs, thereby retaining functionality without interfering with
 * 3rd party class extension or interface demands.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\StaticBaselineContract
 */
trait StaticBaselineTrait
{

    use BaselineInternalsTrait
    {
        BaselineInternalsTrait::_validate as _baselineValidate;
    }

    /**
     * Represents the instance fingerprint, which will updated
     * whenever reinitialized. This presents a simple means of checking if a static object
     * has changed from an expected state since last use by a dependent class.
     * @var string
     */
    private static $_static_fingerprint = null;

    /**
     * Represents whether the baseline static initialization has occurred
     * at least one time.
     *
     * This will fire by calling the static method staticInitialize,
     * and will be automatically called if the class also uses the
     * BaselineTrait on the first initialization of that object.
     * This sets the low-level fingerprint, designating a specific
     * chain of inheritance, and allows for universalized dependencies,
     * flags, and parameters to be provided across all objects that
     * extend from the base level class using this trait.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * @var bool
     */
    private static $_static_baseline_initialized = false;

    /**
     * Represents child instances of the implementing class that are actively instantiated.
     * This array represents an associative array where the key is the object fingerprint,
     * and the value is the fully qualified class name of the live instance. This does
     * not track references to those instances, it only tracks which instances are currently
     * active. Child instances will typically register themselves when their fingerprint
     * is first set, and clear it in their destructor.
     * @var array
     */
    private static $_static_baseline_child_instances = array();

    /**
     * Represents whether initialization may be called again,
     * and override the previous initialization.
     *
     * By default, re-initialization is allowed.
     *
     * This functionality can be overridden by calling the
     * _baselineDisableReinitialization method in your constructor.
     *
     * @var bool
     */
    private static $_static_baseline_allow_reinitialization = true;

    /**
     * Represents whether dependency validation will occur during initialization
     * or parameter setting. If this is true, only parameters existing in the
     * $_baseline_dependencies_valid property will be accepted.
     *
     * By default, dependency setting is not restricted in the object scope.
     *
     * @var bool
     */
    private static $_static_baseline_use_dependency_validation = false;

    /**
     * Represents whether parameter validation will occur during initialization
     * or setting a property. If this is true, only parameters existing in the
     * $_baseline_parameters_valid property will be accepted.
     *
     * By default, parameter setting is not restricted in the object scope.
     *
     * @var bool
     */
    private static $_static_baseline_use_parameter_validation = false;

    /**
     * Represents whether strict type checking is enabled for parameter
     * validation. If parameter validation is enabled and this is also
     * enabled, then only strict checks (===) will be used for validation of
     * parameters. If this is false, then loose checks (==) will be enabled.
     *
     * By default, strict checking is not enabled.
     *
     * @var bool
     */
    private static $_static_baseline_use_parameter_validation_strict_checks = false;

    /**
     * Represents whether flag validation will occur during initialization
     * or flag setting. If this is true, only parameters existing in the
     * $_baseline_flags_valid property will be accepted.
     *
     * By default, flag setting is not restricted in the object scope.
     *
     * @var bool
     */
    private static $_static_baseline_use_flag_validation = false;

    /**
     * Represents whether flags are allowed to be set externally.
     * If true, flags may be passed into the constructor, initialization,
     * and passed via the setFlag method. If false, attempting to set
     * flags will raise an exception.
     *
     * By default, flag setting is not restricted.
     *
     * This functionality may be overridden by calling the _baselinePreventFlagSetting()
     * method, which will prevent all subsequent flags from being set.
     *
     * @var bool
     */
    private static $_static_baseline_allow_flag_setting = true;

    /**
     * Represents whether flags are allowed to be removed externally.
     * If true, flags may be removed via the unsetFlag method. If false,
     * attempting to remove a flag will raise an exception.
     *
     * By default, flag unsetting is not restricted.
     *
     * This functionality may be overridden by calling the _baselinePreventFlagUnSetting()
     * method, which will prevent all subsequent flags from being set.
     *
     * @var bool
     */
    private static $_static_baseline_allow_flag_unsetting = true;

    /**
     * Represents whether parameters persist when reinitialization occurs.
     * If false, all parameters will be taken from the given array when
     * reinitialization occurs. If true, provided parameters will be appended
     * to the existing set if they do not already exist in it and replace
     * existing values if they do, but parameters already set will be otherwise
     * retained.
     *
     * By default, parameters are removed when reinitialization occurs.
     *
     * This functionalty may be toggled by calling
     * _baselineSetParameterPersistence(true)
     * or
     * _baselineSetParameterPersistence(false)
     *
     * @var bool
     */
    private static $_static_baseline_persist_parameters = false;

    /**
     * Represents whether dependencies persist when reinitialization occurs.
     * If false, all dependencies will be taken from the given array when
     * reinitialization occurs. If true, provided dependencies will be appended
     * to the existing set if they do not already exist in it or replace
     * existing values if they do, but dependencies already set will be
     * otherwise retained.
     *
     * By default, dependencies are removed when reinitialization occurs.
     *
     * This functionalty may be toggled by calling
     * _baselineSetDependencyPersistence(true)
     * or
     * _baselineSetDependencyPersistence(false)
     *
     * @var bool
     */
    private static $_static_baseline_persist_dependencies = false;

    /**
     * Represents whether flags persist when reinitialization occurs.
     * If false, all flags will be taken from the given array when
     * reinitialization occurs. If true, provided flags will be appended
     * to the existing set if they do not already exist in it, but flags
     * already set will be retained.
     *
     * By default, flags are removed when reinitialization occurs.
     *
     * This functionalty may be toggled by calling
     * _baselineSetFlagPersistence(true)
     * or
     * _baselineSetFlagPersistence(false)
     *
     * @var bool
     */
    private static $_static_baseline_persist_flags = false;

    /**
     * Represents the baseline dependencies created during initialization
     * or passed in through dependency injection.
     * @var array
     */
    private static $_static_baseline_dependencies = array();

    /**
     * Represents any dependencies that MUST be passed for instantiation to resolve.
     * This can be defined by overriding the method _baselineRequireDependencies()
     * and returning an associative array.
     *
     * By default, no dependencies are required.
     *
     * @var array
     */
    private static $_static_baseline_dependencies_required = array();

    /**
     * Represents the valid dependencies that the object may receive.
     * Dependency injection objects will be rejected if they do not
     * exist in this array.
     *
     * You may update this array by calling the
     * _baselineSetDependenciesValid() method
     *
     * @var array
     */
    private static $_static_baseline_dependencies_valid = array();

    /**
     * Represents the valid dependencies that the object may receive.
     * Dependency injection objects will be rejected if they do not
     * exist in this array.
     *
     * You may update this array by calling the
     * _baselineSetDependenciesDefault() method
     *
     * @var array
     */
    private static $_static_baseline_dependencies_default = array();

    /**
     * Represents the parameters set during initialization.
     * An array is used by default, but this is not enforced,
     * and child classes are free to use any other variable
     * type as their parameter declaration.
     * @var array
     */
    private static $_static_baseline_parameters = array();

    /**
     * Represents any parameters that MUST be passed for instantiation to resolve.
     * This can be defined by overriding the method _baselineRequireParameters()
     * and returning an associative array.
     *
     * By default, no parameters are required.
     *
     * @var array
     */
    private static $_static_baseline_parameters_required = array();

    /**
     * Represents an associative array of valid initialization parameters.
     * If the property $_baseline_use_parameter_validation is true, only
     * parameters that exist in this array will be accepted through injection,
     * and an exception will be raised if any other parameter is given.
     *
     * By default, this is not checked.
     *
     * @var array
     */
    private static $_static_baseline_parameters_valid = array();

    /**
     * Represents an associative array of default parameters set by the object
     * during initialization if not provided through parameter injection.
     *
     * This may be set in child classes by overriding the
     * _baselineSetParametersDefault() method, which will be called during
     * baseline initialization, and returning whatever baseline parameter(s)
     * you wish to set if not provided, typically an array or an abstraction
     * of an array of some sort, although no specific schema for these is
     * enforced at this level.
     *
     * @var array
     */
    private static $_static_baseline_parameters_default = array();

    /**
     * Represents the flags passed in through initialization,
     * or set with the setFlag method.
     * @var type
     */
    private static $_static_baseline_flags = array();

    /**
     * Represents any flags that MUST be passed for instantiation to resolve.
     * This can be defined by overriding the method _baselineRequireFlags()
     * and returning an array.
     *
     * By default, no parameters are required.
     *
     * @var array
     */
    private static $_static_baseline_flags_required = array();

    /**
     * Represents the valid flags that can be set. If the property
     * $_baseline_use_flag_validation is true, then only flags that exist in
     * this array will be allowed to be passed through initialization or the
     * getFlag method. All other flags passed will raise an exception.
     *
     * By default, setting flags is not restricted.
     *
     * @var array
     */
    private static $_static_baseline_flags_valid = array();

    /**
     * Represents any flags set by the object by default on initialization.
     * If any flags exist in this array, they will be provided automatically
     * when the final set of flags to evaluate is created during initialization.
     *
     * No parameters are passed to these methods.
     *
     * By default, no flags are set.
     *
     * @var type
     */
    private static $_static_baseline_flags_default = array();

    /**
     * Represents an array of methods to call immediately prior to
     * completion of initialization. These must exist as valid internals
     * of the class that adds them, must have a visibility of protected or public,
     * and may not be external methods, closures, or callbacks, although they
     * may be static or otherwise.
     *
     * No parameters are passed to these methods.
     *
     * This array is always added to non-destructively, and no methods may be
     * removed from the queue by child classes.
     *
     * @var array
     */
    private static $_static_baseline_initialization_tasks = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\core\StaticBaselineContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Static Baseline Initialization Method>
     * Initializes a class in the static scope, so that related configurations,
     * dependencies, flags, and settings are accessible across a range of child
     * classes. This also allows for extension logic to be immediately accessible
     * to all child classes if extensions are used.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * Stateful static initialization should only ever occur in a
     * dedicated base object that does not have a deep inheritance chain,
     * and only in very specialized cases. The functionality is not outright
     * prevented, however it does require several overrides to enable, because
     * its use in such a manner is highly discouraged to avoid introducing
     * instability in your code. Internal Oroboros logic always disables this
     * functionality, but you may roll your own classes using this trait that
     * implement it pretty easily if need be.
     *
     * @param type $params (optional) Any parameters that need to be accessible across all child objects.
     * @param type $dependencies (optional) Any dependencies that need to be accessible across all child objects.
     * @param type $flags (optional) Any flags that need to be accessible across all child objects.
     * @return void
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        self::_staticBaselineInitialize( $params, $dependencies, $flags );
        return get_called_class();
    }

    /**
     * <Static Baseline Initialization Check Method>
     * Returns a boolean determination as to whether the class
     * is statically initialized. This parameter is only ever set by the
     * _staticBaselineInitialize method if it has fully resolved at least
     * one time, and indicates that all internal static properties are
     * available to all child objects of the class.
     * @return bool
     */
    public static function isStaticInitialized()
    {
        return self::_staticBaselineIsInitialized();
    }

    /**
     * <Static Baseline Static Type Context Getter Method>
     * Returns the default declared runtime class type context.
     * If the CLASS_TYPE class constant is not set on this object,
     * it will instead fall back to the static compiled class type,
     * which will in turn return either a compiled class type or false.
     *
     * @return string|bool
     */
    public static function getCompiledType()
    {
        $class = get_called_class();
        return $class::_staticBaselineGetCompiledType();
    }

    /**
     * <Static Baseline Static Scope Context Getter Method>
     * Returns the default declared runtime class scope context.
     * If the CLASS_SCOPE class constant is not set on this object,
     * it will instead fall back to the static compiled class scope,
     * which will in turn return either a compiled class scope or false.
     *
     * @return string|bool
     */
    public static function getCompiledScope()
    {
        $class = get_called_class();
        return $class::_staticBaselineGetCompiledScope();
    }

    /**
     * <Static Baseline Static Api Context Getter Method>
     * Returns the default declared runtime class api context.
     * If the API class constant is not set on this object,
     * it will instead fall back to the static compiled class api,
     * which will in turn return either a compiled class api or false.
     *
     * @return string|bool
     */
    public static function getCompiledApi()
    {
        $class = get_called_class();
        return $class::_staticBaselineGetCompiledApi();
    }

    /**
     * <Static Baseline Public Flag Setter Method>
     * Sets a given flag, if flag setting is allowed,
     * if it passes the whitelist if one exists, and
     * if it is a scalar value.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If setting flags is not allowed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given flag is not scalar, or if a whitelist is defined and
     *     the flag is not in it.
     */
    public static function setCompiledFlag( $flag )
    {
        self::_staticBaselineSetFlag( $flag, false );
    }

    /**
     * <Static Baseline Public Flag Unsetter Method>
     * Removes a given flag, if it exists and unsetting flags is allowed.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed.
     */
    public static function unsetCompiledFlag( $flag )
    {
        self::_staticBaselineUnsetFlag( $flag, false );
    }

    /**
     * <Static Baseline Flag Check Method>
     * Returns a boolean determination as to whether a given flag exists
     * in the current objects flag set.
     * @param scalar $flag
     * @return bool
     */
    public static function checkCompiledFlag( $flag )
    {
        return self::_staticBaselineCheckFlag( $flag );
    }

    /**
     * <Static Baseline Flag Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public static function getCompiledFlags()
    {
        return self::$_static_baseline_flags;
    }

    /**
     * <Static Baseline Parameter Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public static function getCompiledParameters()
    {
        return self::$_static_baseline_parameters;
    }

    /**
     * <Static Baseline Dependency Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public static function getCompiledDependencies()
    {
        return self::$_static_baseline_dependencies;
    }

    /**
     * <Static Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledRequiredParameters()
    {
        return self::$_static_baseline_parameters_required;
    }

    /**
     * <Static Baseline Required Dependency Getter Method>
     * Returns a list of all required dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledRequiredDependencies()
    {
        return self::$_static_baseline_dependencies_required;
    }

    /**
     * <Static Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledRequiredFlags()
    {
        return self::$_static_baseline_flags_required;
    }

    /**
     * <Static Baseline Valid Parameters Getter Method>
     * Returns a list of all valid parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledValidParameters()
    {
        return self::$_static_baseline_parameters_valid;
    }

    /**
     * <Static Baseline Valid Dependencies Getter Method>
     * Returns a list of all valid dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledValidDependencies()
    {
        return self::$_static_baseline_dependencies_valid;
    }

    /**
     * <Static Baseline Valid Flags Getter Method>
     * Returns a list of all required flags.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public static function getCompiledValidFlags()
    {
        return self::$_static_baseline_flags_valid;
    }

    /**
     * <Static Baseline Static Fingerprint Getter Method>
     * Returns the underlying parent class fingerprint.
     * This value does not change during runtime.
     *
     * @return string|bool
     */
    public static function getCompiledFingerprint()
    {
        return self::_staticBaselineGetFingerprint();
    }

    /**
     * <Static Baseline Active State Child Object Tracker Method>
     * Returns an associative array of all child Baseline objects that currently
     * have active state and extend from the class implementing this trait,
     * provided they use the BaselineTrait and implement the BaselineContract.
     *
     * This trait does not retain references to the actual objects, only their
     * fingerprint and absolute classname. There is not a way to get the specific
     * object directly from this logic without some access to the actual object
     * itself. This method is only provided as a meta value to determine realtime
     * weight of child objects in existence at any given time, and validate their
     * specific fingerprints to rule out tampering or spoofing.
     *
     * @return array
     */
    public static function getActiveInstanceFingerprints()
    {
        //This prevents any pass-by-reference methods
        //from ever mutating this value externally.
        $instances = self::$_static_baseline_child_instances;
        return $instances;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Static Baseline Disable Flag Setter Method>
     * Disables public flag setting through the setFlag method,
     * as well as passing additional flags into reinitialization calls.
     * @return void
     */
    protected static function _staticBaselinePreventFlagSetting()
    {
        self::$_static_baseline_allow_flag_setting = false;
    }

    /**
     * <Static Baseline Disable Flag Setter Method>
     * Disables public flag unsetting through the unsetFlag method.
     * @return void
     */
    protected static function _staticBaselinePreventFlagUnSetting()
    {
        self::$_static_baseline_allow_flag_unsetting = false;
    }

    /**
     * <Static Baseline Parameter Persistence Setter Method>
     * Allows for the persistence of parameters when
     * reinitialization occurs to be toggled on or off.
     * @param bool $opt
     * @return void
     */
    protected static function _staticBaselineSetParameterPersistence( $opt )
    {
        self::$_static_baseline_persist_parameters = ($opt
            ? true
            : false );
    }

    /**
     * <Static Baseline Dependency Persistence Setter Method>
     * Allows for the persistence of dependencies when
     * reinitialization occurs to be toggled on or off.
     * @param bool $opt
     * @return void
     */
    protected static function _staticBaselineSetDependencyPersistence( $opt )
    {
        self::$_static_baseline_persist_dependencies = ($opt
            ? true
            : false );
    }

    /**
     * <Static Baseline Flag Persistence Setter Method>
     * Allows for the persistence of flags when
     * reinitialization occurs to be toggled on or off.
     * @param bool $opt
     * @return void
     */
    protected static function _staticBaselineSetFlagPersistence( $opt )
    {
        self::$_static_baseline_persist_flags = ($opt
            ? true
            : false );
    }

    /**
     * <Static Baseline Disable Re-Initialization Method>
     * If this method is called in a constructor,
     * initialization will only be allowed to proceed once.
     * If called at any time after initialization, it will block
     * all subsequent attempts to reinitialize.
     * @return void
     */
    protected static function _staticBaselineDisableReinitialization()
    {
        self::$_static_baseline_allow_reinitialization = false;
    }

    /**
     * <Static Baseline Enable Parameter Validation Method>
     * If called in a constructor, this will force passed parameters
     * to be validated against the internally provided specifications.
     *
     * If called at any other time, reinitialization will be affected.
     *
     * @see _baselineSetParametersValid
     * @param type $use_strict_checks (optional) If true, all dependency checks
     *     will use strict checking instead of loose checking. Default false.
     * @return void
     */
    protected static function _staticBaselineEnableParameterValidation( $use_strict_checks
    = false )
    {
        self::$_static_baseline_use_parameter_validation = true;
        self::$_static_baseline_use_parameter_validation_strict_checks = $use_strict_checks
            ? true
            : false;
    }

    /**
     * <Static Baseline Enable Dependency Validation Method>
     * If called in a constructor, this will force passed dependencies
     * to be validated against the internally provided specifications.
     *
     * If called at any other time, reinitialization will be affected.
     *
     * @see _baselineSetDependenciesValid
     * @return void
     */
    protected static function _staticBaselineEnableDependencyValidation()
    {
        self::$_static_baseline_use_dependency_validation = true;
    }

    /**
     * <Static Baseline Enable Flag Validation Method>
     * If called in a constructor, this will force passed flags
     * to be validated against the internally provided specifications.
     *
     * If called at any other time, reinitialization and the
     * setFlag method will be affected.
     *
     * @see _baselineSetFlagsValid
     * @return void
     */
    protected static function _staticBaselineEnableFlagValidation()
    {
        self::$_static_baseline_use_flag_validation = true;
    }

    /**
     * <Static Baseline Internal Initialization Method>
     * Initializes a class in the static scope, so that related configurations,
     * dependencies, flags, and settings are accessible across a range of child
     * classes. This also allows for extension logic to be immediately accessible
     * to all child classes if extensions are used.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * Stateful static initialization should only ever occur in a
     * dedicated base object that does not have a deep inheritance chain,
     * and only in very specialized cases. The functionality is not outright
     * prevented, however it does require several overrides to enable, because
     * its use in such a manner is highly discouraged to avoid introducing
     * instability in your code. Internal Oroboros logic always disables this
     * functionality, but you may roll your own classes using this trait that
     * implement it pretty easily if need be.
     *
     * This is the internal method that resolves static initialization.
     *
     * @param type $params (optional) Any parameters that need to be accessible across all child objects.
     * @param type $dependencies (optional) Any dependencies that need to be accessible across all child objects.
     * @param type $flags (optional) Any flags that need to be accessible across all child objects.
     * @return void
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    protected static function _staticBaselineInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        try
        {
            $class = get_called_class();
            self::_staticBaselineGetFingerprint();
            $class::_staticBaselineSetInitializationDefaults();
            $class::_staticBaselineCheckReinitializationAllowed();
            $class::_staticBaselineSetParameters( $class::_staticBaselineGetParameters( $params ) );
            $class::_staticBaselineSetDependencies( $class::_staticBaselineGetDependencies( $dependencies ) );
            $class::_staticBaselineSetFlags( $class::_staticBaselineGetFlags( $flags ) );
            $class::_staticBaselineRunInitializationTasks();
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s] in instance of [%s].' . PHP_EOL . \oroboros\format\StringFormatter::indent('Static initialization could not resolve due to an error:' . PHP_EOL . '%s', 4),
                    __METHOD__, get_called_class(),
                    \oroboros\format\StringFormatter::indent( $e->getMessage(),
                        4 ) ), 'initialization', $e );
        }
        self::$_static_baseline_initialized = true;
    }

    /**
     * <Child Class Registration Method>
     * Removes a child class from actively being tracked by fingerprint
     * when it is removed from memory. This method fires from the _baselineSetFingerprint
     * method of the of the BaselineTrait when the fingerprint is first set,
     * but not again after that.
     *
     * @param \oroboros\core\interfaces\contract\core\BaselineContract $object
     * A child class of the specific class implementing this trait.
     *
     * @return void
     */
    protected static function _registerChildInstance( \oroboros\core\interfaces\contract\core\BaselineContract $object )
    {
        self::_validate( 'instance-of', $object, get_called_class(), true );
        if ( !array_key_exists( $object->getFingerprint(),
                self::$_static_baseline_child_instances ) )
        {
            self::$_static_baseline_child_instances[$object->getFingerprint()] = get_class( $object );
        }
    }

    /**
     * <Child Class Registration Method>
     * Removes a child class from actively being tracked by fingerprint
     * when it is removed from memory. This method fires from the destructor
     * of the BaselineTrait.
     *
     * @param \oroboros\core\interfaces\contract\core\BaselineContract $object
     * A child class of the specific class implementing this trait.
     *
     * @return void
     */
    protected static function _unregisterChildInstance( \oroboros\core\interfaces\contract\core\BaselineContract $object )
    {
        self::_validate( 'instance-of', $object, get_called_class(), true );
        if ( array_key_exists( $object->getFingerprint(),
                self::$_static_baseline_child_instances ) )
        {
            unset( self::$_static_baseline_child_instances[$object->getFingerprint()] );
        }
    }

    /**
     * <Static Baseline Internal Initialization Check Method>
     * Returns a boolean determination as to whether the class
     * is statically initialized. This parameter is only ever set by the
     * _staticBaselineInitialize method if it has fully resolved at least
     * one time, and indicates that all internal static properties are
     * available to all child objects of the class.
     *
     * This is the internal method that always returns the correct value,
     * and cannot be overridden. The public method may be overridden for
     * external use as required, but this method is a reliable way for
     * internals to check this parameter without worry of interference.
     *
     * @return bool
     */
    protected static function _staticBaselineIsInitialized()
    {
        return self::$_static_baseline_initialized;
    }

    /**
     * <Static Baseline Static Type Context Getter Method>
     * Returns the default declared runtime class type context.
     * If the CLASS_TYPE class constant is not set on this object,
     * it will instead fall back to the static compiled class type,
     * which will in turn return either a compiled class type or false.
     *
     * @return string|bool
     */
    protected static function _staticBaselineGetCompiledType()
    {
        $const = __CLASS__ . '::' . 'OROBOROS_CLASS_TYPE';
        return defined( $const )
            ? constant( $const )
            : false;
    }

    /**
     * <Static Baseline Static Scope Context Getter Method>
     * Returns the default declared runtime class scope context.
     * If the CLASS_SCOPE class constant is not set on this object,
     * it will instead fall back to the static compiled class scope,
     * which will in turn return either a compiled class scope or false.
     *
     * @return string|bool
     */
    protected static function _staticBaselineGetCompiledScope()
    {
        $const = __CLASS__ . '::' . 'OROBOROS_CLASS_SCOPE';
        return defined( $const )
            ? constant( $const )
            : false;
    }

    /**
     * <Static Baseline Static Api Context Getter Method>
     * Returns the default declared runtime class api context.
     * If the API class constant is not set on this object,
     * it will instead fall back to the static compiled class api,
     * which will in turn return either a compiled class api or false.
     *
     * @return string|bool
     */
    protected static function _staticBaselineGetCompiledApi()
    {
        $const = __CLASS__ . '::' . 'OROBOROS_API';
        return defined( $const )
            ? constant( $const )
            : false;
    }

    /**
     * Returns the static class fingerprint.
     * One will be generated if it does not already exist.
     * @return string;
     * @final
     */
    protected static function _staticBaselineGetFingerprint()
    {
        if ( is_null( self::$_static_fingerprint ) )
        {
            self::_staticBaselineUpdateFingerprint();
        }
        return self::$_static_fingerprint;
    }

    /**
     * <Static Baseline Required Parameter Setter Method>
     * May be overridden to provide a set of parameters that
     * are required to be provided for initialization to resolve.
     *
     * Any parameters that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected static function _staticBaselineSetParametersRequired()
    {
        return array();
    }

    /**
     * <Static Baseline Valid Parameter Setter Method>
     * May be overridden to provide a validation array for provided parameters.
     * If provided, any provided parameters matching the specified keys MUST
     * resolve to the given type or instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided parameters, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected static function _staticBaselineSetParametersValid()
    {
        return array();
    }

    /**
     * <Static Baseline Default Parameter Setter Method>
     * May be overridden to provide a set of default parameters.
     * If provided, the given set will act as a baseline set of
     * templated parameters, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * parameters.
     *
     * This allows for parameters to be provided in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected static function _staticBaselineSetParametersDefault()
    {
        return array();
    }

    /**
     * <Static Baseline Initialization Parameter Setter>
     * This method is called by the internal initialization method,
     * and is the method that resolves parameter setting and checking
     * against the private internals. This method may be overridden
     * to change the way that inbound parameters are handled, or bypass
     * it entirely.
     * @param array $parameters
     * @return void
     */
    protected static function _staticBaselineSetParameters( $parameters = null )
    {
        $parameters = self::_staticBaselineGetParameters( $parameters );
        self::_staticBaselineValidateParameters( $parameters );
        self::$_static_baseline_parameters = $parameters;
    }

    /**
     * <Static Baseline Required Dependency Setter Method>
     * May be overridden to provide a set of dependencies that
     * are required to be provided for initialization to resolve.
     *
     * Any dependencies that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected static function _staticBaselineSetDependenciesRequired()
    {
        return array();
    }

    /**
     * <Static Baseline Valid Dependency Setter Method>
     * May be overridden to provide a validation array for provided dependencies.
     * If provided, any provided dependencies matching the specified keys MUST
     * resolve to the given instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided dependencies, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected static function _staticBaselineSetDependenciesValid()
    {
        return array();
    }

    /**
     * <Static Baseline Default Dependency Setter Method>
     * May be overridden to provide a set of default dependencies.
     * If provided, the given set will act as a baseline set of
     * templated dependencies, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * dependencies.
     *
     * This allows for dependencies to be injected in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected static function _staticBaselineSetDependenciesDefault()
    {
        return array();
    }

    /**
     * <Static Baseline Initialization Dependency Setter>
     * This method is called by the internal initialization method,
     * and is the method that resolves dependency injection and checking
     * against the private internals. This method may be overridden
     * to change the way that inbound dependencies are handled, or bypass
     * it entirely.
     * @param array $dependencies
     * @return void
     */
    protected static function _staticBaselineSetDependencies( $dependencies = null )
    {
        $dependencies = self::_staticBaselineGetDependencies( $dependencies );
        self::_staticBaselineValidateDependencies( $dependencies );
        self::$_static_baseline_dependencies = $dependencies;
    }

    /**
     * <Static Baseline Required Flags Setter Method>
     * May be overridden to provide a set of flags that
     * are required to be provided for initialization to resolve.
     *
     * Any flags that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected static function _staticBaselineSetFlagsRequired()
    {
        return array();
    }

    /**
     * <Static Baseline Valid Flag Setter Method>
     * May be overridden to provide a validation array for provided flags.
     * If provided, any provided flags MUST exist in the given array of
     * valid values.
     *
     * The array should be a standard numerically keyed list of valid flags.
     *
     * If a non-empty validation array is given and any flags are passed
     * that do not exist in the validation array, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected static function _staticBaselineSetFlagsValid()
    {
        return array();
    }

    /**
     * <Static Baseline Default Flag Setter Method>
     * May be overridden to provide a set of default flags.
     * If provided, the given set will act as a baseline set of
     * templated flags, and public provided parameters will
     * be recursively appended given array, and ignored if they
     * duplicate provided flags.
     *
     * This allows for flags to be provided in a way
     * that appends the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected static function _staticBaselineSetFlagsDefault()
    {
        return array();
    }

    /**
     * <Static Baseline Initialization Flag Setter>
     * This method is called by the internal initialization method,
     * and is the method that resolves flag setting and checking
     * against the private internals. This method may be overridden
     * to change the way that inbound flags are handled, or bypass
     * it entirely.
     * @param array $flags
     * @return void
     */
    protected static function _staticBaselineSetFlags( $flags = null )
    {
        $flags = self::_staticBaselineGetFlags( $flags );
        self::_staticBaselineValidateFlags( $flags );
        self::$_static_baseline_flags = $flags;
    }

    /**
     * <Static Baseline Protected Parameter Getter Method>
     * @param string $id
     * @return null|mixed
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string
     */
    protected static function _staticBaselineGetParameter( $id )
    {
        self::_validate( 'type-of', $id, 'string', true );
        if ( array_key_exists( $id, self::$_static_baseline_parameters ) )
        {
            return self::$_static_baseline_parameters[$id];
        }
        return null;
    }

    /**
     * <Static Baseline Protected Parameter Setter Method>
     * @param type $id
     * @param type $param
     * @return void
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string
     */
    protected static function _staticBaselineSetParameter( $id, $param )
    {
        self::_validate( 'type-of', $id, 'string', true );
        self::$_static_baseline_parameters[$id] = $param;
    }

    /**
     * <Static Baseline Protected Dependency Getter Method>
     * @param string $id
     * @return null|object
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string
     */
    protected static function &_staticBaselineGetDependency( $id )
    {
        self::_validate( 'type-of', $id, 'string', true );
        if ( array_key_exists( $id, self::$_static_baseline_dependencies ) )
        {
            return self::$_static_baseline_dependencies[$id];
        }
        //Must return a reference to a variable,
        //so we will return a null variable.
        $response = null;
        return $response;
    }

    /**
     * <Static Baseline Protected Dependency Setter Method>
     * @param type $id
     * @param type $dependency
     * @return void
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If id is not a string or dependency is not an object
     * @throws \oroboros\core\utilties\exception\InvalidArgumentException
     *     If override is false and the given dependency does not pass
     *     validation
     */
    protected static function _staticBaselineSetDependency( $id, $dependency,
        $override = false )
    {
        self::_validate( 'type-of', $id, 'string', true );
        self::_validate( 'type-of', $dependency, 'object', true );
        if ( !$override )
        {
            self::_staticBaselineCheckDependencyValid( $id, $dependency );
        }
        self::$_static_baseline_dependencies[$id] = $dependency;
    }

    /**
     * <Static Baseline Internal Flag Setter Method>
     * This allows internal logic to set flags while bypassing the public lock,
     * so that internal class operations are not impeded by the lock state.
     * @param scalar $flag
     * @param bool $override (optional) If set to false, the same flag
     *     validation checks will fire that do for public flag setting.
     *     Since this is an internal method, the default is true.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed.
     */
    protected static function _staticBaselineSetFlag( $flag, $override = true )
    {
        self::_staticBaselineSetFlagInternal( $flag, $override );
    }

    /**
     * <Static Baseline Internal Flag Unsetter Method>
     * This allows internal logic to remove flags while bypassing the public lock,
     * so that internal class operations are not impeded by the lock state.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed.
     */
    protected static function _staticBaselineUnsetFlag( $flag )
    {
        self::_staticBaselineUnsetFlagInternal( $flag, true );
    }

    /**
     * <Static Baseline Flag Check Method>
     * Checks if a given flag is set.
     * @param string $flag The flag to check
     * @return bool Returns true if set, false otherwise
     */
    protected static function _staticBaselineCheckFlag( $flag )
    {
        return in_array( $flag, self::$_static_baseline_flags );
    }

    /**
     * <Static Baseline Initialization Task Registration Method>
     * Adds an initialization task to the queue to run
     * after initialization completes.
     * @param string $method An accessible method from the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not valid and callable in the current scope
     */
    protected static function _staticBaselineRegisterInitializationTask( $method )
    {
        $class = get_called_class();
        self::_validate( 'type-of', $method, 'string', true );
        if ( !(method_exists( $class, $method ) && is_callable( array(
                $class,
                $method ) ) ) )
        {
            throw self::_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'valid callable method in the current scope',
                $method ) );
        }
        if ( !in_array( $method, self::$_static_baseline_initialization_tasks ) )
        {
            self::$_static_baseline_initialization_tasks[] = $method;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     *
     * Updates the static class fingerprint.
     * This method should only be called on initialization, or if global state
     * changes in a way that makes usage incompatible with the previous state.
     * It is generally suggested to keep static classes stateless for consistent
     * usage.
     */
    private static function _staticBaselineUpdateFingerprint()
    {
        $hash = sha1( microtime() . get_called_class() );
        self::$_static_fingerprint = $hash;
    }

    /**
     * <Static Baseline Initialization Internal Setup>
     * Sets the initialization default object property values,
     * calling any given overrides that have been provided, or
     * alternately providing nulled values if no overrides exist.
     * @return void
     */
    private static function _staticBaselineSetInitializationDefaults()
    {
        self::_staticBaselinePreflightFlags();
        self::_staticBaselinePreflightDependencies();
        self::_staticBaselinePreflightParameters();
    }

    /**
     * <Static Baseline Internal Parameter Settings Preflight Method>
     * Preflights initialization parameters to insure
     * that the internal class settings are correct.
     * @return void
     */
    private static function _staticBaselinePreflightParameters()
    {
        self::$_static_baseline_parameters_default = self::_staticBaselineSetParametersDefault();
        self::$_static_baseline_parameters_valid = self::_staticBaselineSetParametersValid();
        self::$_static_baseline_parameters_required = self::_staticBaselineSetParametersRequired();
        self::_baselineInternalsCheckConfiguration( 'parameter',
            self::$_static_baseline_parameters_default,
            self::$_static_baseline_parameters_required,
            self::$_static_baseline_parameters_valid,
            self::$_static_baseline_use_parameter_validation
        );
    }

    /**
     * <Static Baseline Internal Dependency Settings Preflight Method>
     * Preflights initialization dependencies to insure
     * that the internal class settings are correct.
     * @return void
     */
    private static function _staticBaselinePreflightDependencies()
    {
        self::$_static_baseline_dependencies_default = self::_staticBaselineSetDependenciesDefault();
        self::$_static_baseline_dependencies_valid = self::_staticBaselineSetDependenciesValid();
        self::$_static_baseline_dependencies_required = self::_staticBaselineSetDependenciesRequired();
        self::_baselineInternalsCheckConfiguration( 'dependency',
            self::$_static_baseline_dependencies_default,
            self::$_static_baseline_dependencies_required,
            self::$_static_baseline_dependencies_valid,
            self::$_static_baseline_use_dependency_validation
        );
    }

    /**
     * <Static Baseline Internal Flag Settings Preflight Method>
     * Preflights initialization flags to insure
     * that the internal class settings are correct.
     * @return void
     */
    private static function _staticBaselinePreflightFlags()
    {
        self::$_static_baseline_flags_default = self::_staticBaselineSetFlagsDefault();
        self::$_static_baseline_flags_valid = self::_staticBaselineSetFlagsValid();
        self::$_static_baseline_flags_required = self::_staticBaselineSetFlagsRequired();
        self::_baselineInternalsCheckConfiguration( 'flag',
            self::$_static_baseline_flags_default,
            self::$_static_baseline_flags_required,
            self::$_static_baseline_flags_valid,
            self::$_static_baseline_use_flag_validation
        );
    }

    /**
     * <Reinitialization Check Method>
     * Checks if reinitialization is allowed, and blocks subsequent
     * initialization attempts if initialization has occurred and
     * reinitializing is disallowed.
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If initialization has already occurred and reinitialization
     *     is disabled.
     */
    private static function _staticBaselineCheckReinitializationAllowed()
    {
        if ( !self::$_static_baseline_allow_reinitialization && self::$_static_baseline_initialized )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s]. Reinitialization for [%s] is not allowed.',
                    __METHOD__, get_class( $this ) ),
                'initialization-already-initialized' );
        }
    }

    /**
     * <Static Baseline Internal Flag Setter Logic>
     * Performs the task of checking the validity of a
     * given flag and setting it if allowed.
     * @param scalar $flag The flag to set
     * @param bool $override (optional) if provided, bypasses the
     *     whitelist check. The protected flag setter uses this to
     *     allow internal flag setting regardless of the public lock.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a flag does not exist in the given validation array, or
     *     if it is a non-scalar value.
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed and the override switch
     *     is not set to true.
     */
    private static function _staticBaselineSetFlagInternal( $flag,
        $override = false )
    {
        if ( !$override && !self::$_static_baseline_allow_flag_setting )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s]. Setting flags for [%s] is not allowed.',
                    __METHOD__, get_class( $this ) ), 'security-locked-resource' );
        }
        self::_staticBaselineCheckFlagValid( $flag, $override );
        //Duplicate flags are unneccessary.
        if ( in_array( $flag, self::$_static_baseline_flags ) )
        {
            return;
        }
        self::$_static_baseline_flags[] = $flag;
    }

    /**
     * <Static Baseline Internal Flag Un-Setter Logic>
     * Performs the task of checking if unsetting of flags is allowed,
     * and removing it if it is and it exists.
     * @param scalar $flag The flag to unset
     * @param bool $override (optional) if provided, bypasses the whitelist
     *     check. The protected flag setter uses this to allow internal flag
     *     setting regardless of the public lock.
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If unsetting flags is not allowed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed, or if a whitelist was defined and
     *     the flag is not in it, and override is not enabled.
     */
    private static function _staticBaselineUnsetFlagInternal( $flag,
        $override = false )
    {
        if ( !$override && !self::$_static_baseline_allow_flag_unsetting )
        {
            self::_throwException( 'runtime',
                sprintf( 'Error encountered at [%s]. Unsetting flags for [%s] is not allowed.',
                    __METHOD__, get_class( $this ) ), 'security-locked-resource' );
        }
        //Remove the flag if it exists
        if ( in_array( $flag, self::$_static_baseline_flags ) )
        {
            unset( self::$_static_baseline_flags[array_search( $flag,
                    self::$_static_baseline_flags )] );
        }
    }

    /**
     * <Static Baseline Internal Flag Validator Logic>
     * Checks that a given flag is scalar, and that it exists in the
     * whitelisted set if one was defined.
     * @param type $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-scalar flag is passed, or if a whitelist was defined and
     *     the flag is not in it, and the override switch is not enabled.
     */
    private static function _staticBaselineCheckFlagValid( $flag,
        $override = false )
    {
        self::_validate( 'scalar', $flag, null, true );
        $whitelist = self::$_static_baseline_flags_valid;
        self::_baselineInternalsCheckValid( 'any-of', $flag, $whitelist,
            $override );
    }

    /**
     * <Static Baseline Internal Parameter Validator Logic>
     * Checks that a given parameter valid.
     * @param type $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed.
     */
    private static function _staticBaselineCheckParameterValid( $key,
        $parameter, $override = false )
    {
        $whitelist = self::$_static_baseline_parameters_valid;
        if ( class_exists( $whitelist[$key] ) || interface_exists( $whitelist[$key] ) )
        {
            self::_baselineInternalsCheckValid( 'instance-of', $parameter,
                $whitelist[$key], $override );
        } elseif ( is_string( $whitelist[$key] ) && in_array( $whitelist[$key],
                self::_validationModes() ) )
        {
            self::_baselineInternalsCheckValid( $whitelist[$key], $parameter,
                true, $override );
        } else
        {
            self::_baselineInternalsCheckValid( 'type-of', $parameter,
                $whitelist[$key], $override );
        }
    }

    /**
     * <Static Baseline Internal Dependency Validator Logic>
     * Checks that a given parameter valid.
     * @param type $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid dependency is passed.
     */
    private static function _staticBaselineCheckDependencyValid( $key,
        $dependency, $override = false )
    {
        $whitelist = self::$_static_baseline_dependencies_valid;
        self::_baselineInternalsCheckValid( 'instance-of', $dependency,
            $whitelist[$key], $override );
    }

    /**
     * Validates the provided parameters to insure that they match
     * the expected values, and that all required parameters have been
     * provided.
     * @param array $parameters
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If parameter checking is enabled and an invalid parameter is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a required parameter is not given.
     */
    private static function _staticBaselineValidateParameters( $parameters )
    {
        if ( self::$_static_baseline_use_parameter_validation )
        {
            $whitelist = self::$_static_baseline_parameters_valid;
            $required = self::$_static_baseline_parameters_required;
            $invalid = array();
            $missing = array();
            //Check valid
            foreach ( $parameters as
                $name =>
                $parameter )
            {
                if ( array_key_exists( $name, $whitelist ) )
                {
                    try
                    {
                        self::_staticBaselineCheckParameterValid( $name,
                            $parameter, false );
                    } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
                    {
                        $invalid[$name] = $whitelist[$name];
                    }
                }
            }
            //Check missing
            foreach ( $required as
                $require )
            {
                if ( !array_key_exists( $require, $parameters ) )
                {
                    $missing[] = $require;
                }
            }
            if ( !empty( $invalid ) || !empty( $missing ) )
            {
                self::_baselineInternalsThrowInvalidException( $missing,
                    $invalid, 'parameters', __METHOD__ );
            }
        }
    }

    /**
     * Validates the provided dependencies to insure that they match
     * the expected values, and that all required dependencies have been
     * provided.
     * @param array $dependencies
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If dependency checking is enabled and an invalid dependency is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a required dependency is not given.
     */
    private static function _staticBaselineValidateDependencies( $dependencies )
    {
        if ( self::$_static_baseline_use_dependency_validation )
        {
            $whitelist = self::$_static_baseline_dependencies_valid;
            $required = self::$_static_baseline_dependencies_required;
            $invalid = array();
            $missing = array();
            //Check valid
            foreach ( $dependencies as
                $name =>
                $dependency )
            {
                if ( array_key_exists( $name, $whitelist ) )
                {
                    try
                    {
                        self::_staticBaselineCheckDependencyValid( $name,
                            $dependency, false );
                    } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
                    {
                        $invalid[$name] = $required[$name];
                    }
                }
            }
            //Check missing
            foreach ( $required as
                $require )
            {
                if ( !array_key_exists( $require, $dependencies ) )
                {
                    $missing[$require] = $whitelist[$require];
                }
            }
            if ( !empty( $invalid ) || !empty( $missing ) )
            {
                self::_baselineInternalsThrowInvalidException( $missing,
                    $invalid, 'dependencies', __METHOD__ );
            }
        }
    }

    /**
     * Validates the provided flags to insure that they match
     * the expected values, and that all required flags have been
     * provided.
     * @param array $flags
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If flag checking is enabled and an invalid flag is passed.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a required flag is not given.
     */
    private static function _staticBaselineValidateFlags( $flags )
    {
        if ( self::$_static_baseline_use_flag_validation )
        {
            $whitelist = self::$_static_baseline_flags_valid;
            $required = self::$_static_baseline_flags_required;
            $invalid = array();
            $missing = array();
            //Check valid
            foreach ( $flags as
                $flag )
            {
                try
                {
                    self::_staticBaselineCheckFlagValid( $flag );
                } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
                {
                    $invalid[] = $flag;
                }
                if ( !self::$_static_baseline_allow_flag_setting )
                {
                    self::_throwException( 'runtime',
                        sprintf( 'Error encountered at [%s]. Setting flags for [%s] is not allowed.',
                            __METHOD__, get_class( $this ) ),
                        'security-locked-resource' );
                }
            }
            //Check required
            foreach ( $required as
                $require )
            {
                if ( !in_array( $require, $flags ) )
                {
                    $missing[] = $require;
                }
            }
            if ( !empty( $invalid ) || !empty( $missing ) )
            {
                self::_baselineInternalsThrowInvalidException( $missing,
                    $invalid, 'flags', __METHOD__ );
            }
        }
    }

    /**
     * Returns the merged set of provided dependencies and defaults.
     * @param type $dependencies
     * @return type
     */
    private static function _staticBaselineGetDependencies( $dependencies = null )
    {
        return self::_baselineInternalsMergeArrayDefaults( $dependencies,
                self::$_static_baseline_dependencies_default );
    }

    /**
     * Returns the merged set of provided flags and defaults.
     * @param array $flags
     * @return array
     */
    private static function _staticBaselineGetFlags( $flags = null )
    {
        return self::_baselineInternalsMergeArrayDefaults( $flags,
                self::$_static_baseline_flags_default );
    }

    /**
     * Returns the merged set of provided parameters and defaults,
     * if a mergeable set is provided.
     * @param mixed $params
     * @return mixed
     */
    private static function _staticBaselineGetParameters( $params = null )
    {
        return self::_baselineInternalsMergeArrayDefaults( $params,
                self::$_static_baseline_parameters_default );
    }

    /**
     * Runs any registered initialization tasks
     * immediately prior to initialization.
     * @return void
     */
    private static function _staticBaselineRunInitializationTasks()
    {
        $class = get_called_class();
        foreach ( self::$_static_baseline_initialization_tasks as
            $initialization_task )
        {
            if ( method_exists( $class, $initialization_task ) && is_callable( array(
                    $class,
                    $initialization_task ) ) )
            {
                $class::$initialization_task();
            }
        }
    }

}
