<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core\context;

/**
 * <Oroboros Core Context Trait>
 * This trait provides the basis for core internal contexts, and designates
 * that the category and subcategory should be inherited from the api
 * interface of the concrete class.
 *
 * Extensions and 3rd parties may leverage this functionality without overrides
 * by setting a valid api interface on their concrete classes that outlines
 * their packages API_CODEX and API_SCOPE class constant values, which will
 * be used as the context category and subcategory respectively.
 *
 * This will also enable automatic contextual matching in the codex
 * for their package contexts.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\abstracts\core\context\JsonSerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\SerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\ContextContract
 */
trait CoreContextTrait
{

    use JsonSerialContextTrait
    {
        JsonSerialContextTrait::initialize as private json_initialize;
    }

    /**
     * <Core Context Initialization>
     * Declares the category and subcategory automatically for any class
     * that implements a valid api interface, or declares one as its class
     * constant OROBOROS_API.
     *
     * The category will be derived from the standard API_CODEX class constant
     * declaration in the api interface if it exists.
     * If this value cannot be found it will be declared as null.
     * If the category has already been defined, this operation will be skipped.
     *
     * The subcategory will be derived from the standard API_SCOPE class constant
     * declaration in the api interface if it exists.
     * If this value cannot be found it will be declared as null.
     * If the category has already been defined, this operation will be skipped.
     *
     * Classes may also directly declare these constants within the class to
     * propagate through to the context category and subcategory.
     *
     * class context context objects.
     * @param type $params (optional) Not used.
     * @param type $dependencies (optional) Not used.
     * @param type $flags (optional) Not used.
     * @return void
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $class = get_called_class();
        if ( is_null( $this->_category ) )
        {
            $this->_setCategory( $this->_coreContextSearchGivenApiValue( $class,
                    'API_CODEX' ) );
        }
        if ( is_null( $this->_subcategory ) )
        {
            $this->_setSubCategory( $this->_coreContextSearchGivenApiValue( $class,
                    'API_SCOPE' ) );
        }
        $this->json_initialize( $params, $dependencies, $flags );
    }

    /**
     * Evaluates that a given class constant exists and returns its value,
     * or false if it does not exist.
     * @param bool|string $subject A valid class name or interface name, or false
     * @param string $key The name of the class constant to evaluate
     * @return null|scalar
     * @internal
     */
    private function _coreContextEvaluateApiConstant( $subject, $key )
    {
        if ( $subject
            && ( class_exists( $subject, true )
            || interface_exists( $subject, true )
            )
        )
        {
            $expected = $subject . '::' . $key;
            if ( $expected && defined( $expected ) && constant( $expected ) )
            {
                return constant( $expected );
            }
        }
        return false;
    }

    /**
     * Searches first in the provided subject for a given class constant,
     * and then in the declared OROBOROS_API for the same class constant
     * if it is a valid interface and contains the given constant.
     *
     * If the value is found in either of these locations, it will be returned.
     * If neither yield the expected class constant key, null will be returned.
     *
     * @param type $subject The class to search
     * @param type $key The key name of the class constant
     *     to pull the value from.
     * @return null|scalar
     * @internal
     */
    private function _coreContextSearchGivenApiValue( $subject, $key )
    {
        $check = $this->_coreContextEvaluateApiConstant( $subject, $key );
        if ( $check )
        {
            return $check;
        }
        $loose_api = 'OROBOROS_API';
        $api = $this->_coreContextEvaluateApiConstant( $subject, $loose_api );
        if ( $api )
        {
            $secondary = $this->_coreContextEvaluateApiConstant( $api, $key );
            if ( $secondary )
            {
                return $secondary;
            }
        }
        return null;
    }

}
