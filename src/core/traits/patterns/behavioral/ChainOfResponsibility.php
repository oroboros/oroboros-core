<?php

/**
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @copyright (c) 2014, Brian Dayhoff all rights reserved.
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * Chain of Responsibility design pattern generic implementation
 * @version 0.1.1 alpha
 * @category traits
 * @category design patterns
 * @category behaviorial patterns
 */
trait ChainOfResponsibility {
    //trait variables

    /**
     * The registry of commands to check requests against
     * @var array $_commands
     */
    private $_chainOfResponsibilityCommands = array();

    //trait methods

    /**
     * Registers a new command into the Chain of Responsibility
     * @param string $command : The name of the command to register into the chain of responsibility
     * @return boolean : True if command is successfully registered, false if already registered
     * @throws \ErrorException
     */
    public function addChainOfResponsibilityCommand($command, $object) {
        if (!in_array($command, $this->_chainOfResponsibilityCommands)) {
            $this->_chainOfResponsibilityCommands[] = $command;
            return TRUE;
        } else {
            throw new \oroboros\lib\exception\LibraryException('WARNING: Chain of responsibility command already registered for: ' . $command, self::_EXCEPTION_CODE_GENERAL_LOGIC_ERROR);
            return FALSE;
        }
    }

    /**
     * Checks a request against the chain of responsibility
     * @param string $name : The name of the request to pass along the chain of responsibility
     * @param mixed $args : The parameter(s) to pass into the request method
     * @return mixed : \ErrorException : Request not found error OR request response, if present
     */
    public function runChainOfResponsibilityCommand($name, $args) {
        foreach ($this->_chainOfResponsibilityCommands as $command) {
            if (!is_null($this->$command->onChainOfResponsibilityRequest($name, $args))) {
                $response = $this->$command->onChainOfResponsibilityRequest($name, $args);
                return $response;
            }
        }
        return new \ErrorException('WARNING: Method not found for request: ' . $name);
    }

}
