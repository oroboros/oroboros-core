<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * <Manager Pattern>
 * A manager is a more advanced Director. Where a standard Director is capable
 * of aggregating a category of work amongst workers, a Manager is capable of
 * taking in dependency injection, settings, and flags, and also delegating the
 * creation of workers to a Factory or Prototyper of workers for its pool within
 * its category of work if they are not provided. Most of the higher level apis
 * of various libraries in Oroboros Core occur from the interaction
 * with a Manager, which presents a standardized approach to creating an object
 * that will fulfill a desired result.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category patterns
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\BaselineContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract
 */
trait ManagerTrait
{

    /**
     * The Director functionality is provided by the Director trait.
     */
    use DirectorTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Manager Constructor>
     * This is the standard library constructor in Oroboros Core.
     * It presents the opportunity to inject dependencies, settings,
     * and flags to modify behavior throughout its entire category of work.
     *
     * This constructor is a proxy to the initialize method, which does the
     * actual effort of initializing. These methods are separated so that
     * re-initialization can occur on the same object without the weight of
     * re-instantiation, which allows objects to be recycled or reset more
     * performantly than creating a new object.
     *
     * @param type $params (optional) An array of settings to pass to the
     *     manager to determine its operation setup
     * @param type $dependencies (optional) An array of dependencies to inject
     * @param type $flags (optional) An array of flags to pass to the manager
     *     to determine modifiers to execution
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if
     *     any parameter is provided that does not validate
     * @since 0.2.5
     */
    public function __construct( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->initialize( $params, $dependencies, $flags );
    }

    /**
     * <Manager Initialization>
     * This method performs the actual initialization of the object.
     * By default, this is called during instantiation, but may be called again
     * at any time to reset initialization state. This allows an instantiated
     * object to be templated out as a base instance and populated with settings
     * post-instantiation as needed, making it compatible with prototypical object
     * creation approaches.
     *
     * @param type $params (optional) An array of settings to pass to the
     *     manager to determine its operation setup
     * @param type $dependencies (optional) An array of dependencies to inject
     * @param type $flags (optional) An array of flags to pass to the manager
     *     to determine modifiers to execution
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     if any parameter is provided that does not validate
     * @throws \oroboros\core\utilities\exception\RuntimeException if any
     *     dependency is created that cannot resolve
     * @since 0.2.5
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        try
        {
            $this->_baselineRegisterInitializationTask( '_managerPostInitialization' );
            $this->_baselineInitialize( $params, $dependencies, $flags );
        } catch ( \InvalidArgumentException $e )
        {
            throw $this->_getException( 'invalid-argument-exception',
                'core-pattern-failure',
                array(
                $e->getMessage()
                ), $e );
        } catch ( \Exception $e )
        {
            throw $this->_getException( 'runtime-exception',
                'core-pattern-failure',
                array(
                $e->getMessage()
                ), $e );
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * This method MUST be overridden to set the Director
     * category so that the manager can receive workers.
     * This method cannot make assumptions about the director
     * category automatically, so it must be set manually.
     *
     * Within this method, the method $this->_setDirectorCategory( $category )
     * should be called to set a valid category for the Director.
     * This method enforces that this is done during instantiation of
     * the object.
     * @return void
     */
    abstract protected function _managerSetDirectorCategory();

    /**
     * The default behavior of this method is to use the class CLASS_TYPE
     * class constant as the worker category, and ignore the worker category
     * if the object does not have the CLASS_TYPE class constant. This is not
     * a metric for absolute consistency, as there are cases where the
     * CLASS_TYPE is not an appropriate measure of the category,
     * in which cases this method will be overridden to avoid the need
     * for excessive enumeration. 3rd party workers should provide either
     * a valid Enumerated Interface of their own CLASS_TYPES, or provide
     * their own system for tracking worker categories.
     *
     * This method MUST be overridden to set the Worker category so that the
     * manager can be registered as a Worker to other Directors if you want a
     * different category than the class type.
     *
     * Within this method, the method $this->_setWorkerCategory( $category )
     * should be called to set a valid category for Worker registration.
     * This method enforces that this is done during instantiation
     * of the object.
     * @return void
     */
    protected function _managerSetWorkerCategory()
    {
        $class = get_called_class();
        $const = $class . '::OROBOROS_CLASS_TYPE';
        if ( defined( $const ) )
        {
            $this->_setWorkerScope( $this::OROBOROS_CLASS_TYPE );
        }
    }

    /**
     * The default behavior of this method is to set the worker scope to the
     * CLASS_SCOPE class constant if it exists, and ignore the worker scope if the
     * CLASS_SCOPE class constant does not exist. 3rd party workers should provide
     * a valid Enumerated Interface of their own class scopes, or provide their
     * own system for tracking class scope if they do not want to do that.
     *
     * This method MUST be overridden to set the Worker scope so that Directors
     * receiving this object can determine which body of work they cover within
     * their category if you want a different category from the class scope.
     *
     * Within this method, the method $this->_setWorkerScope( $scope )
     * should be called to set a valid scope for worker task delegation.
     * This method enforces that this is done during instantiation of the object.
     * @return void
     */
    protected function _managerSetWorkerScope()
    {
        $class = get_called_class();
        $const = $class . '::OROBOROS_CLASS_SCOPE';
        if ( defined( $const ) )
        {
            $this->_setWorkerScope( $this::OROBOROS_CLASS_SCOPE );
        }
    }

    /**
     * <Manager Dependency Registration Method>
     * This method will receive the final array of passing dependencies
     * prior to initialization completing.
     *
     * This gives implementing classes and traits an opportunity to
     * perform any last-minute adjustments to internal logic based
     * on the resulting set of dependencies just before initialization
     * resolves.
     *
     * @param array &$dependencies
     * @return void
     */
    protected function _managerRegisterDependencies( &$dependencies )
    {
        //no-op
    }

    /**
     * <Dependencies Getter>
     * Returns an array of all dependencies.
     * @return array
     */
    protected function _getDependencies()
    {
        return $this->_baseline_dependencies;
    }

    /**
     * <Setting Check>
     * Checks if a given setting is exactly the given value.
     * Returns true if so, and false if not or if the key
     * does not exist.
     * @param scalar $setting
     * @param mixed $value
     * @return bool
     */
    protected function _checkSetting( $setting, $value )
    {
        return ( is_scalar( $setting )
            && array_key_exists( $setting, $this->_baseline_parameters )
            && $this->_baseline_parameters[$setting] === $value);
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Runs the post-initialization operations for the manager.
     * @return void
     */
    private function _managerPostInitialization()
    {
        $this->_managerSetDirectorCategory();
        $class = get_class( $this );
        $this->_setWorkerCategory( defined( $class . '::OROBOROS_CLASS_TYPE' )
                ? $class::OROBOROS_CLASS_TYPE
                : false  );
        $this->_setWorkerScope( defined( $class . '::OROBOROS_CLASS_SCOPE' )
                ? $class::OROBOROS_CLASS_SCOPE
                : false  );
        $this->_managerRegisterDependencies( $this->_baseline_dependencies );
    }

}
