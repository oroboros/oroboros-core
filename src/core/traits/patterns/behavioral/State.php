<?php
/**
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @copyright (c) 2014, Brian Dayhoff all rights reserved.
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * <State Trait>
 * @version 0.1.1
 * @category traits
 * @category design patterns
 */
trait State {

    private $_state;
    private $_valid_states = [];

    protected function _registerState($state) {
        if (!is_string($state)) {
            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (!in_array($state, $this->_valid_states)) {
            $this->_valid_states[] = $state;
            return TRUE;
        }
        return FALSE;
    }

    protected function _registerStates($states) {
        if (!is_array($states) && !is_object($states)) {
            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        array_map(function($state) {
            $this->_registerState($state);
        }, ((is_object($states)) ? get_object_vars($states) : $states));
    }

    protected function _checkState() {
        return $this->_state;
    }

    protected function _checkValidStates() {
        return $this->_valid_states;
    }

    protected function _updateState($state) {
        if (!is_string($state)) {
            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (!in_array($state, $this->_valid_states)) {
            throw new \oroboros\lib\exception\LibraryException('Invalid state: ' . $state . ' specified at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        $this->_state = $state;
    }

    abstract protected function _onStateChange();
}
