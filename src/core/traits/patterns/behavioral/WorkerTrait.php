<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * <Worker Pattern>
 * Workers are straightforward, focused objects concerned with accomplishing a
 * very narrow body of work. They are organized under a Director that aggregates
 * the correct task to the correct worker in a broader scope. Where a Director
 * might represent parsing data, a worker would represent parsing a specific
 * format of data. The Director knows which worker to assign the task to, and
 * the worker knows how to complete the task, and neither is aware of the
 * operations of the other. This separation keeps their relation both highly
 * cohesive and loosely coupled.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category patterns
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\BaselineContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 */
trait WorkerTrait
{

    use \oroboros\core\traits\core\BaselineTrait;

    /**
     * Represents the category of work that the worker is related to.
     * If this does not match the director's assigned category, then
     * the worker will not be considered a valid member of its pool.
     *
     * For ease of reference in oroboros core, these are defined in
     * enumerated interfaces to insure consistency,
     * typically as class scopes.
     *
     * @var string
     */
    private $_worker_category;

    /**
     * Represents the specific body of work the worker is intended to do
     * within a broader category of work.
     * @var string
     */
    private $_worker_scope;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Worker Scope Getter>
     * Allows a Director to check which scope of work the worker can cover
     * within its category. This determines which tasks the Director
     * will delegate to the worker. Workers with no scope will not be retained,
     * as the Director has no way of knowing which area they cover and will not
     * retain objects that cannot perform work.
     * @return string|bool
     */
    public function getWorkerScope()
    {
        if ( is_null( $this->_worker_scope ) )
        {
            return false;
        }
        return $this->_worker_scope;
    }

    /**
     * <Worker Category Getter>
     * Allows a Director to check the category of the Worker to see if
     * it is a match for its broader category of work. Workers with no category
     * or a mismatched category will be refused by Directors.
     * @return string|bool
     */
    public function getWorkerCategory()
    {
        if ( is_null( $this->_worker_category ) )
        {
            return false;
        }
        return $this->_worker_category;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Worker Category Setter>
     * Internally used to set the category for the worker.
     * This should be called in the constructor or initialization method
     * of implementing classes or traits.
     * @param string $category
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a value is passed that is not a string
     */
    protected function _setWorkerCategory( $category )
    {
        if ( !is_string( $category ) && $category !== false )
        {
            throw $this->_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'string|false',
                gettype( $category ) ) );
        }
        $this->_worker_category = $category;
    }

    /**
     * <Worker Scope Setter>
     * Internally used to set the scope for the worker.
     * This should be called in the constructor or initialization method
     * of implementing classes or traits.
     * @param string $category
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a value is passed that is not a string
     */
    protected function _setWorkerScope( $scope )
    {
        if ( !is_string( $scope ) && $scope !== false )
        {
            throw $this->_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'string|false',
                gettype( $scope ) ) );
        }
        $this->_worker_scope = $scope;
    }

}
