<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\concurrency;

/**
 * <Object Pool Trait>
 * This trait provides a way for managing multiple requests
 * against a pool of instantiated instances that can handle it.
 *
 * It is useful for load balancing, multiplexing, and
 * asynchronous calls that can be honored by multiple endpoints,
 * and can also be used as an intentional throttle on available instances
 * (eg: for limiting how many group sessions can be in a single lobby,
 * how many members can be in one group, etc).
 *
 * Implementing this strategy into production code allows for easy scalability.
 * This should be factored into the core architecture for maximum forward portability.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait ObjectPoolTrait {
    //@todo
    //@note this will be implemented for the 0.5+ release. For now it just needs to be in place.
}
