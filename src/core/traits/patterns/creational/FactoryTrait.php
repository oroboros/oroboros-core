<?php

/**
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @copyright (c) 2014, Brian Dayhoff all rights reserved.
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\creational;

/**
 * <Factory Trait>
 * Provides default factory loading capabilities to classes that implement it.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait FactoryTrait {

    private $_factory;
    private $_namespace;
    private $_path;

    protected function _initializeFactory() {
        $this->_factory = new \oroboros\patterns\creational\FactoryFactory();
        $this->_factory->setNamespace('\\oroboros', 'src');
        if (!is_null($this->_namespace)) {
            $this->_restoreCustomFactoryNamespace();
        }
    }

    protected function _setFactoryNamespace($namespace, $path) {
        $this->_factory->setNamespace($namespace, $path);
        $this->_namespace = $namespace;
        $this->_path = $path;
    }

    protected function _setFactoryDefaultNamespace() {
        $this->_factory->setNamespace('\\oroboros', 'src');
    }

    protected function _load($type, $class, array $args = array(), array $flags = array()) {
        $this->_factory->load('factory', ucwords(strtolower($type)) . 'Factory');
        $this->_factory->initialize($args, $flags);
        $factory = $this->_factory->instance();
        $factory->setNamespace($this->_factory->getNamespace(), $this->_factory->getPath());
        $factory->load(strtolower($type), $class);
        $factory->initialize();
        return $factory->instance();
    }

    private function _restoreCustomFactoryNamespace() {
        $this->_factory->setNamespace($this->_namespace, $this->_path);
    }
}
