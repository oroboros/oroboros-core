<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\creational;

/**
 * <PrototyperTrait Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage traits
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
trait PrototyperTrait
{

    //This is what we are using to index classes
    use \oroboros\core\traits\patterns\behavioral\RegistryTrait;

    private $_default_contract = '\\oroboros\\core\\interfaces\\contracts\\patterns\\creational\\PrototypicalContract';
    private $_current_contract = '\\oroboros\\core\\interfaces\\contracts\\patterns\\creational\\PrototypicalContract';

    /**
     * SETTINGS ARE NOT YET IMPLEMENTED
     * @var array
     */
    private $_default_settings = array(
        //if false, will halt any requested prototype operation
        'enabled' => true,
        //if true, will not accept additional prototypes.
        //This can be used for managed dependency injection.
        'read-only' => false
    );
    private $_reserved_keys = array(
        'prototypes',
        'settings'
    );
    //whether or not we have initialized.
    private $_is_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * Prototyper public initialization method.
     * Exposes the interal initialization publicly
     * in a way where the core process cannot be overridden.
     * @param type $params
     * @param type $flags
     * @return void
     */
    public function initialize( $params = array(), $flags = array() )
    {
        $this->_init( $params, $flags );
    }

    /**
     * Prevents direct access to prototypes or settings,
     * but otherwise performs like the standard setter.
     * @param type $id
     * @param type $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException [$id] is settings or prototypes
     */
    public function set( $id, $value )
    {
        $this->_autoInit();
        if ( in_array( $id, $this->_reserved_keys ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Cannot request reserved key [%s] in [%s].', $id,
                __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_setRegistryValue( 'prototypes.' . $id, $value );
    }

    /**
     * Prevents direct access to prototypes or settings,
     * but otherwise performs like the standard getter.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException [$id] is settings or prototypes
     * @param type $id
     */
    public function get( $id )
    {
        $this->_autoInit();
        if ( in_array( $id, $this->_reserved_keys ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Cannot request reserved key [%s] in [%s].', $id,
                __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_getRegistryValue( 'prototypes', $id );
    }

    /**
     * Gets an instance of a prototypical object, if this object has a template for it.
     * @param string $type A fully qualified classname
     * @return bool|object returns false if the object did not exist, otherwise returns an instance of the object
     * @throws \oroboros\core\utilities\exception\LogicException if the requested class failed it's clone operation. You've got a bug if you see this.
     */
    public function getPrototype( $type )
    {
        $this->_autoInit();
        return $this->_instantiateNewPrototypeObject( $type );
    }

    /**
     * Returns a new instance of a prototyped object
     * @param type $class An instance of $this->_current_contract
     * @return object
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if given a non-prototypical class, or parameter that cannot resolve into a class
     * @throws \oroboros\core\utilities\exception\UnexpectedValueException if the given object already exists in the prototype registry.
     */
    public function setPrototype( $class )
    {
        if ( !(is_object( $class ) || ( is_string( $class ) && class_exists( $class ) ) ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string|object', gettype( $class ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_autoInit();
        if ( is_string( $class ) )
        {
            $class = new $class();
        }
        $id = get_class( $class );
        if ( in_array( $id, $this->_reserved_keys ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Cannot request reserved key [%s] in [%s].', $id,
                __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_registerNewPrototypeObject( $class );
    }

    /**
     * unsets an existing prototype
     * @param string $class A fully qualified classname (use get_class($class))
     * @return bool true if the object was unset, false if it was not set
     */
    public function unSetPrototype( $class )
    {
        $this->_autoInit();
        $id = ( is_object( $class ) )
            ? get_class( $class )
            : $class;
        if ( in_array( $id, $this->_reserved_keys ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Cannot request reserved key [%s] in [%s].', $id,
                __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_unRegisterNewPrototypeObject( $class );
    }

    /**
     * Checks if the given class object can be prototyped by this object.
     * @param object $class
     * @return bool
     */
    public function checkIfPrototypical( $class )
    {
        $this->_autoInit();
        return $this->_checkIfPrototypical( $class );
    }

    /**
     * Checks if the specified class is registered as a prototype
     * @param string $class a fully qualified classname
     * @return bool true if has an instance of the class, false if not
     */
    public function checkPrototype( $class )
    {
        $this->_autoInit();
        return $this->_checkIfPrototypeExists( $class );
    }

    /**
     * Returns a list of all currently registered prototypes
     * @return array
     */
    public function listPrototypes()
    {
        $this->_autoInit();
        return $this->_listPrototypes();
    }

    /**
     * Resets the prototype registry to empty
     * @return type
     */
    public function reset()
    {
        $this->_autoInit();
        return $this->_reset();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Returns a list of all currently registered prototypes
     * @return array
     */
    protected function _listPrototypes()
    {
        $this->_autoInit();
        $prototypes = array_keys( $this->_getRegistryValue( 'prototypes' ) );
        foreach ( $prototypes as
            &$prototype )
        {
            $prototype = '\\' . ltrim( $prototype, '\\' );
        }
        return $prototypes;
    }

    /**
     * Clears the prototype registry entirely.
     * @return void
     */
    protected function _reset()
    {
        $this->_autoInit();
        $this->_unsetRegistryIndex( 'prototypes' );
        $this->_setRegistryValue( 'prototypes', array() );
    }

    /**
     * Checks if the specified class is registered as a prototype
     * @param string $class a fully qualified classname
     * @return bool true if has an instance of the class, false if not
     */
    protected function _checkIfPrototypeExists( $class )
    {
        $this->_autoInit();
        if ( is_object( $class ) )
        {
            $class = get_class( $class );
        }
        return array_key_exists( $class,
            $this->_getRegistryValue( 'prototypes' ) );
    }

    /**
     * Checks if the provided class is prototypical, by comparing it to the
     * supplied class constant PROTOTYPICAL_INTERFACE in the current scope.
     * uses late static binding to accomplish this in a way that is compatible with
     * PHP 5.4+
     *
     * @note When all versions of php that are supported are 7.0+, this will be converted to use the static language construct for further releases.
     *
     * @param type $class
     * @return bool
     */
    protected function _checkIfPrototypical( $class )
    {
        $this->_autoInit();
        $current_class = get_called_class();
        $expected = '\\oroboros\\core\\interfaces\\contract\\patterns\\creational\\PrototypicalContract';
        //if this is not defined for some reason, cannot proceed.
        //should be non-blocking, so prototyping can be disabled easily by default.
        if ( !$expected )
        {
            return false;
        }
        return ($class instanceof $expected);
    }

    /**
     * Registers a new template object into the prototype registry.
     * @param string|object $class If a string, must be a valid class that can be instantiated and has the defined prototypical interface. If an object, must implement the specified prototypical interface
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if given a non-prototypical class, or parameter that cannot resolve into a class
     * @throws \oroboros\core\utilities\exception\UnexpectedValueException if the given object already exists in the prototype registry.
     * @return void
     */
    protected function _registerNewPrototypeObject( $class )
    {
        $this->_autoInit();
        if ( !is_object( $class ) && !is_string( $class ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string|class', gettype( $class ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( !$this->_checkIfPrototypical( $class ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PATTERN_FAILURE_MESSAGE,
                __METHOD__,
                '[Object ' . (is_object( $class )
                    ? get_class( $class )
                    : ( is_array( $class )
                        ? 'array(' . implode( ', ', $class ) . ')'
                        : (string) $class) ) . ' is not an instance of required prototypical interface: ' . $this->_current_contract . ']' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PATTERN_FAILURE_PROTOTYPE
            );
        }

        $prototype = $this->_getRegistryValue( 'prototypes', $class );
        if ( !empty( $prototype ) )
        {
            $msg = 'Error encountered at [%s]. Instance of [%s] already exists in [%s].';
            throw new \oroboros\core\utilities\exception\UnexpectedValueException(
            sprintf( $msg, __METHOD__, get_class( $class ), get_class( $this ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_SETTER_FAILURE );
        }
        //the prototype is not registered. This is what we want here.
        if ( is_string( $class ) )
        {
            $class = new $class();
        }
        $this->_setRegistryValue( 'prototypes.' . get_class( $class ), $class, 1 );
    }

    /**
     * Unregisters an existing prototype object
     * @param type $class
     * @return bool True if exists and unset, false if not known
     */
    protected function _unRegisterNewPrototypeObject( $class )
    {
        $this->_autoInit();
        if ( is_object( $class ) )
        {
            $class = get_class( $class );
        }
        if ( !is_string( $class ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $class ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $prototypes = $this->_getRegistryValue( 'prototypes' );
        if ( array_key_exists( $class, $prototypes ) )
        {
            $this->_unsetRegistryIndex( 'prototypes.' . $class );
            return true;
        }
        return false;
    }

    /**
     * Creates a new prototype instance and returns it.
     * Returns false if the requested instance does not exist in the prototype registry.
     * @param type $class
     * @return object|bool Returns false if a non-existant object was requested, otherwise returns a clone of the requested object.
     * @throws \oroboros\core\utilities\exception\LogicException if the requested class failed it's clone operation. You've got a bug if you see this.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the passed parameter is not a string.
     */
    protected function _instantiateNewPrototypeObject( $class )
    {
        $this->_autoInit();
        if ( !is_string( $class ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $class ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        try
        {
            $prototype = $this->_getRegistryValue( 'prototypes.' . $class );
            return clone $prototype;
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            //no instance of the requested class.
            return false;
        } catch ( \Exception $e )
        {
            throw new \oroboros\core\utilities\exception\LogicException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_CLONE_FAILURE_MESSAGE,
                __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC
            );
        }
    }

    /**
     * <Prototypical Instance Setter>
     * This allows you to define a custom extension of
     * the standard PrototypicalContract to track.
     * The standard prototypical contract is the following:
     * @see \oroboros\core\interfaces\contract\patterns\creational\PrototypicalContract
     * @param string $contract a child interface of the above mentioned contract interface
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the passed parameters are not valid
     * @return void
     */
    protected function _setPrototypical( $contract )
    {
        $this->_autoInit();
        if ( !is_string( $contract ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_PARAMETERS_MESSAGE,
                'string: instanceof ' . $this->_getDefaultPrototypical(),
                gettype( $contract ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }

        if ( !( $new_contract instanceof $this->_default_contract ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_WRONG_INSTANCE_SUPPLIED_MESSAGE,
                $this->_default_contract, $new_contract ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }

        $this->_setCurrentPrototypicalInstance( $contract );
    }

    /**
     * <Prototypical Instance Getter>
     * @return string
     */
    protected function _getPrototypical()
    {
        $this->_autoInit();
        return $this->_getCurrentPrototypicalInstance();
    }

    /**
     * <Default Prototypical Instance Getter>
     * @return string
     */
    protected function _getDefaultPrototypical()
    {
        $this->_autoInit();
        return $this->_getDefaultPrototypicalInstance();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Private Initialization Method>
     * This is the real initialization method.
     * It insures that core initialization logic is not overridden,
     * and backreference always points to this trait on execution.
     * @param type $params
     * @param type $flags
     */
    private function _init( $params = array(), $flags = array() )
    {
        $this->_registryInitialize();
        $this->_setRegistryValue( 'prototypes', array() );
        $this->_setRegistryValue( 'settings', $this->_default_settings );
        $this->_is_initialized = true;
    }

    /**
     * <Private Default Prototypical Getter>
     * This is the getter interacted with internally,
     * which is called from the protected method: _getDefaultPrototypical
     * @return string
     */
    private function _getDefaultPrototypicalInstance()
    {
        $this->_autoInit();
        return $this->_default_contract;
    }

    /**
     * <Private Prototypical Getter>
     * This is the getter interacted with internally,
     * which is called from the protected method: _getPrototypical
     * @return string
     */
    private function _getCurrentPrototypicalInstance()
    {
        $this->_autoInit();
        return $this->_current_contract;
    }

    /**
     * <Private Prototypical Setter>
     * This is the setter interacted with internally,
     * which is called from the protected method: _setPrototypical
     * @param string $new_contract This should be an instance of the default contract. This is validated above in the protected method, which is the only method to call this one, so it cannot be excluded from child logic and reach this point.
     * @return void
     */
    private function _setCurrentPrototypicalInstance( $new_contract )
    {
        $this->_autoInit();
        $this->_current_contract = $new_contract;
    }

    private function _autoInit()
    {
        if ( !$this->_is_initialized )
        {
            $this->initialize();
        }
    }

}
