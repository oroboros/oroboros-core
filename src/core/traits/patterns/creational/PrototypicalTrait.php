<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\creational;

/**
 * <Prototypical Trait>
 * Provides methods to honor the PrototypicalInterface
 * @see \oroboros\core\interfaces\contracts\patterns\creational\PrototypicalContract
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait PrototypicalTrait
{

    private $_prototype_operations = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\creational\PrototypicalContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Prototypical Clone Method>
     * Runs a queue of internally defined clone operation methods if any
     * have been registered before allowing the clone to resolve fully.
     * This allows implementing classes to queue or unqueue clone operations
     * without overriding the clone method.
     * @see _registerPrototypeTask To add a method, pass the method name into this function
     * @see _unregisterPrototypeOperation To remove a method, pass the method name into this function
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException If any exceptions are encountered during the clone operation.
     */
    public function __clone()
    {
        $this->_prototypicalRunPrototypeOperations();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Clone Operation Setter Method>
     * Allows the class to add a queue of methods that automatically
     * run when the object is cloned. Methods may only be queued to run
     * one time per clone operation.
     * @param string $method The name of a callable method in the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid method is passed
     */
    protected function _registerPrototypeTask( $method )
    {
        $this->_prototypicalAddPrototypeOperation( $method );
    }

    /**
     * <Clone Operation Unsetter Method>
     * Allows the class to remove a queued method that is set
     * to run on clone operations.
     * @param string $method The name of a callable method in the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid method is passed
     */
    protected function _unregisterPrototypeOperation( $method )
    {
        $this->_prototypicalRemovePrototypeOperation( $method );
    }

    /**
     * <Clone Operation Queue List Getter Method>
     * Returns the existing array of queued prototype operations
     * that will fire when the object is cloned.
     * @return array
     */
    protected function _getPrototypeOperationQueue()
    {
        return $this->_prototype_operations;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Adds a new clone operation method to the queue.
     * @param string $method The name of a callable method in the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid method is passed
     */
    private function _prototypicalAddPrototypeOperation( $method )
    {
        if ( !is_string( $method ) && !method_exists( $this, $method ) && is_callable( array(
                $this,
                $method ) ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'callable method in the current scope', $method ),
            \oroboros\core\interfaces\enumerated\exception\DesignPatternExceptionCodes::ERROR_PATTERN_FAILURE_PROTOTYPE,
            $e );
        }
        if ( !in_array( $method, $this->_prototype_operations ) )
        {
            $this->_prototype_operations[] = $method;
        }
    }

    /**
     * Removes a queued clone operation method.
     * @param string $method The name of a callable method in the current scope
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid method is passed
     * @internal
     */
    private function _prototypicalRemovePrototypeOperation( $method )
    {
        if ( !is_string( $method ) && !method_exists( $this, $method ) && is_callable( array(
                $this,
                $method ) ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'callable method in the current scope', $method ),
            \oroboros\core\interfaces\enumerated\exception\DesignPatternExceptionCodes::ERROR_PATTERN_FAILURE_PROTOTYPE,
            $e );
        }
        if ( in_array( $method ) )
        {
            unset( $this->_prototype_operations[array_search( $method,
                    $this->_prototype_operations )] );
        }
    }

    /**
     * Runs the queue of clone operations when prototyping occurs.
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException If any exceptions are encountered during the clone operation.
     * @internal
     */
    private function _prototypicalRunPrototypeOperations()
    {
        foreach ( $this->_prototype_operations as
            $method )
        {
            try
            {
                $this->$method();
            } catch ( \Exception $e )
            {
                throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PATTERN_FAILURE_MESSAGE,
                    __TRAIT__, $e->getMessage() ),
                \oroboros\core\interfaces\enumerated\exception\DesignPatternExceptionCodes::ERROR_PATTERN_FAILURE_PROTOTYPE,
                $e );
            }
        }
    }

}
