<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\structural;

/**
 * <Control Api Trait>
 * Provides an easy means of creating self documenting control apis.
 * This trait affects instantiated objects.
 * For static control methods, use StaticControlApi.
 * This functionality is separated into two traits
 * to maintain  object organization.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @see \oroboros\traits\patterns\structural\StaticControlApi
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\libraries\patterns\structural\ControlApiContract
 */
trait ControlApiTrait
{

    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;
    use \oroboros\core\traits\utilities\core\StringUtilityTrait;
    use \oroboros\core\traits\core\BaselineTrait;

    private $_control_methods = array();
    private $_api = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the control api contract
     *
     * @satisfies \oroboros\core\interfaces\contract\libraries\patterns\structural\ControlApiContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Api Index Method>
     * This method acts as a means of checking the dynamically generated api at runtime.
     *
     * This method MUST return false if there are no known matches for the provided api.
     *
     * This method MUST be able to return case-insensitive api results in a dash-separated format.
     *
     * This method MUST return [false] if [$index] is null and [$action] is not.
     * This prevents possible ambiguity when two or more indexes have the same
     * method declared.
     *
     * Example:
     *   - function exampleTestMethod() exists under index "example" and is represented as "test-method" in the api.
     *
     *   - function ExampleTestMethod() follows identical criteria to the above.
     *
     *   - function _exampleTestMethod() follows identical criteria to the above.
     *
     *   - function _ExampleTestMethod() follows identical criteria to the above.
     *
     * In this case, calling $object->api() would return:
     *
     * array{
     *     "example" => array{
     *         "test-method" => array( ... )
     *     }
     * }
     *
     * Calling $object->api("example") would return:
     *
     * array{
     *     "test-method" => array ( ... )
     * }
     *
     * And calling $object->api("example", "test-method") would return
     * only the detail array for the test method.
     *
     * Calling $object->api("test-method") would return [false],
     * because "test-method" is not a valid api index.
     *
     * Api arrays for valid actions MUST contain all of the following keys:
     *
     * "params"
     *   What parameters the function takes if any, or false if none.
     *   the params array should have a key that corresponds to the param name
     *   without the dollar sign, and the type or types it represents.
     *   If there are multiple, it MAY be represented by the keyword "mixed"
     *   or list individual parameter types separated by pipes.
     *
     * "returns"
     *   What to expect the method to return, or "void" if nothing is returned.
     *   If multiple return formats are possible, it MAY be represented by the
     *   keyword "mixed" or list individual parameter types separated by pipes.
     *
     * "throws"
     *   Any exceptions that are thrown by the method, or false if no
     *   exceptions are thrown. This MUST anticipate if further stack levels
     *   will throw exceptions within it's logic that are not caught by the method.
     *   If one or more exceptions are thrown, this should be an associative array
     *   with keys corresponding to the exception names, and values corresponding
     *   to the reason they are thrown, or an empty string if no reason is supplied.
     *   If an exception can be thrown for multiple reasons, then it should return
     *   a numerically keyed array of reasons for that exception represented as
     *   strings, or empty strings if no reason is provided.
     *
     * @param string $index (optional) If provided, returned api will be scoped to the provided set, if it exists.
     * @param type $action (optional) If provided, returned api will be scoped to the provided action, if it exists.
     * @return array|boolean Returns false if the specified parameters do not exist, otherwise returns an array of methods within the provided scope.
     */
    public function api( $index = null, $action = null )
    {

    }

    /**
     * <Api Check Method>
     * Checks if a supplied index or method of an index exists.
     *
     * This method MUST NOT throw an exception under any circumstances.
     *
     * This method MUST return true if the index exists and $action is null,
     * or if $action is supplied and it exists within the supplied index.
     *
     * This method MUST return false if the specified params do not exist,
     * or if any invalid parameters are passed.
     *
     * @param string $index (optional) If provided, searches only in the scope of the provided index.
     * @param string $action (optional) If provided, searches only for the provided api method.
     * @return bool
     */
    public function check( $index, $action = null )
    {

    }

    /**
     * <Control Api Extension Method>
     * This method allows the ControlApi to be extended with a nested ControlApi,
     * which will allow the base object to provide the api of sub objects as if
     * it were part of it's own. All ControlApiExtensionContract instances are
     * also ControlApis, which makes this pretty simple to do.
     *
     * Objects passed through the extend method are always ControlApis.
     * The object receiving the extension must add its api to its own,
     * and have a means of designating which api methods internally are
     * representative of other control apis and which are locally accessible.
     * The public provided api does not reveal this information, however the
     * internal selector should be aware of it, and not waste a ton of resources
     * trying all possibilities before reaching the correct method.
     *
     * Extension apis MUST receive their own index. They MUST NOT be mixed
     * with non-extension indexes. Commands that pass through to the existing
     * api of the extension should be prefixed with this index key, and the
     * key should be stripped before passing the subsequent key to the nested
     * api.
     *
     * If this causes an illegal collision within the logic of the ControlApi,
     * it MUST throw an InvalidArgumentException, with an explanation of what
     * the collision entails.
     *
     * @param string $index
     * @param \oroboros\core\interfaces\contract\patterns\structural\ControlApiExtensionContract $extension
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public function extend( $index,
        \oroboros\core\interfaces\contract\pattern\structural\ControlApiExtensionContract $extension )
    {

    }

    /**
     * <Index Registration>
     * Allows external registration of an Api index. This is most useful to decorators.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external population, or if an attempt is made to
     * override an existing method and the implementing class does not allow this.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * retain the passed index.
     *
     * The implementing object MAY refuse to accept external registration if indexes,
     * and MAY simply throw an InvalidArgumentException any time this method is called.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public function registerIndex( $index )
    {

    }

    /**
     * <Index Unregistration>
     * Allows external unregistration of an existing Api index.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external unregistration, or if an attempt is made to
     * override an existing method and the implementing class does not allow this.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * remove the passed index and that index did actually exist in its indexes.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public function unregisterIndex( $index )
    {

    }

    /**
     * <Method Registration>
     * Allows external method registration for a specified index. This is most
     * useful to decorators.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST take any callable parameter for $method.
     *
     * This method MUST NOT accept a callable parameter that is not enclosed
     * in it's own scope. For this purpose, the following things are considered
     * to be enclosed in it's scope correctly:
     *
     * - A method the class has.
     * - A closure passed as a variable.
     * - An array containing an instantiated object and a public method.
     * - An invokeable object that is already instantiated.
     *
     * The following are NOT considered to be enclosed in it's scope,
     * and should raise an exception:
     *
     * - A string representing a function name.
     * - A string representing a static method that does not exist
     *   in this object, or comes from a class that this object is
     *   not an instance of.
     * - An array containing a method name and a string representation
     *   of a class that this object is not an instance of.
     *
     * These MUST NOT be accepted. This is to prevent ambiguity
     * and sphaghetti code-ish practices.
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external population, or if an attempt is made to
     * override an existing method and the implementing class does not allow this.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * retain the passed index.
     *
     * The implementing object MAY refuse to accept external registration if indexes,
     * and MAY simply throw an InvalidArgumentException any time this method is called.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @param callable $method
     * @param callable $callable (optional) If $method is not a callable, this MUST be a callable to represent the key
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public function registerMethod( $index, $method, $callable = null )
    {

    }

    /**
     * <Method Unregistration>
     * Allows external unregistration of an existing api method for a specified index.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external unregistration, or if an attempt is made to
     * override an existing method and the implementing class does not allow this.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * remove the passed index method and that index method did actually exist
     * in its specified index.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index The name of the index to unregister the method from
     * @param string method The name of the method to unregister
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public function unregisterMethod( $index, $method )
    {

    }

    /**
     * <Api Call Magic Method>
     * Allows api indexes to have their methods called,
     * even if no passthrough was registered for that api.
     *
     * The __call method for the ControlApi is tasked with executing
     * canonicalized methods,and resolving their names to a set of
     * internal methods that exist for the object.
     *
     * The [$name] parameter MUST correspond to an existing api index.
     *
     * An InvalidArgumentException MUST be raised if the [$name]
     * parameter is not a valid index.
     *
     * If a method exits named [$name], or an object has been supplied
     * representing that index and that object has that method name,
     * then this method MUST return a call to that method passing $args
     * as its only parameter.
     *
     * If no method representing a passthrough exists and $args is empty,
     * this method MUST raise an InvalidArgumentException.
     *
     * If no method representing a passthrough exists and $args is not empty,
     * it's first parameter should be anticipated to be the correct canonicalized
     * method name.
     *
     * If the above is true and the parameter is not a string,
     * this method MUST raise an InvalidArgumentException.
     *
     * If the first key of $args is a string, it must reverse canonicalize
     * to a valid method registered for a registered index and method.
     * If such a method exists and no passthrough for the index exists,
     * it MUST be called passing args as it's only argument, WITHOUT the
     * key representing the method name being passed in that array. If such a
     * method exists and a passthrough for that index DOES exist,
     * the return value for this method should be the result of the passthrough
     * for that method MUST be called instead, providing the first key of $args
     * as it's first parameter, and the rest of $args without that key as it's
     * second parameter.
     *
     * This method MUST wrap method calls in a try catch block, and anticipate
     * the possibility of an exception being thrown. If the exception is an
     * instance of InvalidArgumentException, then that exception MUST be
     * rethrown as is. If ANY OTHER exception is thrown, It must raise a
     * RuntimeException and be passed as the third parameter of that exception.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $name The api index name
     * @param array $args The api index arguments
     * @throws \InvalidArgumentException if any invalid parameter is passed.
     * @throws \RuntimeException if the referenced method does exist but raises an exception other than an InvalidArgumentException.
     * @return mixed
     */
    public function __call( $name, $args = array() )
    {

    }

    /**
     * <Api Index Method>
     * Provides an api of control methods for quick useage documentation.
     * @param string $index (optional) If provided, returns the api within the scope of the specified control method. Otherwise returns an array of all registered control methods and their individual apis
     * @param string $method (optional) If provided, constrains the scope of the specified control method to the specific action. If not provided, returns the default action. Has no effect if [$index] is not provided
     * @return array|false Returns false if no api exists as specified, otherwise returns an array of information
     */
//    public function api($index = null, $action = null) {
//        if (!isset($index) && !isset($action)) {
//            //return the full api
//            $api = array();
//            foreach ($this->_control_methods as $control => $params) {
//                $methods = preg_grep('/^_' . $control . '/', get_class_methods($this));
//                foreach ($methods as $method) {
//                    $id = $this->_reverseParseControlMethodName($control, $method);
//                    $api[$id] = $this->_getControlMethodApi($method);
//                }
//            }
//        } elseif (!array_key_exists($index, $this->_control_methods)) {
//            //unknown control method
//            return false;
//        } elseif (isset($index) && !isset($action)) {
//            //return the api for the specified control method
//            $api = array();
//            $methods = preg_grep('/^_' . $index . '/', get_class_methods($this));
//            foreach ($methods as $method) {
//                    $id = $this->_reverseParseControlMethodName($index, $method);
//                    $api[$id] = $this->_getControlMethodApi($method);
//                }
//        } else {
//            //get the specific api, if it exists
//            $api = $this->_getControlMethodApi(
//                    $this->_constructControlWorkerMethodName(
//                            $index, $action
//                    )
//            );
//        }
//        return $api;
//    }

    /**
     * Provides the control method api when methods are called directly.
     * @param string $name
     * @param string $args
     * @param ... This method can recieve an arbitrary number of arguments
     * @throws \oroboros\lib\exception\LibraryException if no control method is found to handle the request
     * @return mixed
     */
//    public function __call($name, $args = array()) {
//        if (!array_key_exists($name, $this->_control_methods)) {
//            throw new \oroboros\lib\exception\LibraryException(
//            'unknown method: [' . $name . ']. Call api() to retrieve '
//            . 'valid control methods and their subset of actions. '
//            . 'Thrown at: ' . __LINE__ . ' of ' . __TRAIT__, \oroboros\interfaces\utility\ExceptionCodeInterface::EXCEPTION_CODE_BAD_PARAMETERS);
//        }
//        $method = $this->_constructControlWorkerMethodName($name, ((is_null($args) || ( is_array($args) && empty($args))) ? null : ((is_string($args)) ? $args : ((is_array($args)) ? array_shift($args) : null) ))
//        );
//        if (!method_exists($this, $method)) {
//            throw new \oroboros\lib\exception\LibraryException(
//            'unknown method: [' . $method . ']. in instance of [' . get_class($this) . ']. '
//            . 'Thrown at: ' . __LINE__ . ' of ' . __TRAIT__, \oroboros\interfaces\utility\ExceptionCodeInterface::EXCEPTION_CODE_NOT_IMPLEMENTED
//            );
//        }
//        $command = (is_array($args) ? array_shift($args) : null);
//        $arguments = (is_array($args) ? array_shift($args) : null);
//        $flags = (is_array($args) ? array_shift($args) : null);
//        return $this->_runControlMethod($method, $command, $arguments, $flags, $args, ((is_array($args)) ? count($args) : 0));
//    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Control Method Registration>
     * @param type $method
     * @return boolean
     * @throws \oroboros\lib\exception\LibraryException
     */
    protected function _registerControlMethodSet( $method )
    {
        if ( !is_string( $method ) || !preg_match( '/[a-z]/', $method ) )
        {
            //invalid string parameter
            //Explicit checking instead of typecasting
            //is used for backwards compatibility
            throw new \oroboros\lib\exception\LibraryException(
            'Invalid control method identifier. '
            . 'Thrown at: ' . __LINE__ . ' of ' . __TRAIT__,
            \oroboros\interfaces\utility\ExceptionCodeInterface::EXCEPTION_CODE_BAD_PARAMETERS );
        }
        if ( !method_exists( $this,
                $this->_constructControlWorkerMethodName( $method ) ) )
        {
            //default method does not exist. Cannot register subset.
            throw new \oroboros\lib\exception\LibraryException(
            'Cannot register [' . $method . '] because the default method [_'
            . $method . '()] does not exist in the scope of class: ['
            . get_class( $this ) . ']. '
            . 'Thrown at: ' . __LINE__ . ' of ' . __TRAIT__,
            \oroboros\interfaces\utility\ExceptionCodeInterface::EXCEPTION_CODE_NOT_IMPLEMENTED );
        }
        echo 'Registering [' . $method . ']';
        if ( !in_array( $method, $this->_control_methods ) )
        {
            $this->_control_methods[$method] = array();
        }
        return true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    private function _loadMethod( $name, $method = null, $args = null,
        $flags = null )
    {
        $worker = '_' . strtolower( $name )
            . str_replace( ' ', null,
                ucwords(
                    str_replace( '-', ' ', strtolower( $method ) ) ) );
        return call_user_func_array( $this->_control_methods[$worker], $args );
    }

    private function _constructControlWorkerMethodName( $control, $method = null )
    {
        if ( !isset( $method ) )
        {
            //return the default control method
            return '_' . strtolower( $control );
        } else
        {
            return '_' . strtolower( $control )
                . str_replace( ' ', null,
                    ucwords(
                        str_replace( '-', ' ', strtolower( $method ) ) ) );
        }
    }

    private function _runControlMethod( $method, $command, $arguments, $flags,
        $args, $arg_count )
    {
        //we have to do it this way to avoid overriding
        //default params that may be set in worker methods
        if ( is_null( $command ) && is_null( $arguments ) && is_null( $flags ) )
        {
            return $this->$method();
        } elseif ( !is_null( $command ) && is_null( $arguments ) && is_null( $flags ) )
        {
            return $this->$method( $command );
        } elseif ( !is_null( $command ) && !is_null( $arguments ) && is_null( $flags ) )
        {
            return $this->$method( $command, $arguments );
        } elseif ( !is_null( $command ) && !is_null( $arguments ) && !is_null( $flags ) &&
            $arg_count === 0 )
        {
            return $this->$method( $command, $arguments, $flags );
        } else
        {
            return $this->$method( $command, $arguments, $flags, $args );
        }
    }

    private function _getControlMethodApi( $method )
    {
        if ( !method_exists( $this, $method ) )
        {
            return 'Method does not exist in this scope.';
        }
        $reflector = new \ReflectionMethod( $this, $method );
        $docblock = $reflector->getDocComment();
        if ( !$docblock )
        {
            return 'No api provided.';
        }
        $explode = explode( PHP_EOL, $docblock );
        foreach ( $explode as
            $key =>
            $line )
        {
            $explode[$key] = $line = ltrim( $line, '/* ' );
            if ( $line == '' )
            {
                unset( $explode[$key] );
            }
        }
        return implode( PHP_EOL, $explode );
    }

    private function _reverseParseControlMethodName( $index, $method = null )
    {
        $raw = str_replace( $this->_constructControlWorkerMethodName( $index ),
            null, $method );
        if ( $raw === '' )
        {
            return '__default';
        }
        return strtolower( implode( '-', $this->_splitAtUpperCase( $raw ) ) );
    }

    private function _splitAtUpperCase( $s )
    {
        return preg_split( '/(?=[A-Z])/', $s, -1, PREG_SPLIT_NO_EMPTY );
    }

}
