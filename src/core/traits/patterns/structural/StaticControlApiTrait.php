<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\structural;

/**
 * <Static Control Api Trait>
 * Provides an easy means of creating static control apis. Static control apis
 * are classes that provide subsets of statically accessible methods dynamically.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\libraries\patterns\structural\StaticControlApiContract
 */
trait StaticControlApiTrait
{

    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;
    use \oroboros\core\traits\utilities\core\StringUtilityTrait;
    use \oroboros\core\traits\core\StaticBaselineTrait
    {
        \oroboros\core\traits\core\StaticBaselineTrait::staticInitialize as private baseline_staticInitialize;
    }

    /**
     * Represents the indexes of the static control api
     * @var array
     */
    private static $_static_control_api_indexes = array();

    /**
     * Represents a cache of the static control api parsed api methods
     * @var \oroboros\collection\Collection
     */
    private static $_static_control_api_cache;

    /**
     * Represents whether the api method cache has been compiled or not.
     * This will prevent the overhead of redundant calls to cache compiling
     * @var bool
     */
    private static $_static_control_api_cache_compiled = false;

    /**
     * Represents a blacklist of methods that fit a given index
     * but will be ignored by the public control api.
     * @var array
     */
    private static $_static_control_api_method_blacklist = array();

    /**
     * Represents a cache of the static control api commands,
     * and the methods they belong to.
     * @var array
     */
    private static $_static_control_api_method_cache = array();

    /**
     * Represents the indexes of the static control api
     * that have been manually passed in.
     * @var array
     */
    private static $_static_control_api_customizations = array();

    /**
     * Represents extensions to the static control api
     * @var array
     */
    private static $_static_control_api_extensions = array();

    /**
     * Represents reserved keys that MUST NOT be allowed
     * to be declared or the class will break.
     * @var array
     */
    private static $_static_control_api_reserved_indexes = array(
        'api',
        'register',
        'unregister',
        'check'
    );

    /**
     *
     * @var \oroboros\core\libraries\par
     */
    private static $_static_control_api_parser;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the static control api contract
     *
     * @satisfies \oroboros\core\interfaces\contract\libraries\patterns\structural\StaticControlApiContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Api Index Method>
     * This method acts as a means of checking the dynamically generated api at runtime.
     *
     * This method MUST return false if there are no known matches for the provided api.
     *
     * This method MUST be able to return case-insensitive api results in a dash-separated format.
     *
     * This method MUST return [false] if [$index] is null and [$action] is not.
     * This prevents possible ambiguity when two or more indexes have the same
     * method declared.
     *
     * Example:
     *   - function exampleTestMethod() exists under index "example" and is represented as "test-method" in the api.
     *
     *   - function ExampleTestMethod() follows identical criteria to the above.
     *
     *   - function _exampleTestMethod() follows identical criteria to the above.
     *
     *   - function _ExampleTestMethod() follows identical criteria to the above.
     *
     * In this case, calling $object->api() would return:
     *
     * array{
     *     "example" => array{
     *         "test-method" => array( ... )
     *     }
     * }
     *
     * Calling $object->api("example") would return:
     *
     * array{
     *     "test-method" => array ( ... )
     * }
     *
     * And calling $object->api("example", "test-method") would return
     * only the detail array for the test method.
     *
     * Calling $object->api("test-method") would return [false],
     * because "test-method" is not a valid api index.
     *
     * Api arrays for valid actions MUST contain all of the following keys:
     *
     * "params"
     *   What parameters the function takes if any, or false if none.
     *   the params array should have a key that corresponds to the param name
     *   without the dollar sign, and the type or types it represents.
     *   If there are multiple, it MAY be represented by the keyword "mixed"
     *   or list individual parameter types separated by pipes.
     *
     * "returns"
     *   What to expect the method to return, or "void" if nothing is returned.
     *   If multiple return formats are possible, it MAY be represented by the
     *   keyword "mixed" or list individual parameter types separated by pipes.
     *
     * "throws"
     *   Any exceptions that are thrown by the method, or false if no
     *   exceptions are thrown. This MUST anticipate if further stack levels
     *   will throw exceptions within it's logic that are not caught by the method.
     *   If one or more exceptions are thrown, this should be an associative array
     *   with keys corresponding to the exception names, and values corresponding
     *   to the reason they are thrown, or an empty string if no reason is supplied.
     *   If an exception can be thrown for multiple reasons, then it should return
     *   a numerically keyed array of reasons for that exception represented as
     *   strings, or empty strings if no reason is provided.
     *
     * @param string $index (optional) If provided, returned api will be
     *     scoped to the provided set, if it exists.
     * @param type $action (optional) If provided, returned api will be
     *     scoped to the provided action, if it exists.
     * @return array|boolean Returns false if the specified parameters
     *     do not exist, otherwise returns an array of methods within
     *     the provided scope.
     */
    public static function api( $index = null, $action = null )
    {
        $class = get_called_class();
        return $class::_getStaticControlApi( $index, $action );
    }

    /**
     * <Api Check Method>
     * Checks if a supplied index or method of an index exists.
     *
     * This method MUST NOT throw an exception under any circumstances.
     *
     * This method MUST return true if the index exists and $action is null,
     * or if $action is supplied and it exists within the supplied index.
     *
     * This method MUST return false if the specified params do not exist,
     * or if any invalid parameters are passed.
     *
     * @param string $index (optional) If provided, searches only in the
     *     scope of the provided index.
     * @param string $action (optional) If provided, searches only for
     *     the provided api method.
     * @return bool
     */
    public static function check( $index = null, $action = null )
    {
        $class = get_called_class();
        return $class::_checkStaticControlApiCommand( $index, $action );
    }

    /**
     * <Control Api Extension Method>
     * This method allows the ControlApi to be extended with a nested ControlApi,
     * which will allow the base object to provide the api of sub objects as if
     * it were part of it's own. All StaticControlApiExtensionContract instances are
     * also StaticControlApis, which makes this pretty simple to do.
     *
     * Objects passed through the extend method are always StaticControlApis.
     * The object receiving the extension must add its api to its own,
     * and have a means of designating which api methods internally are
     * representative of other control apis and which are locally accessible.
     * The public provided api does not reveal this information, however the
     * internal selector should be aware of it, and not waste a ton of resources
     * trying all possibilities before reaching the correct method.
     *
     * Extension apis MUST receive their own index. They MUST NOT be mixed
     * with non-extension indexes. Commands that pass through to the existing
     * api of the extension should be prefixed with this index key, and the
     * key should be stripped before passing the subsequent key to the nested
     * api.
     *
     * If this causes an illegal collision within the logic of the StaticControlApi,
     * it MUST throw an InvalidArgumentException, with an explanation of what
     * the collision entails. This will most generally occur when attempting to
     * override an existing index, which is not allowed in the case of static
     * control apis.
     *
     * @param \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiExtensionContract $extension
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public static function extend( $extension )
    {
        $class = get_called_class();
        $class::_registerStaticControlApiExtension( $extension );
    }

    /**
     * <Index Registration>
     * Allows external registration of an Api index. This is most useful to decorators.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external population, or if an attempt is made to
     * override an existing method and the implementing class does not allow this.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * retain the passed index.
     *
     * The implementing object MAY refuse to accept external registration of indexes,
     * and MUST NOT override existing indexes.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public static function registerIndex( $index )
    {
        $class = get_called_class();
        $class::_registerStaticControlApiIndex( $index );
    }

    /**
     * <Method Registration>
     * Allows external method registration for a specified index. This is most
     * useful to decorators.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST take any callable parameter for $method.
     *
     * This method MUST NOT accept a callable parameter that is not enclosed
     * in it's own scope. For this purpose, the following things are considered
     * to be enclosed in it's scope correctly:
     *
     * - A method the class has.
     * - A closure passed as a variable.
     * - An array containing an instantiated object and a public method.
     * - An invokeable object that is already instantiated.
     *
     * The following are NOT considered to be enclosed in it's scope,
     * and should raise an exception:
     *
     * - A string representing a function name.
     * - A string representing a static method that does not exist
     *   in this object, or comes from a class that this object is
     *   not an instance of.
     * - An array containing a method name and a string representation
     *   of a class that this object is not an instance of.
     *
     * These MUST NOT be accepted. This is to prevent ambiguity
     * and sphaghetti code-ish practices.
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external population.
     *
     * This method MUST raise an InvalidArgumentException on any attempt
     * to override an existing method.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * retain the passed index.
     *
     * The implementing object MAY refuse to accept external registration if indexes,
     * and MAY simply throw an InvalidArgumentException any time this method is called.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @param string|callable $method
     * @param callable $callable (optional) If $method is not a callable,
     *     this MUST be a callable to represent the key
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public static function registerMethod( $index, $method, $callable = null )
    {
        $class = get_called_class();
        $class::_registerStaticControlApiIndexMethod( $index, $method, $callable );
    }

    /**
     * <Api Call Magic Method>
     * Allows api indexes to have their methods called,
     * even if no passthrough was registered for that api.
     *
     * The __call method for the ControlApi is tasked with executing
     * canonicalized methods,and resolving their names to a set of
     * internal methods that exist for the object.
     *
     * The [$name] parameter MUST correspond to an existing api index.
     *
     * An InvalidArgumentException MUST be raised if the [$name]
     * parameter is not a valid index.
     *
     * If a method exits named [$name], or an object has been supplied
     * representing that index and that object has that method name,
     * then this method MUST return a call to that method passing $args
     * as its only parameter.
     *
     * If no method representing a passthrough exists and $args is empty,
     * this method MUST raise an InvalidArgumentException.
     *
     * If no method representing a passthrough exists and $args is not empty,
     * it's first parameter should be anticipated to be the correct canonicalized
     * method name.
     *
     * If the above is true and the parameter is not a string,
     * this method MUST raise an InvalidArgumentException.
     *
     * If the first key of $args is a string, it must reverse canonicalize
     * to a valid method registered for a registered index and method.
     * If such a method exists and no passthrough for the index exists,
     * it MUST be called passing args as it's only argument, WITHOUT the
     * key representing the method name being passed in that array. If such a
     * method exists and a passthrough for that index DOES exist,
     * the return value for this method should be the result of the passthrough
     * for that method MUST be called instead, providing the first key of $args
     * as it's first parameter, and the rest of $args without that key as it's
     * second parameter.
     *
     * This method MUST wrap method calls in a try catch block, and anticipate
     * the possibility of an exception being thrown. If the exception is an
     * instance of InvalidArgumentException, then that exception MUST be
     * rethrown as is. If ANY OTHER exception is thrown, It must raise a
     * RuntimeException and be passed as the third parameter of that exception.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $name The api index name
     * @param array $args The api index arguments
     * @throws \InvalidArgumentException if any invalid parameter is passed.
     * @throws \RuntimeException if the referenced method does exist but raises
     *     an exception other than an InvalidArgumentException.
     * @return mixed
     */
    public static function __callStatic( $name, $args = array() )
    {
        $class = get_called_class();
        return $class::_callStaticControlApiCommand( $name, $args );
    }

    /**
     * <Static Control Api Initialization Method>
     * Initializes a class in the static scope, so that related configurations,
     * dependencies, flags, and settings are accessible across a range of child
     * classes. This also allows for extension logic to be immediately accessible
     * to all child classes if extensions are used.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * Stateful static initialization should only ever occur in a
     * dedicated base object that does not have a deep inheritance chain,
     * and only in very specialized cases. The functionality is not outright
     * prevented, however it does require several overrides to enable, because
     * its use in such a manner is highly discouraged to avoid introducing
     * instability in your code. Internal Oroboros logic always disables this
     * functionality, but you may roll your own classes using this trait that
     * implement it pretty easily if need be.
     *
     * @param type $params (optional) Any parameters that need to be accessible
     *     across all child objects.
     * @param type $dependencies (optional) Any dependencies that need to be
     *     accessible across all child objects.
     * @param type $flags (optional) Any flags that need to be accessible
     *     across all child objects.
     * @return void
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        self::baseline_staticInitialize( $params, $dependencies, $flags );
        return get_called_class();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Api Getter>
     * Fetches the existing api for the class.
     * Can be scoped to a specific index or method of an index.
     * Returns false if nothing exists in the defined scope.
     * If no parameters are passed and this returns false,
     * the class has an empty api.
     *
     * If an api exists, this method will return a collection
     * scoped to the specified parameters.
     *
     * @param string $index
     * @param string $action
     * @return bool|\oroboros\collection\Collection
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid argument is passed.
     */
    protected static function _getStaticControlApi( $index = null,
        $action = null )
    {
        if ( !is_null( $index ) )
        {
            self::_validate( 'stringable', $index, null, true );
        }
        if ( !is_null( $action ) )
        {
            self::_validate( 'stringable', $index, null, true );
        }
        if ( !self::$_static_control_api_cache_compiled )
        {
            self::_staticControlApiInitializeApiCache();
        }
        $full_api = json_decode( json_encode( self::$_static_control_api_cache ),
            1 );
        if ( empty( $full_api ) )
        {
            //If there are no methods, don't
            //bother crunching the check logic.
            return false;
        }
        if ( is_null( $index ) )
        {
            return new \oroboros\collection\Collection( $full_api );
        }
        if ( !array_key_exists( $index, $full_api ) )
        {
            return false;
        }
        $full_api = $full_api[$index];
//        d($full_api, $action, debug_backtrace(1));
        if ( is_null( $action ) )
        {
            return new \oroboros\collection\Collection( $full_api );
        }
        if ( !array_key_exists( $action, $full_api ) )
        {
            return false;
        }
//        d($full_api[$action]);
        return new \oroboros\collection\Collection( $full_api[$action] );
    }

    /**
     * <Api Index Setter>
     * Defines a new api index. The index MUST NOT already be defined, and MUST
     * be a value that can be cast to an alphanumeric lower-cased string with
     * words separated by single hyphens. Underscores, spaces, prefixing or
     * suffixing hyphens, or double hyphens will all cause this to fail.
     *
     * @param mixed $index Any value that can be cast to a
     *     correct canonicalized string
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid index or one that is already defined is passed
     */
    protected static function _registerStaticControlApiIndex( $index )
    {
        $index = strtolower( $index );
        self::_staticControlApiValidateIndex( $index );
        self::$_static_control_api_indexes[$index] = self::_getStaticControlApiCommandsForIndex( $index );
    }

    /**
     * <Api Method Setter>
     * Registers an arbitrary handler for a static control api command.
     * The command cannot already exist as declared.
     * @param type $index The index to place the method under.
     *     It must already exist.
     * @param type $method A method name callable from the same class,
     *     or a canonicalized keyword if the third parameter is supplied.
     * @param callable $callable (optional) Must be provided if $method is not
     *     a callable method in the top level class.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    protected static function _registerStaticControlApiIndexMethod( $index,
        $method, callable $callable = null )
    {
        self::_staticControlApiValidateControlApiIndexMethod( $index, $method,
            $callable );
        $class = get_called_class();
        if ( !array_key_exists( $index,
                self::$_static_control_api_customizations ) && array_key_exists( $index,
                self::$_static_control_api_indexes ) )
        {
            //The index key needs to be ported from the regular indexes
            self::$_static_control_api_customizations[$index] = array();
        }
        if ( is_null( $callable ) )
        {
            $callable = $class . '::' . $method;
        }
        $method = self::_canonicalizeStaticControlApiMethod( $index . str_replace( ' ',
                    null, ucwords( str_replace( '-', ' ', $method ) ) ), $index );
        self::_staticControlApiValidateNotAlreadyExisting( $method );
        self::$_static_control_api_customizations[$index][$method] = $callable;
        self::_staticControlApiUpdateMethodCache( $index,
            array(
            $method => $callable ) );
    }

    /**
     * <Static Control Api Blacklist Method>
     * Adds a specific method to the blacklist, so it will not turn up in the
     * public api. This must be done prior to its registration into the
     * public api, or the call will be ignored.
     * @param string $method The method to blacklist
     * @return void
     */
    protected static function _blacklistStaticControlApiMethod( $method )
    {
        self::_staticControlApiBlacklistMethod( $method );
    }

    /**
     * <Api Extension Setter>
     * Registers an entire extension subset. None of the extension's methods
     * can be already declared in the existing static control api.
     * @param string $index the index key to identify the extension by
     * @param \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiExtensionContract $extension
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If any of the extension's methods are already declared in
     *     this api scope.
     */
    protected static function _registerStaticControlApiExtension( $extension )
    {
        self::_validate( 'instance-of', $extension,
            '\\oroboros\\core\\interfaces\\contract\\patterns\\structural\\StaticControlApiContract',
            true );
        $index = get_class( $extension );
        if ( array_key_exists( $index, self::$_static_control_api_extensions ) )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error detected at [%s]. Cannot register extension '
                    . 'under key [%s] because it already exists in [%s].',
                    __METHOD__, $index, get_called_class() ),
                self::_getExceptionCode( 'php-bad-method-call' ) );
        }
        if ( !$extension::isStaticInitialized() )
        {
            $extension::staticInitialize();
        }
        $class = get_called_class();
        $ext_api = $extension::api();
        if ( $ext_api )
        {
            foreach ( $extension::api() as
                $extension_index =>
                $methods )
            {
                if ( !array_key_exists( $extension_index,
                        self::$_static_control_api_indexes )
                    && !array_key_exists( $extension_index,
                        self::$_static_control_api_customizations )
                )
                {
                    //this index does not already exist, so it is safe.
                    continue;
                }
                foreach ( $methods as
                    $method =>
                    $details )
                {
                    if ( $class::check( $extension_index, $method ) )
                    {
                        self::_throwException( 'invalid-argument-exception',
                            sprintf( 'Error detected at [%s]. Cannot register extension '
                                . 'under key [%s] because it contains methods that already '
                                . 'exist in [%s], and would cause an ambiguous reference.',
                                __METHOD__, $extension_index, get_called_class() ),
                            self::_getExceptionCode( 'logic-ambiguous-reference' ) );
                    }
                }
            }
        }
        self::$_static_control_api_extensions[$index] = $extension;
    }

    /**
     * <Api Caller>
     * This function handles calling the correct static control api method.
     * @param type $command
     * @param type $args
     * @return mixed
     * @throws \oroboros\core\utilities\exception\BadMethodCallException
     *     If an invalid method is referenced.
     * @throws \Exception Any exception type may be raised depending on
     *     the actual api being adhered to. Consult the documentation for
     *     the specific api for inforation about what to expect.
     */
    protected static function _callStaticControlApiCommand( $command,
        $args = array() )
    {
        $suffix = null;
        $class = get_called_class();
        $slug = $command;
        if ( !empty( $args ) && is_string( $args[0] ) )
        {
            $suffix = array_shift( $args );
            $slug = $slug . '-' . $suffix;
        }
        $method = self::_getStaticControlApiMethod( $slug );
        if ( $method )
        {
            //call this method directly
            return self::_callMethod( $class . '::' . $method, $args );
        }
        //check customizations
        if ( array_key_exists( $command,
                self::$_static_control_api_customizations ) && array_key_exists( $suffix,
                self::$_static_control_api_customizations[$command] ) )
        {
            return self::_callMethod( self::$_static_control_api_customizations[$command][$suffix],
                    $args );
        }
        //check extensions
        foreach ( self::$_static_control_api_extensions as
            $key =>
            $extension )
        {
            if ( $extension::check( $command, $suffix ) )
            {
                array_unshift( $args, $suffix );
                return call_user_func_array( get_class( $extension ) . '::' . $command,
                    $args );
            }
        }
        //Method not found
        d($command,
            $args,
            get_called_class(),
            __CLASS__,
            debug_backtrace(),
            json_decode( json_encode(self::api()), 1 ),
            self::$_static_control_api_indexes,
            self::$_static_control_api_cache,
            self::$_static_control_api_cache_compiled,
            self::$_static_control_api_customizations,
            self::$_static_control_api_extensions,
            self::$_static_control_api_indexes,
            self::$_static_control_api_method_blacklist,
            self::$_static_control_api_method_cache,
            self::$_static_control_api_reserved_indexes,
            self::$_static_baseline_use_parameter_validation,
            self::$_static_baseline_use_parameter_validation_strict_checks,
            self::$_static_baseline_persist_parameters,
            self::$_static_baseline_parameters,
            self::$_static_baseline_parameters_default,
            self::$_static_baseline_parameters_required,
            self::$_static_baseline_parameters_valid,
            self::$_static_baseline_use_dependency_validation,
            self::$_static_baseline_persist_dependencies,
            self::$_static_baseline_dependencies,
            self::$_static_baseline_dependencies_default,
            self::$_static_baseline_dependencies_required,
            self::$_static_baseline_dependencies_valid,
            self::$_static_baseline_use_flag_validation,
            self::$_static_baseline_persist_flags,
            self::$_static_baseline_flags,
            self::$_static_baseline_flags_default,
            self::$_static_baseline_flags_required,
            self::$_static_baseline_flags_valid,
            self::$_static_baseline_initialized,
            self::$_static_baseline_allow_reinitialization,
            self::$_static_baseline_initialization_tasks,
            self::$_static_baseline_child_instances,
            self::$_static_fingerprint
        );
        throw self::_getException( 'bad-method-call-exception',
            'bad-method-call', array(
            $command ) );
    }

    /**
     * Checks for the existence of a valid index and/or index api command.
     * @param string $index the index to check. This corresponds to
     *     a top level static method call.
     * @param string $command (optional) the command handled under
     *     the index, if not checking the index directly.
     * @return bool
     */
    protected static function _checkStaticControlApiCommand( $index,
        $command = null )
    {
        $slug = $index;
        if ( !is_null( $command ) )
        {
            $slug .= '-' . $command;
        }
        $method = self::_getStaticControlApiMethod( $slug );
        if ( is_null( $command ) && ( array_key_exists( $index,
                self::$_static_control_api_indexes )
            || array_key_exists( $index,
                self::$_static_control_api_customizations )
            ) )
        {
            return true;
        }
        if ( !$method )
        {
            //check extensions if the method does not exist by this point
            foreach ( self::$_static_control_api_extensions as
                $key =>
                $extension )
            {
                if ( $extension::check( $index, $command ) )
                {
                    return true;
                }
            }
        }
        return $method
            ? true
            : false;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Validates a proposed static control api index.
     * @param string $index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given index is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given index already exists
     * @internal
     */
    private static function _staticControlApiValidateIndex( $index )
    {
        if ( !self::_checkStaticControlApiIndexSyntax( $index ) )
        {
            throw self::_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'stringable value',
                $index
            ) );
        } elseif ( self::_checkStaticControlApiIndexValid( $index ) )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Cannot redeclare static control api index [%s] in [%s].',
                    $index, get_called_class() ),
                self::_getExceptionCode( 'initialization-already-initialized' ) );
        }
    }

    /**
     * Validates a proposed static control api method.
     * @param string $index
     * @param string $method
     * @param callable $callable (optional)
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If any parameters fail validation
     * @internal
     */
    private static function _staticControlApiValidateControlApiIndexMethod( $index,
        $method, $callable = null )
    {
        $class = get_called_class();
        self::_validate( 'type-of', $index, 'string', true );
        self::_validate( 'type-of', $method, 'string', true );
        if ( !is_null( $callable ) )
        {
            self::_validate( 'callable', $callable, null, true );
        }
        if ( !self::_validate( 'any-of', $index,
                array_merge( array_keys( self::$_static_control_api_customizations ),
                    array_keys( self::$_static_control_api_indexes ) ) ) )
        {
            //Invalid key passed
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Tried to register api method '
                    . '[%s] for non-existent index [%s]', __METHOD__, $method,
                    $callable ),
                self::_getExceptionCode( 'logic-bad-parameters' ) );
        }
        if ( !method_exists( get_called_class(), $method ) && !is_callable( $callable ) )
        {
            //Nonexistent method and no callable substitution passed
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Method [%s] does not exist,'
                    . ' and no callable substitute was provided.', __METHOD__,
                    $method ), self::_getExceptionCode( 'logic-bad-parameters' ) );
        }
    }

    /**
     * Checks if a method already exists and throws an
     * invalid argument exception if it does.
     * @param string $method
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the proposed method already exists
     * @internal
     */
    private static function _staticControlApiValidateNotAlreadyExisting( $method )
    {
        if ( array_key_exists( $method,
                self::$_static_control_api_customizations[$index] ) || array_key_exists( $method,
                self::$_static_control_api_indexes[$index] ) )
        {
            //static control api does not allow overrides.
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Method [%s] already exists,'
                    . ' and cannot be overridden in the static control api.',
                    __METHOD__, $method ),
                self::_getExceptionCode( 'logic-bad-parameters' ) );
        }
    }

    /**
     * Receives a string method name and returns the corresponding canonicalized
     * command name.
     * @param string $method A method name to canonicalize
     * @param string $index The index, which will be removed from
     *     the beginning of the canonicalized result
     * @return string
     * @internal
     */
    private static function _canonicalizeStaticControlApiMethod( $method, $index )
    {
        $raw = strtolower( preg_replace( '/(?<!^)([A-Z])/', '-\\1',
                ltrim( $method, '_' ) ) );
        return substr( $raw, strlen( $index ) + 1, strlen( $raw ) );
    }

    /**
     * Casts a canonicalized string to camelcase.
     * @param string $command
     * @return string
     * @internal
     */
    private static function _uncanonicalizeStaticControlApiCommand( $command )
    {
        $result = lcfirst( str_replace( ' ', null,
                ucwords( str_replace( '-', ' ', $command ) ) ) );
        return $result;
    }

    /**
     * Un-canonicalizes the specified command, and returns the
     * method name corresponding to it, or false if not found.
     * @param type $command
     * @return string|bool
     * @internal
     */
    private static function _getStaticControlApiMethod( $command )
    {
        $class = get_called_class();
        $method = self::_uncanonicalizeStaticControlApiCommand( $command );
        if ( method_exists( $class, $method ) )
        {
            return $method;
        }
        //Check also with hyphen prefix
        if ( method_exists( $class, '_' . $method ) )
        {
            return '_' . $method;
        }
        return false;
    }

    /**
     * Calls the actual method from any of the prior call methods,
     * to prevent code duplication.
     * @param callable $method
     * @param array $args
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-callable parameter is passed.
     * @throws \Exception Any exceptions thrown by the method called
     *     will not be caught.
     * @internal
     */
    private static function _callMethod( $method, array $args = array() )
    {
        self::_validate( 'callable', $method, null, true );
        return self::_callableUtilityCall( $method, $args );
    }

    /**
     * Casts any stringable parameter to a string, otherwise throws an exception.
     * @param mixed $value
     * @return string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-stringable value is passed
     * @internal
     */
    private static function _getStaticControlApiStringParameterValue( $value )
    {
        if ( !self::_checkStaticControlApiCommandSyntax( $value ) )
        {
            throw self::_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'string or stringable value',
                $value ) );
        }
        $value = (string) $value;
        return $value;
    }

    /**
     * Checks if a specified index exists in current api indexes or extensions.
     * @param string $index
     * @return bool
     * @internal
     */
    private static function _checkStaticControlApiIndexValid( $index )
    {
        if ( empty( self::$_static_control_api_extensions )
            && empty( self::$_static_control_api_indexes )
            && empty( self::$_static_control_api_customizations ) )
        {
            //no need to crunch the indexes if these are both empty.
            return false;
        }
        $keys = array_merge( array_keys( self::$_static_control_api_indexes ),
            array_keys( self::$_static_control_api_customizations ),
            array_keys( self::$_static_control_api_extensions ) );
        return in_array( $index, $keys );
    }

    /**
     * Safely checks if the value can be cast to a to a string that represents
     * a canonicalized control api command
     * @param mixed $index Should be a string, but can be any value that
     *     can be cast to string, including objects with __toString
     * @return bool
     * @internal
     */
    private static function _checkStaticControlApiIndexSyntax( $index )
    {
        //safety check for string type or stringable
        return self::_validate( 'stringable', $index )
            && preg_match( \oroboros\regex\interfaces\enumerated\Regex::REGEX_ALPHANUMERIC,
                (string) $index );
    }

    /**
     * Checks if a given index is a valid format.
     * @param type $index
     * @return bool
     * @internal
     */
    private static function _checkStaticControlApiCommandSyntax( $index )
    {
        //Reverse lookups in regex are often slow.
        //Native language constructs can perform this
        //part of the evaluation faster, hence the
        //strpos statement.
        return self::_validate( 'stringable', $value )
            && preg_match( '/^[a-z\\d]([a-z\\d]{1,}[-]?){1,}/', (string) $index )
            && strpos( $index[strlen( $index )], '-' ) !== false;
    }

    /**
     * Checks if a specified command is valid.
     * @param string $command The canonicalized command passed to check.
     * @return bool
     * @internal
     */
    private static function _checkStaticControlApiCommandValid( $command )
    {
        $index = self::_getStaticControlApiIndex( $command );
        $method = self::_getStaticControlApiMethod( $command );
        if ( !self::_checkStaticControlApiIndex( $index ) )
        {
            return false;
        }
        if ( in_array( $index,
                array_keys( self::$_static_control_api_extensions ) ) )
        {
            //pass recursively into extension api check method
            dd( 'This is an extension method. Finish this development.' );
        }
        return in_array( $method, self::$_static_control_api_indexes[$index] );
    }

    /**
     * Provides the initial set of api commands for an internal class
     * index of any class implementing this trait.
     * @param type $index
     * @return \oroboros\collection\Collection
     * @internal
     */
    private static function _getStaticControlApiCommandsForIndex( $index )
    {
        $commands = array();
        $blacklist = self::_staticControlApiGetBlacklist();
        $lex = new \oroboros\codex\LexiconEntry( get_called_class() );
        foreach ( get_class_methods( get_called_class() ) as
            $key )
        {
            if ( strpos( $key, '__' ) === 0 || in_array( $key, $blacklist ) )
            {
                //do not parse magic methods or possible magic methods,
                //and do not include methods declared in this specific trait.
                continue;
            }
            if ( strpos( ltrim( strtolower( $key ), '_' ), strtolower( $index ) )
                === 0 )
            {
                $commands[self::_canonicalizeStaticControlApiMethod( $key,
                        $index )] = array(
                    'class' => get_called_class(),
                    'method' => $key
                );
            }
        }
        self::_staticControlApiUpdateMethodCache( $index, $commands );
        return new \oroboros\collection\Collection( $commands );
    }

    /**
     * Returns a list of the blacklisted methods that cannot be used
     * for api declarations. These methods correspond to internals of
     * the StaticControlApi pattern, and would cause erratic behavior
     * if used in a higher level api.
     * @return array
     * @internal
     */
    private static function _staticControlApiGetBlacklist()
    {
        if ( empty( self::$_static_control_api_method_blacklist ) )
        {
            self::_staticControlApiRegisterBaselineBlacklist();
        }
        return self::$_static_control_api_method_blacklist;
    }

    /**
     * Blacklists a specific method from being called.
     * This may not be used to remove previously registered
     * methods.
     * @param string $method
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method name is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method name is already registered
     *     in the api as a valid method
     * @internal
     */
    private static function _staticControlApiBlacklistMethod( $method )
    {
        self::_validate( 'type-of', $method, 'string', true );
        $existing = array();
        foreach ( self::$_static_control_api_method_cache as
            $canonical =>
            $details )
        {
            if ( is_array( $details ) && array_key_exists( 'method', $details ) )
            {
                $existing = $details['method'];
            }
        }
        self::_validate( 'none-of', $method, $existing, true );
        if ( !in_array( $method, self::$_static_control_api_method_blacklist ) )
        {
            self::$_static_control_api_method_blacklist[] = $method;
        }
    }

    /**
     * Prevents all baseline static control api methods
     * from being registered by the blacklist.
     * @return void
     * @internal
     */
    private static function _staticControlApiRegisterBaselineBlacklist()
    {
        $ref = new \oroboros\codex\LexiconEntry( __TRAIT__ );
        foreach ( array_keys( $ref->getMethods()->toArray() ) as
            $method )
        {
            self::_staticControlApiBlacklistMethod( $method );
        }
    }

    /**
     * Returns the full api, including all custom
     * and extension methods currently in scope.
     * @return \oroboros\collection\Collection
     * @internal
     */
    private static function _staticControlApiGetFullApi()
    {
        $full_api = array();
        self::_staticControlApiUpdateLocalMethodApis();
        self::_staticControlApiUpdateExtensionMethodApis();
        $full_api = self::$_static_control_api_cache;
        return $full_api;
    }

    /**
     * Updates the internal method cache.
     * @param string $index
     * @param array $new
     * @return void
     * @internal
     */
    private static function _staticControlApiUpdateMethodCache( $index,
        $new = array() )
    {
        if ( !array_key_exists( $index, self::$_static_control_api_method_cache ) )
        {
            self::$_static_control_api_method_cache[$index] = array();
        }
        $blacklist = self::_staticControlApiGetBlacklist();
        foreach ( $new as
            $command =>
            $method )
        {
            if ( is_array( $method ) && array_key_exists( 'method', $method ) &&
                in_array( $method, $blacklist ) )
            {
                continue;
            }
            self::$_static_control_api_method_cache[$index][$command] = $method;
        }
        self::$_static_control_api_method_cache[$index] = $new;
    }

    /**
     * Builds the initial api cache.
     * @return void
     * @internal
     */
    private static function _staticControlApiInitializeApiCache()
    {
        self::$_static_control_api_cache = self::_staticControlApiGetFullApi();
        self::$_static_control_api_cache_compiled = true;
    }

    /**
     * Updated the internal api cache, so subsequent lexical
     * scrapes do not need to occur again.
     * @param string $key The name of the index to create or update
     * @param string $index (optional) The list of commands to append
     * @return void
     * @internal
     */
    private static function _staticControlApiUpdateApiCache( $key,
        $index = array() )
    {
        if ( !array_key_exists( $key, self::$_static_control_api_method_cache ) )
        {
            self::$_static_control_api_method_cache[$key] = $index;
            return;
        }
        foreach ( $index as
            $command =>
            $api )
        {
            self::$_static_control_api_method_cache[$key][$command] = $api;
        }
    }

    /**
     * Gets the api documentation array for an object
     * @param mixed $details
     * @return array|bool
     * @internal
     */
    private static function _staticControlApiGetMethodApi( $details )
    {
        $api = false;
        if ( is_object( $details ) && ($details instanceof \Closure) )
        {
            $api = self::_staticControlApiParameterizeClosureApi( $details );
        } else
        {
            $api = self::_staticControlApiParameterizeMethodApi( $details );
        }
        return $api;
    }

    private static function _staticControlApiParameterizeMethodApi( $class,
        $method )
    {
        $reflector = new \ReflectionMethod( $class . '::' . $method );
        $docblock = $reflector->getDocComment();
        $parser = new \oroboros\parse\DocBlock( $docblock );
        return $parser->getParsed();
    }

    /**
     * Creates an api declaration for closures.
     * If no comment was attached to the api,
     * it will generate one.
     * @param \Closure $closure
     * @return array
     * @internal
     */
    private static function _staticControlApiParameterizeClosureApi( \Closure $closure )
    {
        $api = array();
        $ref = new \ReflectionFunction( $closure );
        $args = $ref->getParameters();
        $comment = $ref->getDocComment();
        if ( $comment )
        {
            $parser = new \oroboros\parse\DocBlock( $comment );
            $api = $parser->getParsed()->toArray();
        } else
        {
            //generate an api for the closure
            $api['comment'] = sprintf( 'Closure declared at line [%s] of [%s], in namespace [%s].',
                $ref->getStartLine(), $ref->getFileName(),
                $ref->getNamespaceName() );
            foreach ( $ref->getParameters() as
                $param )
            {
                $api['attributes']['param'][$param->name] = 'mixed';
            }
        }
        return $api;
    }

    private static function _staticControlApiUpdateLocalMethodApis()
    {
        if ( is_null( self::$_static_control_api_cache ) || !self::$_static_control_api_cache )
        {
            self::$_static_control_api_cache = new \oroboros\collection\Collection();
        }
        foreach ( self::$_static_control_api_method_cache as
            $index =>
            $methods )
        {
            if ( !self::$_static_control_api_cache->has( $index ) )
            {
                self::$_static_control_api_cache[$index] = array();
            }
            $updated_index = self::$_static_control_api_cache[$index];
            foreach ( $methods as
                $canonical =>
                $method )
            {
                $check = $index . '.' . $canonical;
                if ( self::$_static_control_api_cache->has( $check ) )
                {
                    //do not rehash already compiled apis
                    //because it's terrible for performance
                    continue;
                }
                $api = self::_staticControlApiParameterizeMethodApi( $method['class'],
                        $method['method'] );
                $updated_index[$canonical] = $api;
            }
            if ( empty( $updated_index ) )
            {
                $updated_index = false;
            }
            if ( empty( $updated_index ) )
            {
                unset( self::$_static_control_api_cache[$index] );
            } else
            {
                self::$_static_control_api_cache[$index] = $updated_index;
            }
            if ( empty( self::$_static_control_api_cache ) )
            {
                self::$_static_control_api_cache = false;
            }
        }
    }

    private static function _staticControlApiUpdateExtensionMethodApis()
    {
        if ( is_null( self::$_static_control_api_cache ) )
        {
            self::$_static_control_api_cache = new \oroboros\collection\Collection();
        }
        if ( empty( self::$_static_control_api_extensions ) )
        {
            //nothing to update
            return;
        }
        foreach ( self::$_static_control_api_extensions as
            $extension )
        {
            $extension_api = $extension::api();
            if ( !$extension_api )
            {
                //no api methods to gain from the extension
                continue;
            }
            foreach ( $extension_api->toArray() as
                $index =>
                $methods )
            {
                if ( !self::$_static_control_api_cache->has( $index ) )
                {
                    self::$_static_control_api_cache[$index] = $methods;
                    continue;
                }
                //Extensions may not override parent methods under any circumstances.
                $updated_index = array_replace_recursive( $methods,
                    self::$_static_control_api_cache[$index] );
                self::$_static_control_api_cache[$index] = $updated_index;
            }
        }
    }

}
