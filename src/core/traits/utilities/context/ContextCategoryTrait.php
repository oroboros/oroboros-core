<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\context;

/**
 * <Context Category Trait>
 * Provides methods to represent a category of related contexts.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category utilities
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContextCategoryTrait
{

    //use

    /**
     * Represents the context category currently represented
     * @var string
     */
    private $_context_category;

    /**
     * Represents the total set of context categories that have been registered,
     * and the class that represents them.
     * @var array
     */
    private static $_context_categories = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Contextual Constructor Method>
     *
     * @param string $category
     */
    public function __construct( $category = null )
    {
        $this->_initializeContext( $subject );
    }

    public function get()
    {
        return $this->_context_item;
    }

    public static function getCategories()
    {

    }

    public function getType()
    {
        return $this->_context_type;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Context Category Initialization Method>
     * This method initializes the context, sets the subject, and registers
     * the context and category if they have not already been registered.
     * @param mixed $subject
     */
    protected function _initializeContextCategory( $subject )
    {
        $this->_contextCategoryRegister();
    }

    /**
     * <Context Category Registration Method>
     * This method should be overridden to provide the context slug.
     * It needs to return a string.
     * @return string
     */
    abstract protected function _registerContextCategory();

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    private function _contextCategoryRegister()
    {

    }

    private function _contextCategoryRegisterContextCategory()
    {

    }

    private function _contextCategoryRegisterInstance()
    {

    }

    private static function _contextCategoryRegisterContextCategoryHandler( $category )
    {
        $class = get_called_class();
        if (!array_key_exists($category, self::$_context_categories))
        {
            self::$_context_categories[$category] = new \oroboros\collection\Collection();
        }
        if (!self::$_context_categories[$category]->hasValue($category))
        {
            self::$_context_categories[$category][] = $class;
        }
    }

}
