<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\core;

/**
 * <Backtrace Utility Trait>
 * Provides some utility methods for dynamic string handling.
 * These methods are private, and are only directly accessible in the scope
 * that they are used. Extending constructs should provide public or protected
 * accessors as needed if they want to expose these methods for extension to
 * other classes or external use.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category core
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait BacktraceUtilityTrait
{
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Backtrace Parse Method>
     * Cleans up a backtrace array to reflect the real origin,
     * and remove huge parameter sets that bloat var dumps.
     * @param array $trace the result of a debug_backtrace or \Exception->getTrace()
     * @param int $start_index (optional) How deep in the stack the real origin point
     *     reflects. If the backtrace is generated from a method that expects
     *     to be called from external methods, it can exclude its own stack
     *     level so it does not show up in the trace. By default, no levels
     *     are removed.
     * @param bool $strip_parameters (optional) If true, the params will be
     *     removed from the trace values to prevent huge redundant data sets
     *     for simple stack trace operations. Default true. If you pass false,
     *     this will be ignored.
     * @param bool $strip_object (optional) If true, the object will be
     *     removed from the trace values to prevent huge redundant data sets
     *     for simple stack trace operations. Default true. If you pass false,
     *     this will be ignored.
     * @return array
     */
    private static function _backtraceUtilityParseBacktrace( $trace,
        $start_index = 0, $strip_parameters = true, $strip_object = true )
    {
        if ( $start_index > 0 )
        {
            $trace = self::_backtraceUtilityStripIrrelevantStackLevels( $trace,
                    $start_index );
        }
        if ( !$trace )
        {
            return array();
        }
        if ( $strip_parameters )
        {
            $trace = self::_backtraceUtilityStripParameters( $trace );
        }
        if ( $strip_object )
        {
            $trace = self::_backtraceUtilityStripObjects( $trace );
        }
        return $trace;
    }

    /**
     * <Backtrace String Generator Method>
     * Creates a configurable backtrace as a string.
     * This can be used to reflect a loggable entry, an insertable
     * database record, a csv output, or any other specific schema
     * of recording the stacktrace needed.
     * @param array $trace The stacktrace to evaluate
     * @param bool $lines (optional) If true, line number and file name will be
     *     included in the stacktrace. Default true.
     * @param bool $methods (optional) If true, class name and function name
     *     will be included in the stack trace. Default true.
     * @param type $prefix (optional) Designates a prefix for each stack level.
     *     The current stack depth of each trace level will be injected into
     *     this with sprintf. If you would like to use this to generate a
     *     record, this should reflect your record prefix, and the stacklevel
     *     value if required.
     * @param type $delimiter (optional) Designates a suffix for each
     *     stack level. The default is a line break. If you would like to use
     *     this to generate a record, this should reflect your record suffix.
     * @param type $divisor (optional) Designates a separator between the
     *     file/line segment and the class/method segment. If you would like
     *     to use this to generate a record and would like these two separated,
     *     this should reflect the segment between the two that identifies them
     *     as separate values.
     * @return string|false The fully compiled backtrace string.
     *     Returns false if a trace could not be generated.
     */
    private static function _backtraceUtilityTraceAsString( $trace,
        $lines = true, $methods = true, $prefix = '[stacklevel-%s] ',
        $delimiter = PHP_EOL, $divisor = ' > ' )
    {
        $template = '%s%s%s%s%s';
        $result = '';
        $suffix = ($lines && $methods)
            ? $divisor
            : null;
        $line = null;
        $method = null;
        $pref = null;
        $depth = count( $trace );
        foreach ( $trace as
            $stacklevel )
        {
            $pref = sprintf( $prefix, $depth );
            if ( $lines )
            {
                $line = self::_backtraceUtilityTraceStringByFile( $stacklevel );
            }
            if ( $methods )
            {
                $method = self::_backtraceUtilityTraceStringByMethod( $stacklevel );
            }
            $result .= sprintf( $template, $pref, $line, $suffix, $method,
                $delimiter );
            $depth--;
        }
        if ( $result === '' )
        {
            $result = false;
        }
        return $result;
    }

    /**
     * Filters a backtrace and returns a string trace representation of the
     * file and line number.
     * @param type $stack_level The stack level to filter.
     * @return string
     */
    private static function _backtraceUtilityTraceStringByFile( $stack_level )
    {
        if (!array_key_exists( 'file', $stack_level))
        {
            return '{main}';
        }
        return sprintf( '%s:%s', $stack_level['file'], $stack_level['line'] );
    }

    /**
     * Filters a backtrace and returns a string trace representation of the
     * class and method, or a representative marker if these do not exist
     * in the given stack level.
     * @param type $stack_level The stack level to filter.
     * @return string
     */
    private static function _backtraceUtilityTraceStringByMethod( $stack_level )
    {
        $template = '%s%s%s';
        $class = (array_key_exists( 'class', $stack_level )
            ? $stack_level['class']
            : null);
        $type = (array_key_exists( 'type', $stack_level )
            ? $stack_level['type']
            : null);
        $function = (array_key_exists( 'function', $stack_level )
            ? $stack_level['function']
            : null);
        if ( strpos( $function, '{closure}' ) !== false )
        {
            $function = sprintf( rtrim( $function, '}' ) . '@line: %s}',
                $stack_level['line'] );
        }
        return sprintf( $template, $class, $type, $function );
    }

    /**
     * Removes redundant stack levels from the backtrace.
     * @param array $trace The backtrace to evaluate
     * @param int $levels (optional) If greater than zero, will remove a
     *     number of stack levels from the trace up to the count of levels
     *     or count of the trace, whichever is lower. If levels is higher
     *     than the number of stack levels in the trace, the method will
     *     return false.
     * @return array|false Returns false if all stack levels are removed.
     */
    private static function _backtraceUtilityStripIrrelevantStackLevels( array $trace,
        $levels = 0 )
    {
        if ( $levels < 1 )
        {
            return $trace;
        }
        if ( count( $trace ) <= $levels )
        {
            return false;
        }
        for ( $i = 0;
            $i < $levels;
            $i++ )
        {
            array_shift( $trace );
        }
        return $trace;
    }

    /**
     * Removes redundant parameters from all levels of the backtrace.
     * @param array $trace The backtrace to evaluate
     * @return array
     */
    private static function _backtraceUtilityStripParameters( $trace )
    {
        return self::_backtraceUtilityStripByKey( 'args', $trace );
    }

    /**
     * Removes redundant object provisions from all levels of the backtrace.
     * @param array $trace The backtrace to evaluate
     * @return array
     */
    private static function _backtraceUtilityStripObjects( $trace )
    {
        return self::_backtraceUtilityStripByKey( 'object', $trace );
    }

    /**
     * Removeds a specified key from all elements of an array.
     * @param type $key
     * @param type $trace
     * @return type
     */
    private static function _backtraceUtilityStripByKey( $key, $trace )
    {
        foreach ( $trace as
            $k =>
            $value )
        {
            if ( array_key_exists( $key, $value ) )
            {
                unset( $trace[$k][$key] );
            }
        }
        return $trace;
    }

}
