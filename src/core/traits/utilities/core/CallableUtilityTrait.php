<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\core;

/**
 * <Class Utility Trait>
 * Provides some utility methods for dynamic class and object handling.
 * These methods are private, and are only directly accessible in the scope
 * that they are used. Extending constructs should provide public or protected
 * accessors as needed if they want to expose these methods for extension to
 * other classes or external use.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait CallableUtilityTrait
{
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Calls a method dynamically. Will pass the parameters in explicitly
     * for up to 14 parameters. If the method takes more than 14 parameters,
     * it will use call_user_func_array instead.
     *
     * This method outperforms both call_user_func_array
     * and reflection->callMethod/callFunction significantly.
     * If you are nitpicky about performance, this is a
     * good option for you.
     *
     * Parameters in the array must occur in the exact
     * order they are expected by the target function.
     *
     * @param callable $callable Any valid callable
     * @param array $args (optional) An array of parameters to pass into the callable
     */
    private static function _callableUtilityCall( $callable, $args = array() )
    {
        if ( !is_callable( $callable ) )
        {
            //Not a callable parameter
            throw new \oroboros\core\utilities\exception\BadFunctionCallException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_PARAMETERS_MESSAGE,
                'callable', gettype( $callable ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_FUNCTION_CALL
            );
        }
        if ( !is_array( $callable ) )
        {
            switch ( count( $args ) )
            {
                case 0:
                    return $callable();
                    break;
                case 1:
                    return $callable( $args[0] );
                    break;
                case 2:
                    return $callable( $args[0], $args[1] );
                    break;
                case 3:
                    return $callable( $args[0], $args[1], $args[2] );
                    break;
                case 4:
                    return $callable( $args[0], $args[1], $args[2], $args[3] );
                    break;
                case 5:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4] );
                    break;
                case 6:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5] );
                    break;
                case 7:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6] );
                    break;
                case 8:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7] );
                    break;
                case 9:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8] );
                    break;
                case 10:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9] );
                    break;
                case 11:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10] );
                    break;
                case 12:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10], $args[11] );
                    break;
                case 13:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10], $args[11], $args[12] );
                    break;
                case 14:
                    return $callable( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10], $args[11], $args[12], $args[13] );
                    break;
                default:
                    return call_user_func_array( $callable, $args );
                    break;
            }
        }
        $object = array_shift( $callable );
        $method = array_shift( $callable );
        $ref = new \ReflectionMethod( $object, $method );
        $is_static = $ref->isStatic();
        unset( $ref );
        switch ( count( $args ) )
        {
            case 0:
                if ( $is_static )
                {
                    return $object::$method();
                }
                return $object->$method();
                break;
            case 1:
                if ( $is_static )
                {
                    return $object::$method( $args[0] );
                }
                return $object->$method( $args[0] );
                break;
            case 2:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1] );
                }
                return $object->$method( $args[0], $args[1] );
                break;
            case 3:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2] );
                }
                return $object->$method( $args[0], $args[1], $args[2] );
                break;
            case 4:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3] );
                break;
            case 5:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4] );
                break;
            case 6:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5] );
                break;
            case 7:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6] );
                break;
            case 8:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7] );
                break;
            case 9:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7],
                            $args[8] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8] );
                break;
            case 10:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7],
                            $args[8], $args[9] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9] );
                break;
            case 11:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7],
                            $args[8], $args[9], $args[10] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10] );
                break;
            case 12:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7],
                            $args[8], $args[9], $args[10], $args[11] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10], $args[11] );
                break;
            case 13:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7],
                            $args[8], $args[9], $args[10], $args[11], $args[12] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10], $args[11], $args[12] );
                break;
            case 14:
                if ( $is_static )
                {
                    return $object::$method( $args[0], $args[1], $args[2],
                            $args[3], $args[4], $args[5], $args[6], $args[7],
                            $args[8], $args[9], $args[10], $args[11], $args[12],
                            $args[13] );
                }
                return $object->$method( $args[0], $args[1], $args[2], $args[3],
                        $args[4], $args[5], $args[6], $args[7], $args[8],
                        $args[9], $args[10], $args[11], $args[12], $args[13] );
                break;
            default:
                return call_user_func_array(
                    array(
                    $object,
                    $method ), $args );
                break;
        }
    }

}
