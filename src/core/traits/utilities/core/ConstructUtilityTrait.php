<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\core;

/**
 * <Construct Utility Trait>
 * Provides some utility methods for handling classes, traits, interfaces, and functions.
 * These methods are private, and are only directly accessible in the scope
 * that they are used. Extending constructs should provide public or protected
 * accessors as needed if they want to expose these methods for extension to
 * other classes or external use.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ConstructUtilityTrait
{
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Validates a syntactically correct namespace.
     * @param string|object $namespace
     * @return bool
     */
    private static function _classUtilityValidateNamespace( $namespace )
    {
        if ( is_object( $namespace ) )
        {
            return true;
        }
        if ( !is_string( $namespace ) )
        {
            return false;
        }
        if ( trim( $namespace, '\\' ) === '' )
        {
            return true;
        }
        return preg_match( \oroboros\regex\interfaces\enumerated\Regex::REGEX_NAMESPACE,
            $namespace );
    }

    /**
     * Validates a syntactically correct function name, class name, or method name.
     * This will only work on statically declared constructs, not runtime declarations.
     * @param string $function
     * @return bool
     */
    private static function _classUtilityValidateFunctionName( $function )
    {
        if ( !is_string( $function ) )
        {
            return false;
        }
        return preg_match( \oroboros\regex\interfaces\enumerated\Regex::REGEX_FUNCTION,
            $function );
    }

    /**
     * Accepts a string or object parameter, and determines what type of PHP construct it is.
     * Will detect functions, closures, classes, traits, and interfaces.
     * @param string|object $subject
     * @return string|bool function|closure|class|trait|interface|false
     */
    private static function _classUtilityGetType( $subject )
    {
        if ( is_object( $subject ) )
        {
            $subject = get_class( $subject );
        }
        if ( !is_string( $subject ) || !(self::_classUtilityValidateNamespace( $subject ) ||
            self::_classUtilityValidateFunctionName( $subject ) ) )
        {
            //cannot be valid if it is an invalid construct
            return false;
        }
        $subject = trim( $subject, '\\' );
        if ( function_exists( $subject ) )
        {
            return 'function';
        }
        if ( trait_exists( $subject) )
        {
            return 'trait';
        }
        if ( interface_exists( $subject ) )
        {
            return 'interface';
        }
        if ( $subject === 'Closure' || is_subclass_of( $subject, 'Closure') )
        {
            return 'closure';
        }
        if ( class_exists( $subject ) )
        {
            return 'class';
        }
        return false;
    }

    /**
     * Takes a string or object parameter corresponding to a class, trait, interface,
     * or function name, and determines if it has been compiled.
     * Objects will always return true.
     * @param string|object $subject
     * @return bool
     */
    private static function _classUtilityIsCompiled( $subject )
    {
        if ( is_object( $subject ) )
        {
            //objects are always compiled
            return true;
        }
        if ( !is_string( $subject ) || !(self::_classUtilityValidateNamespace( $subject ) ||
            self::_classUtilityValidateFunctionName( $subject ) ) )
        {
            //cannot be compiled if it is an invalid construct
            return false;
        }
        $subject = trim( $subject, '\\' );
        return function_exists( $subject )
            || class_exists( $subject )
            || interface_exists( $subject )
            || trait_exists( $subject );
    }

}
