<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\core;

/**
 * <String Utility Trait>
 * Provides some utility methods for dynamic string handling.
 * These methods are private, and are only directly accessible in the scope
 * that they are used. Extending constructs should provide public or protected
 * accessors as needed if they want to expose these methods for extension to
 * other classes or external use.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category formatting
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait StringUtilityTrait
{
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * This will convert spaces, underscores, camelCased,
     * and BruteCased strings to hyphen separated words.
     *
     * Prefixing and suffixing hyphens will be trimmed.
     *
     * @param string $string
     */
    private static function _stringUtilityCanonicalize( $string )
    {
        $string = preg_replace( "#[[:punct:]]#", "",
            str_replace( '-', ' ', $string ) );
        $raw = strtolower( preg_replace( '/(?<!^)([A-Z])/', '-\\1',
                trim( $string, '-' ) ) );
        $raw = str_replace( ' ', '-', $raw );
        $raw = str_replace( '_', '-', $raw );
        while ( str_replace( '--', '-', $raw ) !== $raw )
        {
            $raw = str_replace( '--', '-', $raw );
        }
        return strtolower( $raw );
    }

    /**
     * Splits a string by the given delimiter and returns
     * an array of the segments.
     * @param type $string The subject to modify.
     * @param type $delimiter (optional) The delimiter to split by.
     *     Default line break.
     * @return array
     */
    private static function _stringUtilitySplitLine( $string,
        $delimiter = PHP_EOL )
    {
        return explode( $delimiter, $string );
    }

    /**
     * Indents a block of text by splitting the lines and padding
     * the beginning of each line with the specified character.
     * @param string $string The string to indent.
     * @param int $count (optional) The number of characters to indent by.
     *     Default 1.
     * @param string $character (optional) The character to indent with.
     *     Default is a space character.
     * @return string
     */
    private static function _stringUtilityIndentLines( $string, $count = 1,
        $character = ' ' )
    {
        $result = '';
        foreach ( self::_stringUtilitySplitLine( $string ) as
            $line )
        {
            $result .= self::_stringUtilityRightPad(
                    self::_stringUtilityLeftPad( $line, $count, $character ), 1,
                    PHP_EOL );
        }
        return rtrim( $result, PHP_EOL );
    }

    /**
     * Casts a canonicalized or standard language string to BruteCase.
     * @param type $string The string to modify
     * @return string
     */
    private static function _stringUtilityBruteCase( $string )
    {
        return preg_replace( "#[[:punct:]]#", "",
            str_replace( ' ', null, ucwords( str_replace( '-', ' ', $string ) ) ) );
    }

    /**
     * Casts a canonicalized or standard language string to camelCase.
     * @param type $string The string to modify
     * @return string
     */
    private static function _stringUtilityCamelCase( $string )
    {
        return lcfirst( self::_stringUtilityBruteCase( $string ) );
    }

    /**
     * Creates an exerpt text of the given length from a supplied string,
     * suffixed with the given suffix.
     * @param type $string The string to modify
     * @param type $max_length (optional) The maximum length of the exerpt text.
     *     This method will split at the last space character prior to the max
     *     length minus the length of the suffix, so the return value is never
     *     longer than the given length. If there are no space characters, the
     *     string will be chopped off at exactly the max length minus the length
     *     of the suffix.
     * @param string $suffix (optional) The suffix string to append at the
     *     end of the exerpt.The length of the suffix will be accounted for
     *     in the return value. Default "...".
     * @return string
     */
    private static function _stringUtilityExerpt( $string, $max_length = 300,
        $suffix = '...' )
    {
        $real_max_length = $max_length - strlen( $suffix );
        $substring = substr( $string, 0, $real_max_length );
        if ( $substring === $string )
        {
            //No need to create a substring if the
            //given string is already short enough.
            return $string;
        }
        if ( strpos( $substring, ' ' ) === false )
        {
            return $substring . $suffix;
        }
        return substr( $substring, 0, strrpos( $substring, ' ' ) ) . $suffix;
    }

    /**
     * Returns a segment of the given string between the first occurrence
     * of left and the last occurrence of right.
     * @param type $string
     * @param type $left (optional)
     * @param type $right (optional)
     * @return string
     */
    private static function _stringUtilityExtract( $string, $left = null,
        $right = null )
    {
        $lpos = 0;
        if ( !is_null( $left ) )
        {
            $pos = strpos( $string, $left );
            if ( $pos !== false )
            {
                $lpos = strlen( $left ) + $pos;
            }
        }
        $rpos = strlen( $string ) - $lpos;
        if ( !is_null( $right ) )
        {
            $pos = strrpos( $string, $right );
            if ( $pos !== false )
            {
                $rpos = $pos - $lpos;
            } else
            {
                $rpos = -1;
            }
        }
        return substr( $string, $lpos, $rpos );
    }

    /**
     * Prefixes a string with the given delimiter by the specified amount.
     * @param string $string The subject to modify.
     * @param int $count (optional) The number of characters to pad.
     *     Default 1.
     * @param string $delimiter (optional) The delimiter to prefix with.
     *     Default is a space character.
     * @return string
     */
    private static function _stringUtilityLeftPad( $string, $count = 1,
        $delimiter = ' ' )
    {
        $len = strlen( $string ) + ($count * strlen( $delimiter ) );
        return str_pad( $string, $len, $delimiter, STR_PAD_LEFT );
    }

    /**
     * Suffixes a string with the given delimiter by the specified amount.
     * @param string $string The subject to modify.
     * @param int $count (optional) The number of characters to suffix.
     *     Default 1.
     * @param string $delimiter (optional) The delimiter to suffix with.
     *     Default is a space character.
     * @return string
     */
    private static function _stringUtilityRightPad( $string, $count = 1,
        $delimiter = ' ' )
    {
        $len = strlen( $string ) + ($count * strlen( $delimiter ) );
        return str_pad( $string, $len, $delimiter, STR_PAD_RIGHT );
    }

    /**
     * Wraps a string in the specified left and right delimiter,
     * and returns the wrapped string.
     * @param type $string The subject to modify.
     * @param string $left (optional) The left-side wrapper. Default "[".
     * @param string $right (optional) The right-side wrapper. Default "]".
     * @return string
     */
    private static function _stringUtilityWrap( $string, $left = '[',
        $right = ']' )
    {
        $len = strlen( $string ) + strlen( $left );
        $str = str_pad( $string, $len, $left, STR_PAD_LEFT );
        $len = strlen( $str ) + strlen( $right );
        return str_pad( $str, $len, $right, STR_PAD_RIGHT );
    }

    /**
     * Replaces markers in a string with values from a set
     * of provided stringable values.
     * @param string $string The string to modify
     * @param array $context (optional) An associative array of replacements,
     *     where the key is the marker to replace, and the value is the
     *     replacement (any scalar value, null, a readable resource, or an
     *     instance of \Psr\Http\Message\StreamInterface)
     * @param string $search_left (optional) The left delimiter
     * @param string $search_right (optional)
     * @param string $left_wrap (optional)
     * @param string $right_wrap (optional)
     * @return string
     */
    private static function _stringUtilityInterpolate( $string,
        $context = array(), $search_left = '[', $search_right = ']',
        $left_wrap = null, $right_wrap = null )
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ( $context as
            $key =>
            $val )
        {
            if ( is_object( $val ) && ($val instanceof \Psr\Http\Message\StreamInterface ) )
            {
                $val = $val->detach();
            }
            // check that the value can be casted to string
            if ( !is_array( $val ) && (!is_object( $val ) || method_exists( $val,
                    '__toString' )) )
            {
                if ( is_resource( $val ) )
                {
                    rewind( $val );
                    $val = stream_get_contents( $val );
                }
                //PHP 7 returns 0 when string casting null.
                //On null, we want an empty string in all cases.
                if ( is_null( $left_wrap ) )
                {
                    $left_wrap = '';
                }
                if ( is_null( $right_wrap ) )
                {
                    $right_wrap = '';
                }
                $replace[$search_left . $key . $search_right] = self::_stringUtilityWrap( $val,
                        (string) $left_wrap, (string) $right_wrap );
            }
        }
        // interpolate replacement values into the message and return
        return strtr( $string, $replace );
    }

}
