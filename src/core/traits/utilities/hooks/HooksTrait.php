<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\hooks;

/**
 * <Hooks Trait>
 * Provides methods to quickly perform replacement
 * functions on hook content.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait HooksTrait {

    use \oroboros\core\traits\core\BaselineTrait;

    private $_hook_pattern = '@\[\:\:[a-zA-Z0-9\-]{1,}[^\-]\:\:\]@';
    private $_function_hook_pattern = '@\[\:\:(([a-zA-Z0-9]){1,}\-?){1,}\[(([a-zA-Z0-9_\-\/\:\?\&\=\#\.\;]){0,},?\s?){0,4}(([a-zA-Z0-9_\-\/\:\?\&\=\#\.\;]){0,},?\s?){0,1}\]\:\:\]@';

    /**
     * Returns an array of all hooks found in the supplied content.
     * @param string $content
     * @return array
     */
    protected function _getHooks($content) {
        $hooks = array();
        if (preg_match($this->_hook_pattern, $content)) {
            preg_match_all($this->_hook_pattern, $content, $tmp, PREG_OFFSET_CAPTURE);
            foreach ($tmp[0] as $match) {
                $hooks[] = str_replace('::]', null, str_replace('[::', null, $match[0]));
            }
        }
        return $hooks;
    }

    /**
     * Replaces the specified hooks with the supplied content for them.
     * @param string $content
     * @param array $hooks An associative array, where the key is the hook id, and the value is the content to replace it with.
     */
    protected function _replaceHooks($content, array $hooks = array()) {

    }

    protected function _getFunctionHooks($content) {

    }
}
