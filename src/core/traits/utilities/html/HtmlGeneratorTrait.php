<?php

namespace oroboros\core\traits\utilities\html;

/**
 * @todo Finish porting this from the old logic to the new setup.
 * It will remain commented out until integration occurs.
 */
trait HtmlGeneratorTrait {
//
//    /**
//     * @var array $_content_methods Provides a list of valid output format generation methods. These methods can be referenced to build structured output quickly.
//     */
//    protected $_content_methods = array(
//        'row',
//        'column',
//        'form',
//        'table',
//        'formtable',
//        'widget',
//        'navigation',
//    );
//    private static $_tabsets = 0;
//    private static $_datatables = 0;
//
//    /* -------- Inernal Operations -------- */
//
//    final private function _checkRequestMethod($method, $checkall = FALSE) {
//        if (!is_string($method) && ($checkall && !(is_array($method) || is_object($method))))
//            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
//        if (!$checkall) {
//            if (is_int($method))
//                return FALSE;
//            else
//                return in_array($method, $this->_content_methods);
//        } elseif ($checkall && is_array($method)) {
//            //check all keys
//            $check = TRUE;
//            foreach ($method as $key => $value) {
//                if (!$this->_checkRequestMethod($key, 0))
//                    $check = FALSE;
//            }
//            return $check;
//        } elseif ($checkall && is_object($method)) {
//            //get object vars and check all keys
//            return $this->_checkRequestMethod(get_object_vars($method), 1);
//        }
//        return FALSE;
//    }
//
//    final private function _evaluateContentArray($array) {
//        if (!is_array($array)) {
//            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
//        }
//        if (!array_numeric($array) && !empty($array) && !$this->_checkRequestMethod($array, 1)) {
//            //handle as a standard content array
//            return $this->_outputCallback($array);
//        } elseif (array_key_exists('template', $array)) {
//            //run the template queue method
//            //@todo request template from template handler if template handler object exists in this object instance
//            throw new \oroboros\lib\exception\LibraryException('Unimplemented request methodology at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_SYSTEM_UNIMPLEMENTED);
//        } elseif (!empty($array) && $this->_checkRequestMethod($array, 1)) {
//            //content array is requesting a unique content output generation method
//            $output = array();
//            foreach ($array as $key => $value) {
//                if (!is_int($key) && method_exists($this, '_' . $key))
//                    $output[] = array($this->${'_' . $key}($value));
//            }
//            return $output;
//        } else {
//            //array is a content output sequence
//            $output = array();
//            foreach ($array as $key => $value) {
//                $output[] = $this->_evaluateData($value);
//            }
//            return $output;
//        }
//    }
//
//    final private function _evaluateContentObject($object) {
//        if (!is_object($object))
//            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
//    }
//
//    final private function _evaluateData($data) {
//        return ((is_string($data)) ? $data : ((!isset($data)) ? NULL : ((is_object($data)) ? $this->_evaluateContentObject($data) : $this->_evaluateContentArray($data)) ) );
//    }
//
//    final private function _evaluateObjectMeta($meta, &$data) {
//        if (!((is_array($meta) && !array_numeric($meta)) || (is_object($meta && !array_numeric(get_object_vars($meta))))))
//            throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
//        foreach (((is_object($meta)) ? get_object_vars($meta) : $meta) as $key => $value) {
//            switch ($key) {
//                case 'template':
//                    //handle template directives
//                    if (isset($this->_template) && is_object($this->_template) && method_exists($this->_template, 'objectMetaParameters')) {
//                        foreach (((is_object($value)) ? get_object_vars($value) : $value) as $name => $args) {
//                            $this->_template->objectMetaParameters($name, $data, $args);
//                        }
//                    } else {
//                        throw new \oroboros\lib\exception\LibraryException('Missing instance of template handler at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_NOT_INITIALIZED);
//                        foreach (((is_object($value)) ? get_object_vars($value) : $value) as $name => $args) {
//
//                        }
//                    }
//                    break;
//                case 'theme':
//                    //handle theme directives
//                    if (isset($this->_theme) && is_object($this->_theme) && method_exists($this->_theme, 'objectMetaParameters')) {
//
//                    } else {
//                        throw new \oroboros\lib\exception\LibraryException('Missing instance of theme handler at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_NOT_INITIALIZED);
//                    }
//                    break;
//                case 'module':
//                    //handle module directives
//                    throw new \oroboros\lib\exception\LibraryException('Module meta parameters are unimplemented at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_SYSTEM_UNIMPLEMENTED);
//                    break;
//                case 'component':
//                    //handle component directives
//                    throw new \oroboros\lib\exception\LibraryException('Component meta parameters are unimplemented at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_SYSTEM_UNIMPLEMENTED);
//                    break;
//                default:
//                    throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
//                    break;
//            }
//        }
//    }
//
//    /* -------- Output Generation -------- */
//
//    protected function _tag($content = NULL, $tag = 'p', $params = NULL, $short = FALSE, $inline = TRUE) {
//        if (!isset($content)) {
//            return;
//        }
//        return $this->_generateOutput($content, $tag, $params, $short);
//    }
//
//    protected function _generateOutput($content, $tag = 'p', $params = NULL, $short = FALSE, $inline = TRUE) {
//        $output = array(
//            'tag' => $tag,
//            'params' => $params,
//            'short' => $short,
//            'inline' => $inline
//        );
//        if (is_array($content)) {
//            if (!array_numeric($content) && !empty($content)) {
//                //output is a standard content array
//                $output['content'] = array($this->_generateOutput(((isset($content['content']) ? $content['content'] : NULL)), $content['tag'], ((isset($content['params'])) ? $content['params'] : NULL)));
//            } elseif (!array_numeric($content) && (!isset($content['content']) || (!array_numeric($content) && isset($content['content']) && empty($content['content'])))) {
//                //no content set, generate an empty result
//                $output['content'] = NULL;
//            } elseif (array_numeric($content)) {
//                $output['content'] = $content;
//            } else {
//                //output is a sequence of content
//                $output['content'] = array_map(array($this, '_generateOutput'), $content);
//            }
//        } elseif (is_string($content)) {
//            //set the string as the content directly
//            $output['content'] = $content;
//        } elseif (is_object($content)) {
//            //parse the data object, and set the resulting data as content
//            $data_obj = self::_OROBOROS_SYSTEM_DATA_OBJECT;
//            if (!($content instanceof \stdClass || $content instanceof $data_obj)) {
//                throw new \OroborosModelException('Invalid object passed at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_INVALID_OBJECT);
//            }
//            $output['content'] = $this->_outputCallback(get_object_vars($content));
//        }
//        return $this->_generateHtmlTag($output['tag'], $output['content'], $output['params'], $output['short'], $output['inline']);
//    }
//
//    /**
//     * Provides an array_map() method for output generation
//     * @see \oroboros\site\traits\utilities\OutputGenerator::_generateOutput()
//     * @param array $params an associative array of parameters, may contain the keys tag (string), content (mixed), params (array), and short (bool)
//     * @return array The formatted content array
//     */
//    final private function _outputCallback($params) {
//        $output = array(
//            'tag' => ((isset($params['tag'])) ? $params['tag'] : 'p'),
//            'content' => ((isset($params['content'])) ? (is_array($params['content']) && !array_numeric($params['content']) ? array($params['content']) : $params['content']) : NULL),
//            'params' => ((isset($params['params'])) ? $params['params'] : NULL),
//            'short' => ((isset($params['short'])) ? $params['short'] : FALSE),
//            'meta' => ((isset($params['meta'])) ? $params['meta'] : NULL)
//        );
//        return $this->_generateOutput($output['content'], $output['tag'], $output['params'], $output['short']);
//    }
//
//    /**
//     *
//     */
//    final private function _filterMetaParameters($meta, &$content) {
//        if (isset($meta)) {
//            if (is_string($meta) || is_int($meta) || (is_array($meta) && array_numeric($meta)))
//                throw new \oroboros\lib\exception\LibraryException('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
//            $meta = ((is_object($meta)) ? get_object_vars($meta) : $meta);
//            foreach ($meta as $key => $value) {
//                switch ($key) {
//                    case 'css':
//                        //handle template output directives
//                        foreach ($args['css'] as $directive => $params) {
//                            if (method_exists($this, '_queueStylesheet')) {
//                                $this->_queueStylesheet($directive, $params['source'], ((isset($params['inline'])) ? $params['inline'] : FALSE), ( (isset($params['local']) ) ? $params['local'] : FALSE));
//                            }
//                        }
//                        break;
//                    case 'scripts':
//                        //handle template output directives
//                        foreach ($args['scripts'] as $directive => $params) {
//                            if (method_exists($this, '_queueStylesheet')) {
//                                $this->_queueScript($directive, $params['source'], ((isset($params['inline'])) ? $params['inline'] : FALSE), ( (isset($params['local']) ) ? $params['local'] : FALSE));
//                            }
//                        }
//                        break;
//                }
//            }
//        }
//    }
//
//    protected function _generateHtmlTag($tag = NULL, $content = NULL, $params = NULL, $short = FALSE, $inline = FALSE) {
//        if (!is_string($tag))
//            throw new \oroboros\lib\exception\LibraryException('Invalid tag parameter at ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_BAD_PARAMETERS);
//        if (isset($content) && (!is_string($content) && !is_array($content)))
//            throw new \oroboros\lib\exception\LibraryException('Invalid content parameter at ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_BAD_PARAMETERS);
//        if (isset($params) && !is_array($params))
//            throw new \oroboros\lib\exception\LibraryException('Invalid tag arguments params at ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_BAD_PARAMETERS);
//        if ($short && $short != TRUE)
//            throw new \oroboros\lib\exception\LibraryException('Invalid short tag boolean parameter at ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_BAD_PARAMETERS);
//        $output = '<' . rtrim(strtolower($tag) . (isset($params) ? ' ' . $this->_packageTagParams($params) . ' ' : NULL), ' ') . (($short) ? ((in_array(strtolower($tag), array('br', 'hr', 'meta', 'link', 'img'))) ? '>' : ' />') : '>') . (($inline) ? NULL : PHP_EOL);
//        if (!isset($content) && $short)
//            return $output . (($inline) ? NULL : PHP_EOL);
//        $output .= ((is_string($content) ? $content : NULL )) . (($inline) ? NULL : PHP_EOL);
//        if (is_array($content)) {
//            if (isset($content['meta'])) {
//                $meta = $this->_handleElementMeta($content['meta']);
//            } else
//                $meta = NULL;
//            foreach ($content as $item) {
//                if (is_string($item)) {
//                    //preformatted content
//                    $output .= $item . (($inline) ? NULL : PHP_EOL);
//                } else {
//                    //unformatted content
//                    if (!isset($item['tag']))
//                        throw new \oroboros\lib\exception\LibraryException('Registered html content does not have a tag value at ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_MISSING_PARAMETER);
//                    $output .= $this->_generateHtmlTag($item['tag'], (isset($item['content']) ? $item['content'] : NULL), (isset($item['params']) ? $item['params'] : NULL), (isset($item['short']) ? $item['short'] : FALSE), ((isset($item['inline'])) ? $item['inline'] : FALSE)) . (($inline) ? NULL : PHP_EOL);
//                }
//            }
//        }
//        return $output . (($short) ? null : '</' . strtolower($tag) . '>' . (($inline) ? NULL : PHP_EOL));
//    }
//
//    private function _packageTagParams($params) {
//        if (!is_array($params))
//            throw new \OroborosLibraryException('HTML tag params must be an associative array', self::_EXCEPTION_CODE_GENERAL_BAD_PARAMETERS);
//        $output = null;
//        foreach ($params as $key => $value) {
//            $output .= $key . '="' . $value . '" ';
//        }
//        return rtrim($output, ' ');
//    }
}
