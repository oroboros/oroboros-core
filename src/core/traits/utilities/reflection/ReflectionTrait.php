<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\reflection;

/**
 * <Reflection Trait>
 * Provides reflection support for classes, methods, functions, etc.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\codex\LexiconEntryContract
 */
trait ReflectionTrait
{

    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;
    use \oroboros\core\traits\core\BaselineTrait;

    /**
     * Represents the string name of the represented property;
     * @var string
     */
    private $_reflection_property;

    /**
     * In the event that the reflector is a property, constant, or method
     * of a class, interface, or trait, this designates the containing construct
     * that the property belongs to.
     * @var string
     */
    private $_reflection_property_of;

    /**
     * Represents the string key name of the
     * appropriate reflector for the property.
     * @var type
     */
    private $_reflection_type;

    /**
     * Represents the reflector for the represented property.
     * @var \Reflector
     */
    private $_reflection_reflector;

    /**
     * Represents reflection types enabled in PHP 7.0 and above.
     * These will not be checked if the runtime version is not sufficient.
     * @todo Add support for 7.0 reflection constructs
     * @var array
     */
    private static $_reflection_types_php7 = array(
        'class-constant' => 'ReflectionClassConstant',
        'generator' => 'ReflectionGenerator',
        'type' => 'ReflectionType'
    );

    /**
     * Represents the valid handled reflection types.
     * @var array
     */
    private static $_reflection_types = array(
        'class' => 'ReflectionClass',
        'trait' => 'ReflectionClass',
        'interface' => 'ReflectionClass',
        'function' => 'ReflectionFunction',
        'extension' => 'ReflectionExtension',
        'zend-extension' => 'ReflectionZendExtension',
        'parameter' => 'ReflectionParameter',
        'property' => 'ReflectionProperty',
        'method' => 'ReflectionMethod',
        'reflector' => 'Reflector' //This is only an interface, but someone may have made a custom reflector by extending it
    );
    private $_reflection_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\LexiconEntryContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Oroboros Reflection Constructor>
     * Provides the default reflection constructor.
     *
     * @param string $property The property to evaluate.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided subject is not reflectable
     */
    public function __construct( $subject, $property_of = null,
        $namespace = null, $type = null )
    {
        $this->_initializeReflection( $subject, $property_of, $namespace, $type );
    }

    /**
     * <Reflection Call Magic Method>
     * Provides a passthrough to the reflector's internal methods.
     * @param string $name
     * @param array $args
     * @return mixed
     * @throws \oroboros\core\utilities\exception\LogicException if the object is not initialized. This means the constructor was overridden and initialization never got called. Someone overrode something they should not have, or did it improperly.
     * @throws \oroboros\core\utilities\exception\BadMethodCallException if an invalid reflection method is called
     */
    public function __call( $name, $args = array() )
    {
        if ( !$this->_reflection_initialized )
        {
            throw new \oroboros\core\utilities\exception\LogicException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_CORE_UTILITY_FAILURE_MESSAGE,
                get_class( $this ), 'object is not initialized' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_UTILITY_FAILURE );
        }
        if ( !method_exists( $this->_reflection_reflector, $name ) )
        {
            throw new \oroboros\core\utilities\exception\BadMethodCallException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_METHOD_CALL_MESSAGE,
                $name, get_class( $this ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_METHOD_CALL
            );
        }
        return self::_callableUtilityCall( array(
                $this->_reflection_reflector,
                $name ), $args );
    }

    /**
     * <Reflection Export Method>
     * Export the actual reflector, not it's subject.
     * This is only used by C internals of PHP.
     *
     * The reflector for this class will use ReflectionClass,
     * which is the most versatile and common. As this is used by C,
     * the arguments have to be considered to be immutable.
     *
     * @return string
     */
    public static function export( $argument, $return = false )
    {
        return \ReflectionClass::export( $argument, $return );
    }

    /**
     * <Reflection String Magic Method>
     * Provides a passthrough to the reflectors internal __toString method.
     * @return string
     */
    public function __toString()
    {
        return (string) $this->_reflection_reflector;
    }

    /**
     * <Reflection Instance Getter Method>
     * Returns the reflection instance to work with directly.
     * @return \Reflector
     */
    public function getReflector()
    {
        return $this->_reflection_reflector;
    }

    /**
     * <Reflection Type Getter>
     * Returns a canonicalized string indicating the type of reflection.
     * @return string
     */
    public function getType()
    {
        return $this->_reflectionGetType();
    }

    /**
     * <Reflection Valid Types Getter>
     * Returns an array of the valid canonicalized strings returned by getType
     * @return array
     */
    public function getValidTypes()
    {
        return $this->_reflectionGetTypes();
    }

    /**
     * <Reflection Type Check Method>
     * Checks if the supplied type corresponds to the current reflector type.
     * This method will not check inheritance, and will return false
     * on subclasses of reflectors. For this reason you
     * should work only with native PHP reflectors.
     * @param string $type
     * @return bool
     */
    public function isType( $type )
    {
        return $this->_reflectionIsType( $type );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Oroboros Reflection Initialization>
     * Initializes the reflection instance.
     *
     * @param string $property The property to evaluate.
     * @param string $type (optional) The type of property. If this is not supplied, it will be determined programmatically if at all possible.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided subject is not reflectable, or an invalid type is supplied.
     */
    protected function _initializeReflection( $subject, $property_of = null,
        $namespace = null, $type = null )
    {
        if ( is_object( $subject ) && ( $subject instanceof \Reflector ) )
        {
            $this->_reflection_reflector = $subject;
            $this->_reflection_type = $this->_reflectionGetTypeByReflector( $subject );
            $this->_reflection_initialized = true;
            return;
        }
        if ( is_object( $subject ) )
        {
            $this->_reflection_type = 'class';
            $this->_reflection_reflector = $this->_generateReflection( $subject, $this->_reflection_type );
            $this->_reflection_initialized = true;
            return;
        }
        if ( is_null( $type ) )
        {
            $type = $this->_reflectionGetTypeBySubject( $subject );
        }
        //Only use the namespace if there is no existing namespace
        if ( strpos( ltrim( $subject, '\\' ), '\\' ) === false
            && !is_null( $namespace ) )
        {
            $subject = trim( $namespace ) . '\\' . ltrim( $subject, '\\' );
        }
        if ( !in_array( $type, $this->_reflectionGetSupportedReflectors() ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                'string: ' . implode( '|',
                    $this->_reflectionGetSupportedReflectors() ),
                is_string( $type )
                    ? 'string: ' . $type
                    : gettype( $type )  ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_reflection_type = $type;
        $this->_reflection_reflector = $this->_generateReflection( $subject, $this->_reflection_type );
        $this->_reflection_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Creates a reflector for the provided subject.
     * @param string|object $subject
     * @param string $type designates the type of reflector to return
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided subject is not reflectable
     */
    private function _generateReflection( $subject, $type, $property_of = null )
    {
        $requires_property_of = array(
            'parameter',
            'property',
            'method'
        );
        if ( PHP_VERSION_ID > 71000 )
        {
            $requires_property_of[] = 'class-constant';
        }
        if ( in_array( $type, $requires_property_of ) && is_null( $property_of ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_MISSING_PARAMETERS_MESSAGE
                . ' Must be provided for the following types: [%s]', __METHOD__,
                '$property_of', implode( '|', $requires_property_of ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_MISSING_PARAMETERS
            );
        }
        $reflection_class = '\\' . self::$_reflection_types[$type];
        switch ( $type )
        {
            //These are fine from PHP 5.4+
            case 'class':
            case 'trait':
            case 'interface':
            case 'function':
            case 'extension':
            case 'zend-extension':
                return new $reflection_class( $subject );
                break;
            case 'parameter':
            case 'property':
            case 'method':
                return new $reflection_class( $subject, $property_of );
                break;
            //requires PHP 7.1+
            case 'class-constant':
                if ( PHP_VERSION_ID < 71000 )
                {
                    throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                    sprintf( 'Error encountered at [%s]. This feature is not supported by your PHP version. You must have version [%s] or higher, and you have version [%s]',
                        __METHOD__, '7.0', PHP_VERSION ),
                    \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
                    );
                }
                return new $reflection_class( $subject, $property_of );
            //requires PHP 7.0+
            case 'generator':
            case 'type':
                if ( PHP_VERSION_ID < 70000 )
                {
                    throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                    sprintf( 'Error encountered at [%s]. This feature is not supported by your PHP version. You must have version [%s] or higher, and you have version [%s]',
                        __METHOD__, '7.0', PHP_VERSION ),
                    \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
                    );
                }
                return new $reflection_class( $subject );
                break;
            default:
                throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                    __METHOD__,
                    'string: ' . implode( '|',
                        $this->_reflectionGetSupportedReflectors() ),
                    is_string( $type )
                        ? 'string: ' . $type
                        : gettype( $type )  ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
                );
                break;
        }
        if ( is_object( $subject ) )
        {
            $this->_reflection_reflector = new \ReflectionClass( $subject );
            $this->_reflection_property = get_class( $subject );
            return;
        } elseif ( is_string( $subject ) )
        {
            foreach ( self::$_reflection_types as
                $key =>
                $reflector )
            {
                if ( in_array( $key,
                        array(
                        'parameter' ) ) )
                {
                    continue;
                }
                try
                {
//                    d( $class );
                    $class = '\\' . $reflector;
                    $ref = new $class( $subject );
                    $this->_reflection_reflector = $ref;
                    $this->_reflection_property = $subject;
                    return;
                } catch ( \Exception $e )
                {
                    //no-op
                }
            }
        }
        throw new \oroboros\core\utilities\exception\InvalidArgumentException(
        sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
            __METHOD__, 'reflector or reflectable string', gettype( $subject ) ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
        );
    }

    /**
     * Returns the type of the current reflector.
     * If no reflector is set, returns null.
     * @return string|null
     */
    private function _reflectionGetType()
    {
        if ( is_null( $this->_reflection_reflector ) )
        {
            return null;
        }
        $class = get_class( $this->_reflection_reflector );
        foreach ( self::$_reflection_types as
            $type =>
            $type_class )
        {
            if ( $class !== $type_class )
            {
                continue;
            }
            return $type;
        }
        return null;
    }

    /**
     * Returns a set of the canonicalized type keys
     * @return array
     */
    private function _reflectionGetTypes()
    {
        return $this->_reflectionGetSupportedReflectors();
    }

    /**
     * Checks if the supplied type corresponds to the current reflector type.
     * This method will not check inheritance, and will return false
     * on subclasses of reflectors. For this reason you
     * should work only with native PHP reflectors.
     * @param string $type
     * @return bool
     */
    private function _reflectionIsType( $type )
    {
        if ( !is_string( $type ) || !array_key_exists( strtolower( $type ),
                self::$_reflection_types ) )
        {
            return false;
        }
        return get_class( $this->_reflection_reflector ) === self::$_reflection_types[strtolower( $type )];
    }

    private function _reflectionGetTypeBySubject( $subject, $property_of = null )
    {
        $subject = ltrim( $subject, '\\' );
        if ( PHP_VERSION_ID > 70000 && is_object( $subject ) && ( $subject instanceof \Generator ) )
        {
            return 'generator';
        }
        if ( in_array( $subject, get_declared_traits() ) )
        {
            return 'trait';
        }
        if ( in_array( $subject, get_declared_interfaces() ) )
        {
            return 'interface';
        }
        if ( in_array( $subject, get_declared_classes() ) )
        {
            return 'class';
        }
        if ( in_array( $subject,
                array_merge( get_defined_functions()['internal'],
                    get_defined_functions()['user'] ) ) )
        {
            return 'function';
        }
        if ( in_array( $subject, get_loaded_extensions( 1 ) ) )
        {
            return 'zend-extension';
        }
        if ( in_array( $subject, get_loaded_extensions() ) )
        {
            return 'extension';
        }
    }

    private function _reflectionGetTypeByReflector( $reflector )
    {
        $reflector_name = get_class( $reflector );
        if ( in_array( $reflector_name, self::$_reflection_types_php7 ) )
        {
            return array_search( $reflector_name, self::$_reflection_types_php7 );
        }
        if ( in_array( $reflector_name, self::$_reflection_types ) )
        {
            return array_search( $reflector_name, self::$_reflection_types );
        }
        //It's an override then, so we'll have to
        //search from the specific to the general to find it.
        foreach ( self::$_reflection_types_php7 as
            $type =>
            $class )
        {
            if ( $reflector_name instanceof $class )
            {
                return $type;
            }
        }
        foreach ( self::$_reflection_types as
            $type =>
            $class )
        {
            if ( $reflector_name instanceof $class )
            {
                return $type;
            }
        }
        //no idea, so we'll return the interface name
        return 'reflector';
    }

    private function _reflectionGetSupportedReflectors()
    {
        $base = array_keys( self::$_reflection_types );
        if ( PHP_VERSION_ID >= 70000 )
        {
            $extra = self::$_reflection_types_php7;
            if ( PHP_VERSION_ID < 71000 )
            {
                unset( $extra['class-constant'] );
            }
            $base = array_merge( $base, array_keys( $extra ) );
        }
        return $base;
    }

}
