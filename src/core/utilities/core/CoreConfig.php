<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\utilities\core;

/**
 * <Oroboros Core Configuration>
 * Loads the core configuration files, insuring they are only parsed one time,
 * and provides read-only access to default settings.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class CoreConfig
{

    const OROBOROS_CLASS_TYPE = false;
    const OROBOROS_CLASS_SCOPE = false;
    const OROBOROS_API = false;
    const OROBOROS_CORE = true;
    const OROBOROS_PATTERN = false;
    const OROBOROS_UTILITY = true;
    const OROBOROS_LIBRARY = false;
    const OROBOROS_ROUTINE = false;
    const OROBOROS_EXTENSION = false;
    const OROBOROS_MODULE = false;
    const OROBOROS_COMPONENT = false;
    const OROBOROS_ADAPTER = false;
    const OROBOROS_CONTROLLER = false;
    const OROBOROS_MODEL = false;
    const OROBOROS_VIEW = false;

    private static $_core_config_defaults = array(
        'oroboros' => 'oroboros.json',
        'core' => 'conf/core.json',
        'settings' => 'conf/settings_default.json',
        'build' => 'conf/build_default.json',
        'database' => 'conf/database_default.json'
    );
    private static $_core_config_overrides = array(
        'oroboros' => false,
        'core' => false,
        'settings' => 'conf/settings.json',
        'build' => 'conf/build.json',
        'database' => 'conf/database.json'
    );
    private static $_core_config_constants = array(
        'oroboros' => false,
        'core' => false,
        'settings' => 'OROBOROS_SETTINGS',
        'build' => 'OROBOROS_BUILD',
        'database' => 'OROBOROS_DATABASE'
    );
    private static $_core_config_env = array(
        'oroboros' => false,
        'core' => false,
        'settings' => 'OROBOROS_SETTINGS',
        'build' => 'OROBOROS_BUILD',
        'database' => 'OROBOROS_DATABASE'
    );
    private static $_core_config = array();
    private static $_core_env;
    private static $_configs_built = false;

    public function __construct()
    {
        if ( !self::$_configs_built )
        {
            $this->_buildConfigs();
        }
    }

    public static function get( $key = null, $subkey = null )
    {
        if ( $key === null )
        {
            return self::$_core_config;
        }
        if ( !(is_string( $key ) && array_key_exists( $key, self::$_core_config )) )
        {
            return false;
        }
        if ( !is_null( $subkey ) )
        {
            if ( !(is_string( $subkey ) && array_key_exists( $subkey,
                    self::$_core_config[$key] ) ) )
            {
                return false;
            }
            return self::$_core_config[$key][$subkey];
        }
        return self::$_core_config[$key];
    }

    public static function flush()
    {
        self::$_configs_built = false;
        self::$_core_config = array();
        self::$_core_env = null;
    }

    private function _buildConfigs()
    {
        $keys = array_keys( self::$_core_config_defaults );
        foreach ( $keys as
            $key )
        {
            $config = array_replace_recursive( $this->_checkDefault( $key ),
                $this->_checkOverride( $key ), $this->_checkEnv( $key ),
                $this->_checkConstant( $key ) );
            self::$_core_config[$key] = $config;
        }
        self::$_core_env = ( defined( 'OROBOROS_MODE' )
            ? OROBOROS_MODE
            : ( getenv( 'OROBOROS_MODE' )
            ? getenv( 'OROBOROS_MODE' )
            : self::$_core_config['settings']['core']['mode'] ) );
        foreach ( self::$_core_config['settings']['core'] as
            $key =>
            $set )
        {
            if ( is_array( $set ) && array_key_exists( self::$_core_env, $set ) )
            {
                self::$_core_config['settings']['core'][$key] = self::$_core_config['settings']['core'][$key][self::$_core_env];
            }
        }
        $this->_compileConfig();
        self::$_configs_built = true;
    }

    private function _compileConfig()
    {
        $this->_compileServerLanguageData();
        foreach ( self::$_core_config['build']['extensions']['core'] as
            $vendor =>
            $extensions )
        {
            foreach ( $extensions as
                $id =>
                $class )
            {
                self::$_core_config['extensions']['core'][$class] = (array_key_exists( $id,
                        self::$_core_config['settings']['core'] )
                    ? self::$_core_config['settings']['core'][$id]
                    : null );
            }
        }
        self::$_core_config['composer'] = $this->_loadConfig( OROBOROS_ROOT_DIRECTORY . self::$_core_config['build']['config']['composer'] );
        self::$_core_config['modules'] = self::$_core_config['build']['modules'];
        self::$_core_config['components'] = self::$_core_config['build']['components'];
    }

    private function _compileServerLanguageData()
    {
        $lang_dir = OROBOROS_ROOT_DIRECTORY . 'conf/lang/';
        $lang = strtolower( self::$_core_config['settings']['core']['lang'] );
        $localized_lang_dir = $lang_dir . $lang . DIRECTORY_SEPARATOR;
        if ( !is_readable( $localized_lang_dir ) )
        {
            //load the default en-us lang files
            //if the requested language is not found
            $localized_lang_dir = $lang_dir . 'en-us' . DIRECTORY_SEPARATOR;
        }
        //load the language files from the lang dir
        $lang_files = glob( $localized_lang_dir . '*.ini' );
        foreach ( $lang_files as
            $lang )
        {
            $key = substr( $lang, strlen( $localized_lang_dir ) );
            $key = substr( $key, 0, strrpos( $key, '.' ) );
            $data = parse_ini_file( $lang, true );
            self::$_core_config['lang'][$key] = $data;
        }
    }

    private function _checkDefault( $key )
    {
        if ( !self::$_core_config_defaults[$key] )
        {
            return array();
        }
        $path = OROBOROS_ROOT_DIRECTORY . self::$_core_config_defaults[$key];
        return $this->_loadConfig( $path );
    }

    private function _checkOverride( $key )
    {
        if ( !self::$_core_config_overrides[$key] )
        {
            return array();
        }
        $path = OROBOROS_ROOT_DIRECTORY . self::$_core_config_overrides[$key];
        return $this->_loadConfig( $path );
    }

    private function _checkConstant( $key )
    {
        if ( !self::$_core_config_constants[$key] || !defined( self::$_core_config_constants[$key] ) )
        {
            return array();
        }
        $path = constant( self::$_core_config_constants[$key] );
        return $this->_loadConfig( $path );
    }

    private function _checkEnv( $key )
    {
        if ( !self::$_core_config_env[$key] || !getenv( self::$_core_config_env[$key] ) )
        {
            return array();
        }
        $path = getenv( self::$_core_config_env[$key] );
        return $this->_loadConfig( $path );
    }

    private function _loadConfig( $path )
    {
        if ( !is_readable( $path ) )
        {
            return array();
        }
        $contents = json_decode( file_get_contents( $path ), 1 );
        if ( !$contents )
        {
            return array();
        }
        return $contents;
    }

}
