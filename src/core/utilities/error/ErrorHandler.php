<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\utilities\error;

/**
 * <General Error Handler>
 * Provides a simplified, default generic error handler.
 *
 * This error handler by default makes no assumption about error context.
 * It will register itself and allow all errors to be handled by whatever
 * prior error handler was registered, or the PHP internal error handler
 * if no prior error handler is set. This error handler only collects
 * error statistics, calls the logger when errors occur, and enables
 * the possibility of defining a fatal error handling method that
 * allows last second cleanup and reporting if the script dies.
 *
 * By default, this error handler will use whatever the prior existing
 * error level setting is as its default, whatever the prior existing
 * visibility setting is as its error visibility, and will not interrupt
 * expected PHP execution in any way from how it exists at the time
 * it is registered. Its only purpose is to standardize reporting on
 * errors as they occur.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.2.5
 */
final class ErrorHandler
    extends \oroboros\core\abstracts\utilities\error\AbstractErrorHandler
{

    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\UtilityClassScopes::CLASS_SCOPE_UTILITY_ERROR_HANDLER;

}
